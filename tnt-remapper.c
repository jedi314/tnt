/** 
 \file tnt-remapper.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief The remapper 

 +---------+            +-------------+         +-------------+ 
 |         |/|          |             |         |             |
-+ CAMERA  | |----+-----+ RESIZER 1/2 +----+----+   REMAPPER  +-----+       
 |         |\|    |     |             |    |    |             |     |
 +---------+      |     +-------------+    |    +-------------+     |
                  |                        |                        |
                  |                        |                        |
            +-----V------+           +-----V------+          +------V-------+
            |            |           |            |          |              |
            | FULL FRAME |           |   RESIZED  |          |   REMAPPED&  |
            | 1920x1080  |           |   480x270  |          |    PADDED    |
            |            |           |            |          |   480x288    |
            |            |           |            |          |              |
            +------------+           +------------+          +--------------+



catches  resized1/2 frames and 

*/

#include <linux/proc_fs.h>
#include "tnt-remapper.h"
#include "tnt-processor.h"
#include "tnt-allocator.h"
#include "tnt-cmd.h"
#include "tntvdo.h"

#define LUT_NAME "lut"

extern bool enable_print;

static inline void select_remapper(struct tnt_device *dev, int selector)
{
	tnt_write(dev, START_REMAPPER_SELECTOR, selector);
}

inline static int remapper_debug_info(struct tnt_device *dev)
{
	int info;
	info = tnt_read(dev, DBG_REMAPPER);
	return info;
}

static inline void remapper_dest_addr(struct tnt_device *dev, dma_addr_t addr)
{
	tnt_write(dev, PTR_DMA11, addr);
}

static inline void remapper_resizer_addr(struct tnt_device *dev,
					 dma_addr_t addr)
{
	tnt_write(dev, PTR_DMA12, addr);
}

static inline void remapper_source_addr(struct tnt_device *dev, dma_addr_t addr)
{
	tnt_write(dev, BADDR_SRC_IMG_REMAPPER, addr);
}

static inline void remapper_lut(struct tnt_device *dev, dma_addr_t addr)
{
	tnt_write(dev, BADDR_LUT_REMAPPER, addr);
}

static inline void new_delta_coef(struct tnt_device *dev, u32 ymin, u32 ymax)
{
	tnt_write(dev, TRG_NEW_DELTA_COEFF_REMAPPER, 1);
	tnt_write(dev, FIRST_LINE_MIN_IMG_REMAPPER, ymin);
	tnt_write(dev, FIRST_LINE_MAX_IMG_REMAPPER, ymax);
}

static inline void set_delta_coef(struct tnt_device *dev, u32 dy)
{
	tnt_write(dev, SET_DELTA_COEFF_REMAPPER, dy);
}

static inline void remapper_starting_row(struct tnt_device *dev, u32 n)
{
	tnt_write(dev, NUM_ROWS_TO_START_REMAPPER, n);
}

static inline void remapper_enable_dma(struct tnt_device *dev)
{
	tnt_write(dev, EN_DMA11_REMAPPER_OUTPUT, 1);
}

static inline void remapper_disable_dma(struct tnt_device *dev)
{
	tnt_write(dev, EN_DMA11_REMAPPER_OUTPUT, 0);
}

static __maybe_unused void remapper_enable_resizer(struct tnt_device *dev)
{
	tnt_write(dev, EN_DMA12, 1);
}

static __maybe_unused void remapper_disable_resizer(struct tnt_device *dev)
{
	tnt_write(dev, EN_DMA12, 0);
}

/* ------ */
/* L U T  */

static inline int lut_init(struct lut *lut, const struct file_operations *lops,
			   char *name)
{
	lut->name = name;
	proc_create_data(lut->name, 0, NULL, lops, lut);
	return 0;
}

static void lut_remove(struct lut *lut)
{
	remove_proc_entry(lut->name, NULL);
}

/* After unmap. */
/* static void lut_close(struct vm_area_struct *vma) */
/* { */
/*     pr_info("lut_close\n"); */
/* } */

static int lut_mmap(struct file *filp, struct vm_area_struct *vma)
{
	struct lut *lut = (struct lut *)filp->private_data;
	struct tnt_remap *remapper = container_of(lut, struct tnt_remap, lut);

	size_t size = vma->vm_end - vma->vm_start;
	phys_addr_t offset = (phys_addr_t)vma->vm_pgoff << PAGE_SHIFT;

	char *vaddr = (char *)remapper->lut.data->vaddr;

	/* Does it even fit in phys_addr_t? */
	if (offset != lut->data->addr)
		return -EINVAL;

	/* We cannot mmap too big areas */
	if (size > lut->size)
		return -EINVAL;

	/* Remap-pfn-range will mark the range VM_IO */
	if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size,
			    vma->vm_page_prot))
		return -EAGAIN;

	vaddr[0] = '*';
	vaddr[1] = '#';

	return 0;
}

static int lut_open(struct inode *inode, struct file *filp)
{
	struct lut *lut = PDE_DATA(file_inode(filp));

	filp->private_data = lut;

	return 0;
}

long lut_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct lut *lut = filp->private_data;
	struct tnt_device *dev = lut->dev;
	int ret = 0;
	char *name = DEV_NAME(filp);

	switch (cmd) {
	case REMAPPER_NEW_DELTA_COEF: {
		struct delta d;
		enable_print = true;
		ret = copy_from_user(&d, (struct delta *)arg, sizeof(d));
		new_delta_coef(dev, d.ymin, d.ymax);
		enable_print = false;
	} break;
	case REMAPPER_SET_DELTA_COEF: {
		u64 dy = (u64)arg;

		enable_print = true;
		set_delta_coef(dev, dy);
		enable_print = false;
	} break;
	default:
		pr_info("%s: Wrong ioctl cmd [ 0x%08x ]\n", name, cmd);
		//pr_info("Wrong ioctl cmd [ 0x%08x ]\n", cmd );
		ret = -ENOTTY;
	}

	return ret;
}

static int lut_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static const struct file_operations lut_fops = {
	.mmap = lut_mmap,
	.open = lut_open,
	.release = lut_release,
	.unlocked_ioctl = lut_ioctl,

	//.read = read,
	//.write = write,
};

static void *create_lut_table(struct tnt_processor *pr)
{
	struct tnt_remap *remapper = (struct tnt_remap *)pr->info;
	struct tnt_allocator *alc = get_allocator(pr);
	struct lut *lut = &remapper->lut;
	struct alc_chunk *chunk;
	int size;
	int n = 1;

	size = (2 * lut->bpp * lut->width * lut->height) >> 3;
	lut->id = alloc_ctx(alc, "LUT", n, &size, NULL);
	chunk = alc_alloc_chunk(alc, lut->id);

	if (!chunk)
		return ERR_PTR(-ENOMEM);

	lut->size = size;
	lut->data = chunk;

	return chunk;
}

static inline void set_lut_info(struct tnt_remap *remapper)
{
	struct lut *lut = &remapper->lut;

	lut->width = remapper->in.w;
	lut->height = remapper->in.h;
}

int remapper_destroy(struct tnt_processor *pr)
{
	struct tnt_remap *remapper = (struct tnt_remap *)pr->info;
	lut_remove(&remapper->lut);

	return 0;
}

static void enable_remapper(struct tnt_processor *pr)
{
	struct tnt_device *dev = get_dev(pr);
	enable_print = true;

	select_remapper(dev, 0);
	remapper_starting_row(dev, 530);
	remapper_enable_dma(dev);
	remapper_disable_resizer(dev);
	enable_print = false;
}

static int remapper_init(struct tnt_processor *pr)
{
	struct tnt_remap *remapper = (struct tnt_remap *)pr->info;
	struct tnt_device *dev = get_dev(pr);

	set_lut_info(remapper);
	create_lut_table(pr);

	lut_init(&remapper->lut, &lut_fops, LUT_NAME);
	remapper->lut.dev = dev;
	enable_remapper(pr);

	return 0;
}

/**
\brief ioctl execute after a call to a ioctl 
of /dev/video or directly calling /dev/v4l2-subdev

\param sd: pointer to vsl2_subdev function 
\param cmd: command code as genenrated vy _IO
\param arg: argument passed ad defined durinf subdev initialization

\return 0 in case cmd is found or  -ENOTTY
*/
long remapper_ioctl(struct v4l2_subdev *sd, unsigned int cmd, void *arg)
{
	struct tnt_processor *pr =
		(struct tnt_processor *)v4l2_get_subdevdata(sd);
	struct tnt_remap *remapper = (struct tnt_remap *)pr->info;
	int ret = 0;

	switch (cmd) {
	case REMAPPER_INIT:
		remapper_init(pr);
		break;
	case REMAPPER_ENABLE:
		enable_remapper(pr);
		break;
	case REMAPPER_REQUEST_LUT: {
		struct lut_info *ls = (struct lut_info *)arg;
		struct lut *lut = &remapper->lut;

		if (lut == NULL)
			return -ENOSYS;
		ls->phy = lut->data->addr;
		ls->w = lut->width;
		ls->h = lut->height;
		ls->size = lut->size;
		ls->max_displ = remapper->max_displace;
		ls->delta_max = remapper->delta_max;
	} break;
	case REMAPPER_GET_FMT: {
		struct tnt_resize *out = (struct tnt_resize *)arg;
		memcpy(out, &remapper->out, sizeof(*out));
	} break;
	default:
		pr_info("%s: Wrong ioctl cmd [ 0x%08x ]\n", sd->name, cmd);
		ret = -ENOTTY;
	}
	return ret;
}

__maybe_unused static int remapper_open(struct v4l2_subdev *sd,
					struct v4l2_subdev_fh *fh)
{
	//struct tnt_processor *pr = (struct tnt_processor*)v4l2_get_subdevdata(sd);
	return 0;
}

__maybe_unused static int remapper_close(struct v4l2_subdev *subdev,
					 struct v4l2_subdev_fh *fh)
{
	pr_info("CLOSE subdevice");
	return 0;
}

inline struct tnt_processor *find_processor(struct tntvdo *vdo, char *name);
int remapper_set_addr(struct tnt_processor *pr, void *b)
{
	struct tntvdo *vdo = pr->vdo;
	struct tnt_remap *remapper = (struct tnt_remap *)pr;
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct tnt_processor *resizer;
	struct alc_chunk *chunk_resized;
	struct alc_chunk *chunk_remapped =
		buf->vb.planes[pr->plane_id].mem_priv;
	struct lut *lut = &remapper->lut;
	struct tnt_device *dev = get_dev(pr);

	resizer = find_processor(vdo, "resizer");
	chunk_resized = buf->vb.planes[resizer->plane_id].mem_priv;

	/* REMAPPED */
	remapper_source_addr(dev, chunk_resized->addr);
	remapper_lut(dev, lut->data->addr);
	remapper_dest_addr(dev, chunk_remapped->addr);

	return 0;
}

static __maybe_unused void remapper_fill_plane(struct alc_ctx *ctx,
					       struct vb2_plane *vp,
					       struct v4l2_plane *p)
{
	struct alc_chunk *chunk;

	p->length = ctx->chunk_size;
	p->bytesused = ctx->chunk_size;
	chunk = vp->mem_priv;
	p->m.mem_offset = chunk->addr;
}

static u32 remapper_plane_size(struct tnt_processor *processor)
{
	struct tnt_remap *remapper = (struct tnt_remap *)processor->info;

	return remapper->out.w * remapper->out.h;
}

static int remapper_info(struct tnt_processor *processor,
			 struct processor_layout *l)
{
	struct tnt_remap *remapper = (struct tnt_remap *)processor->info;

	l->w = remapper->out.w;
	l->h = remapper->out.h;
	l->size = remapper_plane_size(processor);

	return 0;
}

struct tnt_processor_ops remapper_ops = { .init = remapper_init,
					  .plane_size = remapper_plane_size,
					  .set_addr = remapper_set_addr,
					  .destroy = remapper_destroy,
					  .info = remapper_info };

static const struct v4l2_subdev_core_ops remapper_core_ops = {
	.ioctl = remapper_ioctl,
};

const struct v4l2_subdev_ops remapper_sdev_ops = {
	.core = &remapper_core_ops,
};

/* ---------------------- */
