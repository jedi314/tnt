/** 
 \file tnt-stats.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2021  s.r.l.
 

 \brief
*/

#include <linux/string.h>
#include <linux/math64.h>
#include <linux/uaccess.h>

#include "tntvdo.h"
#include "tnt-stats.h"

inline u32 tnt_fpga_fsm(struct tnt_device *dev);

#define MS(a) (jiffies + msecs_to_jiffies(a))

enum fsm_state {
	FSM_IDLE = 0,
	FSM_START_CHA,
	FSM_START_CHB,
	FSM_WAIT_CENSUS,
	FSM_START_TRIGGER,
	FSM_WAIT_FVAL,
};

char *fpga_state[] = {
	[FSM_IDLE] = "Idle",
	[FSM_START_CHA] = "Channel A start",
	[FSM_START_CHB] = "Channel B start",
	[FSM_WAIT_CENSUS] = "Wait Census",
	[FSM_START_TRIGGER] = "Start TRigger",
	[FSM_WAIT_FVAL] = "Wait FVAL",
};

void avg_init(struct tnt_avg *avg)
{
	memset(avg, 0, sizeof(*avg));
	avg->period = WINDOW_SIZE;
}

inline u64 avg_older(u64 p)
{
	u32 rem;
	div_u64_rem(p + 1, WINDOW_SIZE, &rem);

	return rem;
}

inline u64 *avg_begin_range(struct tnt_avg *avg, u32 period)
{
	u64 org = avg->curr;
	if (org > period)
		org = org - period;
	else
		org = WINDOW_SIZE - period + org;

	return &avg->window[org];
}

inline u64 *avg_next_pointer(struct tnt_avg *avg, u64 *p)
{
	u64 *next = p + 1;

	if (next > &avg->window[WINDOW_SIZE - 1])
		next = &avg->window[0];

	return next;
}

inline void avg_add(struct tnt_avg *avg, u64 value)
{
	u64 older;

	older = avg->window[avg->curr];
	avg->window[avg->curr++] = value;
	avg->sum += value;
	avg->sum -= older;
}

u64 avg_sum(struct tnt_avg *avg, u32 period)
{
	u64 i, sum = 0;
	u64 *p;

	p = avg_begin_range(avg, period);
	for (i = 0; i < period; i++) {
		sum += *p;
		p = avg_next_pointer(avg, p);
	}

	return sum;
}

void tnt_stats_start(struct tnt_stats *stats)
{
	mod_timer(&stats->timer, MS(1000));
}

static inline void avg_fps(struct timer_list *list)
{
	struct tnt_stats *stats = container_of(list, struct tnt_stats, timer);
	struct tnt_avg *avg = &stats->avg;

	avg->fps = stats->n;
	avg_add(avg, avg->fps);
	stats->n = 0;
	tnt_stats_start(stats);
}

void tnt_stats_reset(struct tnt_stats *stats)
{
	avg_init(&stats->avg);
}

static int stats_open(struct inode *i_file, struct file *file)
{
	struct tnt_stats *stats = (struct tnt_stats *)i_file->i_private;

	file->private_data = stats;

	return 0;
}

static ssize_t avg_mean(struct file *file, char __user *buf, size_t count,
			loff_t *f_pos)
{
	struct tnt_stats *stats = (struct tnt_stats *)file->private_data;
	struct tnt_avg *avg = (struct tnt_avg *)&stats->avg;
	int ret, size;
	u64 sum, mean;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	sum = avg_sum(avg, avg->period);
	mean = div_u64(sum, avg->period);
	size = sprintf(n, "%lld\n", mean);

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static inline char *str_state(enum fsm_state state)
{
	return fpga_state[state & 0x07];
}

static ssize_t read_fsm(struct file *file, char __user *buf, size_t count,
			loff_t *f_pos)
{
	struct tnt_stats *stats = (struct tnt_stats *)file->private_data;
	struct tntdrv *drv = container_of(stats, struct tntdrv, stats);
	int ret, size;
	enum fsm_state state;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	state = tnt_fpga_fsm(&drv->device);
	pr_info("state %d\n", state & 7);
	size = sprintf(n, "%s\n", str_state(state));
	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static struct file_operations avg_ops = {
	.open = stats_open,
	.read = avg_mean,
};

static struct file_operations fsm_ops = {
	.open = stats_open,
	.read = read_fsm,
};

int tnt_stats_init(struct tnt_stats *stats, struct tdm_debug *parent)
{
	int ret = 0;
	struct tnt_avg *avg = &stats->avg;
	struct tdm_watch wstats[] = {
		{ "fps", 0400, TDM_UINT64, 0, { &avg->fps } },
		{ "period", 0600, TDM_UINT32, 0, { &avg->period } },
		{ "average", 0200, TDM_CUSTOM, &avg_ops },
		{ "fsm", 0400, TDM_CUSTOM, &fsm_ops },
		{}
	};

	stats->dbg.dirname = "stats";
	if (parent != NULL)
		stats->dbg.parent = parent->dir;

	ret = tdm_init(&stats->dbg);
	if (ret)
		return ret;

	tdm_append_watches(&stats->dbg, &wstats[0], stats);
	timer_setup(&stats->timer, avg_fps, 0);

	return ret;
}

void tnt_stats_release(struct tnt_stats *stats)
{
	del_timer(&stats->timer);
}

int ioctl_stats(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct tntdrv *drv = ((struct tntvdo *)video_drvdata(filp))->driver;
	struct tnt_device *dev = &drv->device;
	struct tnt_cmd_stats stats;
	int ret = 0;

	ret = copy_from_user(&stats, (struct tnt_cmd_stats *)arg,
			     sizeof(stats));
	switch (stats.cmd) {
	case STATS_AVERAGE:
		stats.value = avg_sum(&drv->stats.avg, stats.value);
		break;
	case STATS_FSM:
		stats.value = tnt_fpga_fsm(dev);
		strcpy(stats.desc, str_state(stats.value));
		break;
	case STATS_REINIT:
		stats.value = 0;
		break;
	default:
		return -EINVAL;
	}

	ret = copy_to_user((struct tnt_cmd_stats *)arg, &stats, sizeof(stats));
	return ret;
}
