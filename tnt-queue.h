/** 
 \file tnt-queue.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_QUEUE_H
#define _TNT_QUEUE_H

#include "tntvdo.h"

struct ctx_info {
	char *name;
	struct tntvdo *vdo;
};

int tnt_init_queue(struct tntdrv *drv, int n_min_bufs);
int get_context_info(struct tntdrv *drv, struct ctx_info *ctx);
void tnt_remove_active_frame(struct tntdrv *drv);
void tnt_remove_busy_frame(struct tntdrv *drv);
void tnt_deactivate_all_buffers(struct tntdrv *drv);

#endif
