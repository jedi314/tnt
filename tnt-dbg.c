/** 
 \file tnt-dbg.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#include <linux/io.h>
#include <linux/delay.h>
#include <linux/math64.h>
#include <linux/sort.h>

#include "tnt-dbg.h"
#include "debugfs/trace.h"
#include "tnt-videobuf2-core.h"
#include "list.h"
#include "tnt-access.h"
#include "tntvdo.h"
#include "tnt-imx265.h"
#include "tnt-fpga-utils.h"
#include "fpga-cs.h"

struct tnt_range {
	u64 t_pre;
	u64 t_post;
	int *index;
	int n;
	s64 d_uid;
	u64 last_uid;
};

inline void tnt_spin_lock(struct tntdrv *drv);
inline void tnt_spin_unlock(struct tntdrv *drv);

static inline bool match_range(struct tnt_buf *buf, void *range);

static inline int wait_for_slot(struct tntdrv *drv)
{
	int ret = 1;

	if (drv->stop == 0)
		ret = wait_slot(&drv->access);

	return ret;
}

u64 time_one_function(void (*func)(void *p), void *p)
{
	const int num_iterations = 256;
	ktime_t before, after, delta;
	int i;

	before = ktime_get_ns();
	for (i = 0; i < num_iterations; i++) {
		func(p);
	}
	after = ktime_get_ns();

	/* Time it took to do all iterations in nanoseconds */
	delta = after - before;

	/* Return roughly the time in nanoseconds for a single call */
	return delta >> 8;
}

/* 5080 ns to dqueue all buffers */
void loop_list(void *p)
{
}

void test_loop(struct tntdrv *drv)
{
	u64 ftime;

	ftime = time_one_function(loop_list, (void *)drv);
	pr_info("ELAPSED TIME: %lld\n", ftime);
}

/**
\brief Print PID process calling ioctl
\return 
*/
inline void dump_process(void)
{
	printk("The process id is %d\n", (int)task_pid_nr(current));
	printk("The process vid is %d\n", (int)task_pid_vnr(current));
	printk("The process name is %s\n", current->comm);
}

static void list_insert_sorted_el(struct tntdrv *drv, struct tnt_buf *b)
{
	struct tnt_buf *buf;

	list_for_each_entry (buf, &drv->active_list, active_entry) {
		if (b->a <= buf->a) {
			list_add(&b->active_entry, buf->active_entry.prev);
			return;
		}
	}
	list_add_tail(&b->active_entry, &buf->active_entry);
}

static __maybe_unused ssize_t tlist_write(struct file *flip,
					  const char __user *buf, size_t count,
					  loff_t *f_pos)
{
	unsigned long long int val;
	int ret;
	struct tnt_buf *tb;

	struct tntdrv *drv = ((struct seq_file *)flip->private_data)->private;

	ret = kstrtoull_from_user(buf, count, 0, &val);
	if (ret) {
		/* Negative error code. */
		pr_info("ko = %d\n", ret);
		return ret;
	}

	tb = kzalloc(sizeof(*tb), GFP_KERNEL);
	if (tb == NULL) {
		pr_err("tb kzalloc error\n");
		return 0;
	}
	tb->a = (int)val;
	list_insert_sorted_el(drv, tb);

	*f_pos += count;
	return count;
}

/* show() method */
static int show_active(struct seq_file *seq, void *v)
{
	struct tnt_buf *buf; /* list element */
	struct tntdrv *drv = (struct tntdrv *)seq->private;

	if (((u64)v >> 48) == 0xdead) {
		pr_info("Broken list\n");
		return -1;
	}

	wait_for_slot(drv);
	if (list_empty(&drv->active_list))
		return 0;

	buf = list_entry(v, struct tnt_buf, active_entry);
	//pr_info("%px %px\n", &buf->active_entry, buf->active_entry.next);
	seq_printf(seq, "Buf Index: %3d (%px) state=%d t= %lld uid= %lld\n",
		   buf->vb.index, buf, buf->vb.state, buf->vb.timestamp,
		   buf->uid);

	return 0;
}

static int show_dqueued(struct seq_file *seq, void *v)
{
	struct tnt_buf *buf; /* list element */
	struct tntdrv *drv = (struct tntdrv *)seq->private;

	if (((u64)v >> 48) == 0xdead) {
		pr_info("Broken list\n");
		return -1;
	}

	wait_for_slot(drv);
	buf = list_entry(v, struct tnt_buf, dqueued_entry);

	seq_printf(
		seq,
		"Buf Index: %3d (%px) ref_count= %d state=%d t= %lld uid= %lld\n",
		buf->vb.index, buf, buf->refcount, buf->vb.state,
		buf->vb.timestamp, buf->uid);
	return 0;
}

/* show() method */
static int show_queued(struct seq_file *seq, void *v)
{
	struct tnt_buf *buf; /* list element */
	struct tntdrv *drv = (struct tntdrv *)seq->private;

	if (((u64)v >> 48) == 0xdead) {
		pr_info("Broken list\n");
		return -1;
	}

	wait_for_slot(drv);
	buf = list_entry(v, struct tnt_buf, vb.queued_entry);
	seq_printf(seq, "Buf Index: %3d (%px) state=%d t= %lld\n",
		   buf->vb.index, buf, buf->vb.state, buf->vb.timestamp);
	return 0;
}

/* show() method */
static int show_done(struct seq_file *seq, void *v)
{
	struct tnt_buf *buf; /* list element */
	struct tntdrv *drv = (struct tntdrv *)seq->private;

	if (((u64)v >> 48) == 0xdead) {
		pr_info("Broken list\n");
		return -1;
	}

	wait_for_slot(drv);
	buf = list_entry(v, struct tnt_buf, vb.done_entry);
	seq_printf(seq, "Buf Index: %3d (%px) state=%d t= %lld\n",
		   buf->vb.index, buf, buf->vb.state, buf->vb.timestamp);
	return 0;
}

//DECLARE_LIST_FOPS(tnt_seqops, tntvdo, active_queue)
DECLARE_LIST_FOPS(active_seqops, tntdrv, active_list, show_active)

DECLARE_LIST_FOPS(dqueued_seqops, tntdrv, dqueued_list, show_dqueued)
DECLARE_LIST_FOPS(queued_seqops, vb2_queue, queued_list, show_queued)
DECLARE_LIST_FOPS(done_seqops, vb2_queue, done_list, show_done)

static struct file_operations lst_fops = {};

static int activate_open(struct inode *i_file, struct file *file)
{
	struct tntdrv *drv = (struct tntdrv *)i_file->i_private;

	file->private_data = drv;

	return 0;
}

int queue_current_buffer(struct tntdrv *drv, struct tnt_buf *buf);

void tnt_remove_active_frame(struct tntdrv *drv);
void tnt_remove_busy_frame(struct tntdrv *drv);

void __vb2_queue_cancel(struct vb2_queue *q);
int __vb2_queue_free(struct vb2_queue *q, unsigned int buffers);

static ssize_t activate_buffers(struct file *file, const char __user *buf,
				size_t count, loff_t *f_pos)
{
	int cmd;
	int ret;
	struct tntdrv *drv = file->private_data;
	struct vb2_queue *q = &drv->queue;

	ret = sscanf(buf, "%d", &cmd);
	if (ret != 1 || cmd == 0)
		return -EINVAL;

	switch (cmd) {
	case 1: /* activate all buffers */
		tnt_activate_buffers(&drv->queue);
		break;
	case 2: /* set current buffer */
		if (drv->active != NULL)
			queue_current_buffer(drv, drv->active);
		drv->active = tnt_activate_older_buffer(&drv->active_list);
		break;
	case 3: /* set done  */
		queue_current_buffer(drv, drv->active);
		break;
	case 4: /* free queue */
		tnt_remove_active_frame(drv);
		tnt_remove_busy_frame(drv);
		break;
	case 5:
		__vb2_queue_cancel(q);
		break;
	case 6:
		__vb2_queue_free(q, q->num_buffers);
		INIT_LIST_HEAD(&drv->dqueued_list);

		break;
	default:
		break;
	}
	*f_pos += count;

	return count;
}

static ssize_t current_buffer(struct file *file, char __user *buf, size_t count,
			      loff_t *f_pos)
{
	struct tntdrv *drv = file->private_data;
	struct tnt_buf *active = drv->active;
	int ret, size;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	if (active == NULL)
		return 0;

	size = sprintf(n, "Buf Index: %3d (%px) state=%d t= %lld uid= %lld\n",
		       active->vb.index, active, active->vb.state,
		       active->vb.timestamp, active->uid);

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static ssize_t busy_buffer(struct file *file, char __user *buf, size_t count,
			   loff_t *f_pos)
{
	struct tntdrv *drv = file->private_data;
	struct tnt_buf *busybuf = drv->busybuf;
	int ret, size;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	if (busybuf == NULL)
		return 0;

	size = sprintf(n, "Buf Index: %3d (%px) state=%d t= %lld uid= %lld\n",
		       busybuf->vb.index, busybuf, busybuf->vb.state,
		       busybuf->vb.timestamp, busybuf->uid);

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

#if 0
void* convert_el2buf(struct list_head* el, void* c)
{
    struct tnt_buf *buf;

    buf = container_of(el, struct tnt_buf, vb.done_entry);
    return buf;
    
}
#endif

/**
\brief Wrapper for debugfs operatin of dq_buffer.

\param file: pointer to file
\param _buf: buffer write from userspace
\param count: number of bytes written
\param f_pos: file position
\return n. of byte written
*/
static ssize_t dbg_dq_buffer(struct file *file, const char __user *_buf,
			     size_t count, loff_t *f_pos)
{
	int id;
	int ret;
	struct v4l2_buffer vb;
	struct v4l2_plane planes[DOUBLE];
	struct tntdrv *drv = file->private_data;

	ret = sscanf(_buf, "%d", &id);
	if (ret != 1)
		return -EINVAL;

	vb.index = id; /* for debug select by id */
	vb.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (FG_SOURCE(drv->grabmode) == DOUBLE)
		vb.m.planes = planes;
	ret = dq_buffer(drv, &vb);

	*f_pos += count;
	return count;
}

static inline bool match_range(struct tnt_buf *buf, void *r)
{
	struct tnt_range *range = (struct tnt_range *)r;

	if (buf->vb.timestamp >= range->t_pre &&
	    buf->vb.timestamp <= range->t_post) {
		//pr_info("(%d) %llu <= %llu <= %llu\n", buf->vb.index, range->t_pre, buf->vb.timestamp, range->t_post);
		range->index[range->n++] = buf->vb.index;
		return true;
	}
	return false;
}

static inline bool match_uid(struct tnt_buf *buf, void *r)
{
	struct tnt_range *range = (struct tnt_range *)r;

	//pr_info("(%d) %llu <= %llu <= %llu\n", buf->vb.index, range->t_pre, buf->uid, range->t_post);

	if (buf->uid > range->t_pre && buf->uid <= range->t_post) {
		//pr_info("OK\n");
		range->index[range->n++] = buf->vb.index;
		return true;
	}
	return false;
}

inline u64 last_timestamp(struct tntdrv *drv)
{
	return drv->last_timestamp;
}

int dqueue_buffer(struct tntdrv *drv, int index)
{
	int ret;
	struct v4l2_buffer vb = { .type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
				  .memory = V4L2_MEMORY_MMAP,
				  .index = index };

	ret = dq_buffer(drv, &vb);
	return ret;
}

inline struct tnt_buf *buf_from_index(struct tntdrv *drv, int index);
typedef bool (*fn_match)(struct tnt_buf *buf, void *range);

void foreach_frame_entry(struct tntdrv *drv, void *range, fn_match match,
			 bool dq)
{
	struct tnt_buf *buf;
	struct vb2_queue *q = &drv->queue;
	int res, i;

	if (list_empty(&drv->active_list)) {
		return;
	}

	for (i = 0; i < q->num_buffers; i++) {
		buf = buf_from_index(drv, i);
		if (buf->state == VB2_BUF_STATE_ACQ) {
			continue;
		}
		res = match(buf, range);
		if (res && dq) {
			dqueue_buffer(drv, buf->vb.index);
		}
	}
}

#if 0
/**
\brief Computes the time interval to match
all buffer.
Time values are:
+ t_pre time before timestamp requested
+ t_post time after timestamp but in th past
+ n_post ar number of frames to be dequeued in future.

\param drv: pointer to struct tntdrv
\param range: pointer to struct rang containig time values
\param frm: pointer to struct frames from userspae
\return number of future frames
*/
static inline
int search_time_interval(struct tntdrv* drv, struct tnt_range *range, struct tnt_frames* frm)
{
    u64 t_post;
    u64 t_pre;
    int n_future = 0;
    u64 half_t = drv->period >> 1;
    u64 t_last = last_timestamp(drv);
    u64 last_uid = drv->last_uid;

    if(frm->timestamp == 0){    /* i.e. get next */
        frm->timestamp = t_last + drv->period;
        frm->post = 0;
        frm->pre = 0;
    } else if(frm->timestamp == -1){ /* i.e get last */
        frm->timestamp = t_last;
        frm->post = 0;
        frm->pre = 0;
    }    
    
    frm->uid = last_uid;
   
    t_post = frm->timestamp + frm->post * drv->period + half_t ;
    t_pre  = frm->timestamp - frm->pre  * drv->period - half_t ;
    
    range->index = &frm->index[0];
    range->n = 0;

    if(frm->timestamp < t_last){
        range->t_pre = t_pre;
        range->t_post = min(t_post, t_last);
        n_future = max((s64)0, div64_s64(t_post - t_last, drv->period));
    }else{
        range->t_pre = min(t_last + (half_t >> 1), t_pre);
        range->t_post = t_last + (half_t >> 1) ;
        n_future = frm->post +div64_s64(frm->timestamp + half_t - max(t_pre, t_last), drv->period);
    }

    return n_future;
}
#endif

static inline void timestamp_to_uid(struct tntdrv *drv, struct tnt_range *range,
				    struct tnt_frames *frm)
{
	u64 t_last = last_timestamp(drv);
	u64 last_uid = drv->last_uid;
	s64 dt;
	s64 d_uid;

	if (frm->timestamp == 0) { /* i.e. get next */
		frm->timestamp = t_last + drv->period;
	} else if (frm->timestamp == -1) { /* i.e get last */
		frm->timestamp = t_last;
	}

	dt = frm->timestamp - t_last; /* D = T_s + P/2 - T_last */
	/* NOTE: if it is not in continuous mode 
       then period is not garanteed and d_uid
       could be wrong
    */
	d_uid = div64_s64(dt, drv->period);
	frm->uid = last_uid + d_uid;

	range->last_uid = last_uid;
	range->d_uid = d_uid;
}

static inline void uid_to_uid(struct tntdrv *drv, struct tnt_range *range,
			      struct tnt_frames *frm)
{
	u64 last_uid = drv->last_uid;

	//frm->uid = frm->timestamp;

	if (frm->uid == 0) { /* i.e. get next */
		frm->uid = last_uid + 1;
	} else if (frm->uid == -1) { /* i.e get last */
		frm->uid = last_uid;
	}

	range->last_uid = last_uid;
	range->d_uid = frm->uid - last_uid;
}

/**
\brief Computes the range of search interval to match
buffer. 

+ d_uid: is # of id from last uid dt/period could be negative
+ n_last: # of frames from last frame and requested frames

\param drv: pointer to struct tntdrv
\param range: pointer to struct rang containig time values
\param frm: pointer to struct frames from userspae
\return number of future frames
*/
static inline u64 frame_range(struct tntdrv *drv, struct tnt_range *range,
			      struct tnt_frames *frm)
{
	u64 n_future = 0;
	u64 last_uid = range->last_uid;
	s64 d_uid = range->d_uid;
	u64 n_last;

	n_last = abs(d_uid);
	range->t_pre = min(last_uid - (frm->pre - d_uid) - 1, last_uid);
	range->t_post = min(frm->uid + frm->post, last_uid);

	if (frm->uid <= last_uid) {
		n_future = max((s64)0, (s64)frm->post - (s64)n_last);
	} else {
		n_future = frm->post + n_last;
	}

	range->index = &frm->index[0];
	range->n = 0;

	return n_future;
}

/**
\brief Span over all buffer list
locking the queue. 
The span looks for frame inside requested time 
interval and  if dq value is true the frame is also 
dequeued from active list.

\param drv: pointer to struct tntdrv
\param range:  pointer to struct rang containig time values
\param dq: boole
\return  number of frames find.
*/
inline int span_index_list(struct tntdrv *drv, struct tnt_range *range, bool dq)
{
	foreach_frame_entry(drv, range, match_uid, dq);
	return range->n;
}

static int compare_ts(const void *lhs, const void *rhs, const void *par)
{
	int i0 = *(const int *)lhs;
	int i1 = *(const int *)rhs;
	struct tntdrv *drv = (struct tntdrv *)par;
	struct vb2_queue *q = &drv->queue;

	if (q->bufs[i0]->timestamp < q->bufs[i1]->timestamp)
		return -1;

	if (q->bufs[i0]->timestamp > q->bufs[i1]->timestamp)
		return 1;

	return 0;
}

void sort_frames_ts(struct tntdrv *drv, struct tnt_frames *frm)
{
	sort_r(&frm->index[0], frm->n, sizeof(int), compare_ts, NULL, drv);
}

typedef void (*range_fn)(struct tntdrv *, struct tnt_range *,
			 struct tnt_frames *);

static inline bool frame_under_limit(struct tnt_frames *frames, int n_active)
{
	return n_active - N_MIN_BUFS <= frames->pre + frames->post + 1;
}

/**
\brief deque a set of buffer over a timestamp
set by userspace application.
The struct tnt_frames contains the timestamp 
and the number of frames before and after 
timestamp.

\param drv: pointer to struct tntdrv
\param frames: pointer to struct tnt_frames
\param f_range: callback function of ts -> uid convertion
\param dq: true if frames must be dqueued
\return 0 or Parameter too big in case of frames requested
greater than N_MIN_BUFFER
*/
int dequeue_buffers(struct tntdrv *drv, struct tnt_frames *frames,
		    range_fn f_range, bool dq)
{
	struct tnt_range range;
	int n_active = atomic_read(&drv->n_active);
	int mode = FG_GMODE(drv->grabmode);

	if (mode == CONTINUOUS && frame_under_limit(frames, n_active)) {
		return -E2BIG;
	}

	tnt_spin_lock(drv);

	f_range(drv, &range, frames);
	frames->n_future = frame_range(drv, &range, frames);
	frames->n = span_index_list(drv, &range, dq);

	tnt_spin_unlock(drv);

	if (frames->n != 0)
		sort_frames_ts(drv, frames);

	return 0;
}

/**
\brief deque a set of buffer over a timestamp
set by userspace application.
The struct tnt_frames contains the timestamp 
and the number of frames before and after 
timestamp.

\param drv: pointer to struct tntdrv
\param frames: pointer to struct tnt_frames
\return 0 or Parameter too big in case of frames requested
greater than N_MIN_BUFFER
*/
inline int dequeue_buffers_by_ts(struct tntdrv *drv, struct tnt_frames *frames)
{
	int ret = 0;

	ret = dequeue_buffers(drv, frames, timestamp_to_uid, true);

	return ret;
}

/**
\brief deque a set of buffer over an uid
set by userspace application.
The struct tnt_frames contains the uid 
and the number of frames before uid.

\param drv: pointer to struct tntdrv
\param frames: pointer to struct tnt_frames
\return 0 or Parameter too big in case of frames requested
greater than N_MIN_BUFFER
*/
inline int dequeue_buffers_by_uid(struct tntdrv *drv, struct tnt_frames *frames)
{
	int ret = 0;

	ret = dequeue_buffers(drv, frames, uid_to_uid, true);

	return ret;
}

/**
\brief Wrapper for debugfs operatin of dq_buffer.

\param file: pointer to file
\param _buf: buffer write from userspace
\param count: number of bytes written
\param f_pos: file position
\return n. of byte written
*/
static ssize_t dbg_tdq_buffer(struct file *file, const char __user *_buf,
			      size_t count, loff_t *f_pos)
{
	struct tntdrv *drv = file->private_data;
	static struct tnt_frames frames;
	int ret;

	ret = sscanf(_buf, "%lld %d %d", &frames.timestamp, &frames.pre,
		     &frames.post);

	if (ret != 3)
		return -EINVAL;

	if (frames.timestamp == -1)
		frames.timestamp = last_timestamp(drv);

	//pr_info("index: %lld %d %d\n", frames.timestamp, frames.pre, frames.post);
	dequeue_buffers_by_ts(drv, &frames);

	*f_pos += count;
	return count;
}

inline int wait_next_frames(struct tntdrv *drv, u64 uid, u64 ms);

static ssize_t dbg_frames_buffer(struct file *file, const char __user *_buf,
				 size_t count, loff_t *f_pos)
{
	struct tntdrv *drv = file->private_data;
	struct tnt_frames frm;
	struct tnt_frames next_frm;
	int ret, uid;
	u64 timeout;

	ret = sscanf(_buf, "%lld %d %d %d", &frm.timestamp, &frm.pre, &frm.post,
		     &uid);

	if (ret != 4)
		return -EINVAL;

	if (uid == 0) { /* timestamp */
		ret = dequeue_buffers(drv, &frm, timestamp_to_uid, false);
	} else {
		frm.uid = frm.timestamp;
		ret = dequeue_buffers(drv, &frm, uid_to_uid, false);
	}

	pr_info("frame n %d: future: %lld\n", frm.n, frm.n_future);
	if (frm.n == 0) {
		pr_info("No frames\n");
	} else {
		for (uid = 0; uid < frm.n; uid++) {
			pr_info("id# %d\n", frm.index[uid]);
		}
	}

	/* deque next */
	if (frm.n < frm.pre + frm.post + 1) {
		next_frm.uid = frm.uid + frm.post;
		next_frm.pre = frm.n_future - 1;
		next_frm.post = 0;

		timeout = (frm.pre + 2) * drv->period;
		ret = wait_next_frames(drv, next_frm.uid, timeout);
		ret = dequeue_buffers(drv, &next_frm, uid_to_uid, false);
		pr_info("frame n %d: future: %lld\n", frm.n, frm.n_future);

		for (uid = 0; uid < next_frm.n; uid++) {
			pr_info("id# %d\n", next_frm.index[uid]);
		}
	}
	*f_pos += count;
	return count;
}

inline void dquid_buffer(struct tntdrv *drv, u64 uid)
{
	tnt_spin_lock(drv);
	foreach_frame_entry(drv, (void *)uid, match_range, true);
	tnt_spin_unlock(drv);
}

/**
\brief Wrapper for debugfs operatin of udq_buffer.
It dqueue a frame using its uid

\param file: pointer to file
\param _buf: buffer write from userspace
\param count: number of bytes written
\param f_pos: file position
\return n. of byte written
*/
static ssize_t dbg_udq_buffer(struct file *file, const char __user *_buf,
			      size_t count, loff_t *f_pos)
{
	struct tntdrv *drv = file->private_data;
	u64 uid;
	int ret;

	ret = sscanf(_buf, "%lld", &uid);

	if (ret != 1)
		return -EINVAL;

	dquid_buffer(drv, uid);

	*f_pos += count;
	return count;
}

/**
\brief Wrapper for debugfs operatin of q_buffer.

\param file: pointer to file
\param _buf: buffer write from userspace
\param count: number of bytes written
\param f_pos: file position
\return n. of byte written
*/
static ssize_t dbg_q_buffer(struct file *file, const char __user *_buf,
			    size_t count, loff_t *f_pos)
{
	int id;
	int ret;
	struct v4l2_buffer vb;
	struct v4l2_plane planes[DOUBLE];
	struct tntdrv *drv = file->private_data;
	u64 time;

	ret = sscanf(_buf, "%d", &id);
	if (ret != 1)
		return -EINVAL;

	vb.index = id;
	vb.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	vb.memory = V4L2_MEMORY_MMAP;
	if (FG_SOURCE(drv->grabmode) == DOUBLE)
		vb.m.planes = planes;

	if (vb.index >= drv->queue.num_buffers)
		return -EINVAL;

	time = ktime_get_ns();
	wait_for_slot(drv);
	ret = q_buffer(drv, &vb);
	//pr_info("%lld\n", ktime_get_ns() - time);

	*f_pos += count;
	return count;
}

static ssize_t dbg_set_grabmode(struct file *file, const char __user *_buf,
				size_t count, loff_t *f_pos)
{
	int id;
	int ret;
	struct tntdrv *drv = file->private_data;

	ret = sscanf(_buf, "%d", &id);
	if (ret != 1)
		return -EINVAL;

	ret = tnt_set_grabmode(drv, id);
	if (ret)
		return -EINVAL;

	*f_pos += count;
	return count;
}

static ssize_t dbg_get_grabmode(struct file *file, char __user *buf,
				size_t count, loff_t *f_pos)
{
	struct tntdrv *drv = file->private_data;
	int ret, size, mode;
	char n[4];

	if (*f_pos != 0) {
		return 0;
	}

	mode = tnt_get_grabmode(drv);
	size = sprintf(n, "%d", mode);

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

int load_next_frame(struct tntdrv *drv);

static ssize_t dbg_set_next(struct file *file, const char __user *_buf,
			    size_t count, loff_t *f_pos)
{
	int id;
	int ret;
	struct tntdrv *drv = file->private_data;

	ret = sscanf(_buf, "%d", &id);

	ret = load_next_frame(drv);
	if (ret)
		return -EINVAL;

	*f_pos += count;
	return count;
}

static ssize_t dbg_read_busy_catch(struct file *file, char __user *buf,
				   size_t count, loff_t *f_pos)
{
	int status, size, ret;
	struct tntdrv *drv = file->private_data;
	char n[4];

	if (*f_pos != 0) {
		return 0;
	}

	status = tnt_fpga_read_busy_catch(&drv->device);
	size = sprintf(n, "%d", status);
	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static ssize_t dbg_clear_busy_catch(struct file *file, const char __user *buf,
				    size_t count, loff_t *f_pos)
{
	int id;
	int ret;
	struct tntdrv *drv = file->private_data;

	ret = sscanf(buf, "%d", &id);
	if (ret != 1)
		return -EINVAL;

	tnt_fpga_clear_busy_catch(&drv->device);

	*f_pos += count;
	return count;
}

static ssize_t dbg_read_end_frame(struct file *file, char __user *buf,
				  size_t count, loff_t *f_pos)
{
	int ret, size, status;
	struct tntdrv *drv = file->private_data;
	char n[4];

	if (*f_pos != 0) {
		return 0;
	}

	status = tnt_fpga_read_end_frame(&drv->device);
	size = sprintf(n, "%d", status);
	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static ssize_t dbg_clear_end_frame(struct file *file, const char __user *_buf,
				   size_t count, loff_t *f_pos)
{
	int id;
	int ret;
	struct tntdrv *drv = file->private_data;

	ret = sscanf(_buf, "%d", &id);
	if (ret != 1)
		return -EINVAL;

	tnt_fpga_clear_end_frame(&drv->device);
	*f_pos += count;
	return count;
}

int vb2_start_streaming(struct vb2_queue *q);
void tnt_prepare_grab(struct tntdrv *drv);
void arm_fsm(struct tntdrv *drv);
inline void wait_last_img_queued(struct tntdrv *drv);

inline void wait_last_frame(struct tntdrv *drv)
{
	int timeout = 0;

	if (drv->stop == 0) {
		drv->stop = 1;
		while (drv->stop != 2 || timeout < 5) {
			mdelay(1);
			timeout++;
		}
	}
	drv->stop = 2;
}

static ssize_t dbg_start_continuous(struct file *file, const char __user *buf,
				    size_t count, loff_t *f_pos)
{
	int cmd;
	int ret;
	struct tntdrv *drv = file->private_data;
	struct tnt_device *dev = &drv->device;

	ret = sscanf(buf, "%d", &cmd);
	switch (cmd) {
	case 0:
		wait_last_frame(drv);
		break;
	case 1:
		vb2_start_streaming(&drv->queue);
		break;
	case 2:
		drv->oo = 0;
		drv->o = 0;
		load_next_frame(drv);
		tnt_fpga_start_fsm(dev);
		drv->stop = 0;
		break;
	case 3:
		test_loop(drv);
		break;
	}

	*f_pos += count;
	return count;
}

int tnt_rle_init(struct tntdrv *drv);

static ssize_t dbg_rle(struct file *file, const char __user *buf, size_t count,
		       loff_t *f_pos)
{
	int cmd;
	int ret;
	struct tntdrv *drv = file->private_data;

	ret = sscanf(buf, "%d", &cmd);
	switch (cmd) {
	case 1:
		tnt_rle_init(drv);
		tnt_write(&drv->cha.device, RST_IRQ_END_PHOTO, 1);
		break;
	}

	*f_pos += count;
	return count;
}

static ssize_t n_end_frame(struct file *file, char __user *buf, size_t count,
			   loff_t *f_pos)
{
	int ret, size;
	struct tntdrv *drv = file->private_data;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	size = sprintf(
		n, "busy_catch: %d\nend_frame: %d\nend_cha:%d\nend_chb:%d\n",
		drv->o, drv->oo, drv->cnt_cha, drv->cnt_chb);

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static ssize_t n_act_frame(struct file *file, char __user *buf, size_t count,
			   loff_t *f_pos)
{
	int ret, size;
	struct tntdrv *drv = file->private_data;
	char n[8];
	int n_active = atomic_read(&drv->n_active);

	if (*f_pos != 0) {
		return 0;
	}

	size = sprintf(n, "%d\n", n_active);
	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static ssize_t print_slot(struct file *file, char __user *buf, size_t count,
			  loff_t *f_pos)
{
	int ret, size;
	struct tntdrv *drv = file->private_data;
	char n[128];
	u64 time = hrtimer_get_remaining(&drv->access.t);

	if (*f_pos != 0) {
		return 0;
	}

	size = sprintf(n, "slot time reamining :%llu\n", time);

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static inline u64 random_ts_frame(struct tntdrv *drv)
{
	u32 rand;

	get_random_bytes(&rand, sizeof(rand));
	rand %= drv->queue.num_buffers;

	return drv->queue.bufs[rand]->timestamp;
}

static ssize_t random_ts(struct file *file, char __user *buf, size_t count,
			 loff_t *f_pos)
{
	int ret, size;
	struct tntdrv *drv = file->private_data;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	size = sprintf(n, "%llu", random_ts_frame(drv));

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static ssize_t print_stop(struct file *file, char __user *buf, size_t count,
			  loff_t *f_pos)
{
	int ret, size;
	struct tntdrv *drv = file->private_data;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	size = sprintf(n, "stop status :%d\n", drv->stop);

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static ssize_t dbg_get_timestamp(struct file *file, char __user *buf,
				 size_t count, loff_t *f_pos)
{
	//struct tntdrv *drv = file->private_data;
	int ret, size;
	char n[128];

	if (*f_pos != 0) {
		return 0;
	}

	size = sprintf(
		n,
		"get: %lld\nget_ns: %lld\nboot: %lld\nreal: %lld\nraw: %lld\n",
		ktime_get(), ktime_get_ns(), ktime_get_boottime_ns(),
		ktime_get_real_ns(), ktime_get_raw_ns());

	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

#if 0

static
ssize_t dbg_print_cache(struct file *file, char __user *_buf, size_t count, loff_t *f_pos)
{
    struct tntdrv *drv = file->private_data;
    volatile struct tnt_buf *buf;
    int ret, size;
    char n[1024] = "";
    u64 t_last ;
    unsigned long flag;
    
    if(*f_pos != 0){
        return 0;
    }

    size = 0;
    if(drv->stop == 0)
        wait_slot(&drv->access);
    
    t_last = last_timestamp(drv);
    if(!list_empty(&drv->cache_list)){
        list_for_each_entry(buf, &drv->cache_list, cache_entry){
            size = sprintf(n, "%s %s %d %s", n, buf->vb.timestamp == t_last?"[": "",
                           buf->vb.index, buf->vb.timestamp == t_last? "]":"");
        }
    }
    
    //size = sprintf(n, "N cached: %d\n", n_cache);
    ret = copy_to_user(_buf, n, size);
    if (ret != 0)
		return -EFAULT;    

    *f_pos += size;
    return size;

}


static
struct file_operations cache_ops = {
    .open  = activate_open, 
    .read = dbg_print_cache,
};

#endif

static struct file_operations current_ops = {
	.open = activate_open,
	.read = current_buffer,
};

static struct file_operations busybuf_ops = {
	.open = activate_open,
	.read = busy_buffer,
};

static struct file_operations stop_ops = {
	.open = activate_open,
	.read = print_stop,
};

static struct file_operations endf_ops = {
	.open = activate_open,
	.read = n_end_frame,
};

static struct file_operations n_active_ops = {
	.open = activate_open,
	.read = n_act_frame,
};

static struct file_operations slot_ops = {
	.open = activate_open,
	.read = print_slot,
};

static struct file_operations random_ops = {
	.open = activate_open,
	.read = random_ts,
};

static struct file_operations activate_ops = {
	.open = activate_open,
	.write = activate_buffers,
};

static struct file_operations dq_ops = {
	.open = activate_open,
	.write = dbg_dq_buffer,
};

static struct file_operations tdq_ops = {
	.open = activate_open,
	.write = dbg_tdq_buffer,
	//.read  = dbg_dq_print,
};

static struct file_operations frames_ops = {
	.open = activate_open,
	.write = dbg_frames_buffer,
	//.read  = dbg_dq_print,
};

static struct file_operations udq_ops = {
	.open = activate_open,
	.write = dbg_udq_buffer,
	//.read  = dbg_dq_print,
};

static struct file_operations q_ops = {
	.open = activate_open,
	.write = dbg_q_buffer,
};

static struct file_operations grabmode_ops = {
	.open = activate_open,
	.write = dbg_set_grabmode,
	.read = dbg_get_grabmode,
};

static struct file_operations next_ops = {
	.open = activate_open,
	.write = dbg_set_next,
};

static struct file_operations end_ops = {
	.open = activate_open,
	.write = dbg_clear_end_frame,
	.read = dbg_read_end_frame,
};

static struct file_operations busy_ops = {
	.open = activate_open,
	.read = dbg_read_busy_catch,
	.write = dbg_clear_busy_catch,
};

static struct file_operations start_ops = {
	.open = activate_open,
	.write = dbg_start_continuous,
};

static struct file_operations rle_ops = {
	.open = activate_open,
	.write = dbg_rle,
};

static struct file_operations timestamp_ops = {
	.open = activate_open,
	.read = dbg_get_timestamp,
};

static struct tdm_watch queues[] = {
	{ "active_list", 0600, TDM_LIST, &lst_fops, { 0, &active_seqops } },
	{ "queued_list", 0600, TDM_LIST, &lst_fops, { 0, &queued_seqops } },
	{ "done_list", 0600, TDM_LIST, &lst_fops, { 0, &done_seqops } },
	{ "dqueued_list", 0600, TDM_LIST, &lst_fops, { 0, &dqueued_seqops } },
	{}
};

void trace_queue_lists(struct tntdrv *drv, struct tdm_debug *dbg)
{
	int ret;
	struct tdm_watch chunks[] = {
		{ "nbufs", 0400, TDM_UINT32, 0, { &drv->queue.num_buffers } },
		{ "queue_cmd", 0200, TDM_CUSTOM, &activate_ops },
		{ "dqbuf", 0200, TDM_CUSTOM, &dq_ops },
		{ "qbuf", 0200, TDM_CUSTOM, &q_ops },
		{ "cbuf", 0400, TDM_CUSTOM, &current_ops },
		{ "busybuf", 0400, TDM_CUSTOM, &busybuf_ops },
		{ "grab_mode", 0600, TDM_CUSTOM, &grabmode_ops },
		{ "next_frame", 0600, TDM_CUSTOM, &next_ops },
		{ "end_frame", 0600, TDM_CUSTOM, &end_ops },
		{ "busy_catch", 0600, TDM_CUSTOM, &busy_ops },
		{ "start_continuous", 0600, TDM_CUSTOM, &start_ops },
		{ "n_end_frame", 0400, TDM_CUSTOM, &endf_ops },
		{ "n_active", 0400, TDM_CUSTOM, &n_active_ops },
		{ "stop", 0400, TDM_CUSTOM, &stop_ops },
		{ "slot", 0400, TDM_CUSTOM, &slot_ops },
		{ "random", 0400, TDM_CUSTOM, &random_ops },
		{ "timestamp", 0400, TDM_CUSTOM, &timestamp_ops },
		{ "tdqbuf", 0600, TDM_CUSTOM, &tdq_ops },
		{ "frames", 0600, TDM_CUSTOM, &frames_ops },
		{ "udqbuf", 0600, TDM_CUSTOM, &udq_ops },
		{ "rle", 0200, TDM_CUSTOM, &rle_ops },
		{}
	};
	queues[0].opt.param = drv;
	queues[1].opt.param = &drv->queue;
	queues[2].opt.param = &drv->queue;

	dbg->dirname = "tnt";
	ret = tdm_init(dbg);
	if (ret) {
		pr_info("Error Init tdm %s (err. %d)\n", dbg->dirname, ret);
		return;
	}

	tdm_append_watches(dbg, &queues[0], drv);
	tdm_append_watches(dbg, &chunks[0], drv);
}

void free_queue_trace(struct tntdrv *drv)
{
	if (drv->queue_trace.dir != NULL)
		debugfs_remove_recursive(drv->queue_trace.dir);
}
