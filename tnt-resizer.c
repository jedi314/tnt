/** 
 \file tnt-resizer.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief The resizer processor scales the size 
 of  a frame.
 It is codeded in fpga and it is exported by v4l sub device.
 The processor constists of a set of operations exportef to 
 processor list.
*/

#define PTR_DMA10 0x0002017C

#include <linux/types.h>
#include "tntvdo.h"
#include "tnt-processor.h"
#include "tnt-allocator.h"

static inline void resizer_set_dmaptr(struct tnt_device *dev, dma_addr_t addr)
{
	tnt_write(dev, PTR_DMA10, addr);
}

static u32 resizer_plane_size(struct tnt_processor *processor)
{
	struct tnt_resize *resizer = (struct tnt_resize *)processor->info;

	return resizer->w * resizer->h;
}

__maybe_unused static int resizer_set_addr(struct tnt_processor *pr, void *b)
{
	struct tnt_device *dev = get_dev(pr);
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct alc_chunk *chunk = buf->vb.planes[pr->plane_id].mem_priv;

	resizer_set_dmaptr(dev, chunk->addr);
	return 0;
}

static int resizer_info(struct tnt_processor *processor,
			struct processor_layout *l)
{
	struct tnt_resize *resizer = (struct tnt_resize *)processor->info;

	l->w = resizer->w;
	l->h = resizer->h;
	l->size = resizer_plane_size(processor);

	return 0;
}

/**
\brief ioctl execute after a call to a ioctl 
of /dev/video or directly calling /dev/v4l2-subdev

\param sd: pointer to vsl2_subdev function 
\param cmd: command code as genenrated vy _IO
\param arg: argument passed ad defined durinf subdev initialization

\return 0 in case cmd is found or  -ENOTTY
*/
long resizer_ioctl(struct v4l2_subdev *sd, unsigned int cmd, void *arg)
{
	return 0;
}

struct tnt_processor_ops resizer_ops = {
	.plane_size = resizer_plane_size,
	.set_addr = resizer_set_addr,
	.info = resizer_info,
};

const struct v4l2_subdev_core_ops resizer_core_ops = {
	.ioctl = resizer_ioctl,
};

const struct v4l2_subdev_ops resizer_sdev_ops = {
	.core = &resizer_core_ops,
};
