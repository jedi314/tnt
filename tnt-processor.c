/** 
 \file tnt-processor.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
 A processor is a subdevice inside fpga used for
 image manipulation.

 It can be installed to every physical channel i.e. could have 
 e set operation and exports a /dev/v4l-subdevX device file.

*/

#include "tnt-processor.h"
#include "tnt-allocator.h"

#define call_proc_op(p, op, args...)                                           \
	({                                                                     \
		int ret = 1;                                                   \
		if (p && p->ops->op)                                           \
			ret = p->ops->op(args);                                \
		ret;                                                           \
	})

#define call_void_op(p, op)                                                    \
	({                                                                     \
		if (p && p->ops->op)                                           \
			p->ops->op();                                          \
	})

inline struct tnt_allocator *get_allocator(struct tnt_processor *p)
{
	return p->vdo->driver->allocator;
}

inline struct tnt_device *get_dev(struct tnt_processor *p)
{
	return &p->vdo->device;
}

static int processor_open(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh)
{
	//struct tnt_processor *pr = v4l2_get_subdevdata(sd);
	//struct tntvdo* vdo = pr->vdo;

	//pr_info(">>> OPEN subdevice %s.%d\n", fh->vfh.vdev->name, vdo->id);
	return 0;
}

static int processor_close(struct v4l2_subdev *subdev,
			   struct v4l2_subdev_fh *fh)
{
	//pr_info("CLOSE subdevice\n");
	return 0;
}

__maybe_unused static long processor_ioctl(struct v4l2_subdev *sd,
					   unsigned int cmd, void *arg)
{
	//pr_info("IOCTL subdevice\n");
	return 0;
}

const struct v4l2_subdev_internal_ops processor_internal_ops = {
	.open = processor_open,
	.close = processor_close,

};

/* static const struct v4l2_subdev_core_ops processor_core_ops = { */
/*     .ioctl = processor_ioctl, */
/* }; */

/* static const struct v4l2_subdev_ops processor_ops = { */
/*     .core = &processor_core_ops, */
/* }; */

inline struct tnt_processor *find_processor(struct tntvdo *vdo, char *name)
{
	int i;
	struct tnt_processor *processor_list = vdo->processor_list;

	for (i = 0; i < vdo->n_processor; i++) {
		if (strcasecmp(processor_list[i].name, name) == 0)
			return &processor_list[i];
	}

	return NULL;
}

static int register_processor_subdev(struct tntvdo *vdo,
				     struct tnt_processor *processor)
{
	int ret;
	struct v4l2_subdev *sd;
	sd = &processor->sub;

	/* sub device */
	pr_info("VDO# %d: Subdev [%s] initialized\n", vdo->id, processor->name);
	v4l2_subdev_init(sd, processor->sdev_ops);

	snprintf(sd->name, sizeof(sd->name), "%s", processor->name);
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;
	v4l2_set_subdevdata(sd, processor);
	sd->dev = vdo->device.dev;
	sd->internal_ops = processor->internal_ops;

	ret = v4l2_device_register_subdev(&vdo->v4l2_dev, sd);
	if (ret) {
		pr_err("tnt subdev nodes registration failed (err=%d)\n", ret);
		return ret;
	}

	ret = v4l2_device_register_subdev_nodes(&vdo->v4l2_dev);
	if (ret) {
		pr_err("tnt subdev nodes registration failed (err=%d)\n", ret);
		return ret;
	}
	return 0;
}

int tnt_register_processors(struct tntvdo *vdo)
{
	struct tnt_processor *processor;
	int i, ret = 0;

	for (i = 0; i < vdo->n_processor; i++) {
		processor = &vdo->processor_list[i];
		processor->vdo = vdo;
		ret = register_processor_subdev(vdo, processor);
		if (ret)
			break;
		call_proc_op(processor, init, processor);
		processor->cntx_id = -1;
	}

	return ret;
}

void tnt_release_processors(struct tntvdo *vdo)
{
	struct tnt_processor *processor;
	int i;

	if (vdo->n_processor == 0)
		return;

	for (i = 0; i < vdo->n_processor; i++) {
		processor = &vdo->processor_list[i];
		v4l2_device_unregister_subdev(&processor->sub);
		call_proc_op(processor, destroy, processor);
		pr_info("Removed processor <%s>", processor->name);
	}

	kfree(vdo->processor_list);
}

void processor_alloc_plane(struct tnt_processor *processor,
			   struct device **alloc_dev, unsigned int *nbuffers,
			   unsigned int *size)
{
	int id;
	unsigned int chunk_size;
	struct tntvdo *vdo = processor->vdo;
	struct tntdrv *drv = vdo->driver;
	struct tnt_allocator *allocator =
		(struct tnt_allocator *)drv->allocator;

	chunk_size = processor->ops->plane_size(processor);

	/* chunck size is PAGE_SIZE aligned by alloc_ctx */
	id = alloc_ctx(allocator, processor->name, *nbuffers, &chunk_size,
		       NULL);

	*size = chunk_size;
	*alloc_dev = (struct device *)&allocator->ctxs[id];
	dev_set_drvdata(*alloc_dev, allocator);
	processor->cntx_id = id;
}

/*  */

int tnt_processor_create_ctx(struct tntvdo *vdo, struct device **alloc_dev,
			     unsigned int *nbuffers, unsigned int *size,
			     int *np)
{
	int i;
	struct tnt_processor *pr;

	for (i = 0; i < vdo->n_processor; i++) {
		pr = &vdo->processor_list[i];
		processor_alloc_plane(pr, &alloc_dev[i], nbuffers, &size[i]);

		pr->plane_id = (*np)++;
		pr_info("Allocator [%s]: %px, size=%u bytes plane# %u id=%d\n",
			pr->name, *alloc_dev, *size, pr->plane_id, pr->cntx_id);
	}
	return 0;
}

int tnt_processors_release_ctx(struct tnt_allocator *allocator,
			       struct tntvdo *vdo)
{
	int i;
	struct tnt_processor *pr;

	for (i = 0; i < vdo->n_processor; i++) {
		pr = &vdo->processor_list[i];
		release_ctx(allocator, pr->cntx_id);
	}

	return 0;
}

int tnt_mmap_processors(struct tntvdo *vdo, struct vm_area_struct *vma)
{
	int i, ret = 0;
	struct tnt_processor *pr;

	for (i = 0; i < vdo->n_processor; i++) {
		pr = &vdo->processor_list[i];
		ret = call_proc_op(pr, mmap, pr, vma);
		if (ret == 0)
			return i;
	}

	return -1;
}

/* inline int processor_init(struct tnt_processor* pr) */
/* { */
/*     pr_info("PROCESSOR: %d\n", pr->cntx_id); */
/*     return call_proc_op(pr, init, pr); */
/*     pr->cntx_id = -1; */
/* } */

/* int tnt_processors_init(struct tntvdo* vdo) */
/* { */
/*     int i, ret = 0; */
/*     struct tnt_processor *pr; */

/*     TRACE; */
/*     for(i=0; i<vdo->n_processor; i++){ */
/*         pr = &vdo->processor_list[i]; */
/*         ret = processor_init(pr); */
/*         if(ret) */
/*             return ret; */
/*     } */

/*     return 0; */
/* } */

int tnt_processors_setaddr(struct tntvdo *vdo, struct tnt_buf *buf)
{
	int i;
	struct tnt_processor *pr;

	for (i = 0; i < vdo->n_processor; i++) {
		pr = &vdo->processor_list[i];
		call_proc_op(pr, set_addr, pr, buf);
	}

	return 0;
}

int processor_info(struct tntvdo *vdo, struct processor_layout *pl)
{
	struct tnt_processor *pr;

	if (pl->index >= vdo->n_processor)
		return -EINVAL;

	pr = &vdo->processor_list[pl->index];
	strcpy(pl->name, pr->name);
	strcpy(pl->devname, pr->sub.devnode->name);
	pl->plane_id = pr->plane_id;
	call_proc_op(pr, info, pr, pl);

	return 0;
}

int get_processor_info(struct tntvdo *vdo, struct processor_layout *pl)
{
	struct tnt_processor *pr;

	pr = find_processor(vdo, pl->name);

	if (pr == NULL)
		return -EINVAL;

	strcpy(pl->name, pr->name);
	strcpy(pl->devname, pr->sub.devnode->name);
	pl->plane_id = pr->plane_id;
	return 0;
}
