/** 
 \file tnt-subdev.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_PROCESSOR_H_
#define _TNT_PROCESSOR_H_

#include <media/v4l2-device.h>
#include <linux/mm.h>
#include "tntvdo.h"
#include "tnt-cmd.h"

int tnt_register_processor_subdev(struct tntvdo *vdo);
void tnt_release_processors(struct tntvdo *vdo);
int tnt_register_processors(struct tntvdo *vdo);
int tnt_mmap_processors(struct tntvdo *vdo, struct vm_area_struct *vma);
inline struct tnt_allocator *get_allocator(struct tnt_processor *p);
inline struct tnt_device *get_dev(struct tnt_processor *p);
int tnt_processor_create_ctx(struct tntvdo *vdo, struct device **alloc_dev,
			     unsigned int *nbuffers, unsigned int *size,
			     int *np);
int processor_info(struct tntvdo *vdo, struct processor_layout *pl);
int tnt_processors_setaddr(struct tntvdo *vdo, struct tnt_buf *buf);
int processor_info(struct tntvdo *vdo, struct processor_layout *pl);
int get_processor_info(struct tntvdo *vdo, struct processor_layout *pl);
int tnt_processors_release_ctx(struct tnt_allocator *allocator,
			       struct tntvdo *vdo);

#endif
