/** 
 \file tnt-regs.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Low level accces to fpga registers.
*/

#include <linux/io.h>
#include <asm/ioctl.h>

#include "tnt-io.h"
#include "tnt-cmd.h"

bool enable_print = false;

inline void tnt_write(struct tnt_device *tntdev, int reg, int val)
{
	writel(val, tntdev->org + reg);
}

inline u32 tnt_read(struct tnt_device *tntdev, int reg)
{
	return readl(tntdev->org + reg);
}

#if 0

inline void tnt_disable_intr(struct tnt_device *tntdev, u32 ch)
{
}

inline void tnt_enable_intr(struct tnt_device *tntdev, u32 ch)
{
}

#endif
