/** 
 \file tnt-rle.c
 
 

 \brief RLE fpga
*/

#include "fpga-cs.h"
#include "tnt-rle.h"

static const u8 tnt_rle_lut_default[TNT_RLE_LUT_LEN] = {
	38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21,
	20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9,	8,  8,	8,  8,	8,  9,
	9,  9,	9,  9,	10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 12, 12, 12, 12,
	12, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 16, 16,
	16, 16, 16, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19,
	20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 23, 23, 23,
	23, 23, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 27,
	27, 27, 28, 29, 29, 29, 29, 29, 30, 30, 30, 30, 30, 31, 31, 31, 31, 32,
	32, 32, 32, 32, 33, 33, 33, 33, 33, 34, 34, 34, 34, 34, 35, 35, 35, 35,
	36, 36, 36, 36, 36, 37, 37, 37, 37, 37, 38, 38, 38, 38, 38, 39, 39, 39,
	41, 41, 42, 42, 42, 42, 42, 43, 43, 43, 43, 44, 44, 44, 44, 44, 45, 45,
	45, 45, 46, 46, 46, 46, 46, 47, 47, 47, 47, 47, 48, 48, 48, 48, 49, 49,
	49, 49, 49, 50, 50, 50, 50, 51, 51, 51, 51, 51, 52, 52, 52, 52, 53, 53,
	53, 53, 53, 54, 54, 54, 54, 55, 55, 55, 55, 55, 56, 56, 56, 56, 57, 57,
	57, 57, 57, 58
};

static u32 numseg_size[RLE_ALGO_NUM];

// Imposta parametri algoritmo
int tnt_set_rle_cha(struct tnt_device *dev, const struct tnt_rle_conf *par,
		    u8 index)
{
	u32 i;

	if (par == NULL) {
		pr_info("tnt_set_rle_cha(): NULL pointer");
		return -EFAULT;
	}

	if (par->max_hor_size_char > 0xFFFF) {
		pr_info("tnt_set_rle_cha(): wrong max_hor_size_char parameter");
		return -EINVAL;
	}

	if ((par->min_hor_size_char > 0xFFFF) ||
	    (par->min_hor_size_char > par->max_hor_size_char)) {
		pr_info("tnt_set_rle_cha(): wrong min_hor_size_char parameter");
		pr_info("tnt_set_rle_cha(): wrong max_hor_size_char parameter");
		return -EINVAL;
	}

	if (par->max_hor_size_for_reload_start > 0xFFFF) {
		pr_info("tnt_set_rle_cha(): wrong max_hor_size_for_reload_start parameter");
		return -EINVAL;
	}

	if ((par->num_differential_pair > 3) ||
	    (par->num_differential_pair < 1)) {
		pr_info("tnt_set_rle_cha(): wrong num_differential_pair parameter");
		return -EINVAL;
	}

	if (par->pixel_distance > 7) {
		pr_info("tnt_set_rle_cha(): wrong Pixel_distance parameter");
		return -EINVAL;
	}

	if (par->size_y > 4095) {
		pr_info("tnt_set_rle_cha(): wrong Size_y parameter");
		return -EINVAL;
	}

	if (par->size_x > 4095) {
		pr_info("tnt_set_rle_cha(): wrong Size_x parameter");
		return -EINVAL;
	}

	if (par->num_byte_max & 0x1FF) {
		pr_info("tnt_set_rle_cha(): wrong Num_byte_max parameter, it must be aligned to 512 Byte");
		return -EINVAL;
	}

	if (index > 1) {
		pr_info("tnt_set_rle_cha(): wrong index parameter, it must be less than 1");
		return -EINVAL;
	}

	if (par->black_plates_enable > 1) {
		pr_info("tnt_set_rle_cha(): wrong info_parameters value, it must be less than 1");
		return -EINVAL;
	}

	if (par->black_plates_enable == 1) {
		pr_info("This function is not supported in this FPGA version!");
	}

	numseg_size[index] = (par->size_y + (128 - 1)) & -128;

	if (index == 0) {
		tnt_write(dev, EN_ALGH_A_CHA, 1);

		//imposta la lut dei gradienti
		for (i = 0; i < 256; i++) {
			tnt_write(dev, SET_LUT_ALGH_A_CHA,
				  ((i << 8) | (par->lut[i] & 0xff)));
		}

		//imposta pixel_distance
		tnt_write(dev, PIXEL_DISTANCE_X_ALGH_A_CHA,
			  par->pixel_distance);

		//imposta size_y
		tnt_write(dev, SIZE_Y_ALGH_A_CHA, par->size_y);

		//imposta size_x
		tnt_write(dev, SIZE_X_ALGH_A_CHA, par->size_x);

		//num_byte_max
		tnt_write(dev, SIZE_NUM_BYTE_MAX_ALGH_A_CHA, par->num_byte_max);

		//max_hor_size_char	(larghezza massima rle)
		tnt_write(dev, MAX_HOR_SIZE_CHAR_ALGH_A_CHA,
			  par->max_hor_size_char);

		//min_hor_size_char	(larghezza minima rle)
		tnt_write(dev, MIN_HOR_SIZE_CHAR_ALGH_A_CHA,
			  par->min_hor_size_char);

		//max_hor_size_for_reload_start (dopo uno start segmento devo trovare uno stop entro max_hor_size_for_reload_start, altrimenti si sposta lo start)
		tnt_write(dev, X_SIZE_FOR_RELOAD_START_ALGH_A_CHA,
			  par->max_hor_size_for_reload_start);

		//enable erase line with too many strokes
		tnt_write(dev,
			  ENABLE_ERASE_LINE_WITH_TOO_MANY_STROKE_ALGH_A_CHA, 0);

		//num_diff_pair
		tnt_write(dev, NUM_PAIR_ALGH_A_CHA, par->num_differential_pair);

		//imposta rle_output_format
		tnt_write(dev, RLE_OUTPUT_FORMAT_ALGH_A_CHA, 1);

		//black_plates_enable
		tnt_write(dev, BLACK_PLATES_ENABLE_ALGH_A_CHA,
			  par->black_plates_enable);

		tnt_write(dev, EN_ALGH_A_CHA, 0);
	} else {
		tnt_write(dev, EN_ALGH_B_CHA, 1);

		//imposta la lut dei gradienti
		for (i = 0; i < 256; i++) {
			tnt_write(dev, SET_LUT_ALGH_B_CHA,
				  ((i << 8) | (par->lut[i] & 0xff)));
		}

		//imposta pixel_distance
		tnt_write(dev, PIXEL_DISTANCE_X_ALGH_B_CHA,
			  par->pixel_distance);

		//imposta size_y
		tnt_write(dev, SIZE_Y_ALGH_B_CHA, par->size_y);

		//imposta size_x
		tnt_write(dev, SIZE_X_ALGH_B_CHA, par->size_x);

		//num_byte_max
		tnt_write(dev, SIZE_NUM_BYTE_MAX_ALGH_B_CHA, par->num_byte_max);

		//max_hor_size_char	(larghezza massima rle)
		tnt_write(dev, MAX_HOR_SIZE_CHAR_ALGH_B_CHA,
			  par->max_hor_size_char);

		//min_hor_size_char	(larghezza minima rle)
		tnt_write(dev, MIN_HOR_SIZE_CHAR_ALGH_B_CHA,
			  par->min_hor_size_char);

		//max_hor_size_for_reload_start (dopo uno start segmento devo trovare uno stop entro max_hor_size_for_reload_start, altrimenti si sposta lo start)
		tnt_write(dev, X_SIZE_FOR_RELOAD_START_ALGH_B_CHA,
			  par->max_hor_size_for_reload_start);

		//enable erase line with too many strokes
		tnt_write(dev,
			  ENABLE_ERASE_LINE_WITH_TOO_MANY_STROKE_ALGH_B_CHA, 0);

		//num_diff_pair
		tnt_write(dev, NUM_PAIR_ALGH_B_CHA, par->num_differential_pair);

		//imposta rle_output_format
		tnt_write(dev, RLE_OUTPUT_FORMAT_ALGH_B_CHA, 1);

		//black_plates_enable
		tnt_write(dev, BLACK_PLATES_ENABLE_ALGH_B_CHA,
			  par->black_plates_enable);

		tnt_write(dev, EN_ALGH_B_CHA, 0);
	}

	return 0;
}

static void tnt_rle_set_default(struct tnt_rle_conf *par)
{
	int i;

	memset(par, 0, sizeof(*par));

	par->max_hor_size_for_reload_start = 25;
	par->max_hor_size_char = 50;
	par->min_hor_size_char = 3;
	par->num_differential_pair = 2;
	par->pixel_distance = 3;
	par->num_byte_max = 1536 * 128 * 4;
	par->size_x = 2048;
	par->size_y = 1536;
	par->black_plates_enable = 0;

	for (i = 0; i < TNT_RLE_LUT_LEN; i++)
		par->lut[i] = tnt_rle_lut_default[i];
}

int tnt_set_rle_ptr(struct tnt_device *dev, u32 numseg_ptr, u32 rle_ptr,
		    u8 index)
{
	if (index > 1 || index >= RLE_ALGO_NUM) {
		pr_info("tnt_set_rle_ptr(): wrong index parameter, it must be less than 1");
		return -EINVAL;
	}

	if (index == 0) {
		//enable DMA WR RLE
		tnt_write(dev, EN_FLOW_DDR_RLE_A_CHA, 1);
		//PTR DMA WR RLE
		tnt_write(dev, PTR_RLE_A_IN_CHA, rle_ptr);

		//enable DMA WR NUM SEGMENT
		tnt_write(dev, EN_FLOW_DDR_NSEG_A_CHA, 1);
		//PTR DMA WR NUM SEGMENT
		tnt_write(dev, PTR_N_SEG_A_IN_CHA, numseg_ptr);
	} else {
		//enable DMA WR RLE
		tnt_write(dev, EN_FLOW_DDR_RLE_B_CHA, 1);
		//PTR DMA WR RLE
		tnt_write(dev, PTR_RLE_B_IN_CHA, rle_ptr);

		//enable DMA WR NUM SEGMENT
		tnt_write(dev, EN_FLOW_DDR_NSEG_B_CHA, 1);
		//PTR DMA WR NUM SEGMENT
		tnt_write(dev, PTR_N_SEG_B_IN_CHA, numseg_ptr);
	}

	return 0;
}

inline u32 tnt_dma_cha_words(struct tnt_device *dev, u32 dma_ch);

int tnt_set_rle_ptr_blob(struct tnt_device *dev, u32 ptr)
{
	struct tntdrv *drv = dev_get_drvdata(dev->dev);
	u32 base_algo = ptr;

	tnt_set_rle_ptr(dev, base_algo, base_algo + numseg_size[0], 0);
	base_algo = ptr + numseg_size[0] + drv->rle_conf[0].num_byte_max;
	tnt_set_rle_ptr(dev, base_algo, base_algo + numseg_size[1], 1);

	return 0;
}

void tnt_rle_enable_dma(struct tnt_device *dev)
{
	tnt_write(dev, EN_FLOW_DDR_NSEG_A_CHA, 1);
	tnt_write(dev, EN_FLOW_DDR_RLE_A_CHA, 1);
	tnt_write(dev, EN_FLOW_DDR_NSEG_B_CHA, 1);
	tnt_write(dev, EN_FLOW_DDR_RLE_B_CHA, 1);
}

int tnt_rle_init(struct tntdrv *drv)
{
	int i;

	for (i = 0; i < RLE_ALGO_NUM; i++) {
		tnt_rle_set_default(&drv->rle_conf[i]);
		tnt_set_rle_cha(&drv->cha.device, &drv->rle_conf[i], i);
	}

	drv->rle_enable = 0;
	tnt_write(&drv->cha.device, EN_ALGH_A_CHA, 0);
	tnt_write(&drv->cha.device, EN_ALGH_B_CHA, 0);

	return 0;
}

void tnt_rle_disable_dma(struct tnt_device *dev)
{
	tnt_write(dev, EN_FLOW_DDR_NSEG_A_CHA, 0);
	tnt_write(dev, EN_FLOW_DDR_RLE_A_CHA, 0);
	tnt_write(dev, EN_FLOW_DDR_NSEG_B_CHA, 0);
	tnt_write(dev, EN_FLOW_DDR_RLE_B_CHA, 0);
}

u32 tnt_rle_get_size_blob(struct tntdrv *drv)
{
	int i;
	u32 res = 0;
	for (i = 0; i < RLE_ALGO_NUM; i++)
		res += drv->rle_conf[i].num_byte_max + numseg_size[i];

	return res;
}

int tnt_rle_enable(struct tnt_device *dev)
{
	//abilita o disabilita il calcolo dell'algoritmo
	tnt_write(dev, EN_ALGH_A_CHA, 1);
	tnt_write(dev, EN_ALGH_B_CHA, 1);

	return 0;
}
