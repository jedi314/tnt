/** 
 \file tnt-slot.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
 Access from userspace to video linked lists must absolutely 
 synchronized with  BUSY_CATCH and END_FRAME interrupts to 
 avoid race conditions.
 
 Every ISR must load a time access minus the blind time.
 In this time slot usercape can safely access to list.
 If user space request is in blind time it waits until
 interrupt are handled.

*/

#ifndef _TNT_SLOT_H_
#define _TNT_SLOT_H_

#include <linux/hrtimer.h>
#include <linux/types.h>
#include <linux/wait.h>

#define READOUT_TIME 20200000 /* nS */
#define INTERRUPT_WAIT_TIME 100 /* mS */

struct tnt_slot {
	struct hrtimer t;
	u64 t_image; /**!< time between a BUSY_CATCH and END_FRAME interrupt */
	u64 t_free; /**!< time between a END_FRAME and BUSY_CATCH interrupt */
	u64 t_blind; /**!< time of blind state  */
	wait_queue_head_t
		w; /**!< wait condition for process when in blind time  */
	bool blind;
	u64 tt;
};

inline void slot_del(struct tnt_slot *slot);
inline void restart_slot(struct tnt_slot *slot, u64 expire);
inline int wait_slot(struct tnt_slot *slot);

inline void wakeup_waiting_slot(struct tnt_slot *slot);
inline int start_slot_time(struct tnt_slot *slot, u64 expire);

void slot_time(struct tnt_slot *slot, u64 period, u64 ns_expo);
void slot_init(struct tnt_slot *slot, u64 t_blind);

#endif
