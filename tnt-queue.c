/** 
 \file tnt-queue.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief 
*/

#include "tntvdo.h"
#include "tnt-allocator.h"
#include "tnt-processor.h"
#include "tnt-queue.h"
#include "tnt-fpga-utils.h"
#include "tnt-imx265.h"
#include "tnt-rle.h"

inline void tnt_spin_lock(struct tntdrv *drv);
inline void tnt_spin_unlock(struct tntdrv *drv);

inline void tnt_spin_lock(struct tntdrv *drv)
{
	spin_lock(&drv->list_lock);
	disable_irq(drv->busy_catch);
	disable_irq(drv->end_frame);
}

inline void tnt_spin_unlock(struct tntdrv *drv)
{
	enable_irq(drv->end_frame);
	enable_irq(drv->busy_catch);
	spin_unlock(&drv->list_lock);
}

/* --------------------------------------------------------------------------*/
/* Queue Stuff */

int get_context_info(struct tntdrv *drv, struct ctx_info *ctx)
{
	int n = 0;

	switch (drv->grabmode) {
	case CONTINUOUS_CHA:
	case TRIGGED_CHA:
		ctx[n].name = "ctx-cha";
		ctx[n++].vdo = &drv->cha;
		break;
	case CONTINUOUS_CHB:
	case TRIGGED_CHB:
		ctx[n].name = "ctx-chb";
		ctx[n++].vdo = &drv->chb;
		break;
	case CONTINUOUS_DOUBLE:
	case TRIGGED_DOUBLE:
		ctx[n].name = "ctx-cha";
		ctx[n++].vdo = &drv->cha;
		ctx[n].name = "ctx-chb";
		ctx[n++].vdo = &drv->chb;
		break;
	case TNT_N_MODES:
		break;
	}

	return n;
}

static void tnt_create_ctx(struct tntvdo *vdo, struct device **alloc_dev,
			   unsigned int *nbuffers, unsigned int *size)
{
	int id;
	unsigned int chunk_size;
	struct tnt_video_fmt *fmt;
	struct tntdrv *drv = vdo->driver;
	struct tnt_allocator *allocator =
		(struct tnt_allocator *)drv->allocator;

	fmt = vdo->current_fmt;
	/* TODO check fmt != NULL */

	chunk_size = fmt->sz;

	/* chunck size is PAGE_SIZE aligned by alloc_ctx */
	id = alloc_ctx(allocator, vdo->device.name, *nbuffers, &chunk_size,
		       NULL);

	*size = chunk_size;
	*alloc_dev = (struct device *)&allocator->ctxs[id];
	dev_set_drvdata(*alloc_dev, allocator);
	vdo->cntx_id = id;
	pr_info("Allocator(%s): %px, id=%d, size=%d * %d * %d/8, (format %s) --> %d bytes\n",
		vdo->device.name, *alloc_dev, id, fmt->width, fmt->height,
		fmt->bpp, fmt->name, chunk_size);
}

static void tnt_create_ctx_rle(struct tntvdo *vdo, struct device **alloc_dev,
			       unsigned int *nbuffers, unsigned int *size)
{
	int id;
	unsigned int chunk_size;
	struct tntdrv *drv = vdo->driver;
	struct tnt_allocator *allocator =
		(struct tnt_allocator *)drv->allocator;

	chunk_size = tnt_rle_get_size_blob(drv);

	pr_info("Allocator: %px, size=%d\n", allocator, chunk_size);

	/* chunck size is PAGE_SIZE aligned by alloc_ctx */
	id = alloc_ctx(allocator, "ctx-rle", *nbuffers, &chunk_size, NULL);

	*size = chunk_size;
	*alloc_dev = (struct device *)&allocator->ctxs[id];
	dev_set_drvdata(*alloc_dev, allocator);
}

static inline bool cha_active(int grabmode)
{
	int source = FG_SOURCE(grabmode);

	return source == CHA || source == DOUBLE;
}
/**
\brief Op callback function called from VIDIOC_REQBUFS
and VIDIOC_CREATE_BUFS handlers before many allocation.
It can be called twice: if the original number of requested 
buffers could not be allocated, then it will be called a 
second time with the actually allocated number of buffers
to verify if that is OK. 
The driver should return the required number of buffers 
in *num_buffers, the required number of planes per buffer 
in *num_planes, the size of each plane should be set 
in the sizes[] array and optional per-plane allocator 
specific device in the alloc_devs[] array

\param vq: pointer to queue
\param nbuffers: n. of buffer to be allocated [out]
\param nplanes: n. of planes per buffer [out]
\param sizes[]: size per plane [out]
\param *alloc_devs[]: pointer of allocator ctx per plane.
\return 0 or error code;
*/
static int tnt_queue_setup(struct vb2_queue *vq, unsigned int *nbuffers,
			   unsigned int *nplanes, unsigned int sizes[],
			   struct device *alloc_devs[])
{
	struct tntdrv *drv = (struct tntdrv *)vb2_get_drv_priv(vq);
	struct tntvdo *vdo;
	int i, np = 0;

	for (i = 0; i < DOUBLE; i++) {
		if (drv->active_vdo[i] == NULL) {
			continue;
		}
		vdo = drv->active_vdo[i];
		tnt_create_ctx(vdo, &alloc_devs[np], nbuffers, &sizes[np]);
		pr_info("Queue Setup: plane_id=%d, n. buffers=%d, size=%d\n",
			np, *nbuffers, sizes[np]);
		vdo->device.plane = np++;

		/* Fix it as processor */
		if (drv->rle_enable) {
			tnt_create_ctx_rle(vdo, &alloc_devs[np], nbuffers,
					   &sizes[np]);
			pr_info("%d Setup queue RLE, count=%d, size=%d\n", i,
				*nbuffers, sizes[i]);
			np++;
		}
		tnt_processor_create_ctx(vdo, &alloc_devs[np], nbuffers,
					 &sizes[np], &np);
	}

	*nplanes = np;
	drv->n_planes = np;

	return 0;
}

/**
\brief Called once after allocating a buffer.

\param vb: pointer to allocated buffer
\return  0 or error code.
*/
static int tnt_buf_init(struct vb2_buffer *vb)
{
	struct tnt_buf *buf = container_of(vb, struct tnt_buf, vb);
	struct list_head *entry = &buf->active_entry;

	/* initilize list pointer to point to itself */
	entry->next = LIST_POISON1;
	entry->prev = LIST_POISON2;
	INIT_LIST_HEAD(&buf->dqueued_entry);
	buf->refcount = 0;
	buf->state = VB2_BUF_STATE_PREPARED;
	return 0;
}
/**
\brief Called every time the buffer is queued from userspace 
and from the VIDIOC_PREPARE_BUF() ioctl.
Drivers may perform any initialization required 
before each hardware operation in this callback.

\param vb: pointer to allocated buffer
\return 0 or error code
*/
static int tnt_buf_prepare(struct vb2_buffer *vb)
{
	return 0;
}

/**
\brief compare buffer using its id stored in vbuf

\param buf: pointer to buffer
\param vbuf: ppointer to buffer conteining id
\return 0 if match is ok  else 1
*/
static int minor(void *el, void *new)
{
	struct tnt_buf *lbuf = container_of((struct list_head *)el,
					    struct tnt_buf, active_entry);
	struct tnt_buf *nbuf = container_of((struct list_head *)new,
					    struct tnt_buf, active_entry);

	/* pr_info("*** %d %s %d\n", nbuf->vb.index, nbuf->vb.index <= lbuf->vb.index ? "<=" : ">=", */
	/*         lbuf->vb.index); */
	return nbuf->vb.index <= lbuf->vb.index;
}

/**
\brief compare buffer using its id stored in vbuf

\param buf: pointer to buffer
\param vbuf: ppointer to buffer conteining id
\return 0 if match is ok  else 1
*/
static int less_timestamp(void *el, void *new)
{
	struct tnt_buf *lbuf;
	struct tnt_buf *nbuf;
	bool a;

	//pr_info(">>> %px [%px] %px\n", el, ((struct list_head*)el)->next, new);

	lbuf = container_of((struct list_head *)el, struct tnt_buf,
			    active_entry);
	nbuf = container_of((struct list_head *)new, struct tnt_buf,
			    active_entry);

	/*pr_info("*** %d %s %d\n", nbuf->vb.index, nbuf->vb.index <= lbuf->vb.index ? "<=" : ">=", */
	/*lbuf->vb.index); */

	a = nbuf->vb.timestamp <= lbuf->vb.timestamp;
	return a;
}

/**
\brief Passes buffer vb to the driver; 
driver may start hardware operation on this buffer; 
driver should give the buffer back by calling 
vb2_buffer_done() function; 
it is allways called after calling VIDIOC_STREAMON() ioctl; 
might be called before start_streaming callback
if user pre-queued buffers before calling VIDIOC_STREAMON().

\param vb: pointer to allocated buffer
\return
*/
static void tnt_buf_queue(struct vb2_buffer *vb)
{
	struct tntdrv *drv = vb2_get_drv_priv(vb->vb2_queue);
	struct tnt_buf *buf = container_of(vb, struct tnt_buf, vb);

	struct list_ops active_ops = {
		.match = minor,
		.convert = NULL,
		.ptr = NULL,
	};

	if (just_insert(&buf->active_entry))
		return;

	//pr_info(">>>>>>>>>>INDEX %d STATE %d\n",  vb->index, buf->state);
	switch (buf->state) {
	case VB2_BUF_STATE_DEQUEUED:
		active_ops.match = less_timestamp;
		tnt_spin_lock(drv);
		//pr_info("<<< %px %px\n", drv->active, drv->busybuf );
		insert_ordered(&drv->active_list, &buf->active_entry,
			       &active_ops);
		tnt_spin_unlock(drv);
		break;
	case VB2_BUF_STATE_PREPARED:
		insert_ordered(&drv->active_list, &buf->active_entry,
			       &active_ops);
		break;
	case VB2_BUF_STATE_REQUEUEING:
		list_add_tail(&buf->active_entry, &drv->active_list);
		break;
	}
}

static __maybe_unused int less_then(void *el, void *new)
{
	struct tnt_buf *lbuf = container_of((struct list_head *)el,
					    struct tnt_buf, dqueued_entry);
	struct tnt_buf *nbuf = container_of((struct list_head *)new,
					    struct tnt_buf, dqueued_entry);

	//pr_info("%d %s %d\n",nbuf->vb.index,nbuf->vb.index<=lbuf->vb.index?"<=":">=", lbuf->vb.index);
	return nbuf->vb.index <= lbuf->vb.index;
}

/**
\brief called before every dequeue of the buffer back to userspace; 
the buffer is synced for the CPU, so drivers can access/modify 
the buffer contents; 
drivers may perform any operations required before userspace accesses the buffer; 
optional. 
The buffer state can be one of the following: 
DONE and ERROR occur while streaming is in progress, 
and the PREPARED state occurs when the queue has been canceled and all 
pending buffers are being returned to their default DEQUEUED state. 
Typically you only have to do something if the state is VB2_BUF_STATE_DONE, 
since in all other cases the buffer contents will be ignored anyway.


In this callback op function the frame is also 
queued in dqueued list.

\param vb: pointer to buffer
\return 
*/
void tnt_buf_finish(struct vb2_buffer *vb)
{
	struct tntdrv *drv = vb2_get_drv_priv(vb->vb2_queue);
	struct tnt_buf *buf = container_of(vb, struct tnt_buf, vb);

	list_add_tail(&buf->dqueued_entry, &drv->dqueued_list);
	buf->refcount++;
}

void tnt_prepare_grab(struct tntdrv *drv);

/**
\brief Called once to enter ‘streaming’ state; 
the driver may receive buffers with buf_queue callback 
before start_streaming is called; 
the driver gets the number of already queued buffers 
in count parameter; 
driver can return an error if hardware fails, in that 
case all buffers that have been already given by the 
buf_queue callback are to be returned by the driver 
by calling vb2_buffer_done() with VB2_BUF_STATE_QUEUED. 

If you need a minimum number of buffers before you 
can start streaming, then set min_buffers_needed in 
the vb2_queue structure. 

If that is non-zero then start_streaming won’t be called 
until at least that many buffers have been queued 
up by userspace.

\param vq: pointer to vb queue
\param count: queued buffers.
\return 0 or error
*/

enum trigger_swhw {
	SW = 0,
	HW = 1,
};

static inline void trigger_swhw(struct tntdrv *drv, enum trigger_swhw trigger)
{
	struct tnt_device *active_dev;
	int i;

	for (i = 0; i < DOUBLE; i++) {
		active_dev = &drv->active_vdo[i]->device;
		if (active_dev == NULL)
			continue;
		tnt_fpga_hwsw(active_dev);
	}
}

void tnt_prepare_grab(struct tntdrv *drv);
void arm_fsm(struct tntdrv *drv);
int catch_frame_fsm(struct tntdrv *drv);

static int tnt_start_streaming(struct vb2_queue *vq, unsigned int count)
{
	struct tntdrv *drv = container_of(vq, struct tntdrv, queue);
	struct tntvdo *vdo;
	struct tnt_device *dev = &drv->device;
	int gmode = FG_GMODE(drv->grabmode);
	int i;

	switch (gmode) {
	case TRIGGED:
		tnt_fpga_shutdown_continuous(dev);
		for (i = 0; i < DOUBLE; i++) {
			vdo = drv->active_vdo[i];
			if (vdo == NULL)
				continue;
			tnt_fpga_clear_single_frame(&vdo->device);
			tnt_fpga_enable_trigged_interrupt(&vdo->device);
			tnt_fpga_enable_trigged_interrupt(&vdo->device);
		}
		break;
	case CONTINUOUS:
		for (i = 0; i < DOUBLE; i++) {
			vdo = drv->active_vdo[i];
			if (vdo == NULL)
				continue;
			tnt_fpga_disable_trigged_interrupt(&vdo->device);
		}
		trigger_swhw(drv, SW);
		tnt_prepare_grab(drv);
		tnt_fpga_set_continuous_gmode(dev);
		tnt_fpga_start_fsm(dev);
		tnt_fpga_reset_interrupts(dev);
		tnt_fpga_enable_interrupts(dev);
		arm_fsm(drv);
		tnt_fpga_catch(dev, catch_frame_fsm(drv));
		break;
	}
	return 0;
}

void remove_dqueued_list(struct tntdrv *drv)
{
	struct tnt_buf *buf, *node;

	list_for_each_entry_safe (buf, node, &drv->dqueued_list,
				  dqueued_entry) {
		list_del_init(&buf->dqueued_entry);
	}
	INIT_LIST_HEAD(&drv->dqueued_list);
}

void tnt_deactivate_all_buffers(struct tntdrv *drv)
{
	int i;
	struct vb2_queue *q = &drv->queue;
	struct tntvdo *vdo;

	tnt_fpga_disable_interrupts(&drv->device);
	for (i = 0; i < DOUBLE; i++) {
		vdo = drv->active_vdo[i];
		if (vdo == NULL)
			continue;
		//pr_info("deactivate all buffers\n");
		tnt_fpga_disable_trigged_interrupt(&vdo->device);
	}
	for (i = 0; i < drv->queue.num_buffers; ++i) {
		if (q->bufs[i]->state == VB2_BUF_STATE_ACTIVE) {
			pr_warn("stop_streaming; change  buf %d (0x%px) to DONE state\n",
				q->bufs[i]->index, q->bufs[i]);
			tnt_vb2_buffer_done(q->bufs[i], VB2_BUF_STATE_ERROR);
		}
	}

	atomic_set(&drv->queue.owned_by_drv_count, 0);
	INIT_LIST_HEAD(&drv->active_list);
}

inline void tnt_remove_active_frame(struct tntdrv *drv)
{
	drv->active = NULL;
}

inline void tnt_remove_busy_frame(struct tntdrv *drv)
{
	drv->busybuf = NULL;
}

static void tnt_stop_streaming(struct vb2_queue *vq)
{
	struct tntdrv *drv = container_of(vq, struct tntdrv, queue);
	struct tnt_device *dev = &drv->device;
	int gmode = FG_GMODE(drv->grabmode);

	switch (gmode) {
	case CONTINUOUS:
		tnt_fpga_stop_fsm(dev);
		tnt_remove_active_frame(drv);
		tnt_remove_busy_frame(drv);
		break;
	}
}

static const struct vb2_ops tnt_buf_ops = {
	.queue_setup = tnt_queue_setup,
	.buf_init = tnt_buf_init,
	.buf_prepare = tnt_buf_prepare,
	.buf_queue = tnt_buf_queue,
	.buf_finish = tnt_buf_finish,
	.start_streaming = tnt_start_streaming,
	.stop_streaming = tnt_stop_streaming,
	.wait_prepare = vb2_ops_wait_prepare,
	.wait_finish = vb2_ops_wait_finish,
};

/**
\brief Allocate video memory private data, 
The returned private structure will then be passed as buf_priv 
argument to other ops in this structure. 
Additional gfp_flags to use when allocating 
the are also passed to this operation. 
These flags are from the gfp_flags field of vb2_queue.

\param dev: pointer
\param attrs: attribute, in this case is ctx id.
\param size: size of buffer
\param dma_dir: 
\param gfp_flags:
\return ERR_PTR() on failure or a pointer to allocator private, 
per-buffer data on success.
*/
static void *alc_alloc(struct device *dev, unsigned long attrs,
		       unsigned long size, enum dma_data_direction dma_dir,
		       gfp_t gfp_flags)
{
	struct tnt_allocator *al = (struct tnt_allocator *)dev_get_drvdata(dev);
	struct alc_chunk *chunk;
	struct alc_ctx *ctx = (struct alc_ctx *)dev;
	int id = ctx->id;

	if (WARN_ON(!dev)) {
		return ERR_PTR(-EINVAL);
	}

	chunk = alc_alloc_chunk(al, id);
	if (!chunk)
		return ERR_PTR(-ENOMEM);

	refcount_set(&chunk->refcount, 1);

	return chunk;
}

/**
\brief Inform the allocator 
that the buffer will no longer be used; 
usually will result in the allocator 
freeing the buffer (if no other users of this buffer are present); 
the buf_priv argument is the allocator private per-buffer 
structure previously returned from the alloc callback.

\param buf_priv: allocate buffer struct
\return 
*/
static void alc_put(void *buf_priv)
{
	struct alc_chunk *chunk = buf_priv;

	if (!refcount_dec_and_test(&chunk->refcount))
		return;

	kfree(chunk);
}

/**
\brief Setup a userspace mapping for a given memory 
buffer under the provided virtual memory region.

\param buf_priv: pointer to chunk struct(i.e. private area)
\param vma: VM area struct
\return  0 or ERROR_VALUE
*/
static int alc_mmap(void *buf_priv, struct vm_area_struct *vma)
{
	struct alc_chunk *chunk = buf_priv;
	int ret;
	int size;

	if (!chunk) {
		printk(KERN_ERR "No buffer to map\n");
		return -EINVAL;
	}
	size = vma->vm_end - vma->vm_start;
	vma->vm_flags |= VM_DONTEXPAND | VM_DONTDUMP;
	/* restore phy addr from offset */
	vma->vm_pgoff = chunk->addr >> PAGE_SHIFT;
	ret = remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size,
			      vma->vm_page_prot);
	if (ret) {
		pr_info("remap_pfn_range error (%d)\n", ret);
		return -EINVAL;
	}

	/* TODO MMAP ops if nneded */
	/* vma->vm_private_data = &buf->handler; */
	/* vma->vm_ops	= &vb2_common_vm_ops; */
	/* vma->vm_ops->open(vma); */

	return 0;
}

static void *alc_cookie(void *buf_priv)
{
	struct alc_chunk *chunk = buf_priv;
	return &chunk->addr;
}

static void *alc_vaddr(void *buf_priv)
{
	struct alc_chunk *chunk = buf_priv;
	return chunk->vaddr;
}

static void alc_prepare(void *buf_priv)
{
}

static void alc_finish(void *buf_priv)
{
}

static unsigned int alc_num_users(void *buf_priv)
{
	struct alc_chunk *chunk = buf_priv;
	return refcount_read(&chunk->refcount);
}

static const struct vb2_mem_ops tnt_memops = {
	.alloc = alc_alloc,
	.put = alc_put,
	.cookie = alc_cookie,
	.vaddr = alc_vaddr,
	.mmap = alc_mmap,
	.prepare = alc_prepare,
	.finish = alc_finish,
	.num_users = alc_num_users,
};

int tnt_init_queue(struct tntdrv *drv, int n_min_bufs)
{
	struct vb2_queue *q = &drv->queue;
	int ret;

	/* Buffer queue */
	q->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	q->io_modes = VB2_MMAP | VB2_READ;
	q->lock = &drv->cha.lock;
	q->drv_priv = drv;
	q->buf_struct_size = sizeof(struct tnt_buf);
	q->ops = &tnt_buf_ops;
	q->mem_ops = &tnt_memops;
	q->timestamp_flags = V4L2_BUF_FLAG_TIMESTAMP_MONOTONIC;
	q->min_buffers_needed = n_min_bufs; /* NOTE: checks this value  */
	q->dev = drv->device.dev;
	ret = tnt_vb2_queue_init(q);
	if (ret < 0) {
		return ret;
	}

	drv->queue_refs = 0;
	drv->last_uid = 0;
	drv->igid = 0;
	return 0;
}
