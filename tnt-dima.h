/** 
 \file tnt-dima.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2021  s.r.l.
 

 \brief
*/

#ifndef _DIMA_H_
#define _DIMA_H_

#define ENABLE_GPIO_DIMA 0x000201D8 /* REG_WR */
#define SENS_FOR_DIMA_IS_READY 0x00010074 /* REG_RD */
#define FSM_GPIO_DIMA_IS_ENABLE 0x00010078 /* REG_RD */
#define DEBUG_FSM_GPIO_DIMA 0x0001007C /* REG_RD */
#define AUX_GPIO_VALUE 0x00000038 /* REG_RD */

struct dima {
	union {
		struct {
			u32 input : 1;
			u32 en_power : 1;
			u32 buffer : 1;
		};
		u32 v;
	};
};

inline void imx_enable_dima(struct tnt_device *dev);
inline void imx_disable_dima(struct tnt_device *dev);
inline bool imx_dima_is_enable(struct tnt_device *dev);
inline bool imx_dima_sensor_is_ready(struct tnt_device *dev);
inline int imx_dima_input(struct tnt_device *dev);
int ioctl_dima(struct file *filp, unsigned int cmd, unsigned long arg);

#endif
