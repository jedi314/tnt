/** 
 \file tnt-imxops.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
 Generic configuration api for SONY IMX Sensors.
 All call pass throe fpga registers
*/

#ifndef _TNT_IMXOPX_H
#define _TNT_IMXOPX_H
#include <linux/types.h>

#include "spi-sensor.h"

#define IMX_DELAY 10 /* Actually delay not needed */
#define EXTRA_PIXEL_Y 40

struct roi_area {
	u32 h_org; /**!< horizontal cropping position */
	u32 v_org; /**!< vertical cropping position */
	u32 h_size; /**!< horizontal cropping size */
	u32 v_size; /**!< vertical cropping size */
	u32 width;
	u32 height;
};

inline void disable_lvds_clk(struct tnt_device *dev);
inline void enable_lvds_clk(struct tnt_device *dev);
inline void imx_reset(struct tnt_device *dev);
inline void imx_start(struct tnt_device *dev);
inline void enable_hs(struct tnt_device *dev);
inline void disable_hs(struct tnt_device *dev);
inline void trigger_mode(struct tnt_device *dev);
inline void normal_mode(struct tnt_device *dev);
inline void imx_pll_reset(struct tnt_device *dev);
inline void imx_pll_start(struct tnt_device *dev);
bool pll_locked(struct tnt_device *dev);
inline int imx_lock_pll(struct tnt_device *dev);
int imx_conf(struct tnt_device *dev, struct spi_data *conf);
int imx_check_conf(struct tnt_device *dev, struct spi_data *conf);
int imx_sensor_configure(struct tnt_device *dev, struct spi_data *sensor_init);

inline void imx_align_image(struct tnt_device *dev, int n);
bool align_done(struct tnt_device *dev);
int imx_lvds_align(struct tnt_device *dev);
inline u32 gain_level(u32 _gain);
void imx_set_roi_area(struct tnt_device *dev, struct roi_area *roi);
inline void imx_disable_roi(struct tnt_device *dev, int width, int height);
void imx_set_roi(struct tnt_device *dev, struct spi_data *sensor_roi, int width,
		 int height);

int imx_pixel_frame(struct tnt_device *dev, struct tnt_video_fmt *fmt);
int imxyuv_pixel_frame(struct tnt_device *dev, struct tnt_video_fmt *fmt);
int imxbw_enable_dma(struct tnt_device *dev);
int imxbw_disable_dma(struct tnt_device *dev);
int imxyuv_enable_dma(struct tnt_device *dev);
int imxyuv_disable_dma(struct tnt_device *dev);
bool imxbw_image_ready(struct tnt_device *dev);
u32 imxbw_expo_us_to_value(struct tnt_device *dev, u32 us);
u32 imxyuv_expo_us_to_value(struct tnt_device *dev, u32 us);
u32 imxyuv_set_gain(struct tnt_device *dev, u32 value);
u32 imxbw_set_gain(struct tnt_device *dev, u32 value);
u32 imxbw_set_test_mode(struct tnt_device *dev, u32 value);
u32 imxbw_get_model(struct tnt_device *dev, u32 *value);

#endif
