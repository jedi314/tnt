/** 
 \file tnt-regs.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Low level accces to fpga registers from debugfs
*/

#include <linux/debugfs.h>
#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <asm/ioctl.h>
#include "debugfs/trace.h"
#include "tnt-cmd.h"
#include "tnt-regs.h"

/**
\brief open of fpga/reg file.
inode->i_private field contains 
struct tnt_reg.

\param inode:
\param file:
\return 
*/
static int regif_reg_open(struct inode *inode, struct file *file)
{
	file->private_data = inode->i_private;
	return 0;
}

static inline int reg_inside_range(void *base, u64 reg, u64 size)
{
	pr_info(" 0x%08llx < 0x%08llx < 0x%08llx [ %s  ]\n", (u64)base, reg,
		(u64)base + size,
		reg >= (u64)base && reg < (u64)(base) + size ? "OK" : "KO");

	return reg >= (u64)base && reg < (u64)(base) + size;
}

#define MAP_ADDR(base, mask, off) ((base) + ((off)&mask))

static ssize_t regif_reg_store(struct file *file, const char __user *buf,
			       size_t count, loff_t *f_pos)
{
	unsigned long long reg = 0;
	int ret;
	struct tnt_reg *treg;

	treg = (struct tnt_reg *)file->private_data;
	ret = kstrtoull_from_user(buf, count, 0, &reg);
	if (ret) {
		/* Negative error code. */
		pr_info("ko = %d\n", ret);
		return ret;
	}

	/* creg = (u64)MAP_ADDR(treg->base, treg->mask, reg); */
	/* if (!reg_inside_range(treg->base, creg, resource_size(treg->range)) ) */
	/*     return -EFAULT; */

	treg->reg = reg;
	return count;
}

static ssize_t regif_reg_read(struct file *file, char __user *buf, size_t count,
			      loff_t *f_pos)
{
	struct tnt_reg *treg = (struct tnt_reg *)file->private_data;
	int n;

	if (*f_pos != 0)
		return 0;

	n = sprintf(buf, "0x%08x", treg->reg);
	*f_pos += n;
	return n;
}

static int regif_val_open(struct inode *inode, struct file *file)
{
	file->private_data = inode->i_private;
	return 0;
}

static ssize_t regif_val_write(struct file *file, const char __user *buf,
			       size_t count, loff_t *f_pos)
{
	unsigned long long val;
	int ret;
	struct tnt_reg *treg = (struct tnt_reg *)file->private_data;

	/* 0= autodetect value based on semantics 0xNN is hex 0NN is octagonal*/
	ret = kstrtoull_from_user(buf, count, 0, &val);
	pr_info("Write: %lld to 0x%px\n", val,
		MAP_ADDR(treg->base, treg->mask, treg->reg));
	if (ret) {
		/* Negative error code. */
		pr_info("ko = %d\n", ret);
		return ret;
	}

	/* redondant check */
	//if (!reg_inside_range(treg->range, treg->reg))
	//    return -ENOMEM;

	writel(val, MAP_ADDR(treg->base, treg->mask, treg->reg));
	return count;
}

static ssize_t regif_val_read(struct file *file, char __user *buf, size_t count,
			      loff_t *f_pos)
{
	int val;
	int ret, size;
	char n[8];
	struct tnt_reg *treg = (struct tnt_reg *)file->private_data;

	if (*f_pos != 0) {
		return 0;
	}

	val = readl(MAP_ADDR(treg->base, treg->mask, treg->reg));
	size = sprintf(n, "%u", val);
	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static int regif_mask_open(struct inode *inode, struct file *file)
{
	file->private_data = inode->i_private;
	return 0;
}

static ssize_t regif_mask_write(struct file *file, const char __user *buf,
				size_t count, loff_t *f_pos)
{
	unsigned long long val;
	int ret;
	struct tnt_reg *treg = (struct tnt_reg *)file->private_data;

	/* 0= autodetect value based on semantics 0xNN is hex 0NN is octagonal*/
	ret = kstrtoull_from_user(buf, count, 0, &val);
	//pr_info("Write: %lld to 0x%px\n", val, MAP_ADDR(treg->base, treg->mask, treg->reg));
	if (ret) {
		/* Negative error code. */
		pr_info("ko = %d\n", ret);
		return ret;
	}

	treg->mask = val;
	return count;
}

static ssize_t regif_mask_read(struct file *file, char __user *buf,
			       size_t count, loff_t *f_pos)
{
	int ret, size;
	char n[16];
	struct tnt_reg *treg = (struct tnt_reg *)file->private_data;

	if (*f_pos != 0) {
		return 0;
	}

	size = sprintf(n, "0x%08x", treg->mask);
	ret = copy_to_user(buf, n, size);
	if (ret != 0)
		return -EFAULT;

	*f_pos += size;
	return size;
}

static struct file_operations regif_reg_ops = {
	.open = regif_reg_open,
	.write = regif_reg_store,
	.read = regif_reg_read,
};

static struct file_operations regif_val_ops = {
	.open = regif_val_open,
	.read = regif_val_read,
	.write = regif_val_write,
};

static struct file_operations regif_mask_ops = {
	.open = regif_mask_open,
	.read = regif_mask_read,
	.write = regif_mask_write,
};

static struct tdm_watch regif_watch[] = {
	{ "reg", 0600, TDM_REGIF, &regif_reg_ops },
	{ "value", 0600, TDM_REGIF, &regif_val_ops },
	{ "mask", 0600, TDM_REGIF, &regif_mask_ops },
	{}
};

static void release_regif_params(struct tdm_watch *w)
{
	int i;
	for (i = 0; w[i].filename; i++)
		w[i].opt.param = NULL;
}

static struct tnt_reg *regif_initialize(struct tnt_reg *reg, char *name,
					struct resource *range, void *base)
{
	int ret = 0;

	if (reg == NULL)
		reg = kzalloc(sizeof(*reg), GFP_KERNEL);

	if (reg == NULL) {
		pr_info("Error allocating tnt_reg\n");
		return NULL;
	}

	reg->dbg.dirname = name;
	ret = tdm_init(&reg->dbg);
	if (ret) {
		pr_info("Error Init tdm (err. %d)\n", ret);
		return NULL;
	}

	reg->base = base;
	reg->range = range;
	reg->reg = 0;
	reg->mask = 0x00ffffff;
	return reg;
}

struct tnt_reg *regif_init(struct tnt_reg *dreg, char *name,
			   struct resource *range, void *base)
{
	int ret;

	dreg = regif_initialize(dreg, name, range, base);
	if (dreg == NULL)
		return dreg;

	/* default ops. No need error messages. keep good watch  */
	ret = tdm_append_watches(&dreg->dbg, &regif_watch[0], dreg);

	release_regif_params(&regif_watch[0]);

	return dreg;
}

void regif_free(struct tnt_reg *dreg)
{
	//pr_info("Removed Recursive direcotory %s\n", dreg->dbg.dirname);
	debugfs_remove_recursive(dreg->dbg.dir);
}
