/** 
 \file tntof.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief  Video OF reader
 1. Load from kernel.
 2. Read DT fmt config
 3. Register v4l2 device /dev/video0


 TODO: struct containig wht read ant the type 
 example
   
 struct {
        int type;
        void *dest;
        int *read(struct devic_node*, void* dest);
 };


 dt_data[] = {
          { UNSIGNED_INT, &width, of_property_read_u32 },
          {},
 }


*/
#include <linux/types.h>

#include <linux/module.h>
#include <linux/device.h>

#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

#include "tnt-video-formats.h"
#include "tntvdo.h"

static int tnt_get_bilinear_resizer(struct device_node *resizer,
				    struct tntdrv *drv, void **data);
static int tnt_get_resizer(struct device_node *resizer, struct tntdrv *drv,
			   void **data);
static int tnt_get_remapper(struct device_node *remapper, struct tntdrv *drv,
			    void **data);

static const struct tnt_pixel_fmt pixel_formats[] = {
	{
		.name = "GRAY8",
		.fourcc = V4L2_PIX_FMT_GREY,
		.type = GRAY8,
	},
	{
		.name = "BAYER",
		.fourcc = 0,
		.type = BAYER,
	},
	{
		.name = "nv12",
		.fourcc = V4L2_PIX_FMT_NV12M,
		.type = NV12,
	},
	{
		.name = "YCbCr420",
		.fourcc = V4L2_PIX_FMT_YUV420,
		.type = YCBCR420,
	},
	{
		.name = "rgb888",
		.fourcc = V4L2_PIX_FMT_RGB24,
		.type = RGB888,
	},
	{}
};

struct processor_node {
	char *name;
	int (*get_property)(struct device_node *, struct tntdrv *, void **);
	const struct tnt_processor_ops *ops;
	const struct v4l2_subdev_ops *sdev_ops;
	const struct v4l2_subdev_internal_ops *internal_ops;
};

extern const struct tnt_processor_ops bilinear_resizer_ops;
extern const struct tnt_processor_ops remapper_ops;
extern const struct tnt_processor_ops resizer_ops;

extern const struct v4l2_subdev_internal_ops processor_internal_ops;

extern const struct v4l2_subdev_ops bilinear_resizer_sdev_ops;
extern const struct v4l2_subdev_ops resizer_sdev_ops;
extern const struct v4l2_subdev_ops remapper_sdev_ops;

static struct processor_node processor_list[] = {
	{ .name = "resizer",
	  .get_property = tnt_get_resizer,
	  .ops = &resizer_ops,
	  .sdev_ops = &resizer_sdev_ops,
	  .internal_ops = &processor_internal_ops },
	{ .name = "remapper",
	  .get_property = tnt_get_remapper,
	  .ops = &remapper_ops,
	  .sdev_ops = &remapper_sdev_ops,
	  .internal_ops = &processor_internal_ops },
	{ .name = "bilinear-resizer",
	  .get_property = tnt_get_bilinear_resizer,
	  .ops = &bilinear_resizer_ops,
	  .sdev_ops = &bilinear_resizer_sdev_ops,
	  .internal_ops = &processor_internal_ops },
	{}
};

static int tnt_get_pixel_format(char *format_name, u32 *fourcc)
{
	const struct tnt_pixel_fmt *pixel_fmt;

	for (pixel_fmt = &pixel_formats[0]; pixel_fmt->name; pixel_fmt++) {
		if (strcasecmp(pixel_fmt->name, format_name) == 0) {
			*fourcc = pixel_fmt->fourcc;
			return pixel_fmt->type;
		}
	}
	return -EINVAL;
}

static inline void compute_format_sz(struct tnt_video_fmt *f)
{
	f->sz = f->width * f->height * f->bpp / 8;
}

int tnt_get_available_sensors(struct tntdrv *drv)
{
	struct device_node *sensors;
	struct device_node *sensor;
	struct tnt_sensor_desc *sensor_desc = &drv->sensors[0];
	int i = 0;
	//int cnt = 0;
	int format, fourcc;

	sensors = of_find_node_by_path("/tnt/avail-sensors");
	if (!sensors)
		return -EINVAL;

	/* of_property_read_u32(sensors, "tnt,num_sensors",  &cnt); */
	/* if (cnt > N_SENSORS) { */
	/* 	pr_err("/tnt/avail-sensor: too many sensors %d/%d!\n", cnt, N_SENSORS); */
	/* 	return -EINVAL ; */
	/* } */

	for_each_child_of_node (sensors, sensor) {
		of_property_read_string(sensor, "tnt,name",
					&sensor_desc[i].name);
		of_property_read_u32(sensor, "tnt,code", &sensor_desc[i].code);
		of_property_read_string(
			sensor, "tnt,format",
			(const char **)&sensor_desc[i].fmt.name);
		format = tnt_get_pixel_format(sensor_desc[i].fmt.name, &fourcc);
		if (format < 0) {
			return -EINVAL;
		}
		drv->sensors[i].fmt.type = format;
		drv->sensors[i].fmt.fourcc = fourcc;

		of_property_read_u32(sensor, "tnt,width",
				     &sensor_desc[i].fmt.width);
		of_property_read_u32(sensor, "tnt,height",
				     &sensor_desc[i].fmt.height);
		of_property_read_u32(sensor, "tnt,bpp",
				     &sensor_desc[i].fmt.bpp);
		compute_format_sz(&sensor_desc[i].fmt);
		i++;
	}
	drv->n_sensors = i;

	return 0;
}

static inline bool is_disabled(struct device_node *node)
{
	const char *enabled;

	of_property_read_string(node, "status", &enabled);
	if (strcasecmp(enabled, "ok") != 0)
		return true;

	return false;
}

static inline int get_dimension(struct device_node *node,
				struct tnt_resize *dim, const char *w,
				const char *h)
{
	int ret = 0;

	ret = of_property_read_u32(node, w, &dim->w);
	if (ret)
		return ret;

	ret = of_property_read_u32(node, h, &dim->h);
	if (ret)
		return ret;

	return ret;
}

static int tnt_get_bilinear_resizer(struct device_node *resizer,
				    struct tntdrv *drv, void **data)
{
	if (!resizer || is_disabled(resizer))
		return -EINVAL;
	*data = NULL;
	return 0;
}

static int tnt_get_resizer(struct device_node *resizer, struct tntdrv *drv,
			   void **data)
{
	struct tnt_resize *rsz = &drv->resizer;
	int ret = 0;

	if (!resizer)
		return -EINVAL;

	if (is_disabled(resizer))
		return -EINVAL;

	ret = get_dimension(resizer, rsz, "width", "height");
	if (ret)
		return ret;
	*data = rsz;

	return 0;
}

static int tnt_get_remapper(struct device_node *remapper, struct tntdrv *drv,
			    void **data)
{
	int ret = 0;
	struct tnt_remap *remap = &drv->remapper;

	if (!remapper)
		return -EINVAL;

	if (is_disabled(remapper))
		return -EINVAL;

	ret = get_dimension(remapper, &remap->in, "in-width", "in-height");
	if (ret)
		return ret;

	ret = get_dimension(remapper, &remap->out, "out-width", "out-height");
	if (ret)
		return ret;

	ret = of_property_read_u32(remapper, "max-displace",
				   &remap->max_displace);
	if (ret)
		return ret;

	ret = of_property_read_u32(remapper, "lut-bpp", &remap->lut.bpp);
	if (ret)
		return ret;

	ret = of_property_read_u32(remapper, "max-delta", &remap->delta_max);
	*data = remap;
	return ret;
}

static struct processor_node *find_processor_by_name(const char *name)
{
	int i;

	for (i = 0; processor_list[i].name; i++) {
		if (strcasecmp(processor_list[i].name, name) == 0)
			return &processor_list[i];
	}
	return NULL;
}

static struct processor_list *get_processor(struct tntvdo *vdo,
					    const char *name)
{
	int i, n;
	struct processor_list *processor_list = &vdo->driver->processors[0];

	n = vdo->driver->n_processor;

	for (i = 0; i < n; i++) {
		if (strcasecmp(processor_list[i].name, name) == 0)
			return &processor_list[i];
	}
	return NULL;
}

int tnt_get_available_processor(struct tntdrv *drv)
{
	struct device_node *avail_processor, *processor;
	const char *name;
	struct processor_node *pnode;
	int i = 0, ret = 0;
	void *data;

	avail_processor = of_find_node_by_path("/tnt/avail-processor");
	for_each_child_of_node (avail_processor, processor) {
		ret = of_property_read_string(processor, "name", &name);
		if (ret) {
			pr_warn("Skipped processor node %s without name property!\n",
				processor->name);
			continue;
		}
		pnode = find_processor_by_name(name);
		if (pnode == NULL) {
			pr_warn("Skipped processor node %s not found in tnt!\n",
				name);
			continue;
		}
		ret = pnode->get_property(processor, drv, &data);
		drv->processors[i].name = name;
		drv->processors[i++].data = data;
	}
	drv->n_processor = i;
	return 0;
}

inline void print_video_fmt(struct tnt_video_fmt *fmt)
{
	pr_info("\t\tname:%s\n"
		"\t\ttype:%d\n"
		"\t\twidth: %d\n"
		"\t\theight:%d\n"
		"\t\tbbp:%d\n"
		"\t\tsize:%d\n",
		fmt->name, fmt->type, fmt->width, fmt->height, fmt->bpp,
		fmt->sz);
}

inline void print_sensor_info(struct tnt_sensor_desc *sensor, int i)
{
	pr_info("sensor@%d:\n\t\tname: %s\n\t\tcode: %d\n", i, sensor->name,
		sensor->code);
}

void print_processor_info(struct tntdrv *drv)
{
	int i;
	struct processor_list *p;

	for (i = 0; i < drv->n_processor; i++) {
		p = &drv->processors[i];
		pr_info("Prcesss# %d: %s (0x%px)\n", i, p->name, p);
	}
}

void tnt_print_available_sensors(struct tntdrv *drv)
{
	int i;
	struct tnt_sensor_desc *sensor;

	for (i = 0; i < drv->n_sensors; i++) {
		sensor = &drv->sensors[i];
		print_sensor_info(sensor, i);
		print_video_fmt(&drv->sensors[i].fmt);
	}
}

static int allow_sensors(struct tntvdo *vdo, struct device_node *vdo_ch)
{
	int n_allowed;
	u8 *allowed_sensors;
	int ret = 0;

	n_allowed = of_property_count_u8_elems(vdo_ch, "allowed-sensors");

	if (ret) {
		pr_info("Key 'allowed-sensor' is missing in DT!\n");
		goto quit;
	}

	allowed_sensors = kzalloc(sizeof(u8) * (n_allowed + 1), GFP_KERNEL);
	ret = of_property_read_u8_array(vdo_ch, "allowed-sensors",
					allowed_sensors, n_allowed);
	allowed_sensors[n_allowed] = -1;

	if (ret) {
		pr_info("Error reading 'allowed-sensors' property(%d)\n", ret);
		goto allow_error;
	}

	vdo->allowed_sensors = allowed_sensors;
	return 0;

allow_error:
	kfree(allowed_sensors);

quit:
	return ret;
}

static inline void install_processor(struct tntvdo *vdo,
				     struct processor_node *pnode, void *data)
{
	struct tnt_processor *processor =
		&vdo->processor_list[vdo->n_processor];

	pr_info("VDO# %d: install processor <%s>(%px)\n", vdo->id, pnode->name,
		data);
	processor->name = pnode->name;
	processor->ops = pnode->ops;
	processor->sdev_ops = pnode->sdev_ops;
	processor->internal_ops = pnode->internal_ops;
	vdo->n_processor++;
	processor->vdo = vdo;
	processor->info = data;
	processor->cntx_id = -1;
}

static int active_processor(struct tntvdo *vdo, struct device_node *vdo_ch)
{
	int i, n;
	const char *proc_name;
	struct processor_node *pnode;
	struct processor_list *plist;

	vdo->n_processor = 0;
	n = of_property_count_strings(vdo_ch, "processing");
	if (n == -EINVAL) {
		pr_warn("VDO# %d: No processors found!", vdo->id);
		return 0;
	}
	vdo->processor_list =
		kzalloc(n * sizeof(struct tnt_processor), GFP_KERNEL);
	for (i = 0; i < n; i++) {
		of_property_read_string_index(vdo_ch, "processing", i,
					      &proc_name);
		pnode = find_processor_by_name(proc_name);
		plist = get_processor(vdo, proc_name);
		if (pnode == NULL || plist == NULL) {
			pr_err("VDO# %d: Active processor %s not found!\n",
			       vdo->id, proc_name);
			return -EINVAL;
		}
		install_processor(vdo, pnode, plist->data);
	}

	return 0;
}

/**
\brief parse Device Tree to get all information 
about the driver, specially video format device
configuration an set the current format read 
in dt.

 \param vdo: tntvdo driver structure, 
\return 
*/
int tntof_hook_vdo(struct tntdrv *drv, struct tntvdo *vdo, char *path)
{
	struct device_node *vdo_ch;
	int ret = 0;
	const char *name;

	vdo->driver = drv;
	vdo->device.dev = drv->device.dev;

	vdo_ch = of_find_node_by_path(path);
	if (!vdo_ch)
		return -EINVAL;

	if (is_disabled(vdo_ch)) {
		pr_info("Channel %s disabled!", path);
		return 1;
	}

	ret = allow_sensors(vdo, vdo_ch);
	if (ret)
		return ret;

	ret = of_property_read_string(vdo_ch, "channel-name", &name);
	if (ret == 0) {
		vdo->name = (char *)name;
	}

	ret = active_processor(vdo, vdo_ch);

	return ret;
}
