# TNT a video framegrabber v4l2 driver

## What is TNT and what is not
TNT is a video frame grabber driver developed over video4linux and
videobuf2 frameworks.
It's purpose is to control FPGA system accessing to its registers
from one side and exports image buffers to user space.


```

   _________   __________    _______   __________
|\|        |__|          |__|       |_|          |
|   camera |__|   FPGA   |__|  TNT  |_|  ioctl() |
|/|________|  |__________|  |_______| |__________|
```

It is not a general video frame grabber module but it is tailored
on a specific FPGA and CPU (ARM Cortex-A53) Zynq UltraScale+ MPSoC.
It overall a good starting point for Video Frame Grabber driver over
an FPGA device.


## How TNT works
TNT onws a double linked list of frames. The linked list contains 
most recent frames grabbed in chronological order ( i.e. the first is
the oldest). 
User space applications can access to frames of this list with
following search criteria:
  * Index Frame
  * Timestamp range
  * Unique Id

### Frame Management
Fpga periodically fire an interrupt and notify a frame is ready copied
to memory through DMA and requests new fresh memory area for the next.
Interrupt service routine takes the older free frame buffer available,
the firt on list, detaches it and gives the ownership to FPGA.
That's a synchronized access to frames.
On userland one or more applications can request through VIDIOC_DQBUF
frames based on timestamp criteria.
All magic is a race condition challenge between a synchronized (FPGA)
and asynchronized access.

### Frame tracking
A debugfs layer is include in the module, then it is possible see 
all queue list of frames directly cat-ing a file or set parameters 
write on it.


## Build TNT
TNT is tailored over a FPGA then it can compile but it works only 
if a FPGA and a device-tree configuration are set.
To compile module out-of-tree  set the right path in  ``KERNELDIR``
variabile redirecting to kernel source tree in Makefile file.
It is possible change the path to cross compiler located in
``CROSS_COMPILE``  variable and the architecture  name in ``ARCH``.
For debug purpose it is possible directly install on remote machine.

```
make 
make install
```
