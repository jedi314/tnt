/** 
 \file tnt-access.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Buffer access from user space.
 A set of fucntion used to retrieve a specific buffer 
 matching a  rule.
 A buffer is searched depending to working mode.
 
*/

#include "list.h"
#include "tnt-dqueue.h"
#include "tnt-access.h"

#define call_grab_op(gops, op, args...)                                        \
	((drv->grab_ops)->op ? drv->grab_ops->op(args) : 0)

/**
\brief Convertion function from list element to 
its struct tnt_buf container.

\param el: pointer to struct list_head*
\param c: void pointer to a element used by matching.
\return converted buffer.
*/
static void *convert_tntbuf_dq(struct list_head *el, void *c)
{
	struct tnt_buf *buf;

	buf = container_of(el, struct tnt_buf, dqueued_entry);
	return buf;
}

void *convert_tntbuf(struct list_head *el, void *c)
{
	struct tnt_buf *buf;

	buf = container_of(el, struct tnt_buf, active_entry);
	return buf;
}

/**
\brief compare buffer using its id stored in vbuf

\param buf: pointer to buffer
\param vbuf: ppointer to buffer conteining id
\return 0 if match is ok  else 1
*/
static int compare_by_index(void *b, void *vb)
{
	struct vb2_buffer *buf = (struct vb2_buffer *)b;
	struct v4l2_buffer *vbuf = (struct v4l2_buffer *)vb;

	//pr_info("%d %s %d\n", buf->index, buf->index == vbuf->index ? "==" : "!=",  vbuf->index);
	return buf->index == vbuf->index;
}

/**
\brief Checks if a buffer is contained inside a list using 
a struct list_ops delcared inside function.

\param list: pointer to list.
\param vb: void pointer to a element used by matching.
\return buffer pointer or NULL
*/

static __maybe_unused struct tnt_buf *just_queued(struct list_head *list,
						  struct v4l2_buffer *vb)
{
	struct list_ops ops = {
		.convert = convert_tntbuf_dq,
		.match = compare_by_index,
	};

	struct tnt_buf *buf = foreach_entry(list, &ops, vb);
	return buf;
}

inline bool just_dqueued(struct tnt_buf *buf)
{
	return buf->refcount > 0 && buf->vb.state == VB2_BUF_STATE_DEQUEUED;
}

inline struct tnt_buf *buf_from_index(struct tntdrv *drv, int index)
{
	struct vb2_buffer *vb = drv->queue.bufs[index];
	struct tnt_buf *buf = container_of(vb, struct tnt_buf, vb);

	return buf;
}

/**
\brief Check if buffer is active i.e. the buffer
is in driver active list

\param buf: pointer to struct tnt_buf
\return true or false
*/
static inline bool buf_active(struct tnt_buf *buf)
{
	return buf->active_entry.next != LIST_POISON1 &&
	       buf->active_entry.prev != LIST_POISON2;
}

enum tnt_grabmode inline tnt_get_grabmode(struct tntdrv *drv)
{
	return drv->grabmode;
}

/**
\brief dqueue a buffer using a specific 
search criteria( search by id at moment).
If buffer is just dqueued then only its refcount 
is incremented.

\param drv: pointer to struct tntdrv
\param vb:  pointer to struct v4l2_buffer
\return 0 or errno.
*/
int dq_buffer(struct tntdrv *drv, struct v4l2_buffer *vb)
{
	struct tnt_buf *buf = buf_from_index(drv, vb->index);

	if (just_dqueued(buf)) {
		buf->refcount++;
		return 0;
	}

	if (buf_active(buf)) {
		list_del(&buf->active_entry);
	}

	atomic_dec(&drv->n_active);
	return tnt_vb2_remove(&drv->queue, &buf->vb, NULL, vb);
}

/**
\brief Queue a struct tnt_buf in following manner:
   
1. The buffer is looked for in dqueued_list using
its criterium (i. match buffer id) 

2. if buffer is not in dqueued_list, it is never used, 
then it is queued in queued_list.

3. if buffer is in dqueued_list, it is just dqueued.
4. if buffer refcount is == 0, the buffer is relased by
user space and then is requeued.
5. if mode is TRIGGED  queues buffer both in 
done_list than in queued_list

6. if in CONTINUOUS_SYNC queues buffer only 
in active list and it is removed to dqueued_list

\param drv: pointer to struct tntdrv 
\param vb:  pointer to struct v4l2_buffer
\return 0 or errno
*/
int q_buffer(struct tntdrv *drv, struct v4l2_buffer *b)
{
	int ret = 0;
	struct tnt_buf *buf = buf_from_index(drv, b->index);

	/* buf was never queued */
	if (!just_dqueued(buf)) {
		ret = tnt_vb2_qbuf(&drv->queue, b);
		atomic_inc(&drv->n_active);
		return ret;
	}

	/* buf is dqueued and is not used more  */
	if (--buf->refcount == 0) {
		//pr_info("qbuf #%d\n", buf->vb.index);
		buf->state = buf->vb.state;
		/* tnt_vb2_qbuf calls buf_queue to active_list */
		ret = tnt_vb2_qbuf(&drv->queue, b);
		if (ret != 0)
			return ret;

		list_del(&buf->dqueued_entry);
		atomic_inc(&drv->n_active);
	}
	return ret;
}
