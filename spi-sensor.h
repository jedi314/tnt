/** 
 \file spi-sensor.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Refer to IMX265LQR-C trm
 
 Chip ID: 02 Write: 02h / Read: 82h
 Chip ID: 03 Write: 03h / Read: 83h
 Chip ID: 04 Write: 04h / Read: 84h
 Chip ID: 05 Write: 05h / Read: 85h
 Chip ID: 06 Write: 06h / Read: 86h
 Chip ID: 07 Write: 07h / Read: 87h
 Chip ID: 08 Write: 08h / Read: 88h
 Chip ID: 09 Write: 09h / Read: 89h
 Chip ID: 0A Write: 0Ah / Read: 8Ah
 Chip ID: 0B Write: 0Bh / Read: 8Bh
 Chip ID: 0C Write: 0Ch / Read: 8Ch
 Chip ID: 0D Write: 0Dh / Read: 8Dh
 Chip ID: 0E Write: 0Eh / Read: 8Eh
 Chip ID: 0F Write: 0Fh / Read: 8Fh
 Chip ID: 10 Write: 10h / Read: 90h
 Chip ID: 11 Write: 11h / Read: 91h
 Chip ID: 12 Write: 12h / Read: 92h

*/

#ifndef _SPI_SENSOR_H
#define _SPI_SENSOR_H

#include <linux/types.h>
#include "tnt-io.h"

#define SPI_READ_OFFSET 16
#define SPI_REG_OFFSET 8
#define SPI_READ_ORG 0x80
#define SPI_READ_MASK 0xFF
#define SPI_MIN_ID 0x2
#define SPI_MAX_ID 0x12
#define SPI_TIMEOUT 10

enum transaction {
	T_WRITE = 0,
	T_READ = 0x80,
};

struct spi_header {
	u8 id; /*!< Chip ID */
	u8 reg; /*!< Register Address */
	u8 org;
	u8 size;
	u32 mask;
};

struct spi_data {
	struct spi_header header;
	u32 value;
};

int tnt_spi_write(struct tnt_device *dev, struct spi_header *head, u32 value,
		  bool wait, bool mod);
int tnt_spi_read(struct tnt_device *dev, struct spi_header *head, u32 *value);
u32 tnt_spi_scale(struct spi_data *data, u32 value);
int tnt_spi_register(struct tnt_device *dev, struct spi_data *data);

typedef bool (*ftest)(struct tnt_device *);
int tnt_wait_until_ntimes(ftest f, void *param, int timeout, int n);

#define tnt_wait_until(f, p, t) tnt_wait_until_ntimes(f, p, t, 1);

#endif
