/** 
 \file tnt-optical_density.h
 
*/

#ifndef _TNT_OPTICAL_DENSITY_H
#define _TNT_OPTICAL_DENSITY_H

#include "tnt-io.h"
#include "tntvdo.h"

/* RD regs */
#define OPT_DENSITY_Y 0x00010038 //INDEX_14
#define OPT_DENSITY_CB 0x0001003C //INDEX_15
#define OPT_DENSITY_CR 0x00010040 //INDEX_16

#define OPT_DENSITY_START_X 0x000200A0 //INDEX_40
#define OPT_DENSITY_STOP_X 0x000200A4 //INDEX_41
#define OPT_DENSITY_START_Y 0x000200A8 //INDEX_42
#define OPT_DENSITY_STOP_Y 0x000200AC //INDEX_43

enum tnt_od_colour { OD_Y, OD_CB, OD_CR };

enum tnt_od_window { OD_START_X, OD_END_X, OD_START_Y, OD_END_Y };

u32 tnt_optical_density_color(struct tnt_device *dev, enum tnt_od_colour odc);
u32 tnt_optical_density_set_window(struct tnt_device *dev,
				   enum tnt_od_window odw, u32 val);
void tnt_optical_density_save(struct tntdrv *drv);
inline void tnt_od_colors(struct tnt_device *dev, struct tnt_od_ch *odch);

#endif
