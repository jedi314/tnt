/** 
 \file list.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief linked dlist bufeer algorithms
*/

#include <linux/list.h>
#include "debugfs/trace.h"
#include "list.h"

bool inline just_insert(struct list_head *entry)
{
	return entry->next != LIST_POISON1 && entry->prev != LIST_POISON2;
}

void *foreach_entry(struct list_head *list, struct list_ops *ops, void *b)
{
	struct list_head *pos;
	void *el;
	int res;

	if (list_empty(list)) {
		return NULL;
	}
	for (pos = list->next; pos != list; pos = pos->next) {
		el = call_list_op(ops, convert, pos, b);
		if (el == NULL)
			return NULL;
		res = call_list_null(ops, match, el, b);
		if (res) {
			return el;
		}
	}
	return NULL;
}

void *insert_ordered(struct list_head *list, struct list_head *new,
		     struct list_ops *ops)
{
	struct list_head *el = NULL;

	if (list_empty(list)) {
		list_add_tail(new, list);
	} else {
		el = foreach_entry(list, ops, new);
		if (el == NULL) {
			el = list;
		}
		list_add(new, el->prev);
	}
	return el;
}
