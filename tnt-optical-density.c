/** 
 \file tnt-optical_density.c
 
*/

#include "tnt-optical-density.h"

#define REG_OPT_COLOR(a) (OPT_DENSITY_Y + (a) * sizeof(u32))
#define REG_OPT_WINDOW(a) (OPT_DENSITY_START_X + (a) * sizeof(u32))

inline u32 tnt_optical_density_color(struct tnt_device *dev,
				     enum tnt_od_colour od)
{
	u32 r;
	r = tnt_read(dev, REG_OPT_COLOR(od));
	return r;
}

inline u32 tnt_optical_density_set_window(struct tnt_device *dev,
					  enum tnt_od_window od, u32 val)
{
	if (od == OD_START_X || od == OD_END_X)
		val >>= 1;

	tnt_write(dev, REG_OPT_WINDOW(od), val);
	return val;
}

inline void tnt_od_colors(struct tnt_device *dev, struct tnt_od_ch *odch)
{
	odch->y = tnt_optical_density_color(dev, OD_Y);
	odch->cb = tnt_optical_density_color(dev, OD_CB);
	odch->cr = tnt_optical_density_color(dev, OD_CR);
}

/* void tnt_optical_density_save(struct tntdrv *drv) */
/* { */
/*     int source = FG_SOURCE(drv->grabmode); */
/*     struct tnt_buf *active = drv->active; */
/*     struct tnt_optd *optd; */

/*     if(active == NULL ) { */
/*         dev_err(drv->device.dev, "Buffer Active Empty.\n"); */
/*         return; */
/*     } */

/*     optd = active->optd; */
/*     optd->idx = active->vb.index; */

/*     switch (source) { */
/*     case OCR: */
/*         tnt_od_colors(&drv->ocr.device, &optd->ocr); */
/*         break; */
/*     case CTX: */
/*         tnt_od_colors(&drv->ctx.device, &optd->ctx); */
/*         break; */
/*     case DOUBLE: */
/*         tnt_od_colors(&drv->ocr.device, &optd->ocr); */
/*         tnt_od_colors(&drv->ctx.device, &optd->ctx); */
/*         break; */
/*     default: */
/*         break; */
/*     } */
/* } */
