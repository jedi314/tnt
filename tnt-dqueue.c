/** 
 \file tnt-dqueue.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

/* for trace */
#include "tnt-vb2.h"

#include "tnt-dqueue.h"
#include "debugfs/trace.h"

static int debug;

#define dprintk(level, fmt, arg...)                                            \
	do {                                                                   \
		if (debug >= level)                                            \
			pr_info("%s: " fmt, __func__, ##arg);                  \
	} while (0)

#define call_memop(vb, op, args...)                                            \
	((vb)->vb2_queue->mem_ops->op ? (vb)->vb2_queue->mem_ops->op(args) : 0)

#define call_ptr_memop(vb, op, args...)                                        \
	((vb)->vb2_queue->mem_ops->op ? (vb)->vb2_queue->mem_ops->op(args) :   \
					NULL)

#define call_void_memop(vb, op, args...)                                       \
	do {                                                                   \
		if ((vb)->vb2_queue->mem_ops->op)                              \
			(vb)->vb2_queue->mem_ops->op(args);                    \
	} while (0)

#define call_qop(q, op, args...) ((q)->ops->op ? (q)->ops->op(args) : 0)

#define call_void_qop(q, op, args...)                                          \
	do {                                                                   \
		if ((q)->ops->op)                                              \
			(q)->ops->op(args);                                    \
	} while (0)

#define call_vb_qop(vb, op, args...)                                           \
	((vb)->vb2_queue->ops->op ? (vb)->vb2_queue->ops->op(args) : 0)

#define call_void_vb_qop(vb, op, args...)                                      \
	do {                                                                   \
		if ((vb)->vb2_queue->ops->op)                                  \
			(vb)->vb2_queue->ops->op(args);                        \
	} while (0)

#define call_bufop(q, op, args...)                                             \
	({                                                                     \
		int ret = 0;                                                   \
		if (q && q->buf_ops && q->buf_ops->op)                         \
			ret = q->buf_ops->op(args);                            \
		ret;                                                           \
	})

#define call_void_bufop(q, op, args...)                                        \
	({                                                                     \
		if (q && q->buf_ops && q->buf_ops->op)                         \
			q->buf_ops->op(args);                                  \
	})

/*
 * __vb2_dqbuf() - bring back the buffer to the DEQUEUED state
 */
void __vb2_dqbuf(struct vb2_buffer *vb)
{
	struct vb2_queue *q = vb->vb2_queue;
	unsigned int i;

	/* nothing to do if the buffer is already dequeued */
	if (vb->state == VB2_BUF_STATE_DEQUEUED)
		return;

	vb->state = VB2_BUF_STATE_DEQUEUED;

	/* unmap DMABUF buffer */
	if (q->memory == VB2_MEMORY_DMABUF)
		for (i = 0; i < vb->num_planes; ++i) {
			if (!vb->planes[i].dbuf_mapped)
				continue;
			call_void_memop(vb, unmap_dmabuf,
					vb->planes[i].mem_priv);
			vb->planes[i].dbuf_mapped = 0;
		}
}

/*
 * __vb2_wait_for_done_vb() - wait for a buffer to become available
 * for dequeuing
 *
 * Will sleep if required for nonblocking == false.
 */
static int __vb2_wait_for_done_vb(struct vb2_queue *q, int nonblocking)
{
	/*
	 * All operations on vb_done_list are performed under done_lock
	 * spinlock protection. However, buffers may be removed from
	 * it and returned to userspace only while holding both driver's
	 * lock and the done_lock spinlock. Thus we can be sure that as
	 * long as we hold the driver's lock, the list will remain not
	 * empty if list_empty() check succeeds.
	 */

	for (;;) {
		int ret;

#if 0
		if (!q->streaming) {
			dprintk(1, "streaming off, will not wait for buffers\n");
			return -EINVAL;
		}
#endif

		if (q->error) {
			dprintk(1,
				"Queue in error state, will not wait for buffers\n");
			return -EIO;
		}

		if (q->last_buffer_dequeued) {
			dprintk(3,
				"last buffer dequeued already, will not wait for buffers\n");
			return -EPIPE;
		}

		if (!list_empty(&q->done_list)) {
			/*
			 * Found a buffer that we were waiting for.
			 */
			break;
		}

		if (nonblocking) {
			dprintk(3,
				"nonblocking and no buffers to dequeue, will not wait\n");
			return -EAGAIN;
		}

		/*
		 * We are streaming and blocking, wait for another buffer to
		 * become ready or for streamoff. Driver's lock is released to
		 * allow streamoff or qbuf to be called while waiting.
		 */
		call_void_qop(q, wait_prepare, q);

		/*
		 * All locks have been released, it is safe to sleep now.
		 */
		dprintk(3, "will sleep waiting for buffers\n");
		ret = wait_event_interruptible(
			q->done_wq, !list_empty(&q->done_list) ||
					    !q->streaming || q->error);

		/*
		 * We need to reevaluate both conditions again after reacquiring
		 * the locks or return an error if one occurred.
		 */
		call_void_qop(q, wait_finish, q);
		if (ret) {
			dprintk(1, "sleep was interrupted\n");
			return ret;
		}
	}
	return 0;
}

static int __vb2_wait_for_vb(struct vb2_queue *q, int nonblocking)
{
	return __vb2_wait_for_done_vb(q, nonblocking);
}

static void *convert_vb2buf(struct list_head *el, void *c)
{
	struct vb2_buffer *buf;

	buf = container_of(el, struct vb2_buffer, done_entry);
	return buf;
}

/**
\brief compare buffer using its id stored in vbuf

\param buf: pointer to buffer
\param vbuf: ppointer to buffer conteining id
\return 0 if match is ok  else 1
*/
static int compare_by_index(void *b, void *vb)
{
	struct vb2_buffer *buf = (struct vb2_buffer *)b;
	struct v4l2_buffer *vbuf = (struct v4l2_buffer *)vb;

	//pr_info("%d %s %d\n", buf->index, buf->index == vbuf->index ? "==" : "!=",  vbuf->index);
	return buf->index == vbuf->index;
}

static int __vb2_get_done_vb(struct vb2_queue *q, struct vb2_buffer **vb,
			     void *pb)
{
	unsigned long flags;
	int ret = 0;

	struct list_ops done_ops = { .convert = convert_vb2buf,
				     .match = compare_by_index };

	/*
	 * Driver's lock has been held since we last verified that done_list
	 * is not empty, so no need for another list_empty(done_list) check.
	 */
	spin_lock_irqsave(&q->done_lock, flags);

	/* TODO: get buffer from its id through a F search converter */
	//*vb = list_first_entry(&q->done_list, struct vb2_buffer, done_entry);
	*vb = q->list_ops->pick(&q->done_list, pb, &done_ops);
	if (*vb == NULL) {
		spin_unlock_irqrestore(&q->done_lock, flags);
		return -EINVAL;
	}

	/*
	 * Only remove the buffer from done_list if all planes can be
	 * handled. Some cases such as V4L2 file I/O and DVB have pb
	 * == NULL; skip the check then as there's nothing to verify.
	 */
	if (pb)
		ret = call_bufop(q, verify_planes_array, *vb, pb);

	if (!ret)
		list_del(&(*vb)->done_entry);
	spin_unlock_irqrestore(&q->done_lock, flags);

	return ret;
}

/*
 * __vb2_get_done_vb() - get a buffer ready for dequeuing
 *
 * Will sleep if required for nonblocking == false.
 */
static int __vb2_get_vb(struct vb2_queue *q, struct vb2_buffer **vb, void *pb,
			int nonblocking)
{
	int ret = 0;

	/*
	 * Wait for at least one buffer to become available on the done_list.
	 */
	ret = __vb2_wait_for_vb(q, nonblocking);
	if (ret)
		return ret;

	ret = __vb2_get_done_vb(q, vb, pb);

	return ret;
}

/**
\brief Remove a buffer to queued_list
and call fill_user_buffer and buf_finish callbacks

\param q: pointer to vb2 queue
\param pindex: 
\return 0 or error;
*/
int tnt_vb2_remove(struct vb2_queue *q, struct vb2_buffer *vb,
		   unsigned int *pindex, void *pb)
{
	call_void_vb_qop(vb, buf_finish, vb);

	if (pindex)
		*pindex = vb->index;

	/* Fill buffer information for the userspace */
	if (pb)
		call_void_bufop(q, fill_user_buffer, vb, pb);

	/* Remove from videobuf queue */
	list_del(&vb->queued_entry);
	q->queued_count--;

	trace_vb2_dqbuf(q, vb);

	/* go back to dequeued state */
	__vb2_dqbuf(vb);

	dprintk(2, "dqbuf of buffer %d, with state %d\n", vb->index, vb->state);
	return 0;
}

int tnt_vb2_core_dqbuf(struct vb2_queue *q, unsigned int *pindex, void *pb,
		       bool nonblocking)
{
	struct vb2_buffer *vb = NULL;
	int ret;

	/* ret = __get_vb(q, &vb, pb, nonblocking) */
	ret = __vb2_get_vb(q, &vb, pb, nonblocking);
	if (ret < 0)
		return ret;

	switch (vb->state) {
	case VB2_BUF_STATE_DONE:
		dprintk(3, "returning done buffer\n");
		break;
	case VB2_BUF_STATE_ERROR:
		dprintk(3, "returning done buffer with errors\n");
		break;
	default:
		dprintk(1, "invalid buffer state\n");
		return -EINVAL;
	}

	return tnt_vb2_remove(q, vb, pindex, pb);
}
