/** 
 \file tnt-dqueue.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_DQUEUE_H
#define _TNT_DQUEUE_H

#include "tnt-videobuf2-core.h"

void __vb2_dqbuf(struct vb2_buffer *vb);
int tnt_vb2_core_dqbuf(struct vb2_queue *q, unsigned int *pindex, void *pb,
		       bool nonblocking);
int tnt_vb2_remove(struct vb2_queue *q, struct vb2_buffer *vb,
		   unsigned int *pindex, void *pb);

#endif
