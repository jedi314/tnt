/** 
 \file tnt-grabber.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2021  s.r.l.
 

 \brief
*/

#ifndef _TNT_GRABBER_H
#define _TNT_GRABBER_H

int grabber_init(struct tntdrv *drv);
void grabber_destroy(struct tnt_grabber *grabber);

#endif
