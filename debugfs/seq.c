#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/seq_file.h>
#include <linux/printk.h>
#include <linux/debugfs.h>
#include <linux/uaccess.h>



MODULE_LICENSE("GPL");
MODULE_AUTHOR("Giandomenico Rossi");
MODULE_DESCRIPTION("Seq test");
MODULE_VERSION("0.01");





struct tseq_t {
    struct dentry *dir;
    struct dentry *file;
    
    struct list_head bufs;
};

static struct tseq_t *_tseq;

static
void list_insert_sorted_el(struct tseq_t *tseq, struct tnt_buf* b)
{
    struct tnt_buf *buf;
    pr_info("%p\n", &tseq->bufs);
    
    list_for_each_entry(buf, &tseq->bufs, lst){
        if(b->a <= buf->a){
            list_add(&b->lst, buf->lst.prev);
            return;
        }
    }
    pr_info("%p\n", &buf->lst);
    list_add_tail(&b->lst, &buf->lst);
}


/* start() method */
static void *
tseq_start(struct seq_file *seq, loff_t *pos)
{
    struct tseq_t *tseq = (struct tseq_t *)seq->private;

    return seq_list_start(&tseq->bufs, *pos);
}

/* next() method */
static void *
tseq_next(struct seq_file *seq, void *v, loff_t *pos)
{
    struct tseq_t *tseq = (struct tseq_t *)seq->private;
    
    return seq_list_next(v, &tseq->bufs, pos);    
}

/* show() method */
static int
tseq_show(struct seq_file *seq, void *v)
{
    struct tnt_buf *buf;

    buf = list_entry(v, struct tnt_buf, lst);
    seq_printf(seq, "Value: %d\n", buf->a);
    return 0;
}

/* stop() method */
static void
tseq_stop(struct seq_file *seq, void *v)
{
  /* No cleanup needed in this example */
}

/* Define iterator operations */
static struct seq_operations tseq_ops = {
  .start = tseq_start,
  .next  = tseq_next,
  .stop  = tseq_stop,
  .show  = tseq_show,
};

static int
tseq_open(struct inode *inode, struct file *file)
{
	int res = -ENOMEM;
    
    /* Register the operators */
    res = seq_open(file, &tseq_ops);
    if (res == 0)
        ((struct seq_file *)file->private_data)->private = inode->i_private;
    
    return res;
}

static
ssize_t tseq_write(struct file *flip, const char __user *buf, size_t count, loff_t *f_pos)
{
    unsigned long long int val;
    int ret;
    struct tnt_buf *tb;
    
    struct tseq_t *tseq = ((struct seq_file *)flip->private_data)->private;    

    ret = kstrtoull_from_user(buf, count, 10, &val);
    if (ret) {
        /* Negative error code. */
        pr_info("ko = %d\n", ret);
        return ret;
    }

    tb = kzalloc(sizeof(*tb), GFP_KERNEL);
    if(tb == NULL){
        pr_err("tb kzalloc error\n");
        return 0;
    }
    tb->a = (int)val;
    list_insert_sorted_el(tseq, tb);
    //pr_info("WRITE %p val:%lld\n", tb, val);
    
    
    *f_pos += count;
    return count;    
}


static struct file_operations list_fops = {
  .owner   = THIS_MODULE,
  .open    = tseq_open,      /* User supplied */
  .read    = seq_read,       /* Built-in helper function */
  .write   = tseq_write,
  .llseek  = seq_lseek,      /* Built-in helper function */
  .release = seq_release,    /* Built-in helper funciton */
};


static int tseq_init(void)
{
    struct dentry *dir;
    struct dentry *file;

    _tseq = (struct tseq_t*)kzalloc(sizeof(*_tseq), GFP_KERNEL);
    if(!_tseq){
        pr_err("kzalloc error\n");
        return -ENOMEM;
    }

    INIT_LIST_HEAD(&_tseq->bufs);
    dir = debugfs_create_dir("pippo", 0);
    if (!dir) {
        // Abort module load.
        printk(KERN_ALERT "failed to create /sys/kernel/debug/pippo\n");
        goto exit_dir;
    }
    _tseq->dir = dir;
    
    file = debugfs_create_file( "list", 0666, _tseq->dir, _tseq, &list_fops);
    if (!file) {
        // Abort module load.
        printk(KERN_ALERT "debugfs_example2: failed to create /sys/kernel/debug/pippo/list\n");
        goto exit_file;
    }

    
    return 0;

exit_file:
    debugfs_remove_recursive(dir);
exit_dir:
    kfree(_tseq);
    return -1;
}

static void tseq_exit(void)
{
    debugfs_remove_recursive(_tseq->dir);    
    kfree(_tseq);
    printk("<1> Bye, cruel world\n");
}



module_init(tseq_init);
module_exit(tseq_exit);

#ifdef _VERMAGIC_
MODULE_INFO(vermagic, "4.19.0+ SMP mod_unload aarch64");
#endif
