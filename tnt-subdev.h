/** 
 \file tnt-subdev.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_SUBDEV_H_
#define _TNT_SUBDEV_H_

#include <media/v4l2-device.h>
#include "tntvdo.h"

int tnt_register_subdev(struct tntvdo *vdo, char *name);
int tnt_register_subdevice(struct tntvdo *vdo, char *name,
			   const struct v4l2_subdev_ops *ops,
			   const struct v4l2_subdev_internal_ops *iops);

#endif
