/** 
 \file tnt-subdev.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Fpga subdev
*/

#include "tnt-subdev.h"
#include "tnt-cmd.h"
#include "tnt-access.h"
#include "tnt-fpga-utils.h"
#include "tnt-imx265.h"
#include "tnt-imx250.h"
#include "tnt-dbg.h"
#include "tnt-v4l2.h"
#include "tnt-rle.h"
#include "tnt-acq-points.h"
#include "tnt-allocator.h"
#include "tnt-bilinear-resizer.h"

/* subdev  */

static int sensor_open(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh)
{
	//pr_info("SD[%s] open by \"%s\" (pid %i)\n", sd->name, current->comm, current->pid);

	//struct tntvdo* vdo = (struct tntvdo*)v4l2_get_subdevdata(sd);
	//dump_process();
	return 0;
}

static int sensor_close(struct v4l2_subdev *subdev, struct v4l2_subdev_fh *fh)
{
	//pr_info("SD[%s] closed by \"%s\" (pid %i)\n", subdev->name, current->comm, current->pid);
	return 0;
}

static const struct v4l2_subdev_internal_ops sensor_internal_ops = {
	.open = sensor_open,
	.close = sensor_close,

};

static inline struct tnt_video_fmt *
tnt_sensor_fmt_by_index(struct tntvdo *vdo, u32 width, u32 height, char *type)
{
	struct tnt_video_fmt *format;
	struct tntdrv *drv = vdo->driver;
	int i;

	for (i = 0; i < drv->n_sensors; i++) {
		format = &drv->sensors[i].fmt;
		if (format->width == width && format->height == height &&
		    !strcmp(format->name, type)) {
			pr_err("Match format type %s\n", format->name);
			return format;
		}
	}
	return NULL;
}

static inline int sensor_init(struct tntvdo *vdo, enum tnt_sensors sensor,
			      struct tnt_video_fmt *fmt)
{
	switch (sensor) {
	case CMOS_IMX265_BW:
	case CMOS_IMX265_COL:
		return imx265_init(&vdo->device, sensor, fmt);
		break;
	case CMOS_IMX250_BW:
	case CMOS_IMX250_COL:
		return imx250_init(&vdo->device, sensor, fmt);
		break;
	case SONY_IMX265_BW_CROPPED_2MP:
		return imx265cropped_init(&vdo->device, sensor, fmt);
		///return imx265cropped_init(&vdo->device, sensor, fmt);
	default:
		break;
	}
	return -EINVAL;
}

static inline struct tnt_sensor_desc *get_sensor_by_id(struct tntvdo *vdo,
						       int id)
{
	struct tnt_sensor_desc *sensor;
	struct tntdrv *drv = vdo->driver;

	sensor = &drv->sensors[id];

	return sensor;
}

static inline struct tnt_video_fmt *
get_sensor_format(struct tnt_sensor_desc *sens)
{
	return &sens->fmt;
}

static int is_allowed_sensor(struct tntvdo *vdo, enum tnt_sensors sensor_code)
{
	int i, id;
	struct tnt_sensor_desc *sensors = &vdo->driver->sensors[0];
	for (i = 0; vdo->allowed_sensors[i] != (u8)-1; i++) {
		id = vdo->allowed_sensors[i];
		if (sensors[id].code == sensor_code)
			return id;
	}
	pr_err("Sensor code %d not allowed!!\n", sensor_code);

	return -EINVAL;
}

void print_video_fmt(struct tnt_video_fmt *fmt);
void print_sensor_info(struct tnt_sensor_desc *sens, int id);

int tnt_sensor_init(struct tntvdo *vdo, struct tnt_sensor *sensor)
{
	struct tnt_video_fmt *fmt;
	int ret = 0;
	int sensor_id;

	sensor_id = is_allowed_sensor(vdo, sensor->type);
	if (sensor_id < 0) {
		return -EINVAL;
	}

	vdo->current_sensor = get_sensor_by_id(vdo, sensor_id);
	fmt = get_sensor_format(vdo->current_sensor);
	vdo->current_fmt = fmt;
	pr_info("Current Sensor(id=%d) (code=%d):", sensor_id,
		vdo->current_sensor->code);

	print_sensor_info(vdo->current_sensor, sensor_id);
	pr_info("Current format:");
	print_video_fmt(vdo->current_fmt);

	ret = sensor_init(vdo, sensor->type, fmt);
	if (ret) {
		pr_err("Error Initializing sensor(id=%d)\n", sensor->type);
		return ret;
	}

	/* ctrls */
	ret = vdo_ctrl_sensor_enable(vdo, 1);
	if (ret) {
		pr_err("error enabling sensor controls\n");
		goto err_ctrl_en;
	}

	return 0;

err_ctrl_en:
	/* Normally, we would uninitialize the sensor
	 * here, but what does that mean, actually??
	 * No memory is reserved and the current_sensor
	 * is NULL, so the finalize will see the channel
	 * as not initialized*/
	return ret;
}

static int tnt_set_source(struct tntdrv *drv, enum tnt_source source)
{
	struct tnt_device *devcha = &drv->cha.device;
	struct tnt_device *devchb = &drv->chb.device;
	struct tnt_device *dev = &drv->device;

	/* force to restart stremaing if source is changed */
	drv->queue.streaming = 0;

	/* TODO FIX VDO */
	switch (source) {
	case CHA:
		tnt_fpga_double_source_disable(dev);
		tnt_fpga_hwsw(devcha);
		tnt_fpga_disable_dma(devchb);
		tnt_fpga_enable_dma(devcha);
		drv->active_vdo[CHB] = NULL;
		drv->active_vdo[CHA] = &drv->cha;
		break;
	case CHB:
		tnt_fpga_double_source_disable(dev);
		tnt_fpga_hwsw(devchb);
		tnt_fpga_disable_dma(devcha);
		tnt_fpga_enable_dma(devchb);
		//tnt_rle_disable_dma(devcha);
		drv->active_vdo[CHA] = NULL;
		drv->active_vdo[CHB] = &drv->chb;
		break;
	case DOUBLE:
		tnt_fpga_enable_dma(devcha);
		drv->active_vdo[CHA] = &drv->cha;
		tnt_fpga_enable_dma(devchb);
		drv->active_vdo[CHB] = &drv->chb;
		tnt_fpga_double_source_enable(dev);
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static int tnt_set_gmode(struct tntdrv *drv, enum tnt_gmode gmode)
{
	struct tnt_device *dev = &drv->device;

	switch (gmode) {
	case CONTINUOUS:
		//tnt_fpga_reset_counters(dev);
		drv->queue.min_buffers_needed = N_MIN_BUFS;
		break;
	case TRIGGED:
		drv->queue.min_buffers_needed = 1;
		tnt_fpga_set_trigged_gmode(dev);
		break;
	default:
		tnt_fpga_set_trigged_gmode(dev);
		return -EINVAL;
	}

	return 0;
}

static int tnt_enable_proc(struct tntvdo *vdo, int module)
{
	int ret = -EINVAL;

	switch (module) {
	case TNT_OPTICAL_DENSITY:
		ret = 0;
		break;

	case TNT_RLE:
		ret = tnt_rle_enable(&vdo->driver->cha.device);
		vdo->driver->rle_enable = 1;
		break;

	case TNT_REMAPPING:
		ret = 0;
		break;

	case TNT_BILINEAR_RESIZE:
		bilinear_resizer_enable(vdo);
		break;

	default:
		break;
	}

	return ret;
}

static int tnt_set_rle(struct tntdrv *drv, struct tnt_rle_conf *rle, int idx)
{
	int ret;

	ret = tnt_set_rle_cha(&drv->cha.device, rle, idx);

	if (drv->rle_enable)
		tnt_rle_enable(&drv->cha.device);

	if (!ret)
		memcpy(&drv->rle_conf[idx], rle, sizeof(struct tnt_rle_conf));

	return ret;
}

int tnt_set_grabmode(struct tntdrv *drv, enum tnt_grabmode mode)
{
	int ret = 0;
	int source = FG_SOURCE(mode);
	int gmode = FG_GMODE(mode);

	ret = tnt_set_gmode(drv, gmode);
	if (ret)
		return ret;
	ret = tnt_set_source(drv, source);
	if (ret)
		return ret;

	drv->grabmode = mode;
	return ret;
}

/* NOTE: this is not competence of a driver  */
int tnt_finalize(struct tntdrv *drv)
{
	struct tntvdo *ch0, *ch1;
	int en_ch = -1;

	ch0 = &drv->cha;
	ch1 = &drv->chb;

	/* Check that the selected sources match
	 * the channels that have been initialized */
	if (ch0->current_sensor && drv->active_vdo[0] != NULL &&
	    ch1->current_sensor && drv->active_vdo[1] != NULL)
		en_ch = DOUBLE;
	else if (ch0->current_sensor && drv->active_vdo[0] != NULL)
		en_ch = CHA;
	else if (ch1->current_sensor && drv->active_vdo[1] != NULL)
		en_ch = CHB;

	if (FG_SOURCE(drv->grabmode) != en_ch) {
		pr_err("sensors/acquisition sources mismatch (%x vs %x)\n",
		       en_ch, FG_SOURCE(drv->grabmode));
		return -EINVAL;
	}

	drv->finalize = 1;
	pr_info("finalized initialization\n");
	return 0;
}

/**
\brief Copy current sensor and 
video format  information back to 
user space struct tnt_sensor 

\param vdo: pointer to vdo struct of sensor
\param sensor: user space struct sensor
*/
static void copy_sensor_info(struct tntvdo *vdo, struct tnt_sensor *sensor)
{
	struct tnt_video_fmt *fmt = &vdo->current_sensor->fmt;

	strcpy(sensor->format_name, fmt->name);
	sensor->pixel_type = fmt->type;
	sensor->width = fmt->width;
	sensor->height = fmt->height;
	sensor->bpp = fmt->bpp;
	sensor->channel_id = vdo->id;
}

/**
\brief ioctl execute after a call to a ioctl 
of /dev/video or directly calling /dev/v4l2-subdev

\param sd: pointer to vsl2_subdev function 
\param cmd: command code as genenrated vy _IO
\param arg: argument passed ad defined durinf subdev initialization

\return 0 in case cmd is found or  -ENOTTY
*/
static long sensor_ioctl(struct v4l2_subdev *sd, unsigned int cmd, void *arg)
{
	struct tnt_optd *optd;
	struct tnt_rle_conf *rle;
	struct tnt_buf *buf = NULL;
	struct tntvdo *vdo = v4l2_get_subdevdata(sd);
	struct tntdrv *drv = vdo->driver;
	struct acq_points *ap;
	int val = *(int *)arg;
	int ret = 0;
	struct tnt_sensor *sensor;

	switch (cmd) {
	case TNT_SET_GRABMODE:
		ret = tnt_set_grabmode(drv, val);
		break;
	case TNT_SET_OUTPUT:
		tnt_fpga_output_type(val, &vdo->device);
		break;
	case TNT_SET_PERIOD:
		tnt_fpga_set_period(&drv->device, val);
		drv->period = (u64)val * 1000; /* ns */
		break;
	case TNT_INIT_SENSOR:
		sensor = (struct tnt_sensor *)arg;
		ret = tnt_sensor_init(vdo, sensor);
		if (ret)
			return ret;
		copy_sensor_info(vdo, sensor);
		break;
	case TNT_FINALIZE:
		ret = tnt_finalize(drv);
		break;
	case TNT_GET_OPTD:
		optd = (struct tnt_optd *)arg;
		buf = container_of(drv->queue.bufs[optd->idx], struct tnt_buf,
				   vb);
		if (buf)
			memcpy(optd, &buf->optd, sizeof(buf->optd));
		else
			ret = -EINVAL;
		break;
	case TNT_ENABLE_PROCS:
		ret = tnt_enable_proc(vdo, val);
		break;
	case TNT_SET_ACQUISITION_POINTS:
		ap = (struct acq_points *)arg;
		ret = set_acquisition_points(vdo, ap);
		break;
	case TNT_SET_RLE_ALGO_A:
		rle = (struct tnt_rle_conf *)arg;
		if (tnt_set_rle(drv, rle, 0))
			ret = -EINVAL;
		break;
	case TNT_SET_RLE_ALGO_B:
		rle = (struct tnt_rle_conf *)arg;
		if (tnt_set_rle(drv, rle, 1))
			ret = -EINVAL;
		break;

	default:
		pr_info("%s: Unhadled ioctl cmd [ 0x%08x ]\n", sd->name, cmd);
		ret = -ENOTTY;
	}
	return ret;
}

static const struct v4l2_subdev_core_ops sensor_core_ops = {
	.ioctl = sensor_ioctl,
};

static const struct v4l2_subdev_ops sensor_ops = {
	.core = &sensor_core_ops,
};

/**
\brief Registers a v4l2 subdevice of a grabber
generally bounded to a cmos sensor (cha, chb)

\param grabber: pointer of grabber struct
\param name: name of subdevice used in registration
\param ops: pointer to struct ops operation 
\param iops: pouinter to interna operation struct iops
\return 0 or errno
*/
int tnt_register_subdevice(struct tntvdo *vdo, char *name,
			   const struct v4l2_subdev_ops *ops,
			   const struct v4l2_subdev_internal_ops *iops)
{
	int ret;
	struct v4l2_subdev *sd = &vdo->sensor;

	/* sub device */
	pr_info("Init [%s] subdev: \n", name);
	v4l2_subdev_init(sd, ops);
	//pr_info(">>>>>> %s", name);
	snprintf(sd->name, sizeof(sd->name), "sd-%s", name);
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;
	v4l2_set_subdevdata(sd, vdo);
	sd->dev = vdo->device.dev;
	sd->internal_ops = iops;

	ret = v4l2_device_register_subdev(&vdo->v4l2_dev, sd);
	if (ret) {
		pr_err("tnt subdev nodes registration failed (err=%d)\n", ret);
		return ret;
	}

	ret = v4l2_device_register_subdev_nodes(&vdo->v4l2_dev);
	if (ret) {
		pr_err("tnt subdev nodes registration failed (err=%d)\n", ret);
		return ret;
	}

	return 0;
}

int tnt_register_subdev(struct tntvdo *vdo, char *name)
{
	return tnt_register_subdevice(vdo, name, &sensor_ops,
				      &sensor_internal_ops);
}

/* --------------------------------------------------------------------------*/
