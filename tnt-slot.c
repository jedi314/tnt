/** 
 \file tnt-timer.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
 Access from userspace to video linked lists must absolutely 
 synchronized with  BUSY_CATCH and END_FRAME interrupts to 
 avoid race conditions.
 
 Every ISR must load a time access minus the blind time.
 In this time slot usercape can safely access to list.
 If user space request is in blind time it waits until
 interrupt are handled.

*/

#include <linux/fs.h>
#include <linux/delay.h>
#include <linux/jiffies.h>
#include "tnt-slot.h"
#define MIN_SLOT 500000 /* ns */

static enum hrtimer_restart start_blind_time(struct hrtimer *t)
{
	struct tnt_slot *slot = container_of(t, struct tnt_slot, t);

	slot->blind = true;
	//pr_info("Expired slot %llu\n", ktime_get_ns() - slot->tt);

	return HRTIMER_NORESTART;
}

void slot_init(struct tnt_slot *slot, u64 t_blind)
{
	hrtimer_init(&slot->t, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	slot->t.function = start_blind_time;
	slot->t_blind = t_blind;
	slot->blind = true;
	init_waitqueue_head(&slot->w);
}

inline void slot_del(struct tnt_slot *slot)
{
	hrtimer_cancel(&slot->t);
}

inline void slot_time(struct tnt_slot *slot, u64 period, u64 ns_expo)
{
	slot->t_image = READOUT_TIME + ns_expo - slot->t_blind;
	//pr_info("t_image: %d - expo: %d\n", t_image, expo);
	slot->t_free = period - slot->t_image - slot->t_blind;
	slot->tt = ktime_get_ns();
	//pr_info("t_free: %llu\n", slot->t_free);
}

inline int start_slot_time(struct tnt_slot *slot, u64 expire)
{
	if (expire < MIN_SLOT) /* min 100 uS */
		return -1;

	hrtimer_start(&slot->t, ns_to_ktime(expire), HRTIMER_MODE_REL);
	//pr_info("time to expire %llu\n", expire);
	return 0;
}
inline void wakeup_waiting_slot(struct tnt_slot *slot)
{
	wake_up_interruptible(&slot->w);
}

inline void restart_slot(struct tnt_slot *slot, u64 expire)
{
	slot->blind = false;
	start_slot_time(slot, expire);
	udelay(1000);
	wakeup_waiting_slot(slot);
}

/**
\brief Waits for a time slot.
If there is root it returns immediatley else
wait for until it is awaken from interrupt
or until timeout is elapsed.

\param slot:struct tnt_slot
\return -ETIMEDOUT or -ERESTARTSYS in case of error ot 0 if success
*/
inline int wait_slot(struct tnt_slot *slot)
{
	int ret;
	unsigned long timeout = msecs_to_jiffies(INTERRUPT_WAIT_TIME);

	//pr_info("remaining: %lld %d\n", hrtimer_get_remaining(&slot->t), slot->blind);
	ret = wait_event_interruptible_timeout(slot->w, slot->blind == false,
					       timeout);

	if (ret == 0) {
		pr_info(">>>>>>>>>> TIMEDOUT\n");
		return -ETIMEDOUT;
	} else if (ret < 0)
		return ret;

	return 0;
}
