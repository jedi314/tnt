/** 
 \file tntvdo.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Video Driver
 1. Load from kernel.
 2. Read DT fmt config
 3. Register v4l2 device /dev/video0

*/

#include <linux/module.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/of_address.h>
#include <linux/of_reserved_mem.h>
#include <linux/platform_device.h>
#include <linux/of_irq.h>

#include "tntvdo.h"
#include "tntof.h"
#include "debugfs/trace.h"
#include "tnt-allocator.h"
#include "tnt-optical-density.h"

#include "tnt-access.h"
#include "tnt-imx265.h"
#include "tnt-fpga-utils.h"
#include "fpga-cs.h"
#include "tnt-v4l2.h"
#include "tnt-queue.h"
#include "tnt-cmd.h"
#include "tnt-rle.h"
#include "tnt-acq-points.h"

#include "tnt-processor.h"

#define DRIVER_NAME "tntdrv"
#define BLIND_TIME 5000000 /* nS */

enum stop_state {
	STOP = 1,
	STOP_NOLOAD_BUF,
};

__maybe_unused int bw_set_addr(struct tnt_device *dev, void *b)
{
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct tntvdo *vdo = container_of(dev, struct tntvdo, device);
	struct alc_chunk *chunk = buf->vb.planes[dev->plane].mem_priv;

	if (vdo->driver->rle_enable) {
		int source = FG_SOURCE(vdo->driver->grabmode);
		int plane = source == DOUBLE ? 2 : 1;
		struct alc_chunk *chunk_rle = buf->vb.planes[plane].mem_priv;
		tnt_set_rle_ptr_blob(dev, chunk_rle->addr);
	}
	tnt_processors_setaddr(vdo, buf);

	tnt_write(dev, FLAG_Y_nBAYER,
		  1); /* 0 = pass throught to CSC; 1= direct image */
	tnt_write(dev, ENABLE_FLOW_DDR, 1);
	tnt_write(dev, BADDR_LOC_DMA0, chunk->addr);
	return 0;
}

__maybe_unused int chbbw_set_addr(struct tnt_device *dev, void *b)
{
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct tntvdo *vdo = container_of(dev, struct tntvdo, device);
	int source = FG_SOURCE(vdo->driver->grabmode);
	int plane = source == DOUBLE ? 1 : 0;
	struct alc_chunk *chunk = buf->vb.planes[plane].mem_priv;

	tnt_write(dev, FLAG_Y_nBAYER,
		  1); /* 0 = pass throught to CSC; 1= direct image */
	tnt_write(dev, BADDR_LOC_DMA0, chunk->addr);
	return 0;
}

__maybe_unused int bayer_set_addr(struct tnt_device *dev, void *b)
{
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct alc_chunk *chunk = buf->vb.planes[0].mem_priv;

	tnt_write(dev, FLAG_Y_nBAYER,
		  1); /* 0 = pass throught to CSC; 1= direct image */
	tnt_write(dev, BAYER_SELECTOR, PHASE_CSC);
	tnt_write(dev, BADDR_LOC_DMA_Y, chunk->addr);

	return 0;
}

__maybe_unused int yuv_set_addr(struct tnt_device *dev, void *b)
{
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct tntvdo *vdo = container_of(dev, struct tntvdo, device);
	struct alc_chunk *chunk = buf->vb.planes[dev->plane].mem_priv;
	u32 size = vdo->current_fmt->width * vdo->current_fmt->height;
	dma_addr_t addr = chunk->addr;

	tnt_write(dev, FLAG_Y_nBAYER,
		  0); /* 0 = pass throught to CSC; 1= direct image */
	tnt_write(dev, BAYER_SELECTOR, PHASE_CSC);
	tnt_write(dev, BADDR_LOC_DMA_Y, addr);
	addr += size;
	tnt_write(dev, BADDR_LOC_DMA_CB, addr);
	addr += size / 4;
	tnt_write(dev, BADDR_LOC_DMA_CR, addr);
	tnt_write(dev, ENABLE_LOC_DMA_CB, 1);
	tnt_write(dev, ENABLE_LOC_DMA_CR, 1);
	return 0;
}

__maybe_unused int yuv_continuous_set_addr(struct tnt_device *dev, void *b)
{
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct tntvdo *vdo = container_of(dev, struct tntvdo, device);
	u32 size = vdo->current_fmt->width * vdo->current_fmt->height;
	int source = FG_SOURCE(vdo->driver->grabmode);
	int plane = source == DOUBLE ? 1 : 0;
	struct alc_chunk *chunk = buf->vb.planes[plane].mem_priv;
	dma_addr_t addr = chunk->addr;

	tnt_write(dev, FLAG_Y_nBAYER, 0);
	tnt_write(dev, BAYER_SELECTOR, PHASE_CSC);
	tnt_write(dev, BADDR_LOC_DMA_Y, addr);
	addr += size;
	tnt_write(dev, BADDR_LOC_DMA_CB, addr);
	addr += size / 4;
	tnt_write(dev, BADDR_LOC_DMA_CR, addr);
	tnt_write(dev, ENABLE_LOC_DMA_CB, 1);
	tnt_write(dev, ENABLE_LOC_DMA_CR, 1);

	return 0;
}

static bool double_ready(struct tnt_device *dev)
{
	struct tntdrv *drv = container_of(dev, struct tntdrv, device);
	struct tnt_device *dcha = &drv->cha.device;
	struct tnt_device *dchb = &drv->chb.device;
	int ready_cha = 0;
	int ready_chb = 0;

	ready_cha = tnt_read(dcha, CHECK_IMG_READY) & 1;
	ready_chb = tnt_read(dchb, CHECK_IMG_READY) & 1;
	//pr_info("@ %d %d\n", ready_cha, ready_chb);
	return ready_cha == 1 && ready_chb == 1;
}

static struct device_ops imx265fpga = {
	.image_ready = double_ready,
	.set_buffer_addr = bw_set_addr,
};

void __enqueue_in_driver(struct vb2_buffer *vb);

static inline bool must_queued(struct tntdrv *drv, struct tnt_buf *buf)
{
	return drv->n_future != 0 && buf->vb.timestamp >= drv->t_future;
}

static inline void queue_to_active(struct tnt_buf *buf, struct tntdrv *drv)
{
	buf->state = VB2_BUF_STATE_REQUEUEING;
	tnt_vb2_buffer_done(&buf->vb, VB2_BUF_STATE_REQUEUEING);
}

int queue_current_buffer(struct tntdrv *drv, struct tnt_buf *buf)
{
	enum vb2_buffer_state state;

	if (buf == NULL)
		return 0;

	state = VB2_BUF_STATE_REQUEUEING;
	buf->state = VB2_BUF_STATE_REQUEUEING;

	if (!just_dqueued(buf)) {
		tnt_vb2_buffer_done(&buf->vb, state);
	}
	return 0;
}

int catch_frame_fsm(struct tntdrv *drv)
{
	int status = 0x10001;
	int source = FG_SOURCE(drv->grabmode);

	switch (source) {
	case CHA:
		status = 0x10001;
		break;
	case CHB:
		status = 0x20001;
		break;
	case DOUBLE:
		status = 0x30001;
		break;
	default:
		status = 0x10001;
	}

	return status;
}

static inline void prepare_grab(struct tntdrv *drv)
{
	struct tnt_device *active_dev;
	int i;

	for (i = 0; i < DOUBLE; i++) {
		active_dev = &drv->active_vdo[i]->device;
		if (active_dev == NULL)
			continue;

		load_acquisition_points(drv->active_vdo[i]);
		tnt_fpga_prepare_grab(active_dev);
		call_devop(active_dev, set_buffer_addr, active_dev,
			   drv->active);
	}
}

static inline void fpga_catch(struct tntdrv *drv)
{
	tnt_fpga_catch(&drv->device, catch_frame_fsm(drv));
}

void arm_fsm(struct tntdrv *drv)
{
	drv->active = tnt_activate_older_buffer(&drv->active_list);
	prepare_grab(drv);
}

int load_next_frame(struct tntdrv *drv)
{
	arm_fsm(drv);
	fpga_catch(drv);
	return 0;
}

/**
\brief move active buffer 
from its state of active(READY for fpga)
to busy(PROCESSED bu fpga)

\param drv: pointer to struct tntdrv
\param buf: buffer to busy
\return 
*/
static inline void busy_buffer(struct tntdrv *drv, struct tnt_buf *buf)
{
	drv->busybuf = buf;
}

/* --------------------------------------------------------------------------*/

static inline void set_timestamp(struct tnt_buf *buf)
{
	buf->vb.timestamp = ktime_get_ns();
	buf->realtime = ktime_get_real_ns();
}
static inline void set_uid(struct tntdrv *drv, struct tnt_buf *buf)
{
	buf->uid = ++drv->igid;
}

static inline void define_slot_time(struct tntdrv *drv, u64 time)
{
	int res;
	u64 t_expo = drv->cha.ctrls.w.shutter * 1000;

	/* define slot time */
	slot_time(&drv->access, drv->period, t_expo);
	res = start_slot_time(&drv->access, time - ktime_get_ns());
	if (res == 0) {
		drv->access.blind = false;
		wakeup_waiting_slot(&drv->access);
	}
}

static inline struct tnt_metadata *metadata_addr(struct tnt_device *dev,
						 struct tnt_buf *buf)
{
	struct alc_chunk *chunk = buf->vb.planes[dev->plane].mem_priv;
	int size = buf->vb.planes[dev->plane].length - PAGE_SIZE;
	//struct acq_point *ap = (struct acq_point*)((u8*)chunk->vaddr+size);
	struct tnt_metadata *md =
		(struct tnt_metadata *)((u8 *)chunk->vaddr + size);
	return md;
}

static inline struct tnt_od_ch *opd_addr(struct tnt_device *dev,
					 struct tnt_buf *buf)
{
	struct tnt_metadata *md = metadata_addr(dev, buf);
	return &md->od;
}

void copy_metadata(struct tnt_metadata *md, struct tnt_buf *buf,
		   struct tntvdo *vdo)
{
	struct tnt_device *dev = &vdo->device;
	struct tnt_metadata *metadata = metadata_addr(dev, buf);

	memcpy(md, metadata, sizeof(*metadata));
}

static void set_last_frame_reference(struct tntdrv *drv, struct tnt_buf *buf)
{
	spin_lock(&drv->last_lock);
	drv->last_timestamp = buf->vb.timestamp;
	drv->last_uid = buf->uid;
	spin_unlock(&drv->last_lock);
}

static void save_metadata(struct tntvdo *vdo, struct tnt_buf *buf)
{
	struct tnt_device *dev = &vdo->device;
	struct tnt_metadata *md = metadata_addr(dev, buf);

	acq_point_copy(&md->acq, &dev->ctrls->w);
	/* ID  */
	md->uid = buf->uid;
	/* acq index */
	md->i_acq = vdo->c_point;
	/* Timestamp */
	md->timestamp = buf->vb.timestamp;
	md->realtime = buf->realtime;
	md->index = buf->vb.index;
}

static void save_frames_metadata(struct tntdrv *drv, struct tnt_buf *buf)
{
	struct tnt_device *active_dev;
	int i;

	for (i = 0; i < DOUBLE; i++) {
		active_dev = &drv->active_vdo[i]->device;
		if (active_dev == NULL)
			continue;

		save_metadata(drv->active_vdo[i], buf);
	}
}

static inline void save_od(struct tnt_device *dev, struct tnt_buf *buf)
{
	struct tnt_metadata *md = metadata_addr(dev, buf);

	/* optical density */
	tnt_od_colors(dev, &md->od);
}

static void save_frames_od(struct tntdrv *drv, struct tnt_buf *buf)
{
	struct tnt_device *active_dev;
	int i;

	for (i = 0; i < DOUBLE; i++) {
		active_dev = &drv->active_vdo[i]->device;
		if (active_dev == NULL)
			continue;
		save_od(active_dev, buf);
	}
	set_last_frame_reference(drv, buf);
}

/* --------------------------------------------------------------------------*/

/* IRQ SR */

static irqreturn_t tnt_busy_catch_isr(int irq, void *dev_id)
{
	struct tntdrv *drv = (struct tntdrv *)dev_id;
	struct tnt_device *dev = &drv->device;
	u64 t_begin_interrupt = ktime_get_ns();

#if 1
	switch (drv->stop) {
	case STOP:
		pr_info("BUSY: stoppped no other buf loaded (last:%d)\n",
			drv->active->vb.index);
		drv->stop = STOP_NOLOAD_BUF;
		drv->o++;
		return IRQ_HANDLED;
	case STOP_NOLOAD_BUF:
		pr_info("BUSY: stoppped busy end %d\n", drv->active->vb.index);
		drv->o++;
		return IRQ_HANDLED;
	}
#endif

	busy_buffer(drv, drv->active);
	set_timestamp(drv->busybuf);
	set_uid(drv, drv->busybuf);
	save_frames_metadata(drv, drv->busybuf);
	load_next_frame(drv);

	/* slot time is t_free - (t_begin_interrupt - t_end_interrupt)  */
	/* t_end_interrupt is caouted inside define_slot_time */
	define_slot_time(drv, drv->access.t_image + t_begin_interrupt);

	tnt_fpga_clear_busy_catch(dev);
	return IRQ_HANDLED;
}

int dqueue_buffer(struct tntdrv *drv, int index);

static irqreturn_t tnt_end_frame_isr(int irq, void *dev_id)
{
	struct tntdrv *drv = (struct tntdrv *)dev_id;
	struct tnt_device *dev = &drv->device;
	u64 t_begin_interrupt = ktime_get_ns();

	drv->oo++;

#if 1
	switch (drv->stop) {
	case STOP:
		pr_info("END: stoppped but %d must be queued\n",
			drv->active->vb.index);
		drv->stop = STOP_NOLOAD_BUF;
		return IRQ_HANDLED;
	case STOP_NOLOAD_BUF:
		pr_info("END: stoppped without do else\n");
		return IRQ_HANDLED;
	}
#endif
	save_frames_od(drv, drv->busybuf);
	/* if is dqueued in  future dqueue it. else */
	queue_to_active(drv->busybuf, drv);
	drv->stats.n++;

	//tnt_optical_density_save(drv);
	/* slot time is t_free - (t_begin_interrupt - t_end_interrupt)  */
	/* t_end_interrupt is counted inside define_slot_time */
	define_slot_time(drv, drv->access.t_free + t_begin_interrupt);
	drv->ttt = t_begin_interrupt;
	wake_up_interruptible_sync(&drv->wait_future_frames);

	tnt_fpga_clear_end_frame(dev);
	return IRQ_HANDLED;
}

/* ----------------------------------------------------- */
/* TRIGGED */

static irqreturn_t tnt_end_cha_isr(int irq, void *dev_id)
{
	struct tntdrv *drv = (struct tntdrv *)dev_id;
	struct tntvdo *vdo = &drv->cha;
	struct tnt_device *dev = &drv->cha.device;

	tnt_fpga_clear_single_frame(dev);
	if (drv->grabmode == TRIGGED_CHA) {
		set_timestamp(drv->active);
		set_uid(drv, drv->active);
		save_metadata(vdo, drv->active);
		save_od(dev, drv->active);
		queue_current_buffer(drv, drv->active);
		set_last_frame_reference(drv, drv->active);
	}
	atomic_set(&vdo->wq_irq, 1);
	wake_up_interruptible_sync(&vdo->wq_head);
	drv->cnt_cha++;
	return IRQ_HANDLED;
}

static irqreturn_t tnt_end_chb_isr(int irq, void *dev_id)
{
	struct tntdrv *drv = (struct tntdrv *)dev_id;
	struct tntvdo *vdo = &drv->chb;
	struct tnt_device *dev = &drv->chb.device;

	atomic_set(&vdo->wq_irq, 1);

	queue_current_buffer(drv, drv->active);
	set_uid(drv, drv->active);
	set_timestamp(drv->active);
	save_metadata(vdo, drv->active);
	save_od(dev, drv->active);
	wake_up_interruptible_sync(&vdo->wq_head);
	set_last_frame_reference(drv, drv->active);

	drv->cnt_chb++;
	tnt_fpga_clear_single_frame(dev);

	return IRQ_HANDLED;
}

/* --------------------------------------------------------------------------*/

static int tnt_set_interrupt(struct tntdrv *drv, int id, char *irq_name,
			     irq_handler_t irqh, int *irq)
{
	struct device_node *of_node = drv->device.dev->of_node;

	*irq = irq_of_parse_and_map(of_node, id);
	if (*irq == 0)
		return -EINVAL;

	if (request_irq(*irq, irqh, 0, irq_name, (void *)drv)) {
		dev_err(drv->device.dev, "Request VIU IRQ failed.\n");
		return -ENODEV;
	}
	return 0;
}

/* ----- */

/**
\brief Retrieve a resource (i.e. IORESOURCE_MEM) 
from device tree by a name from a platform device

\param pdev: point to platform device
\param name: name of resource to get
\return  pointer of resource if present or error.
*/
static struct resource *tnt_get_resource(struct tnt_device *device, char *name)
{
	struct platform_device *pdev =
		container_of(device->dev, struct platform_device, dev);
	return platform_get_resource_byname(pdev, IORESOURCE_MEM, name);
}

static int tnt_map_registers(struct tnt_device *device, char *name)
{
	struct resource *res;
	int size;

	res = tnt_get_resource(device, name);
	if (IS_ERR_OR_NULL(res)) {
		pr_info("Error parsing register area for %s\n", name);
		return -EINVAL;
	}

	size = resource_size(res);
	device->org = devm_ioremap(device->dev, res->start, size);
	//devm_ioremap_resource(device->dev, res);
	if (IS_ERR(device->org)) {
		pr_info("Error mapping register area [ %llx,  %llx ] for %s (%px)\n",
			res->start, res->end, name, device->org);
		return -EINVAL;
	}

	device->regs = regif_init(NULL, name, res, device->org);
	if (device->regs == NULL) {
		pr_info("error map debugfs register\n");
		return -ENOMEM;
	}

	pr_info("mapped %s (%px); start: 0x%llx size: %lld\n", name,
		device->org, res->start, resource_size(res));

	device->res = res;

	return 0;
}

static struct tnt_allocator *tnt_new_allocator(struct resource *res)
{
	struct tnt_allocator *alc;

	alc = kzalloc(sizeof(*alc), GFP_KERNEL);
	if (alc == NULL) {
		return NULL;
	}

	allocator_init(alc, res, NULL);

	return alc;
}

/**
\brief Called once before the buffer is freed.
Drivers may perform any additional cleanup.

\param vb: pointer to allocated buffer
\return 
*/
static __maybe_unused void tnt_planes_cleanup(struct vb2_buffer *vb)
{
	struct tntdrv *drv = vb2_get_drv_priv(vb->vb2_queue);
	struct tnt_allocator *allocator = drv->allocator;
	struct alc_chunk *chunk;
	int plane;

	for (plane = 0; plane < vb->num_planes; plane++) {
		chunk = vb->planes[plane].mem_priv;
		alc_release_chunk(allocator, vb->planes[plane].mem_priv);
	}
}

static int tnt_pool_init(struct tntdrv *drv)
{
	struct device *dev = drv->device.dev;
	struct device_node *np;
	struct resource res;
	int rc;

	/* Get reserved memory region from Device-tree */
	np = of_parse_phandle(dev->of_node, "memory-region", 0);
	if (!np) {
		dev_err(dev, "No %s specified\n", "memory-region");
		return -EINVAL;
	}

	rc = of_address_to_resource(np, 0, &res);
	if (rc) {
		dev_err(dev, "No memory address assigned to the region\n");
		return -EINVAL;
	}

	drv->allocator = tnt_new_allocator(&res);
	if (drv->allocator == NULL) {
		dev_err(dev, "Allocator alloc error\n");
		return -ENOSPC;
	}

	/*  test */
	/* Get reserved memory region from Device-tree */
#if 0 
    rc = of_reserved_mem_device_init_by_idx(dev, dev->of_node, 1);
    if(rc) {
        dev_err(dev, "Could not get reserved memory\n");
        return -EINVAL;
    }
    
    /* Allocate memory */
    dma_set_coherent_mask(dev, DMA_BIT_MASK(64));
    vaddr = dma_alloc_coherent(dev, 100, &paddr, GFP_KERNEL);
    pr_info("Allocated coherent memory, vaddr: 0x%0llX, paddr: 0x%0llX\n", (u64)vaddr, paddr);
    v = (char*)vaddr;
    for(rc=0; rc<10; rc++)
        v[rc] = '*';
#endif
	return 0;
}

/* ------------------------------------ */

static void tnt_release_map(struct tnt_device *device)
{
	if (device->regs)
		regif_free(device->regs);

	iounmap(*(void __iomem **)device->res);
}

static void tnt_release_area(struct tntdrv *drv)
{
	int i;
	struct tntvdo *vdo;

	for (i = 0; i < DOUBLE; i++) {
		vdo = drv->active_vdo[i];
		if (vdo == NULL)
			continue;
		tnt_release_map(&vdo->device);
	}
}

/**
\brief Map memory address area from fpga

\param drv: pointer to tntdrv driver struc
\return 0 or errno
*/
static int tnt_map_area(struct tntdrv *drv)
{
	int ret = 0;

	/* register CHA  memory map */

	/* ret = tnt_map_registers(&drv->chb.device, "chb"); */
	/* if(ret <0) */
	/*     return -EINVAL; */

	/* resgister FPGA GLOBAL  */
	ret = tnt_map_registers(&drv->device, "global");
	if (ret < 0)
		return -EINVAL;

	return ret;
}

/* Init  */

static int tnt_drv_init(struct tntdrv *drv, struct device_ops *ops)
{
	struct tnt_device *dev = &drv->device;
	//tnt_set_grabmode(drv, TRIGGED_CHB);
	tnt_write(dev, RST_CNT_CHA, 1);
	tnt_write(dev, RST_CNT_CHB, 1);
	tnt_fpga_reset_counters(dev);
	dev->ops = ops;

	return 0;
}

static void tnt_init_isr_waitqueue(struct tntdrv *drv)
{
	mutex_init(&drv->cha.wq_mutex);
	init_waitqueue_head(&drv->cha.wq_head);
	atomic_set(&drv->cha.wq_irq, 0);

	mutex_init(&drv->chb.wq_mutex);
	init_waitqueue_head(&drv->chb.wq_head);
	atomic_set(&drv->chb.wq_irq, 0);
}

#define N_CHANNELS (2)

static inline void free_allowed_sensors(struct tntdrv *drv)
{
	if (drv->cha.allowed_sensors)
		kfree(drv->cha.allowed_sensors);

	if (drv->chb.allowed_sensors)
		kfree(drv->chb.allowed_sensors);
}

static inline int tnt_enable_interrupts(struct tntdrv *drv)
{
	int ret = 0;

	ret = tnt_set_interrupt(drv, 0, "tnt_busy_catch", tnt_busy_catch_isr,
				&drv->busy_catch);
	if (ret < 0)
		goto end_int;
	pr_info("Load busy_catch IRQ# %d\n", drv->busy_catch);

	ret = tnt_set_interrupt(drv, 1, "tnt_end_frame", tnt_end_frame_isr,
				&drv->end_frame);
	if (ret < 0)
		goto end_int;
	pr_info("Load end_frame IRQ# %d\n", drv->end_frame);

	ret = tnt_set_interrupt(drv, 2, "tnt_end_cha", tnt_end_cha_isr,
				&drv->end_cha_frame);
	if (ret < 0)
		goto end_int;
	pr_info("Load end_cha_frame IRQ# %d\n", drv->end_cha_frame);

	ret = tnt_set_interrupt(drv, 3, "tnt_end_chb", tnt_end_chb_isr,
				&drv->end_chb_frame);
	if (ret < 0)
		goto end_int;
	pr_info("Load end_chb_frame IRQ# %d\n", drv->end_chb_frame);

end_int:
	return ret;
}

static inline struct tntvdo *install_vdo(struct tntdrv *drv, struct tntvdo *vdo,
					 int status, int id, char *name)
{
	vdo->id = id;
	if (status > 0) {
		drv->active_vdo[id] = NULL;
		return NULL;
	}

	drv->installed_vdo[id] = vdo;
	vdo->device.name = name;

	return vdo;
}

int strobe_init(struct tntdrv *drv);
void strobe_put(struct tnt_strobe *strb);

/**
\brief 

\param op:
\return 
*/
static int tntdrv_of_probe(struct platform_device *op)
{
	struct device *dev = &op->dev;
	struct tntdrv *drv;
	struct tntvdo *vdos[N_CHANNELS];
	int ret;
	int ch = 0;

	/* Allocate the driver data region */
	/* Memory allocated with this function is automatically freed on driver detach */
	drv = (struct tntdrv *)devm_kzalloc(dev, sizeof(struct tntdrv),
					    GFP_KERNEL);
	if (!drv) {
		dev_err(dev, "Couldn't allocate tntvdo struct\n");
		return -ENOMEM;
	}

	pr_info("%s: v%s\n", THIS_MODULE->name, THIS_MODULE->version);
	vdos[0] = NULL;
	vdos[1] = NULL;
	drv->cha.id = 0;
	drv->cha.name = "cha"; /* default name */
	drv->cha.cntx_id = -1;

	drv->chb.id = 1;
	drv->chb.name = "chb";
	drv->chb.cntx_id = -1;

	drv->stop = 1;
	drv->device.dev = dev;

	ret = tnt_get_available_sensors(drv);
	if (ret) {
		dev_info(drv->device.dev, "Error parsing device tree");
		return -EINVAL;
	}
	//tnt_print_available_sensors(drv);

	ret = tnt_get_available_processor(drv);
	if (ret) {
		dev_info(drv->device.dev, "Error parsing device tree");
		return -EINVAL;
	}
	print_processor_info(drv);

	/* Yes, but with 2 vdo channel I may unrooll them */
	ret = tntof_hook_vdo(drv, &drv->cha, "/tnt/channel@0");
	if (ret < 0)
		goto error_hook;
	if (ret == 0)
		vdos[0] = install_vdo(drv, &drv->cha, ret, 0, "CHA");

	ret = tntof_hook_vdo(drv, &drv->chb, "/tnt/channel@1");
	if (ret < 0)
		goto error_hook;
	if (ret == 0)
		vdos[1] = install_vdo(drv, &drv->chb, ret, 1, "CHB");

	/* exit if both channel are disabled */
	if (vdos[0] == NULL && vdos[1] == NULL)
		goto error_hook;

	dev_set_drvdata(dev, drv);

	/* map fpga memory registers */
	ret = tnt_map_area(drv);
	if (ret < 0)
		goto error_mem;

	/* Pool */
	tnt_pool_init(drv);

	tnt_init_isr_waitqueue(drv);

	/* interrupt */
	ret = tnt_enable_interrupts(drv);
	if (ret < 0)
		goto error_int;

	/* v4l2 stuff for cha and chb initialization */
	for (ch = 0; ch < N_CHANNELS; ch++) {
		if (vdos[ch] == NULL) {
			continue;
		}
		ret = tnt_map_registers(&vdos[ch]->device, vdos[ch]->name);
		if (ret < 0)
			return -EINVAL;

		ret = tnt_video_init(vdos[ch], vdos[ch]->name);
		if (ret < 0)
			goto error_v4l2;

		/* ctrls */
		vdos[ch]->video_dev.queue = &drv->queue;
		ret = vdo_ctrl_init(vdos[ch]);
		if (ret) {
			pr_err("Error Initializing controls\n");
			goto error_queue;
		}

		ret = tnt_register_processors(vdos[ch]);
		if (ret)
			goto error_queue;
		pr_info("vdo#%d: address=%px\n", ch, vdos[ch]);

		mutex_init(&vdos[ch]->m);
	}

	INIT_LIST_HEAD(&drv->active_list);
	INIT_LIST_HEAD(&drv->dqueued_list);

	ret = tnt_init_queue(drv, N_MIN_BUFS);
	if (ret) {
		dev_err(dev, "Failed to initialize vb2 queue\n");
		goto error_queue;
	}

	/* init  stuff */
	ret = tnt_drv_init(drv, &imx265fpga);
	if (ret) {
		goto error_queue;
	}

	/* TODO: make rle as processor */
	/* ret = tnt_rle_init(drv); */
	/* if(ret){ */
	/*     goto error_queue; */
	/* } */

	/* Wait queues */
	init_waitqueue_head(&drv->wait_future_frames);
	slot_init(&drv->access, BLIND_TIME); /* nS */

	trace_queue_lists(drv, &drv->queue_trace);
	strobe_init(drv);

	/* init stats  stuff */
	ret = tnt_stats_init(&drv->stats, &drv->queue_trace);
	if (ret) {
		goto error_queue;
	}

	dev_info(dev, "Module probed %px\n", drv);
	spin_lock_init(&drv->last_lock);
	spin_lock_init(&drv->list_lock);

	return 0;

error_int:
error_queue:
	for (; ch; ch--)
		video_device_release(&vdos[ch - 1]->video_dev);

error_mem:
	tnt_release_area(drv);

error_hook:
	free_allowed_sensors(drv);
error_v4l2:
	return ret; /* Success */
}

static void tnt_release_chunks(struct tntdrv *drv)
{
	struct vb2_queue *q = &drv->queue;
	struct vb2_buffer *vb;
	int i;

	for (i = 0; i < q->num_buffers; i++) {
		vb = q->bufs[i];
		tnt_planes_cleanup(vb);
	}
}

static int tntdrv_of_remove(struct platform_device *op)
{
	struct device *dev = &op->dev;
	struct tntdrv *drv = dev_get_drvdata(dev);
	struct tntvdo *vdo;
	int i;

	if (drv) {
		free_queue_trace(drv);

		/* release alc_chunk */
		tnt_release_chunks(drv);
		allocator_destroy(drv->allocator);
		tnt_fpga_shutdown_continuous(&drv->device);
		free_allowed_sensors(drv);
		free_irq(drv->end_frame, drv);
		free_irq(drv->busy_catch, drv);
		free_irq(drv->end_cha_frame, drv);
		free_irq(drv->end_chb_frame, drv);
		slot_del(&drv->access);
		for (i = 0; i < DOUBLE; i++) {
			vdo = drv->installed_vdo[i];
			if (vdo == NULL) {
				continue;
			}
			v4l2_device_unregister_subdev(&vdo->sensor);
			video_unregister_device(&vdo->video_dev);
			v4l2_device_unregister(&vdo->v4l2_dev);
			tnt_release_processors(vdo);
			tnt_release_map(&vdo->device);
		}
		/* global */
		tnt_release_map(&drv->device);
		strobe_put(&drv->strobe);
		tnt_stats_release(&drv->stats);

		pr_info("Module Freed\n");
	}
	pr_info("Module removed\n");

	return 0;
}

/* Match table for of_platform binding */
static struct of_device_id tntdrv_of_match[] = {
	{
		.compatible = "tnt,video",
	},
	{},
};

MODULE_DEVICE_TABLE(of, tntdrv_of_match);

static struct platform_driver tntdrv_of_driver = {
    .probe  = tntdrv_of_probe,
    .remove = tntdrv_of_remove,
    .driver = {
        .name  = DRIVER_NAME,
        .owner = THIS_MODULE,
        .of_match_table = tntdrv_of_match,
    },
};

module_platform_driver(tntdrv_of_driver);
MODULE_AUTHOR("Giandomenico Rossi <gdrossi@hotmail.com>");
MODULE_DESCRIPTION("Video Driver");
MODULE_LICENSE("GPL v2");
MODULE_VERSION("0.39.5");
//MODULE_INFO(vermagic, "4.19.0+ SMP mod_unload aarch64");

#if 0
    pr_info("org: %px prev: %px next: %px\n", &drv->active_list,
            drv->active_list.prev,  drv->active_list.next);

    pr_info("detach: %px prev: %px next:%px state: %d\n", &drv->active->active_entry,
            drv->active->active_entry.prev, drv->active->active_entry.next, drv->active->vb.state);
    
    list_for_each_entry(b1, &drv->active_list, active_entry){
        pr_info("** buf: %d %px prev: %px next: %px\n", b1->vb.index, &b1->active_entry,
                b1->active_entry.prev,  b1->active_entry.next);
    }
#endif
