/** 
 \file tnt-stats.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2021  s.r.l.
 

 \brief
*/

#ifndef _TNT_STATS_H
#define _TNT_STATS_H

#include <linux/types.h>
#include "debugfs/tdm.h"

#define WINDOW_SIZE 64

struct tnt_avg {
	u64 window[WINDOW_SIZE];
	u64 fps;
	u64 sum;
	u32 period;
	u32 curr;
};

struct tnt_stats {
	struct tnt_avg avg;
	struct tdm_debug dbg;
	struct timer_list timer;
	int n;
};

int tnt_stats_init(struct tnt_stats *stats, struct tdm_debug *parent);
void tnt_stats_start(struct tnt_stats *stats);
void tnt_stats_release(struct tnt_stats *stats);
void tnt_stats_reset(struct tnt_stats *stats);
int ioctl_stats(struct file *filp, unsigned int cmd, unsigned long arg);

#endif
