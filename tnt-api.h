
enum sensor_code {
	CMOS_IMX265_BW = 162,
	CMOS_IMX265_COL = 163,
	CMOS_IMX250_BW = 164,
	CMOS_IMX250_COL = 165,
	CMOS_IMX250_BW_POLARIZED_CROPPED_3MP = 166,
	CMOS_IMX250_COL_POLARIZED_CROPPED_3MP = 167,
};

struct tcd_sensor {
	char *name; /**!< sendor name "ocr", "ctx" ...  */
	enum sensor_code code; /**!< ccd sendsor code   */
	char *subdev; /**!< path to device filename /dev/v4l2-subdev  */
};

struct tcd_channel {
	struct tcd_sensor *sensor;
	....
};

struct tcd_grabber {
	struct tcd_channel channels[2];
	....
};

/**
\brief Attach specific sensor to specific channel 
channel.

\param channel: pointer to struct  channel 
\param sensor: pointer to struct sensor
\return 
*/
void grabber_attach(struct tcd_channel *channel, struct tcd_sensor *sensor);

/**
\brief Set a value grabber property to the grabber.
Possile properties are:
+ "mode"    value ::= TRIGGED | CONTINUOUS
+ "source"  value ::= CHA | CHB | BOTH_CH 
+ "rle"     value ::= ENABLE | DISABLE

\param grabber: pointer to struct grabber 
\param grab_prop: property name 
\parma value: generic pointer to a value 
\return 0 or error code
*/
int grabber_set_property(struct tcd_grabber *grabber, char *grab_prop,
			 void *value);

struct tnt_frames {
	uint32_t n;
	int *id;
};

enum search_criteria {
	BY_ID = 0,
	BY_UID,
	BY_TIMESTAMP,
};

struct search_mode {
	uint64_t at; /**!< point or the Neighbourhood */
	uint64_t before; /**!< number of units before Neighbourhood  */
	uint64_t after; /**!< number of units after Neighbourhood  */
};

/**
\brief Locks a set of frames from grabber using a search criteria.
+ by uid
+ by id
+ by timestamp

The center and the border of search must be specified in struct search mode

\param criteria: type of search
\param mode: the mode of search
\return NULL or a pointer containing image id
*/
struct tnt_frames *grabber_lock(struct tcd_grabber *grabber,
				enum search_criteria criteria,
				struct search_mode *mode);

enum order_criteria { BY_ID = 0, BY_UID, BY_TIMESTAMP, RAW };

enum order_mode { CRESCENT = 0, DECRESCENT };

/**
\brief Get a subset of loked frames.
The returned subset could be sorted ( decrecent or crescent) by
+ ID
+ UID
+ TIMESTAMP
+ NOT ORDERED

\note: ONLY here a mmap will be automatically done!!!!

\param grabber: pointer to struct grabber
\param n: number of requested elements of set
\param tnt_frames: the set of locked frames
\param crit: sort criteria
\param mode: the mode or sorting
\return an array of struct images
*/
struct images *grabber_get_images(struct tcd_grabber *grabber, int n,
				  struct tnt_frames, enum order_criteria crit,
				  enum order_mode mode);

/**
\brief Unlocks all locked frames by grabber_lock.
Makes unmmap and free!!!

\param grabber: pointer to struct grabber
\param frames: poiner of locked images array
\param imgs: pointer to subset of images.
\return 
*/
void grabber_release(struct tct_grabber *grabber, struct tnt_frames *frames,
		     struct images *imgs);

/**
\brief start streaming

\param n_buffer: Optionally specify the number of buffer 
to use 
\return  0 or error code
*/
int grabber_start(int n_buffer = MAX_BUFFER)
