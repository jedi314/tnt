/** 
 \file tnt-acq-points.h
 
 

 \brief
*/

#ifndef _TNT_ACQ_POINTS_H
#define _TNT_ACQ_POINTS_H

inline void load_acquisition_points(struct tntvdo *vdo);
inline void acq_point_copy(struct acq_point *dest, struct acq_point *source);
int set_acquisition_points(struct tntvdo *vdo, struct acq_points *ap);

#endif
