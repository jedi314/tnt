/** 
 \file tnt-regs.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_REGS_H_
#define _TNT_REGS_H_

#include "debugfs/tdm.h"

struct tnt_reg {
	struct tdm_debug dbg;
	void *__iomem base;
	struct resource *range;
	int mask;
	int reg;
};

struct tnt_reg *regif_init(struct tnt_reg *reg, char *name,
			   struct resource *range, void *base);
void regif_free(struct tnt_reg *reg);

#endif
