/** 
 \file tnt-allocator.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief The tnt_allocator manage the per-fpga reserved
 memeory.
 The layout of this region ia about the allocator.
 It divides  the meory in chunks of configurable size

 + Needs an array  of allocator_ops  for custom operation.
 + Keeps the list of dma_adddr_t (phys address) allocated.
 + Checks blocks are PAGE_SIZE bounded.
 
 + Export /dev/allocator char(or block ) device ?

 chunk: a single block of memory requested by TNT and picked up 
 from  a specific ctx.

 ctx: a set of blocks gruped.
 The size of a ctx is N_chunk x chunk_size.
                                      +------ alc_ctx.free
                                      |
                                      v
 +-----------+-----------+-----------+-----------+-----------+
 |           |           |           |           |           |          
 |           |           |           |           |           |
 |  Chunk#1  |  Chunk#2  |  Chunk#3  |  Free Cnk |  Free Cnk | ctx#0
 |           |           |           |           |           |
 |           |           |           |           |           |
 +---------+---------+---------+---------+--------+----------+ 
 |         |         |         |         |        |
 |         |         |         |         |        |
 | Chunk#1 | Chunk#2 | Chunk#3 | Chunk#4 |  Free  | Ctx#1
 |         |         |         |         |        |
 |         |         |         |         |        |
 +---------+---------+---------+---------+---+----+ 
 |                                           |<--- tnt_allocator.free
 |                                           |
 |                                           |
 |                Free Memory                |
 |                                           |
 |                                           |
 |                                           |
 +-----------+-----------+-----------+-------+ 


*/

#include <linux/string.h>
#include <linux/uaccess.h>
#include <asm/io.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/io.h>
#include "tnt-allocator.h"

#include "debugfs/trace.h"

#define FREE_SPACE(a, b) ((a)->org + (a)->b - (a)->free)
//#define CTX_SIZE(a) ((a)->o

#define DEVICE_NAME "allocator" /*!< The device will appear at /dev/allocator */
#define CLASS_NAME                                                             \
	"allocator" /*!< The device class -- this is a character device driver */

static void init_ctx(struct alc_ctx *s, int size, struct ctx_ops *ops)
{
	s->free = s->org;
	s->chunk_size = size;
	s->ops = ops;
}

static unsigned long ctx_offset(struct tnt_allocator *alc, int id)
{
	struct alc_ctx *ctx;
	unsigned long off = 0;

	ctx = &alc->ctxs[id];
	//for(i=0; i < id; i++)
	//    off += alc->ctxs[i].size;

	off = ctx->org - alc->org;
	return off;
}

static void *ctx_vaddr(struct tnt_allocator *alc, int id)
{
	unsigned long off = 0;

	off = ctx_offset(alc, id);
	return (void *)((char *)alc->vaddr + off);
}

/**
\brief Get the ctx id of an address.
The address is referred to the strip
it belongs to

\param alc: allocator pointer.
\param org:address
\return id or -EFAULT
*/
static inline int ctx_id(struct tnt_allocator *alc, dma_addr_t org)
{
	int i;

	for (i = 0; i < alc->n_ctxs; i++) {
		if (org < alc->ctxs[i].org) {
			return i - 1 > 0 ? i : 0;
		}
	}
	return i - 1;
}

/**
\brief return ctx address from its id.

\param al: pointer to allocator
\param id: identifier
\return 
*/
struct alc_ctx *ctx_addr(struct tnt_allocator *al, int id)
{
	if (id > al->n_ctxs)
		return (struct alc_ctx *)ERR_PTR(-EINVAL);

	return &al->ctxs[id];
}

/**
\brief Convert physcal address of ctx strip
to it kernel virtual address

\param alc:  allocator pointer
\param org:  phys address
\return address or NULL in case of error.
*/
static void *ctx_phy_to_virt(struct tnt_allocator *alc, dma_addr_t org)
{
	int id;

	id = ctx_id(alc, org);
	if (id < 0)
		return NULL;

	return alc->ctxs[id].vaddr;
}

static inline u64 chunk_offset(struct alc_ctx *ctx, dma_addr_t addr)
{
	if (ctx->org + ctx->size < addr)
		return -1;

	return addr - ctx->org;
}

static void *chunk_phys_to_virt(struct alc_ctx *ctx, dma_addr_t addr)
{
	u64 offset;

	offset = chunk_offset(ctx, addr);
	if (offset == -1)
		return ERR_PTR(-EINVAL);

	return (char *)ctx->vaddr + offset;
}

u64 alc_chunk_offset(struct tnt_allocator *alc, int id, u64 off)
{
	u64 offset;

	offset = off;
	/* pr_info("??? %d offset: %llx off:%llx org: %llx porg:%llx \n", */
	/*         id, offset, off, alc->ctxs[id].org, (alc->ctxs[id].org >> PAGE_SHIFT)) ; */

	if (offset > alc->size)
		return -1;

	return offset;
}

/*  MMAP ops */

static void remap_open(struct vm_area_struct *vma)
{
	struct alc_fh *fh = vma->vm_private_data;
	struct tnt_allocator *alc = fh->alc;
	long size = (vma->vm_end - vma->vm_start) >> 2;
	dma_addr_t org = vma->vm_pgoff << PAGE_SHIFT;
	void *vaddr = ctx_phy_to_virt(alc, org);
	//int i;
	//int pattern = 0xDEADBEEF;

	/* TODO: check ranges */

	//mutex_lock(&alc->lock);

	pr_info("vaddr %px (0x%llx) size: %ld\n", vaddr, org, size);
	/* for(i=0; i<size; i++){ */
	/*     ((int*)vaddr)[i] = pattern; */
	/* } */

	//mutex_unlock(&alc->lock);

	//memset32(vaddr, pattern, size);
}

static void remap_close(struct vm_area_struct *area)
{
}

static struct vm_operations_struct remap_ops = { .open = remap_open,
						 .close = remap_close };

/* -------------------------- */

/**
\brief Alloc a mememory region used to require chunk.
The funtion returns a ctx_id used to get  chunks.
\note: The ctx size is PAGE_SIZE bounded, then the value 
of chunk_size is rounded PAGE_SIZE


\param al: pointer to allocator context
\param n_chunk: number of chunks per ctx
\param chunk_size: pointer to size of chunck
\return  ctx id or a negative error code 
*/
int alloc_ctx(struct tnt_allocator *al, const char *name, int n_chunk,
	      int *chunk_size, struct ctx_ops *ops)
{
	int ctx_size;
	struct alc_ctx *ctx;
	int chunk_pages;
	struct tdm_watch wctx[] = { { "org", 0400, TDM_XINT64 },
				    { "free", 0400, TDM_XINT32 },
				    { "size", 0400, TDM_UINT32 },
				    { "chunk_size", 0400, TDM_UINT32 },
				    { "n_chunks", 0400, TDM_UINT32 },
				    {} };

	if (n_chunk == 0)
		return 0;

	/* chunk size is bounded to PAGE_SIZE */
	chunk_pages = (*chunk_size >> PAGE_SHIFT) + 1;

	/* 
       if chunk size is not aligned to PAGE_SIZE
       (i.e chunk_size % PAGE_SIZE != 0 then
       another page is added for make room
       to metadata
    */
	if (*chunk_size & (PAGE_SIZE - 1))
		chunk_pages++;

	*chunk_size = chunk_pages * PAGE_SIZE;
	ctx_size = n_chunk * *chunk_size;

	if (ctx_size > FREE_SPACE(al, size)) {
		return -ENOMEM;
	}

	if (al->n_ctxs == TNT_MAX_CTXS) {
		return -EBADF;
	}

	ctx = &al->ctxs[al->n_ctxs];
	ctx->org = al->free;
	ctx->size = ctx_size;
	al->free += ctx_size;
	ctx->id = al->n_ctxs;

	init_ctx(ctx, *chunk_size, ops);
	if (al->ops != NULL && al->ops->init)
		al->ops->init(ctx);
	ctx->vaddr = ctx_vaddr(al, al->n_ctxs);

	if (name == NULL)
		return -EINVAL;

	ctx->dbg.parent = al->dbg.dir;
	ctx->dbg.dirname = (char *)name;

	/* watcher pointers */
	wctx[0].opt.param = &ctx->org;
	wctx[1].opt.param = &ctx->free;
	wctx[2].opt.param = &ctx->size;
	wctx[3].opt.param = &ctx->chunk_size;
	wctx[4].opt.param = &ctx->n_chunks;

	tdm_init(&ctx->dbg);
	tdm_append_watches(&ctx->dbg, &wctx[0], 0);

	pr_info("\nCreated Strip [%s] @ phys_addr: %llx, vdaddr %llx - n: %d chunk_size %d, total_size: %d; id=%d\n",
		name, (u64)ctx->org, (u64)ctx->vaddr, n_chunk, *chunk_size,
		ctx->size, al->n_ctxs);

	return al->n_ctxs++;
}

static void __release_ctx(struct tnt_allocator *alc, struct alc_ctx *ctx)
{
	if (ctx->org == 0)
		return;

	alc->free -= ctx->size;
	alc->n_ctxs--;
	memset(ctx, 0, sizeof(*ctx));
}

int release_ctx(struct tnt_allocator *alc, unsigned int id)
{
	struct alc_ctx *ctx;

	//mutex_lock(&alc->lock);
	if (alc->n_ctxs <= 0 || id > TNT_MAX_CTXS) {
		return -EINVAL;
	}

	pr_info("Releasing ctx# id=%d [%d]\n", id, alc->n_ctxs);

	ctx = &alc->ctxs[id];
	debugfs_remove_recursive(ctx->dbg.dir);
	__release_ctx(alc, ctx);

	//mutex_unlock(&alc->lock);

	return 0;
}

void allocator_release_ctxs(struct tnt_allocator *alc)
{
	int i;

	for (i = alc->n_ctxs - 1; i >= 0; i--) {
		__release_ctx(alc, &alc->ctxs[i]);
	}
}

/**
\brief Allocate a new chunk of a specific ctx.

\param al: Pointer to allocator
\param id: Pointer to ctx id
\return physical addres of new chunk or error.
*/
struct alc_chunk *alc_alloc_chunk(struct tnt_allocator *al, int id)
{
	struct alc_ctx *ctx;
	struct alc_chunk *chunk;

	if (id > al->n_ctxs)
		return (struct alc_chunk *)ERR_PTR(-ENOMEM);

	ctx = ctx_addr(al, id);
	if (IS_ERR_OR_NULL(ctx)) {
		return (struct alc_chunk *)ERR_PTR(-EINVAL);
		;
	}
	if (FREE_SPACE(ctx, size) < ctx->chunk_size) {
		return (struct alc_chunk *)ERR_PTR(-ENOMEM);
	}

	chunk = kzalloc(sizeof(*chunk), GFP_KERNEL);
	chunk->addr = ctx->free;

	if (ctx->ops && ctx->ops->init) {
		ctx->ops->init(chunk);
	}

	ctx->free += ctx->chunk_size;
	ctx->n_chunks++;
	chunk->vaddr = chunk_phys_to_virt(ctx, chunk->addr);

	return chunk;
}

#if 0
static
int get_chunk_id(struct tnt_allocator *al, dma_addr_t chunk, int *s)
{
    int id;
    struct alc_ctx* ctx;

    if(chunk < al->org || chunk > al->org + al->size)
        return -EFAULT;

    for(id = 0; id < al->n_ctxs; id++){
        ctx = &al->ctxs[id];
        if(chunk < ctx->org){
            *s=id - 1;
            return (chunk - ctx->org)/ctx->chunk_size;
        }
    }
    return -EINVAL;
 }

#endif

int alc_release_chunk(struct tnt_allocator *al, struct alc_chunk *chunk)
{
	kfree(chunk);
	/* TODO free chunk must be added */
	return 0;
}

static int allocator_open(struct inode *inode, struct file *file)
{
	struct tnt_allocator *alc =
		container_of(inode->i_cdev, struct tnt_allocator, cdev);
	struct alc_fh *fh;

	fh = kzalloc(sizeof(*fh), GFP_KERNEL);
	if (!fh) {
		pr_info("Error allocating struct alc_fh\n");
		return -ENOMEM;
	}

	fh->alc = alc;
	file->private_data = fh;
#if 0
    {
        int i;
        for(i=524697600 ; i < 524697600+419840000 ; i++){
            ((char*)alc->vaddr)[i] = '*';
            if(i % 0xa00000 == 0)
                pr_info("off %llx\n", i);
        }
    }
#endif

	return 0;
}

static int allocator_release(struct inode *inode, struct file *file)
{
	struct alc_fh *fh = file->private_data;
	struct tnt_allocator *alc = fh->alc;
	int i;

	//pr_info("release ctx# %d\n",
	mutex_lock(&alc->lock);
	for (i = 0; i < fh->n; i++) {
		release_ctx(alc, fh->id[i]);
	}
	kfree(fh);
	mutex_unlock(&alc->lock);

	return 0;
}
static ssize_t allocator_read(struct file *file, char __user *buf, size_t len,
			      loff_t *pos)
{
	pr_info("Read\n");
	return 0;
}

static ssize_t allocator_write(struct file *file, const char __user *buf,
			       size_t len, loff_t *pos)
{
	pr_info("Write\n");
	return 0;
}

static int allocator_mmap(struct file *file, struct vm_area_struct *vma)
{
	struct alc_fh *fh = file->private_data;
	//struct tnt_allocator *alc = fh->alc;
	int size;
	int ret;
	//struct alc_ctx *ctx;

	/* TODO check limits and round strip */
	size = vma->vm_end - vma->vm_start;

	pr_info("Mmap Start: %lx End: %lx, Off: %lx\n", vma->vm_start,
		vma->vm_end, vma->vm_pgoff << PAGE_SHIFT);

	ret = remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size,
			      vma->vm_page_prot);
	if (ret) {
		pr_info("remap_pfn_range error (%d)\n", ret);
		return -EINVAL;
	}

	vma->vm_ops = &remap_ops;
	vma->vm_private_data = fh;
	vma->vm_ops->open(vma);

	return 0;
}

/**
\brief Reset the allocator and remove all 
its ctx
 
\param alc: poiner to allocator.
*/
void allocator_reset(struct tnt_allocator *alc)
{
	int i;

	for (i = 0; i < alc->n_ctxs; i++) {
		release_ctx(alc, i);
	}

	alc->free = alc->org;
	alc->n_ctxs = 0;
}

static long allocator_ioctl(struct file *file, unsigned int cmd,
			    unsigned long arg)
{
	struct alc_fh *fh = file->private_data;
	struct tnt_allocator *alc = fh->alc;
	struct alc_memory mem;
	struct alc_ctx *ctx;
	int ret = 0;

	ret = copy_from_user(&mem, (char *)arg, sizeof(mem));
	if (ret < 0) {
		pr_info("Error passing parameter\n");
		return -1;
	}

	switch (cmd) {
	case TNT_ALLOC_CTX:
		ret = alloc_ctx(alc, mem.name, mem.count, &mem.size, NULL);
		if (ret < 0) {
			pr_info("alloc_ctx: error allocate strip (%d)\n", ret);
			return ret;
		}
		fh->id[fh->n++] = ret;
		mem.id = ret;
		mem.addr = alc->ctxs[ret].org;
		ret = copy_to_user((char *)arg, &mem, sizeof(mem));
		break;
	case TNT_INIT_CTX:
		if (mem.id > alc->n_ctxs)
			return -EINVAL;
		ctx = &alc->ctxs[mem.id];
		memset((void *)ctx->vaddr, mem.val, ctx->size);
		break;
	case TNT_RESET_ALLOCATOR:
		pr_info("RESET ALLCATOR\n");
		allocator_reset(alc);
		break;
	default:
		pr_info("ioctl command error\n");
		return -ENOTTY;
	}

	return ret;
}
const struct file_operations allocator_fops = { .owner = THIS_MODULE,
						.open = allocator_open,
						.read = allocator_read,
						.write = allocator_write,
						.release = allocator_release,
						.unlocked_ioctl =
							allocator_ioctl,
						.mmap = allocator_mmap };

/**
\brief Create a char device for allocator 
in /sys/CLASS_NAME/DEVICE_NAME

\param alc: pointer of alloctar
\return 
*/
static void allocator_chrdev(struct tnt_allocator *alc,
			     const struct file_operations *fops)
{
	int err;
	struct class *class;
	struct device *dev;
	struct cdev *cdev = &alc->cdev;
	dev_t devid;

	err = alloc_chrdev_region(&devid, 0, 1, DEVICE_NAME);
	if (err != 0) {
		pr_info(KERN_ALERT
			"Registering char device allocator failed with %d\n",
			err);
		return;
	}

	alc->devid = devid;
	cdev_init(cdev, fops);
	err = cdev_add(cdev, devid, 1);
	if (err) {
		goto cdev_err;
	}

	/* create class */
	class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(class)) { /* Check for error and clean up if there is */
		pr_info("Failed to register device class\n");
		goto class_err; /* Correct way to return an error on a pointer */
	}

	alc->class = class;

	/* Register the device driver */
	/* save tnt_allocator pointer to driver data */
	dev = device_create(class, NULL, devid, alc, DEVICE_NAME);
	if (IS_ERR(dev)) { /* Clean up if there is an error   */
		pr_info("Failed to create the device\n");
		goto dev_err;
	}
	alc->dev = dev;
	return;

dev_err:
	class_destroy(class);
class_err:
	unregister_chrdev(MAJOR(devid), DEVICE_NAME);
cdev_err:
	unregister_chrdev_region(devid, 1);
}

/**
\brief  Initilize allocator appending  its  custom 
ops set. Could be NULL.

\param org: Address or reserved region
\param size: size of reserved region
\param ops: pointer of cutomized op

*/
void allocator_init(struct tnt_allocator *alc, struct resource *res,
		    struct alc_ops *ops)
{
	struct tdm_watch ctx[] = { { "n_strips", 0400, TDM_UINT32 }, {} };

	memset(alc, 0, sizeof(*alc));

	alc->org = res->start;
	alc->size = resource_size(res);
	pr_info("allocator: start: 0x%llx, end: 0x%llx, size: %u\n", res->start,
		res->end, alc->size);
	alc->vaddr = ioremap_wc(alc->org, alc->size);
	//alc->vaddr = ioremap_nocache(alc->org, alc->size );
	//alc->vaddr = memremap(alc->org, alc->size, MEMREMAP_WB);
	alc->free = alc->org;
	alc->ops = ops;
	alc->dbg.dirname = DEVICE_NAME;

	mutex_init(&alc->lock);

	tdm_init(&alc->dbg);
	tdm_append_watches(&alc->dbg, &ctx[0], &alc->n_ctxs);

	pr_info("allocator: vaddr @ 0x%0llX, org @ 0x%0llX - size: %u, "
		"free pointer: %llx vaddr: 0x%px",
		(u64)alc->vaddr, (u64)alc->org, alc->size, alc->free,
		phys_to_virt(alc->org));

	allocator_chrdev(alc, &allocator_fops);
}

void allocator_destroy(struct tnt_allocator *alc)
{
	//pr_info("Removed Recursive direcotory %s\n", alc->dbg.dirname);
	debugfs_remove_recursive(alc->dbg.dir);

	cdev_del(&alc->cdev);
	device_destroy(alc->class, alc->devid); /* remove the device */
	unregister_chrdev_region(alc->devid, 1);
	class_destroy(alc->class);
}
