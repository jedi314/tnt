/** 
    \file tnt-imxops.c
    \author Giandomenico Rossi <gdrossi@hotmail.com>
    
 

    \brief
*/

#include <linux/jiffies.h>
#include <linux/sched.h>
#include <linux/wait_bit.h>
#include <linux/delay.h>

#include "spi-sensor.h"
#include "fpga-cs.h"
#include "tnt-io.h"
#include "tnt-imx.h"
#include "tntvdo.h"
#include "tnt-fpga-utils.h"

#define IMX_DELAY 10 /* Actually delay not needed */

inline void disable_lvds_clk(struct tnt_device *dev)
{
	tnt_write(dev, CLK_LVDS_IMX, 0x800000);
	msleep(IMX_DELAY);
}

inline void enable_lvds_clk(struct tnt_device *dev)
{
	tnt_write(dev, CLK_LVDS_IMX, 0x80);
	msleep(IMX_DELAY);
}

inline void imx_reset(struct tnt_device *dev)
{
	tnt_write(dev, CLK_LVDS_IMX, 0x80000);
	msleep(IMX_DELAY);
}

inline void imx_start(struct tnt_device *dev)
{
	tnt_write(dev, CLK_LVDS_IMX, 0x8);
	msleep(IMX_DELAY);
}

inline void enable_hs(struct tnt_device *dev)
{
	tnt_write(dev, EN_HS_MANAGE_IMX, 1);
	msleep(IMX_DELAY);
}

inline void disable_hs(struct tnt_device *dev)
{
	tnt_write(dev, EN_HS_MANAGE_IMX, 0);
	msleep(IMX_DELAY);
}

inline void trigger_mode(struct tnt_device *dev)
{
	struct spi_data data = { { 0x2, 0xb, 0, 1, 0xff }, 1 };

	tnt_spi_register(dev, &data);
	msleep(IMX_DELAY);
}

inline void normal_mode(struct tnt_device *dev)
{
	struct spi_data data = { { 0x2, 0x00, 0, 1, 0xff }, 0 };

	tnt_spi_register(dev, &data);
	msleep(IMX_DELAY);
}

//
inline void imx_pll_reset(struct tnt_device *dev)
{
	tnt_write(dev, CLK_LVDS_IMX, 0x10 << 16);
	msleep(IMX_DELAY);
}

inline void imx_pll_start(struct tnt_device *dev)
{
	tnt_write(dev, CLK_LVDS_IMX, 0x10);
}

bool pll_locked(struct tnt_device *dev)
{
	int v;
	int lock_mask = 1 << 25;

	v = tnt_read(dev, SENS_LOCK_IMX);
	return (v & lock_mask) == lock_mask;
}

inline int imx_lock_pll(struct tnt_device *dev)
{
	int timeout = 4000;
	int locked = 0;
	int stable = 500;

	imx_pll_reset(dev);
	imx_pll_start(dev);

	locked = tnt_wait_until_ntimes(pll_locked, dev, timeout, stable);

	return locked == 0 ? locked : -EIO;
}

int imx_conf(struct tnt_device *dev, struct spi_data *conf)
{
	int i;
	int ret = 0;

	for (i = 0; conf[i].header.id; i++) {
		ret = tnt_spi_register(dev, &conf[i]);
		if (ret) {
			pr_info("Error configuring imx265 (%d)\n", ret);
			return ret;
		}
	}

	return ret;
}

__maybe_unused int imx_check_conf(struct tnt_device *dev, struct spi_data *conf)
{
	int i;
	int ret = 0;
	u32 val;

	for (i = 0; conf[i].header.id; i++) {
		ret = tnt_spi_read(dev, &conf[i].header, &val);
		if (ret) {
			pr_info("Error configuring imx265 (%d)\n", ret);
			return ret;
		}
		if (val != conf[i].value) {
			pr_info("%d : Error configuring imx265 %x != %x\n", i,
				val, conf[i].value);
			ret++;
		}
	}

	return ret;
}

int imx_sensor_configure(struct tnt_device *dev, struct spi_data *sensor_init)
{
	int ret = 0;

	ret = imx_conf(dev, &sensor_init[0]);
	if (ret)
		return ret;

	call_devop(dev, config, dev);
	return ret;
}

inline void imx_enable_roi(struct tnt_device *dev)
{
	struct spi_data sensor_roi[] = { { { 0x05, 0x00, 0, 1, 0x3 }, 3 }, {} };

	imx_conf(dev, &sensor_roi[0]);
}

inline void imx_set_roi(struct tnt_device *dev, struct spi_data *sensor_roi,
			int width, int height)
{
	imx_conf(dev, &sensor_roi[0]);

	/* 
       ma un ingegnere che ne sa, 
       sempre a 32 bit non puo' essere la fpga... 
       poi  in una notte di settembre mi svegliai, 
       le correnti parassite, sulle schede il clock senza pll... 
       chissa' dov'era l'anomalia 
       e quell'ingengere che giocava col debugger... 
       Io, ingegnere che son io, 
       ingegnere che non capisco un cazzo...
       programmare io non so 
       ma lassu' mi e' rimasto il baco
    */

	/* don't know what does fuck mean */
	tnt_write(dev, SET_ROI_H_IMX, 8 | ((width + 7) << 16));
	tnt_write(dev, SET_ROI_V_IMX, 15 | ((height + 14) << 16));
}

inline void imx_disable_roi(struct tnt_device *dev, int width, int height)
{
	struct spi_data sensor_roi[] = { { { 0x05, 0x00, 0, 1, 0x3 }, 0 }, {} };

	imx_set_roi(dev, &sensor_roi[0], width, height);
}

void imx_set_roi_area(struct tnt_device *dev, struct roi_area *roi)
{
	struct spi_data sensor_roi[] = {
		{ { 0x05, 0x00, 0, 1, 0x3 }, 3 },
		{ { 0x05, 0x10, 0, 2, 0x1FFF }, roi->h_org },
		{ { 0x05, 0x12, 0, 2, 0x0FFF }, roi->v_org },
		{ { 0x05, 0x14, 0, 2, 0x1FFF }, roi->h_size + 8 + 8 },
		{ { 0x05, 0x16, 0, 2, 0x0FFF }, roi->v_size + 4 + 4 },
		{}
	};

	imx_set_roi(dev, &sensor_roi[0], roi->width, roi->height);
}

inline void start_grab(struct tnt_device *dev, int expo)
{
	tnt_write(dev, SET_EXPO, expo);
	tnt_write(dev, START, 1);
}

inline void imx_align_image(struct tnt_device *dev, int n)
{
	int i;
	for (i = 0; i < n; i++) {
		start_grab(dev, 10);
		msleep(50);
	}
}

bool align_done(struct tnt_device *dev)
{
	int v;

	v = tnt_read(dev, SENS_ALLIGN_STATUS);
	return (v & 0xF) == 0xF;
}

int imx_lvds_align(struct tnt_device *dev)
{
	int timeout = 350;
	int v;

	tnt_write(dev, EN_CHAIN, 0);
	tnt_write(dev, EN_HS_MANAGE_IMX, 1);
	tnt_write(dev, EN_CHAIN, 1);

	v = tnt_wait_until(align_done, dev, timeout);
	return 0;
}

/*  */

inline u32 gain_level(u32 _gain)
{
	u32 gain;

	/* ??????? ask  */
	gain = max(_gain, (u32)128);
	gain = min(gain, (u32)608);

	gain -= 128;
	return gain;
}

u32 imxbw_set_gain(struct tnt_device *dev, u32 value)
{
	struct tntvdo *vdo = container_of(dev, struct tntvdo, device);
	struct tntdrv *drv = vdo->driver;
	struct spi_header low = { 0x4, 0x04, 0, 1, 0xff };
	struct spi_header high = { 0x4, 0x05, 0, 1, 0xff };
	u32 gain = gain_level(value);
	bool wait = false;

	if (FG_GMODE(drv->grabmode) == TRIGGED)
		wait = true;

	tnt_spi_write(dev, &low, gain & 0xff, wait, false);
	tnt_spi_write(dev, &high, (gain >> 8) & 0xff, wait, !wait);

	return value;
}

u32 imxyuv_set_gain(struct tnt_device *dev, u32 value)
{
	struct tntvdo *vdo = container_of(dev, struct tntvdo, device);
	struct tntdrv *drv = vdo->driver;
	struct spi_header low = { 0x4, 0x04, 0, 1, 0xff };
	struct spi_header high = { 0x4, 0x05, 0, 1, 0xff };
	u32 gain = gain_level(value);
	bool wait = false;

	if (FG_GMODE(drv->grabmode) == TRIGGED)
		wait = true;

	tnt_spi_write(dev, &low, gain & 0xff, wait, false);
	tnt_spi_write(dev, &high, (gain >> 8) & 0xff, wait, !wait);

	return value;
}

#define EXPO_UNIT 1269 /* us */
u32 imxbw_expo_us_to_value(struct tnt_device *dev, u32 us)
{
	u64 expo;

	expo = div64_u64(100 * us, EXPO_UNIT);
	return expo;
}

u32 imxyuv_expo_us_to_value(struct tnt_device *dev, u32 us)
{
	u64 expo;

	expo = div64_u64(100 * us, EXPO_UNIT);
	return expo;
}

int imx_pixel_frame(struct tnt_device *dev, struct tnt_video_fmt *fmt)
{
	u32 n_pixels = (fmt->width * fmt->height) >> 2;
	u32 xsize = fmt->width;
	u32 ysize = fmt->height + EXTRA_PIXEL_Y;

	tnt_write(dev, SET_NUM_TOT_PIXEL, n_pixels);
	tnt_write(dev, SET_X_SIZE_FRAME_CHANGE_CLOCK, xsize);
	tnt_write(dev, SIZE_Y_MANAGE_IMX, ysize);
	/* 0 = pass throught to CSC; 1= direct image from sensor*/
	tnt_write(dev, FLAG_Y_nBAYER, 1);
	tnt_write(dev, SET_VGA, 128);
	return 0;
}

int imxyuv_pixel_frame(struct tnt_device *dev, struct tnt_video_fmt *fmt)
{
	u32 n_pixels = (fmt->width * fmt->height) >> 2;
	u32 xsize = fmt->width;
	u32 ysize = fmt->height + EXTRA_PIXEL_Y;

	tnt_write(dev, SET_NUM_TOT_PIXEL, n_pixels);
	tnt_write(dev, SET_X_SIZE_FRAME_CHANGE_CLOCK, xsize);
	tnt_write(dev, SIZE_X_CSC, xsize);
	tnt_write(dev, SIZE_Y_MANAGE_IMX, ysize);
	/* 0 = pass throught to CSC; 1= direct image from sensor */
	tnt_write(dev, FLAG_Y_nBAYER, 0);
	tnt_write(dev, BAYER_SELECTOR, PHASE_CSC);
	tnt_write(dev, SET_VGA, 128);
	tnt_write(dev, FMT_SEMI_PLANAR, 1);
	return 0;
}

int imxbw_enable_dma(struct tnt_device *dev)
{
	tnt_fpga_enable_main_dma(dev);
	return 0;
}

int imxbw_disable_dma(struct tnt_device *dev)
{
	tnt_fpga_disable_main_dma(dev);
	return 0;
}

__maybe_unused int imxyuv_enable_dma(struct tnt_device *dev)
{
	tnt_fpga_enable_main_dma(dev);
	tnt_fpga_enable_cb_dma(dev);
	tnt_fpga_enable_cr_dma(dev);

	return 0;
}

__maybe_unused int imxyuv_disable_dma(struct tnt_device *dev)
{
	tnt_fpga_disable_main_dma(dev);
	tnt_fpga_disable_cb_dma(dev);
	tnt_fpga_disable_cr_dma(dev);

	return 0;
}

bool imxbw_image_ready(struct tnt_device *dev)
{
	int ready = 0;

	ready = tnt_read(dev, CHECK_IMG_READY);
	//pr_info("@ %d\n", ready);
	return (ready & 1) == 1;
}

static inline u32 imxbw_set_standby_mode(struct tnt_device *dev, u32 value)
{
	struct spi_data data = { { 0x2, 0, 0, 1, 0x01 }, 0 };

	data.value = value;

	tnt_spi_register(dev, &data);
	msleep(IMX_DELAY);

	return 0;
}

inline u32 imxbw_set_test_mode(struct tnt_device *dev, u32 value)
{
	struct spi_data data = { { 0x4, 0x38, 0, 1, 0xff }, 0 };

	data.value = value ? 1 : 0; //PGREGEN [0]
	data.value = data.value + (1 << 1); //PGTHRU [1]
	data.value = data.value + (1 << 2); //PGCLKEN [2]
	data.value = data.value + (value << 3); //PGMODE [3:7]

	imxbw_set_standby_mode(dev, 1);

	tnt_spi_write(dev, &data.header, data.value, true, false);
	msleep(IMX_DELAY);

	imxbw_set_standby_mode(dev, 0);

	return 0;
}

inline u32 imxbw_get_model(struct tnt_device *dev, u32 *value)
{
	u32 model, mono;
	int ret = 0;

	struct spi_header header_model = { 0x3, 0x48, 0, 1, 0xC0 };
	struct spi_header header_model2 = { 0x3, 0x49, 0, 1, 0xff };

	ret = tnt_spi_read(dev, &header_model, &model);

	msleep(IMX_DELAY * 2);

	ret = tnt_spi_read(dev, &header_model2, &mono);

	model = ((model & 0xC0) >> 6) | ((mono & 0x7F) << 2);
	mono = mono & 0x80;

	*value = model | (mono << 8);

	msleep(IMX_DELAY);

	return ret;
}
