/** 
 \file tnt-ctrl.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Both sensor and fpga have specific controller settable by user.
 These are:
 + GAIN (CHA, CHB)
 + EXPOSURE (CHA, CHB)
 + STROBO_TIME (FPGA)
 + STROBO_SRC  (FPGA)
 + STROBO_LEVEL (FPGA)

 For each fearure above, regard its sensor dependency a ctrl_item is created.
*/

#ifndef _TNT_CTRL_H
#define _TNT_CTRL_H

#include <linux/types.h>
#include <media/v4l2-ctrls.h>
#include <linux/v4l2-controls.h>
#include "tnt-cmd.h"

struct vdo_balance {
	u32 red;
	u32 green;
	u32 blue;
};

#define V4L2_CID_GREEN_BALANCE V4L2_CID_CHROMA_GAIN

struct vdo_ctrls {
	struct v4l2_ctrl_handler handler;

	struct vdo_balance balance; /*! FPGA red, green blue gain  */
	struct acq_point w; /* work point  */
	bool vflip; /*!< vertical flip  */
	bool hflip; /*!< horizontal flip  */
	bool test_mode; /*!< test mode  */
};

#define N_VDO_CTRL 12

enum strobo_level {
	STROBO_ACTIVE_LOW = 0,
	STROBO_ACTIVE_HIGH = 0,
};

enum strobo_source {
	NO_STROBO = 0, /*!< no strobo */
	STROBO_CHA, /*!< strobo on grabbing channel A  */
	strobo_CHB, /*!< strobo on grabbing channel B  */
	STROBO_CHA_OR_CHB, /*!< strobo on grabbing channel A or channel B */
	STROBO_EMUL, /*!< emulate strobo  */
};

struct strobo {
	enum strobo_level level;
	enum strobo_source source;
};

struct drv_ctrls {
	struct v4l2_ctrl_handler handler;
	struct strobo strobo;
};

#endif
