/** 
 \file tnt-imx265.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_IMX250_H
#define _TNT_IMX250_H

#include "tnt-io.h"
#include "tnt-video-formats.h"
#include "tnt-cmd.h"

int imx250_init(struct tnt_device *dev, enum tnt_sensors sensor,
		struct tnt_video_fmt *fmt);
u32 imx250bw_set_gain(struct tnt_device *dev, u32 value);
u32 imx250yuv_set_gain(struct tnt_device *dev, u32 value);
u32 imx250bw_expo_us_to_value(struct tnt_device *dev, u32 value);
u32 imx250yuv_expo_us_to_value(struct tnt_device *dev, u32 value);

#endif
