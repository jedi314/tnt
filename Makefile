#make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu-
END=
export ARCH:=arm64
export CROSS_COMPILE:=aarch64-linux-gnu-

MODULE_NAME=tnt
MODULE=$(MODNAME).ko
IUSER ?= root
IHOST ?= pordoi
IPATH ?= /root #/lib/modules/4.14.0+/extra/
SCP = scp
MAKEFLAGS += --no-print-directory
KERNELDIR?=/home/jedi/sv1/linux-xlnx/

obj-m += $(MODULE_NAME).o
$(MODULE_NAME)-objs :=  tnt-core.o	\
	tntof.o		\
	tnt-io.o	\
	tnt-allocator.o	\
	tnt-dbg.o	\
	debugfs/tdm.o	\
	tnt-regs.o		\
	tnt-videobuf2-core.o	\
	tnt-videobuf2-v4l2.o	\
	list.o			\
	tnt-access.o	\
	tnt-dqueue.o	\
	spi-sensor.o	\
	tnt-imx265.o	\
	tnt-imx250.o	\
	tnt-fpga-utils.o\
	tnt-ctrl.o	\
	tnt-v4l2.o	\
	tnt-subdev.o	\
	tnt-queue.o	\
	tnt-optical-density.o	\
	tnt-rle.o		\
	tnt-acq-points.o	\
	tnt-slot.o		\
	tnt-processor.o		\
	tnt-bilinear-resizer.o	\
	tnt-remapper.o	\
	tnt-resizer.o	\
	tnt-imx.o	\
	tnt-strobe.o	\
	tnt-stats.o	\
	tnt-dima.o	\
	$(END)


ccflags-y := "-I./debugfs"



.SILENT: test

.PHONY: install print 

all: modules

modules:
	$(MAKE) -C $(KERNELDIR) M=$(shell pwd) $@

clean:
	make -C $(KERNELDIR) M=$(shell pwd) $@
	rm -f *~

install: modules
	$(SCP) $(MODULE_NAME).ko $(IUSER)@$(IHOST):$(IPATH)

print:
	@echo "[ $(ARCH) ] [ $(CROSS_COMPILE) ] [ $(PWD) ] [ $(KDIR) ]"


modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) modules_install


#clean:
#	rm -f *.o *~ core .depend .*.cmd *.ko *.mod.c
#	rm -f Module.markers Module.symvers modules.order
#	rm -rf .tmp_versions Modules.symvers

