/** 
 \file tdm.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Tnt Debug Module
*/

#include <linux/types.h>
#include "tdm.h"
#include "trace.h"



/**
\brief open of fpga/reg file.
inode->i_private field 
seq_ops operations array and also 
private struct.
As seq use private file field, 
opt->param must be moved to seq_file->private.

\param inode:
\param file:
\return 
*/
static int
tlist_open(struct inode *inode, struct file *file)
{
	int res = -ENOMEM;
    struct tdm_opt *opt = (struct tdm_opt *)inode->i_private;

    /* Register the operators */
    res = seq_open(file, opt->seq_ops);
    if (res == 0)
        ((struct seq_file *)file->private_data)->private = opt->param;
    
    return res;
}

static
ssize_t tlist_write(struct file *flip, const char __user *buf, size_t count, loff_t *f_pos)
{
    return count;    
}

static
ssize_t tlist_read(struct file *flip, char __user *buf, size_t count , loff_t *f_pos)
{
    return seq_read(flip, buf, count, f_pos);
}


static struct file_operations list_fops = {
    .owner   = THIS_MODULE,
    .open    = tlist_open,     /* User supplied */
    .read    = tlist_read,     /* Built-in helper function */
    .write   = tlist_write,    /* User supplied */
    .llseek  = seq_lseek,      /* Built-in helper function */
    .release = seq_release,    /* Built-in helper funciton */
};



#define OVERRIDE(a,b,c) do{                     \
        if( (a)->c == NULL )                    \
            (a)->c = (b)->c;                    \
    }while(0)

#if 0
static
void override_seq_ops(struct seq_operations *sops, struct seq_operations* s)
{
    OVERRIDE(s, sops, start);
    OVERRIDE(s, sops, next);
    OVERRIDE(s, sops, stop);
    OVERRIDE(s, sops, show);
}
#endif

static
void override_list_fops( struct file_operations *lfops,  struct file_operations* l)
{
    OVERRIDE(l, lfops, owner);
    OVERRIDE(l, lfops, open);
    OVERRIDE(l, lfops, read);
    OVERRIDE(l, lfops, write);
    OVERRIDE(l, lfops, llseek);
    OVERRIDE(l, lfops, release);
}

static
void __maybe_unused print_list_fops( struct file_operations *lfops,  struct file_operations* l)
{
    pr_info("* %px %px\n", l->open, lfops->open);
    pr_info("* %px %px\n", l->read, lfops->read);
    pr_info("* %px %px\n", l->write, lfops->write);
    pr_info("* %px %px\n", l->llseek, lfops->llseek);
    pr_info("* %px %px\n", l->release, lfops->release);
}
    


/**
\brief It is a wrapper selecting the right 
api call to create a file entry in debugfs.

\param w: pointer of watch info
\return pointer of new created file or PTR_ERR
*/
static 
struct dentry* tdm_create_file( struct dentry* dir, struct tdm_watch* w)
{
    struct dentry* df = NULL;
    
    switch( w->type ){
    case TDM_UINT8:
        df = debugfs_create_u8(w->filename, w->mode, dir, (u8*)w->opt.param);
        break;
    case TDM_UINT16:
        df = debugfs_create_u16(w->filename, w->mode, dir, (u16*)w->opt.param);
        break;
    case TDM_UINT32:
        df = debugfs_create_u32(w->filename, w->mode, dir, (u32*)w->opt.param);
        break;
    case TDM_UINT64:
        df = debugfs_create_u64(w->filename, w->mode, dir, (u64*)w->opt.param);
        break;
    case TDM_XINT8 :
        df = debugfs_create_x8(w->filename, w->mode, dir, (u8*)w->opt.param);
        break;
    case TDM_XINT16:
        df = debugfs_create_x16(w->filename, w->mode, dir, (u16*)w->opt.param);
        break;
    case TDM_XINT32:
        df = debugfs_create_x32(w->filename, w->mode, dir, (u32*)w->opt.param);
        break;
    case TDM_XINT64:
        df = debugfs_create_x64(w->filename, w->mode, dir, (u64*)w->opt.param);
        break;
    case TDM_LONG  :
        df = debugfs_create_ulong(w->filename, w->mode, dir, (unsigned long*)w->opt.param);
        break;
    case TDM_REG32:
        break;
    case TDM_REGIF:
        df = debugfs_create_file(w->filename, w->mode, dir, (void*)w->opt.param, w->ops);
        break;
    case TDM_LIST:
        // print_list_fops(&list_fops, w->ops);
        /* override fops */
        if(w->ops == NULL){
            w->ops = &list_fops;
        } else {
            override_list_fops(&list_fops, w->ops);
        }
        df = debugfs_create_file(w->filename, w->mode, dir, (void*)&w->opt, w->ops);
        break;
    case TDM_CUSTOM:
        df = debugfs_create_file( w->filename, w->mode, dir, w->opt.param, w->ops);        
        break;
    case TDM_SIZE:
        df = debugfs_create_size_t(w->filename, w->mode, dir, (size_t*)w->opt.param);
        break;
    case TDM_ARRAY:
        pr_info("name: %s, mode: %d,  addr: %px, size: %d \n",
                w->filename, w->mode, w->opt.param, w->opt.size  );
        debugfs_create_u32_array(w->filename, w->mode, dir,  (u32*)w->opt.param, w->opt.size);
	df = dir;
	break;
   default:
        df = (struct dentry*)ERR_PTR(-EINVAL);
    }
    
    return df;
}




/**
\brief Create a watch element.
It looks for specific watch type and call rescpetcive
function to create a file entry

\param w: pointer to watche structure
\return 0 or error
*/
int tdm_create_watch(struct tdm_debug *dbg, struct tdm_watch* w)
{
    struct dentry* dir = dbg->dir;
    struct dentry *file;

    file = tdm_create_file(dir, w);
    if(IS_ERR_OR_NULL(file)){
        pr_err("Failed to create /sys/kernel/debug/%s/%s\n", dbg->dirname, w->filename );
        return -ENOMEM;
    }
    return 0;
}



int tdm_append_watches(struct tdm_debug* dbg, struct tdm_watch* watches, void* param)
{
    int i;
    int ret, n_ret;

    n_ret = 0;
    for(i = 0;  i< MAX_WATCHES && watches[i].filename; i++){
        if(watches[i].opt.param == NULL )
            watches[i].opt.param = param;
        ret = tdm_create_watch(dbg, &watches[i]);
        if(ret)
            n_ret++;
    }
    return n_ret;
}



/**
\brief Initialize a set of watches passed 

\param tdm: * to tdm_debug list
\return 0 o errorcode
*/
int tdm_init(struct tdm_debug* tdm)
{
    struct dentry *dir;
    
    if(!tdm->dirname)
        return -ENOTDIR;

    dir = debugfs_create_dir(tdm->dirname, tdm->parent);
    if(!dir){
        pr_err("Failed to create /sys/kernel/debug/%s\n", tdm->dirname);
        return -ENOENT;
    }
    tdm->dir = dir;

    return 0;
}
