/** 
 \file tnt-acq-points.c
 
 

 \brief Before an image grab the value of exposure, gain and strobo
 must be set.
 This set of values are generically called acquistion_points
 or worl_list (wlist).
 Before new image grab, a value of wlist updates ctrls of 
 device. 
 The value is picked uo from an array with an acq  index.

 There are two wlists per device; one is used by driver the other 
 can be update by userspace application.
 After update the two list are swapped.

 If length between two wlist are unchanged acq index is unchanged
 else it is reset.

*/

#include "tntvdo.h"

/**
\brief Cuts off exposure time regard
to FPS or period time.

T_exposure + T_readout <= T_PERIOD

\param s: raw exposure time in uS 
\param p: value of period in nS!!!
\return new exposure time in uS 
*/
inline u32 tnt_cutoff_shutter(u32 s, u64 p)
{
	u32 period = div_u64(p, 1000);
	u32 shutter;

	shutter = min(period - READOUT_TIME, s);
	return shutter;
}

static inline int swap_wlist(struct tntvdo *vdo)
{
	int next = 1 - vdo->c_wlist;
	vdo->current_wlist = &vdo->wlists[next];

	return next;
}

static inline void update_acq_points(struct tntvdo *vdo)
{
	struct acq_points *wl_old;
	unsigned long flags;

	spin_lock_irqsave(&vdo->acq_point_lock, flags);

	wl_old = vdo->current_wlist;
	vdo->c_wlist = swap_wlist(vdo);

	if (!wl_old)
		vdo->c_point = 0;

	if (wl_old->n != vdo->current_wlist->n)
		vdo->c_point = 0;

	atomic_set(&vdo->update_acq, 0);

	spin_unlock_irqrestore(&vdo->acq_point_lock, flags);
}

inline void acq_point_copy(struct acq_point *dest, struct acq_point *source)
{
	memcpy(dest, source, sizeof(*source));
}

static inline void set_acq_point(struct acq_point *dest, struct acq_point *p,
				 u64 period)
{
	p->shutter = tnt_cutoff_shutter(p->shutter, period);
	acq_point_copy(dest, p);
}

inline void load_acquisition_points(struct tntvdo *vdo)
{
	struct tnt_device *dev = &vdo->device;

	/* Return if no acquisition points was set.
	 * Acquisition will start with default values
	 * If acquisition points have been set for the
	 * first time force current wlist to 1 as swap
	 * will choose list 0, which is the desired behaviour */
	if (!vdo->current_wlist) {
		return;
	} else {
		if (atomic_read(&vdo->update_acq)) {
			update_acq_points(vdo);
		}
	}

	acq_point_copy(&dev->ctrls->w, &vdo->current_wlist->pts[vdo->c_point]);
	vdo->c_point++;
	vdo->c_point %= vdo->current_wlist->n;
}

int set_acquisition_points(struct tntvdo *vdo, struct acq_points *ap)
{
	int i;
	struct acq_points *wlist;
	unsigned long flags;

	if (!ap)
		return -EINVAL;

	if (!ap->n || ap->n > MAX_ACQ_POINTS)
		return -EINVAL;

	spin_lock_irqsave(&vdo->acq_point_lock, flags);

	if (!vdo->current_wlist) {
		vdo->c_wlist = 1;
		vdo->c_point = 0;
	}

	wlist = &vdo->wlists[1 - vdo->c_wlist];
	wlist->n = ap->n;

	for (i = 0; i < ap->n; i++)
		acq_point_copy(&wlist->pts[i], &ap->pts[i]);

	vdo->current_wlist = &vdo->wlists[vdo->c_wlist];
	spin_unlock_irqrestore(&vdo->acq_point_lock, flags);
	atomic_set(&vdo->update_acq, 1);
	return 0;
}
