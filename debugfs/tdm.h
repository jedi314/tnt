/** 
 \file tdm.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief TNT debug Module
*/


#ifndef _TDM_H_
#define _TDM_H_

#include<linux/debugfs.h>
#include "trace.h"

#define MAX_WATCHES 128

struct tdm_opt {
    void *param;
    struct seq_operations *seq_ops;
    int size;
};


struct tdm_watch {
    char* filename;
    int mode;                   /* see stat.h */
    int type;                   
   
    struct file_operations *ops;
    struct tdm_opt opt;
    //int size;
};


struct tdm_debug {
    char *dirname;
    struct dentry *dir;
    struct dentry *parent;
    //struct tmd_watch watches[MAX_WATCHES];
};


enum TDM_TYPES {
    TDM_UINT8=1,
    TDM_UINT16, 
    TDM_UINT32,
    TDM_UINT64,
    TDM_XINT8,
    TDM_XINT16,
    TDM_XINT32,
    TDM_XINT64,
    TDM_LONG,
    TDM_REG32, 
    TDM_LIST,
    TDM_CUSTOM,
    TDM_SIZE,
    TDM_REGIF,
    TDM_ARRAY,
};




#define DECLARE_LIST_FOPS(seq_ops, _tname, _list, _show)                \
    static void *                                                       \
    _list##_start(struct seq_file *seq, loff_t *pos)                    \
    {                                                                   \
        struct _tname *_name = (struct _tname *)seq->private;           \
        if(list_empty(&_name->_list)){                                  \
            return NULL;                                                \
        }                                                               \
        return seq_list_start(&_name->_list, *pos);                     \
    }                                                                   \
    static void *                                                       \
    _list##_next(struct seq_file *seq, void *v, loff_t *pos)            \
    {                                                                   \
        void* vv;                                                       \
        struct _tname *_name= (struct _tname *)seq->private;            \
        vv = seq_list_next(v, &_name->_list, pos);                      \
        if (((u64)vv >> 48) == 0xdead )                                 \
            return NULL;                                                \
        return vv;                                                      \
    }                                                                   \
    static void                                                         \
    _list##_stop(struct seq_file *seq, void *v)                         \
    {                                                                   \
    }                                                                   \
    static struct seq_operations seq_ops = {                            \
        .start = _list##_start,                                         \
        .next  = _list##_next,                                          \
        .stop  = _list##_stop,                                          \
        .show  = _show                                                  \
    };



int tdm_init(struct tdm_debug* tdm);
int tdm_append_watches(struct tdm_debug* dbg, struct tdm_watch* watches, void* param);


#endif
