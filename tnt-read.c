/** 
 \file tnt-read.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <getopt.h> /* getopt_long() */

#include <fcntl.h> /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>

#include <time.h>
#include <signal.h>

#include <linux/videodev2.h>

#include "tnt-cmd.h"
#include "include/allocator.h"
#include "/home/jedi/sv1/lab/profile/profile.h"
//#include "/home/jedi/sv1/basic2.0/jpeg/jpeg-ioctl.h"

#ifdef JPEG
#include "/home/jedi/sv1/basic2.0/libtcd/api/include/tcd/jpg/tcd_jp_types.h"
#endif

#include <send_image_wrapper.h>

#include <stdio.h>
#include <setjmp.h>

#define TRY(s)                                                                 \
	do {                                                                   \
		int v;                                                         \
		switch (v = setjmp(s)) {                                       \
		case 0:
#define CATCH(c)                                                               \
	break;                                                                 \
	case c:
#define END_TRY                                                                \
	break;                                                                 \
	}                                                                      \
	}                                                                      \
	while (0)
#define THROW(s, t) longjmp(s, t)
#define CATCH_ALL                                                              \
	break;                                                                 \
	default:

#define FOO_EXCEPTION (1)
#define BAR_EXCEPTION (2)
#define BAZ_EXCEPTION (3)

#define V4L2_CID_GREEN_BALANCE V4L2_CID_CHROMA_GAIN

#define CLEAR(x) memset(&(x), 0, sizeof(x))
#define A(x) ((x) >> 1)
#define B(x) (0x1 & (x))
#define IS_ACTIVE(x, i)                                                        \
	((((~B(x) & ~(i)) | (A(x) & ~B(x)) | (~A(x) & B(x) & i)) & 1))

#define DEBUG_PRINT(fmt, args...)                                              \
	printf("DEBUG: %s:%d:%s: " fmt, __FILE__, __LINE__,                    \
	       __PRETTY_FUNCTION__, ##args)

#define TRACE DEBUG_PRINT("\n")
#define MMAP_ERR 14
#define STOP_CAP 8

#define MB(a) (a) * 1024 * 1024
#define SIZE 3 * 1024 * 1024
#define WIDTH 2048 //1920
#define HEIGHT 1536 //1080

#define IMG_SIZE WIDTH *HEIGHT
#define GRAY_8 0
#define PAGE_SHIFT 12

#define GET_FD                                                                 \
	dev.fd[FG_SOURCE(dev.grabmode) == DOUBLE ? 0 : FG_SOURCE(dev.grabmode)]

struct buffer {
	void *start;
	size_t length;
	uint64_t phy_addr;
};

enum io_method {
	IO_METHOD_READ,
	IO_METHOD_MMAP,
};

struct tjpeg {
	int fd;
	char jpegname[64];
	uint64_t addr;
	int afd;
	void *vaddr;
};

struct _dev {
	char *name[2];
	int fd[2];
	enum io_method io;
	struct buffer *buffers[2];
	unsigned int n_buffers;
	int c_buffer;
	int send;
	int frame_number;
	void *socket;
	TAddressIP ip;
	Tbanco banco[2];
	int grabmode;
	jmp_buf t;
	int dump;
	int expo;
	int gain;
	int r_gain;
	int g_gain;
	int b_gain;
	int jpeg;
	int period;
	struct tjpeg jp;
};

struct _dev dev = {
    .name = { "/dev/video0", "/dev/video1" },
    .fd = { -1, -1},
    .frame_number = 1,
    .grabmode = TRIGGED_CTX,
    .dump = 0,
    .expo = 2000,
    .gain = 400,
    .r_gain = 600,
    .g_gain = 600,
    .b_gain = 600,
    .c_buffer = -1,
    .io =  IO_METHOD_MMAP,
    .period = 500000,
    .jp = {
        .jpegname = {"out.jpeg"},
    },
    .ip = {
        .adr = {
            .adr1 = 172,
            .adr2 = 25,
            .adr3 = 99,
            .adr4 = 6,
        }
    },
    .banco = {
        {
            .codice_oggetto = 0, //TIL_OBJ_CODE_MEM_SYSTEM,
            .numero_oggetto = 0,
            .dim_head_byte = sizeof(Tbanco),
            .dim_x_banco = WIDTH,
            .dim_y_banco = HEIGHT,
            .dim_oggetto_byte = IMG_SIZE,
            .bits_pixel = 8 , //12, 
            .tipo_immagine = 0, //6, // YUV
            .image_format = 0, //TIL_IMAGE_FORMAT_RAW;
            .pixel_ptr = NULL,
        },
        {
            .codice_oggetto = 0, //TIL_OBJ_CODE_MEM_SYSTEM,
            .numero_oggetto = 0,
            .dim_head_byte = sizeof(Tbanco),
            .dim_x_banco = WIDTH,
            .dim_y_banco = HEIGHT,
            .dim_oggetto_byte = IMG_SIZE,
            .bits_pixel = 12, 
            .tipo_immagine = 6, // YUV
            .image_format = 0, //TIL_IMAGE_FORMAT_RAW;
            .pixel_ptr = NULL,
        },
    },
};

static void dump(char *desc, void *addr, int len)
{
	int i;
	unsigned char buff[17];
	unsigned char *pc = (unsigned char *)addr;

	/* Output description if given. */
	if (desc != NULL)
		printf("%s:\n", desc);

	/* Process every byte in the data. */
	for (i = 0; i < len; i++) {
		/* Multiple of 16 means new line (with line offset). */
		if ((i % 16) == 0) {
			/* Just don't print ASCII for the zeroth line. */
			if (i != 0)
				printf("  %s\n", buff);
			printf("  %04x ", i);
		}

		/* Now the hex code for the specific character. */
		printf(" %02x", pc[i]);

		/* And store a printable ASCII character for later. */
		if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
			buff[i % 16] = '.';
		} else {
			buff[i % 16] = pc[i];
		}

		buff[(i % 16) + 1] = '\0';
	}

	/* Pad out last line if not exactly 16 characters. */
	while ((i % 16) != 0) {
		printf("   ");
		i++;
	}
	printf("  %s\n", buff);
}

static void errno_exit(const char *s, int i)
{
	fprintf(stderr, "%s error %d, %s\n", s, errno, strerror(errno));
	THROW(dev.t, i);
}

static int xioctl(int fh, int request, void *arg)
{
	int r;

	do {
		r = ioctl(fh, request, arg);
	} while (-1 == r && EINTR == errno);

	return r;
}

static void send_image(void *p, int size, int src)
{
	int ret;

	dev.banco[src].pixel_ptr = p;
	ret = SendImage(dev.socket, &dev.banco[src]);

	printf("ret: %d\n", ret);
}

static int set_grabmode(void)
{
	int ret = 0;
	int mode = dev.grabmode;
	int period = dev.period;
	int sensor;
	int fd;

	/* sensor #1 */
	fd = open("/dev/v4l-subdev0", O_RDWR);
	if (fd < 0) {
		errno_exit(__FUNCTION__, 1);
	}
	printf("subdev: %s opened (%d)\n", "/dev/v4l-subdev0", fd);
	ret = ioctl(fd, TNT_SET_GRABMODE, &mode);
	printf("0.ret: %d\n", ret);
	if (ret)
		errno_exit(__FUNCTION__, 2);

	ret = ioctl(fd, TNT_SET_PERIOD, &period);
	printf("1.ret: %d\n", ret);
	if (ret)
		errno_exit(__FUNCTION__, 2);

	sensor = CMOS_IMX265_BW;
	printf("2.ret: %ld\n", TNT_INIT_SENSOR);
	ret = ioctl(fd, TNT_INIT_SENSOR, &sensor);
	printf("2.ret: %d\n", ret);

	if (ret)
		errno_exit(__FUNCTION__, 2);

	close(fd);

	/* sensor #2 */
	fd = open("/dev/v4l-subdev1", O_RDWR);
	if (fd < 0) {
		errno_exit(__FUNCTION__, 1);
	}

	sensor = CMOS_IMX265_COL;
	ret = ioctl(fd, TNT_INIT_SENSOR, &sensor);
	if (ret)
		errno_exit(__FUNCTION__, 2);

	close(fd);

	return 0;
}

#ifdef JPEG
void jpeg_configure(void)
{
	int ret;
	tcd_jp_config conf = {
		.type = YCbCr,
		.subsample = SAMPLE_4_2_0,
		.gray = 0,
		.quality = 80,
		.width = WIDTH,
		.height = HEIGHT,
	};

	struct alc_memory mem = {
		.name = "jpeg-out",
		.count = 1,
		.size = MB(4),
		.val = rand() % (128 - 21) + 21,
	};

	ret = ioctl(dev.jp.afd, TNT_ALLOC_CTX, &mem);

	printf("Try mmap phyaddr 0x%lx (%c)\n", mem.addr, mem.val);
	dev.jp.vaddr = mmap(NULL, MB(4), PROT_READ | PROT_WRITE,
			    MAP_SHARED /* recommended */, dev.jp.afd, mem.addr);

	printf("Allocated phy address: 0x%lx\n", mem.addr);
	ioctl(dev.jp.afd, TNT_INIT_CTX, &mem);

	ret = tcd_jp_configure(&conf);
	if (ret)
		errno_exit("JPEG Configure Error", 36);

	dev.jp.addr = mem.addr;
	printf("JPEG CORE configured\n");
}

void jpeg_convert(int source, int index)
{
	int ret = 0;
	char msg[128];

	tcd_jp_image img = {
		.in_addr = dev.buffers[source][index].phy_addr,
		.out_buffer_addr = dev.jp.addr,
		.out_buffer_size = MB(4),
		.offset = 100,
	};

	tcd_jp_convert(&img);
	if (ret)
		errno_exit("Error in Conversion", 37);

	sprintf(msg, "Address begin %p\n", dev.jp.vaddr + 4096 - img.offset);
	dump(msg, dev.jp.vaddr + 4096 - img.offset - 6, 500);
}

static void jpeg_open(void)
{
	int ret;
	char desc[512];

	ret = tcd_jp_get_subsystem();
	if (ret != TCD_SUCCESS)
		errno_exit("NO jpeg encoder found!!!!! \n", 34);

	/* dev.jp.fd = open("/dev/tjpeg0", O_RDWR); */
	/* if(dev.jp.fd < 0) */
	/*     errno_exit("open tjpeg\n", 31); */

	ret = tcd_jp_open_subsystem();
	if (ret != TCD_SUCCESS)
		errno_exit("Error open sub system!!!! \n", 35);

	ret = tcd_jp_get_description(desc, 512);

	dev.jp.afd = open("/dev/allocator", O_RDWR);
	if (dev.jp.afd < 0)
		errno_exit("open allocator\n", 32);
}

#endif

int set_ctrl(int fd, int id, int val, char *action)
{
	struct v4l2_control ctrl;
	int ret;
	char msg[64];

	ctrl.id = id;
	ctrl.value = val;
	ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
	if (ret) {
		sprintf(msg, "set_ctrl ( %d, %d)", id, val);
		perror(msg);
		return 1;
	}
	printf("%s %u\n", action, ctrl.value);
	return ret;
}

static int read_frame(void)
{
	struct v4l2_buffer buf;
	int fd = GET_FD;
	int i;

	switch (dev.io) {
	case IO_METHOD_READ:
		if (-1 == read(fd, dev.buffers[0][0].start,
			       dev.buffers[0][0].length)) {
			switch (errno) {
			case EAGAIN:
				return 0;
			case EIO:
				/* Could ignore EIO, see spec. */
				/* fall through */
			default:
				errno_exit("read", 3);
			}
		}
		if (dev.send)
			send_image(dev.buffers[0][0].start,
				   dev.buffers[0][0].length, 0);
		break;

	case IO_METHOD_MMAP:
		CLEAR(buf);

		buf.type =
			V4L2_BUF_TYPE_VIDEO_CAPTURE; //| V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = dev.c_buffer;

		//printf("DQUEUE buf# %d\n", buf.index);
		//getchar();
		if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
			switch (errno) {
			case EAGAIN:
				return 0;
			case EIO:
				/* Could ignore EIO, see spec. */
				/* fall through */
			default:
				errno_exit("VIDIOC_DQBUF", 4);
			}
		}
		printf("END DQUEUE buf# %d\n", buf.index);
		assert(buf.index < dev.frame_number);

		for (i = 0; i < DOUBLE; i++) {
			if (!IS_ACTIVE(dev.grabmode, i))
				continue;

			if (dev.dump) {
				TRACE;
				dump(NULL, dev.buffers[i][buf.index].start,
				     dev.dump);
			}
			if (dev.send) {
				printf("SEND Buffer %d\n", buf.index);
				send_image(dev.buffers[i][buf.index].start,
					   buf.bytesused, i);
				printf("END SEND Buffer %d\n", buf.index);
			}
		}
#ifdef JPEG
		if (dev.jpeg) {
			TRACE;
			jpeg_convert(1, buf.index);
		}
#endif
		printf("QBUF %d\n", buf.index);
		//getchar();
		if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
			errno_exit("VIDIOC_QBUF", 5);
		printf("END QBUF %d\n", buf.index);
		//getchar();

		break;
	}

	return 1;
}

static void catch_frame(void)
{
	enum v4l2_buf_type type;
	int fd = GET_FD;
	DECLARE_TIM;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (dev.io == IO_METHOD_READ)
		return;

	dev.c_buffer++;
	printf("START STREAMON %d ...\n", dev.c_buffer);
	getchar();
	START_TIME;
	if (-1 == xioctl(fd, VIDIOC_STREAMON, &type)) {
		TRACE;
		errno_exit("VIDIOC_STREAMON", 10);
	}
	DUMP_TIME;
	printf("END STREAMON\n");
	getchar();
}

static void mainloop(void)
{
	unsigned int count;
	fd_set fds;
	struct timeval tv;
	int fd = GET_FD;
	int r;

	count = dev.frame_number;

	while (count-- > 0) {
		for (;;) {
			FD_ZERO(&fds);
			FD_SET(fd, &fds);

			/* Timeout. */
			tv.tv_sec = 2;
			tv.tv_usec = 0;

			catch_frame();
			sleep(2);
			goto exit;

			r = select(fd + 1, &fds, NULL, NULL, &tv);

			printf("r = %d\n", r);
			if (-1 == r) {
				if (EINTR == errno)
					continue;
				errno_exit("select", 6);
			}

			if (0 == r) {
				fprintf(stderr, "select timeout\n");
				//errno_exit("timeout", 7);
				continue;
			}

			//if (read_frame())
			//    break;
			/* EAGAIN - continue select loop. */
		}
		TRACE;
	}
exit:
	printf("Exit\n");
}

static void stop_capturing(void)
{
	enum v4l2_buf_type type;
	int fd = GET_FD;

	switch (dev.io) {
	case IO_METHOD_READ:
		/* Nothing to do. */
		break;

	case IO_METHOD_MMAP:
		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		TRACE;
		if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
			errno_exit("VIDIOC_STREAMOFF", 8);
		break;
	}
	TRACE;
}

static void start_capturing(void)
{
	unsigned int i;
	int fd = GET_FD;

	set_ctrl(fd, V4L2_CID_EXPOSURE, dev.expo, "Set EXPO value to");
	set_ctrl(fd, V4L2_CID_GAIN, dev.gain, "Set GAIN value to");

	if (FG_SOURCE(dev.grabmode) >= CTX) {
		printf("%d %d\n", dev.fd[CTX], fd);
		set_ctrl(dev.fd[CTX], V4L2_CID_RED_BALANCE, dev.r_gain,
			 "Set R GAIN to");
		set_ctrl(dev.fd[CTX], V4L2_CID_GREEN_BALANCE, dev.g_gain,
			 "Set G GAIN to");
		set_ctrl(dev.fd[CTX], V4L2_CID_BLUE_BALANCE, dev.b_gain,
			 "Set B GAIN to");
	}
	switch (dev.io) {
	case IO_METHOD_READ:
		/* Nothing to do. */
		break;

	case IO_METHOD_MMAP:
		for (i = 0; i < dev.frame_number; ++i) {
			struct v4l2_buffer buf;

			CLEAR(buf);
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_MMAP;
			buf.index = i;

			if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
				errno_exit("VIDIOC_QBUF", 9);
		}

		break;
	}
}

static void uninit_device(void)
{
	unsigned int j, i;

	switch (dev.io) {
	case IO_METHOD_READ:
		free(dev.buffers[0][0].start);
		break;

	case IO_METHOD_MMAP:
		for (i = 0; i < DOUBLE; i++) {
			if (!IS_ACTIVE(dev.grabmode, i))
				continue;
			for (j = 0; j < dev.frame_number; ++j) {
				if (-1 == munmap(dev.buffers[i][j].start,
						 dev.buffers[i][j].length))
					errno_exit("munmap", 11);
			}
			free(dev.buffers[i]);
		}
		break;
	}
}

static void init_read(void)
{
	int src = FG_SOURCE(dev.grabmode);

	dev.buffers[0] = calloc(1, sizeof(*dev.buffers[0]));

	if (!dev.buffers) {
		fprintf(stderr, "Out of memory\n");
		exit(EXIT_FAILURE);
	}

	dev.buffers[0][0].length = dev.banco[src].dim_oggetto_byte;
	dev.buffers[0][0].start = malloc(dev.banco[src].dim_oggetto_byte);

	if (!dev.buffers[0][0].start) {
		fprintf(stderr, "Out of memory\n");
		exit(EXIT_FAILURE);
	}
}

static void map(int fd, struct buffer *b, int offset, int length)
{
	printf("=== offset: %x len: %d\n", offset, length);
	b->length = length;
	b->start = mmap(NULL /* start anywhere */, length,
			PROT_READ | PROT_WRITE /* required */,
			MAP_SHARED /* recommended */, fd, offset);

	if (MAP_FAILED == b->start) {
		printf("<-> %s\n", "asdadada");
		errno_exit("mmap", 14);
	}

	b->phy_addr = offset;
	//printf("Mapped offset: %x len: %d\n", offset, length);
	TRACE;
}

static void init_mmap(void)
{
	struct v4l2_requestbuffers req;
	int buffer;
	struct v4l2_buffer buf;
	int fd = dev.fd[FG_SOURCE(dev.grabmode) == DOUBLE ?
				0 :
				FG_SOURCE(dev.grabmode)];
	struct v4l2_plane planes[DOUBLE];
	int i;

	CLEAR(req);
	req.count = dev.frame_number;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
		if (EINVAL == errno) {
			fprintf(stderr,
				"%s does not support "
				"memory mapping\n",
				dev.name[0]);
			exit(EXIT_FAILURE);
		} else {
			errno_exit("VIDIOC_REQBUFS", 12);
		}
	}

	if (req.count < 1) {
		fprintf(stderr, "Insufficient buffer memory on %s\n",
			dev.name[fd]);
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < DOUBLE; i++) {
		if (!IS_ACTIVE(dev.grabmode, i))
			continue;
		dev.buffers[i] = calloc(req.count, sizeof(*dev.buffers[i]));

		if (!dev.buffers[i]) {
			fprintf(stderr, "Out of memory\n");
			exit(EXIT_FAILURE);
		}
	}

	for (buffer = 0; buffer < req.count; ++buffer) {
		CLEAR(buf);
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = buffer;

		if (FG_SOURCE(dev.grabmode) == DOUBLE) {
			buf.m.planes = planes;
		}
		if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
			errno_exit("VIDIOC_QUERYBUF", 13);

		for (i = 0; i < DOUBLE; i++) {
			if (!IS_ACTIVE(dev.grabmode, i))
				continue;
			if (FG_SOURCE(dev.grabmode) == DOUBLE) {
				//pr_info(">>>> %x", buf.m.planes[i].m.mem_offset);
				map(dev.fd[i], &dev.buffers[i][buffer],
				    buf.m.planes[i].m.mem_offset,
				    buf.m.planes[i].length);
			} else {
				map(dev.fd[i], &dev.buffers[i][buffer],
				    buf.m.offset, buf.length);
			}
		}
	}
}

static void init_device(void)
{
	switch (dev.io) {
	case IO_METHOD_READ:
		init_read();
		break;

	case IO_METHOD_MMAP:
		init_mmap();
		break;
	}

#ifdef JPEG
	if (dev.jpeg)
		jpeg_configure();
#endif
}

static void jpeg_close(void)
{
	if (!dev.jpeg || dev.jp.fd < 0)
		return;

	munmap(dev.jp.vaddr, MB(4));
	close(dev.jp.fd);
	close(dev.jp.afd);
}

static void close_device(void)
{
	int i;

	for (i = 0; i < DOUBLE; i++) {
		if (!IS_ACTIVE(dev.grabmode, i))
			continue;

		printf("Close dev: %s\n", dev.name[i]);
		if (-1 == close(dev.fd[i]))
			errno_exit("close", 15);
		dev.fd[i] = -1;
	}

	if (dev.send)
		CloseSocketToSendImage(dev.socket);

	if (dev.jpeg)
		jpeg_close();
}

static void open_device(void)
{
	struct stat st;
	int source = FG_SOURCE(dev.grabmode);
	int i;

	for (i = 0; i < DOUBLE; i++) {
		if (-1 == stat(dev.name[i], &st)) {
			fprintf(stderr, "Cannot identify '%s': %d, %s\n",
				dev.name[i], errno, strerror(errno));
			continue;
		}

		if (!S_ISCHR(st.st_mode)) {
			fprintf(stderr, "%s is no device\n", dev.name[i]);
			continue;
		}

		if (IS_ACTIVE(source, i)) {
			dev.fd[i] = open(dev.name[i], O_RDWR, 0);

			if (-1 == dev.fd[i]) {
				fprintf(stderr, "Cannot open '%s': %d, %s\n",
					dev.name[i], errno, strerror(errno));
				exit(EXIT_FAILURE);
			}
		}
	}

	if (dev.send) {
		dev.socket = OpenSocketToSendImage(NULL, dev.ip, 0, 0);
		if (dev.socket == NULL)
			errno_exit("open socket\n", 20);
		else {
			printf("Open Socket @ %hhu.%hhu.%hhu.%hhu\n",
			       dev.ip.adr.adr1, dev.ip.adr.adr2,
			       dev.ip.adr.adr3, dev.ip.adr.adr4);
		}
	}
#ifdef JPEG
	if (dev.jpeg)
		jpeg_open();
#endif
}

static void usage(FILE *fp, int argc, char **argv)
{
	fprintf(fp,
		"Read single image frome device and send to network"
		"Usage: %s [options]\n\n"
		"Options:\n"
		"-n | --mode          Mode (TRIGGED(0),CONTINUOUS(4))|(OCR(0),CTX(1),DOUBLE(2)) [%d]\n"
		"-h | --help          Print this message\n"
		"-m | --mmap          Use memory mapped buffers [default]\n"
		"-r | --read          Use read() calls\n"
		"-s | --send          send to ip [%s]\n"
		"-d | --dump          dump n bytes\n"
		"-e | --expo          set expo value\n"
		"-i | --ip            Set ip address\n"
		"-j | --jpeg          Compress last image to filename[out.jpeg]\n"
		"-c | --count         Number of frames to grab [%i]\n\n"
		"-p | --period        Period in us [%d]\n\n"
		"-g | --gain          Set global gain value[300]\n"
		"-R | --red           Set ref gain value[100]\n"
		"-G | --green         Set green gain value[100]\n"
		"-B | --blue          Set blue  gain value[100]\n"
		"",
		argv[0], dev.grabmode, "no send", dev.frame_number, dev.period);
}

static const char short_options[] = "n:hmrsd:e:i:fc:g:p:R:G:B:j";

static const struct option long_options[] = {
	{ "mode", required_argument, NULL, 'n' },
	{ "help", no_argument, NULL, 'h' },
	{ "mmap", no_argument, NULL, 'm' },
	{ "read", no_argument, NULL, 'r' },
	{ "send", no_argument, NULL, 's' },
	{ "jpeg", no_argument, NULL, 'j' },
	{ "dump", required_argument, NULL, 'd' },
	{ "expo", required_argument, NULL, 'e' },
	{ "ip", required_argument, NULL, 'i' },
	{ "count", required_argument, NULL, 'c' },
	{ "gain", required_argument, NULL, 'g' },
	{ "red", required_argument, NULL, 'R' },
	{ "green", required_argument, NULL, 'G' },
	{ "blue", required_argument, NULL, 'B' },
	{ "period", required_argument, NULL, 'p' },
	{ 0, 0, 0, 0 }
};

static void sigint_handler(int dummy)
{
	int i;
	enum v4l2_buf_type type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	for (i = 0; i < DOUBLE; i++) {
		if (!IS_ACTIVE(dev.grabmode, i))
			continue;
		TRACE;
		if (-1 == xioctl(dev.fd[i], VIDIOC_STREAMOFF, &type)) {
			errno_exit("VIDIOC_STREAMOFF", 10);
		}
	}
	uninit_device();
	close_device();
}

int main(int argc, char **argv)
{
	int idx;
	int c;
	time_t t;

	/* Intializes random number generator */
	srand((unsigned)time(&t));
	signal(SIGINT, sigint_handler);
	signal(SIGSEGV, sigint_handler);

	for (;;) {
		c = getopt_long(argc, argv, short_options, long_options, &idx);
		if (-1 == c)
			break;

		switch (c) {
		case 0: /* getopt_long() flag */
			break;

		case 'n':
			sscanf(optarg, "%d", &dev.grabmode);
			break;

		case 'p':
			sscanf(optarg, "%d", &dev.period);
			break;

		case 'g':
			sscanf(optarg, "%d", &dev.gain);
			break;

		case 'R':
			sscanf(optarg, "%d", &dev.r_gain);
			break;

		case 'G':
			sscanf(optarg, "%d", &dev.g_gain);
			break;

		case 'B':
			sscanf(optarg, "%d", &dev.b_gain);
			break;

		case 'd':
			sscanf(optarg, "%d", &dev.dump);
			break;

		case 'e':
			sscanf(optarg, "%d", &dev.expo);
			break;

		case 'h':
			usage(stdout, argc, argv);
			exit(EXIT_SUCCESS);

		case 'm':
			dev.io = IO_METHOD_MMAP;
			break;

		case 'j':
			dev.jpeg = 1;
			break;

		case 'r':
			dev.io = IO_METHOD_READ;
			break;

		case 's':
			dev.send = 1;
			break;

		case 'i':
			sscanf(argv[1], "%hhu.%hhu.%hhu.%hhu", &dev.ip.adr.adr1,
			       &dev.ip.adr.adr2, &dev.ip.adr.adr3,
			       &dev.ip.adr.adr4);
			break;
		case 'c':
			errno = 0;
			dev.frame_number = strtol(optarg, NULL, 0);
			if (errno)
				errno_exit(optarg, 16);
			break;

		default:
			usage(stderr, argc, argv);
			exit(EXIT_FAILURE);
		}
	}

	printf("TNT_INIT_SENSOR %lx\n", TNT_INIT_SENSOR);

	TRY(dev.t)
	{
		set_grabmode();
		open_device();
		init_device();
		start_capturing();
		mainloop();
		stop_capturing();
		uninit_device();
		close_device();
	}
	CATCH(MMAP_ERR)
	{
		close_device();
	}
	CATCH(STOP_CAP)
	{
		uninit_device();
		close_device();
	}
	CATCH_ALL
	{
		fprintf(stderr, "ERROR: %d\n", v);
		stop_capturing();
		uninit_device();
		close_device();
	}
	END_TRY;

	return 0;
}
