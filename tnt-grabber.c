/** 
 \file tnt-grabber.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2021  s.r.l.
 

 \brief
*/

#include "tntvdo.h"
#include "debugfs/trace.h"

#define GRABBER_NAME "grabber"
struct tnt_grabber _grabber;

static int grabber_open(struct inode *inode, struct file *file)
{
	printk("GRABBER: Device open\n");
	return 0;
}

static int grabber_release(struct inode *inode, struct file *file)
{
	printk("GRABBER: Device close\n");
	return 0;
}

static long grabber_ioctl(struct file *file, unsigned int cmd,
			  unsigned long arg)
{
	printk("GRABBER: Device ioctl\n");
	return 0;
}

static ssize_t grabber_read(struct file *file, char __user *buf, size_t count,
			    loff_t *offset)
{
	printk("GRABBER: Device read\n");
	return 0;
}

static ssize_t grabber_write(struct file *file, const char __user *buf,
			     size_t count, loff_t *offset)
{
	printk("GRABBER: Device write\n");
	return 0;
}

static const struct file_operations grabber_fops = { .owner = THIS_MODULE,
						     .open = grabber_open,
						     .release = grabber_release,
						     .unlocked_ioctl =
							     grabber_ioctl,
						     .read = grabber_read,
						     .write = grabber_write };

int grabber_init(struct tntdrv *drv)
{
	int err;
	dev_t devid;
	struct tnt_grabber *grabber = &drv->grabber;
	struct class *class = grabber->class;
	struct device *dev = grabber->dev;
	struct cdev *cdev = &grabber->cdev;

	err = alloc_chrdev_region(&devid, 0, 1, GRABBER_NAME);
	if (err != 0) {
		pr_info(KERN_ALERT "GRABBER: Registering failed with %d\n",
			err);
		return err;
	}
	TRACE;
	grabber->devid = devid;
	TRACE;

	cdev_init(cdev, &grabber_fops);
	TRACE;
	err = cdev_add(cdev, devid, 1);
	if (err) {
		goto cdev_err;
	}
	TRACE;

	/* create class */
	class = class_create(THIS_MODULE, GRABBER_NAME);
	if (IS_ERR(class)) { /* Check for error and clean up if there is */
		pr_info("GRABBER: Failed to register device class\n");
		goto class_err; /* Correct way to return an error on a pointer */
	}
	TRACE;

	/* Register the device driver */
	/* save pointer to driver data */
	dev = device_create(class, NULL, devid, drv, GRABBER_NAME);
	if (IS_ERR(dev)) { /* Clean up if there is an error   */
		pr_info("Failed to create the device\n");
		goto dev_err;
	}
	TRACE;

dev_err:
	class_destroy(class);
class_err:
	unregister_chrdev(MAJOR(devid), GRABBER_NAME);
cdev_err:
	unregister_chrdev_region(devid, 1);

	return 0;
}

void grabber_destroy(struct tnt_grabber *grabber)
{
	cdev_del(&grabber->cdev);
	device_destroy(grabber->class, grabber->devid);
	class_unregister(grabber->class);
	unregister_chrdev_region(grabber->devid, 1);
	class_destroy(grabber->class);
}
