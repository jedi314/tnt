/** 
 \file tnt-allocator.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Reserved memory layout:

 chunk: a single block of memory requested by TNT picked up from 
 a specific ctx.

 ctx: a set of blocks gruped.
 The size of a ctx is N_chunk x chunk_size.
                                      +------ alc_ctx.free
                                      |
                                      v
 +-----------+-----------+-----------+-----------+-----------+
 |           |           |           |           |           |          
 |           |           |           |           |           |
 |  Chunk#1  |  Chunk#2  |  Chunk#3  |  Free Cnk |  Free Cnk | Ctx#0
 |           |           |           |           |           |
 |           |           |           |           |           |
 +-----------+-----------+-----------+-----------+-----------+ 
 |           |           |           |           |           |
 |           |           |           |           |           |
 |  Chunk#1  |  Chunk#2  |  Chunk#3  |  Chunk#4  |           | Ctx#1
 |           |           |           |           |           |
 |           |           |           |           |           |
 +-----------+-----------+-----------+-----------+-----------+ 
 |                                               |<--- tnt_allocator.free
 |                                               |
 |                 Free Memory                   |
 |                                               |
 |                                               |
 +-----------+-----------+-----------+-----------+ 


*/

#ifndef _TNT_ALLOCATOR_H
#define _TNT_ALLOCATOR_H

#include <linux/types.h>
#include <linux/ioport.h>
#include <linux/device.h>
#include <linux/cdev.h>

#include "debugfs/tdm.h"
#include "tnt-dbg.h"
#include "include/allocator.h"

#define TNT_MAX_CTXS 4

struct alc_chunk {
	dma_addr_t addr;
	int __iomem *vaddr;
	refcount_t refcount;
};

struct alc_ops {
	void (*init)(void *addr); /*!< init is called when create a new ctx */
};

struct ctx_ops {
	void (*init)(
		struct alc_chunk
			*chunk); /*!< init is called when allocate new chunk */
};

struct alc_ctx {
	struct device dev;
	dma_addr_t org;
	u64 __iomem *vaddr;
	u64 free; /**!< pointer to free area  */
	u32 chunk_size;
	u32 size;
	u32 n_chunks; /**!< for debugfs purpose  */
	u32 id;
	struct ctx_ops *ops;
	struct tdm_debug dbg;
};

struct tnt_allocator {
	struct device *dev;
	struct class *class;
	struct cdev cdev;
	int devid;

	dma_addr_t org;
	u64 __iomem *vaddr;
	u32 size;
	u64 free; /**!< pointer to free area   */
	int n_ctxs;

	struct mutex lock;
	struct alc_ctx ctxs[TNT_MAX_CTXS];
	struct alc_ops *ops;
	struct tdm_debug dbg;
};

struct alc_fh {
	struct tnt_allocator *alc;
	u8 id[TNT_MAX_CTXS];
	u8 n;
};

void allocator_init(struct tnt_allocator *alc, struct resource *res,
		    struct alc_ops *ops);
int alloc_ctx(struct tnt_allocator *al, const char *name, int n_chunk,
	      int *chunk_size, struct ctx_ops *ops);
struct alc_chunk *alc_alloc_chunk(struct tnt_allocator *al, int id);
struct alc_ctx *ctx_addr(struct tnt_allocator *al, int id);
int alc_release_chunk(struct tnt_allocator *al, struct alc_chunk *chunk);
void allocator_destroy(struct tnt_allocator *alc);
u64 alc_chunk_offset(struct tnt_allocator *alc, int id, u64 off);
void allocator_release_ctxs(struct tnt_allocator *alc);
int release_ctx(struct tnt_allocator *alc, unsigned int id);

#endif
