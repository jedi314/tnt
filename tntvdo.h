/** 
 \file tnttvdo.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Header con taining main struct of driver
*/

#ifndef _TNTVDO_H
#define _TNTVDO_H

#include <media/v4l2-device.h>
#include <linux/interrupt.h>

#include <linux/list.h>
#include "tnt-videobuf2-v4l2.h"

#include "tnt-io.h"
#include "tnt-regs.h"
#include "tnt-video-formats.h"
#include "list.h"
#include "tnt-ctrl.h"
#include "tnt-cmd.h"
#include "tnt-slot.h"
#include "tnt-strobe.h"
#include "tnt-stats.h"

#define N_FORMATS 16
#define N_SENSORS 16
#define N_PROCESSOR 16
#define N_MIN_BUFS 3

struct tnt_fh;
struct tnt_buf {
	/* common v4l buffer stuff -- must be first */
	struct vb2_buffer vb;
	struct list_head active_entry;
	struct list_head dqueued_entry;
	struct tnt_optd *optd;

	int refcount;
	u64 realtime;
	int a;
	int state;
	u64 uid;
	int dqueued;
};

struct tntvdo;
struct tntdrv;

typedef struct tnt_buf *(*fn_pick)(struct tntdrv *, struct v4l2_buffer *,
				   fn_compare);

struct grab_mode_ops {
	//int (*q_buffer)(struct tntdrv *drv, struct tnt_buf* buf);
	int (*dq_buffer)(struct tntdrv *drv, struct v4l2_buffer *vb);
};

struct tnt_sensor_desc {
	const char *name;
	enum tnt_sensors code;
	struct tnt_video_fmt fmt;
};

struct tnt_resize {
	u32 w;
	u32 h;
};

struct lut {
	struct alc_chunk *data;
	int width;
	int height;
	int bpp;
	u32 size;
	int id;
	char *name;
	struct tnt_device *dev;
};

struct tnt_remap {
	struct tnt_resize in;
	struct tnt_resize out;
	u32 max_displace;
	u32 delta_max;
	struct lut lut;
};

struct tnt_processor;

struct tnt_processor_ops {
	int (*init)(struct tnt_processor *);
	u32 (*plane_size)(struct tnt_processor *);
	int (*alloc)(struct tnt_processor *, u64 size);
	int (*mmap)(struct tnt_processor *, struct vm_area_struct *vma);
	int (*process)(struct tnt_processor *, void *);
	int (*set_addr)(struct tnt_processor *, void *);
	int (*destroy)(struct tnt_processor *);
	int (*info)(struct tnt_processor *, struct processor_layout *);
};

struct tnt_processor {
	const char *name;
	const struct tnt_processor_ops *ops;
	const struct v4l2_subdev_ops *sdev_ops;
	const struct v4l2_subdev_internal_ops *internal_ops;
	struct v4l2_subdev sub;
	struct tntvdo *vdo;
	int cntx_id;
	int plane_id;
	void *info;
};

struct processor_list {
	const char *name;
	void *data;
};

struct tntvdo {
	struct tnt_device device;

	struct v4l2_device v4l2_dev;
	struct video_device video_dev;
	struct v4l2_subdev sensor;
	struct strobe_unit *strobe;

	struct mutex lock;
	struct tnt_fh *fh;

	int n_formats;
	int cntx_id;
	int users;
	int id;

	struct acq_points wlists[2]; /* current acquisition point list */
	spinlock_t acq_point_lock;
	atomic_t update_acq;
	int c_point;
	int c_wlist; /* current acquisition point list */
	u8 *allowed_sensors;

	char *name;

	/* processor */
	int n_processor;
	struct tnt_processor *processor_list;

	/* dev */
	struct tnt_video_fmt *current_fmt;
	struct tnt_sensor_desc *current_sensor;
	struct acq_points *current_wlist;
	int nbuffers;

	struct tntdrv *driver;
	struct vdo_ctrls ctrls;

	/* Wait queue */
	struct wait_queue_head wq_head;
	struct mutex wq_mutex;
	atomic_t wq_irq;

	/* test var */
	int o;
	struct mutex m;
};

struct tntdrv {
	struct tnt_device device;
	struct tntvdo cha;
	struct tntvdo chb;
	struct tntvdo *master; /*!< main dominant sensor  */
	struct tnt_allocator *allocator;
	struct tntvdo *active_vdo[2];
	struct tntvdo *installed_vdo[2];

	struct tnt_slot access;
	int n_future;
	u64 t_future;
	int n_buf;
	int n_planes;
	u64 ttt;
	spinlock_t last_lock;
	u64 last_timestamp;
	u64 last_uid;
	u64 igid; /* Incremental grab ID */

	atomic_t n_active;
	int n_sensors;
	int n_processor;

	wait_queue_head_t wait_future_frames;
	spinlock_t list_lock;

	enum tnt_grabmode grabmode;
	struct tnt_rle_conf rle_conf[RLE_ALGO_NUM];
	int rle_enable;

	int queue_refs;
	struct vb2_queue queue; /*!< v4l2 videobuf2 queue */

	struct tnt_sensor_desc sensors[N_SENSORS];
	struct processor_list processors[N_PROCESSOR];
	struct tnt_resize resizer;
	struct tnt_remap remapper;
	struct tnt_strobe strobe;

	struct list_head active_list;
	struct list_head dqueued_list;

	spinlock_t vb_lock; /*!< spinlock for the videobuf2 queue ops */
	struct tnt_buf *active; /*!< active buffer */
	struct tnt_buf *busybuf; /*!< buffe really used by fpga */
	int finalize;

	int stop;
	u64 period;
	int busy_catch;
	int end_frame;
	int end_cha_frame;
	int end_chb_frame;
	int o;
	int oo;
	int cnt_cha;
	int cnt_chb;
	struct tnt_stats stats;

	struct tdm_debug queue_trace; /*!< frame queues trace  */
};

/* specific file handle data  */
struct tnt_fh {
	/* must remain the first field of this struct */
	struct v4l2_fh fh;
	struct tntvdo *dev;

	struct tnt_video_fmt *current_fmt;
	int sync;
	int id;

	/* TODO: add specific data here */
	enum v4l2_buf_type type;
};
int vdo_ctrl_init(struct tntvdo *vdo);
int vdo_ctrl_sensor_enable(struct tntvdo *vdo, bool en);

#define DEV_NAME(f) (f)->f_path.dentry->d_iname

#endif
