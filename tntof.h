/** 
 \file tntof.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNTOF_H
#define _TNTOF_H
int tntof_hook_vdo(struct tntdrv *drv, struct tntvdo *vdo, char *path);
int tnt_get_available_sensors(struct tntdrv *drv);
void tnt_print_available_sensors(struct tntdrv *drv);
int tnt_get_available_processor(struct tntdrv *drv);
void print_processor_info(struct tntdrv *drv);

#endif
