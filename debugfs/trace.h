/** 
 \file tnt-dbg.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Debug stuff
*/

#ifndef _TRACE_H
#define _TRACE_H

#include <linux/printk.h>

#define DEBUG_PRINT(fmt, args...) pr_info("DEBUG: %s:%d:%s: " fmt, \
    __FILE__, __LINE__, __PRETTY_FUNCTION__ , ##args)


#define TRACE DEBUG_PRINT("\n")

#endif
