/** 
 \file tnt-access.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_ACCESS_H
#define _TNT_ACCESS_H

#include <linux/videodev2.h>
#include "tntvdo.h"

int dq_buffer(struct tntdrv *drv, struct v4l2_buffer *vb);
int q_buffer(struct tntdrv *drv, struct v4l2_buffer *vb);
enum tnt_grabmode tnt_get_grabmode(struct tntdrv *drv);

int tnt_set_grabmode(struct tntdrv *drv, enum tnt_grabmode mode);
void *convert_tntbuf(struct list_head *el, void *c);
inline bool just_dqueued(struct tnt_buf *buf);

#endif
