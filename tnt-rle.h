/** 
 \file tnt-rle.h
 
 

 \brief
*/

#ifndef _TNT_RLE_H_
#define _TNT_RLE_H_

#include "tnt-io.h"
#include "tnt-cmd.h"
#include "tntvdo.h"

int tnt_rle_init(struct tntdrv *drv);
int tnt_rle_enable(struct tnt_device *dev);
int tnt_set_rle_cha(struct tnt_device *dev, const struct tnt_rle_conf *par,
		    u8 index);
int tnt_set_rle_ptr_blob(struct tnt_device *dev, u32 ptr);
int tnt_set_rle_ptr(struct tnt_device *dev, u32 numseg_ptr, u32 rle_ptr,
		    u8 index);

void tnt_rle_enable_dma(struct tnt_device *dev);
void tnt_rle_disable_dma(struct tnt_device *dev);
u32 tnt_rle_get_size_blob(struct tntdrv *drv);

#endif
