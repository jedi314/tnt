/** 
 \file tnt-dima.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2021  s.r.l.
 

 \brief
*/

#include <media/v4l2-dev.h>
#include "tntvdo.h"
#include "tnt-io.h"
#include "tnt-cmd.h"
#include "tnt-dima.h"

inline void imx_enable_dima(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_GPIO_DIMA, 1);
}

inline void imx_disable_dima(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_GPIO_DIMA, 0);
}

inline bool imx_dima_is_enabled(struct tnt_device *dev)
{
	return tnt_read(dev, FSM_GPIO_DIMA_IS_ENABLE) == 1;
}

inline int imx_dima_state(struct tnt_device *dev)
{
	return tnt_read(dev, AUX_GPIO_VALUE);
}

inline bool imx_dima_is_opened(struct tnt_device *dev)
{
	return tnt_read(dev, SENS_FOR_DIMA_IS_READY) == 1;
}

inline int imx_dima_input(struct tnt_device *dev)
{
	struct dima d;
	d.v = tnt_read(dev, AUX_GPIO_VALUE);

	return d.input;
}

int ioctl_dima(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct tntvdo *vdo = video_drvdata(filp);
	struct tnt_device *dev = &vdo->device;
	struct tnt_cmd dima;
	int ret = 0;

	ret = copy_from_user(&dima, (struct tnt_cmd *)arg, sizeof(dima));

	switch (dima.reg) {
	case DIMA_ENABLE:
		imx_enable_dima(dev);
		break;
	case DIMA_DISABLE:
		imx_disable_dima(dev);
		break;
	case DIMA_IS_OPENED:
		dima.value = imx_dima_is_opened(dev);
		ret = copy_to_user((struct tnt_cmd *)arg, &dima, sizeof(dima));
		break;
	case DIMA_IS_ENABLED:
		dima.value = imx_dima_is_enabled(dev);
		ret = copy_to_user((struct tnt_cmd *)arg, &dima, sizeof(dima));
		break;
	case DIMA_STATE:
		dima.value = imx_dima_state(dev);
		ret = copy_to_user((struct tnt_cmd *)arg, &dima, sizeof(dima));
		break;
	default:
		pr_info("Unhadled dima command: 0x%x\n", dima.reg);
	}

	return ret;
}
