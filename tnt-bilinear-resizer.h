#ifndef _TNT_BILINEAR_RESIZER_H_
#define _TNT_BILINEAR_RESIZER_H_

#include "tntvdo.h"

int bilinear_resizer_enable(struct tntvdo *vdo);

#endif
