#include <linux/types.h>

#include "tnt-allocator.h"
#include "tnt-processor.h"
#include "tntvdo.h"

#define REG_WR_SET_HOR_INDEX 0x00020200
#define REG_WR_SET_VER_INDEX 0x00020204
#define REG_WR_SET_PTR_DMA13 0x00020208
#define REG_WR_ENABLE 0x0002020C

#define BILINEAR_RESIZER_COEFF1 191
#define BILINEAR_RESIZER_COEFF2 409
#define BILINEAR_RESIZER_OUT_W 1280
#define BILINEAR_RESIZER_OUT_H 1024
#define BILINEAR_RESIZER_BOTTOM_MATTE 64

static void bilinear_resizer_ram_init(struct tnt_device *dev, u32 in_w,
				      u32 in_h, u32 out_w, u32 out_h, u32 c1,
				      u32 c2)
{
	u32 idx, delta, i, x1, y1;

	for (i = 0, idx = 0; i < out_w; i++, idx++) {
		x1 = (c1 + (i * c2)) >> 8;
		delta = (c1 + (i * c2)) - (x1 << 8);

		if (x1 != idx) {
			for (; idx < x1; idx++)
				tnt_write(dev, REG_WR_SET_HOR_INDEX,
					  idx & 0x7FF);
		}

		tnt_write(dev, REG_WR_SET_HOR_INDEX,
			  ((delta & 0xFF) << 24) | ((1 & 0x1) << 12) |
				  (idx & 0x7FF));
	}

	for (; idx < in_w; idx++)
		tnt_write(dev, REG_WR_SET_HOR_INDEX, idx & 0x7FF);

	for (i = 0; i < out_h; i++) {
		y1 = (c1 + (i * c2)) >> 8;
		delta = (c1 + (i * c2)) - (y1 << 8);
		tnt_write(dev, REG_WR_SET_VER_INDEX,
			  ((delta & 0xFF) << 24) | ((y1 & 0x7FF) << 12) |
				  (i & 0x7FF));
	}
}

int bilinear_resizer_enable(struct tntvdo *vdo)
{
	bilinear_resizer_ram_init(
		&vdo->device, vdo->current_fmt->width, vdo->current_fmt->height,
		BILINEAR_RESIZER_OUT_W,
		BILINEAR_RESIZER_OUT_H - BILINEAR_RESIZER_BOTTOM_MATTE,
		BILINEAR_RESIZER_COEFF1, BILINEAR_RESIZER_COEFF2);

	tnt_write(&vdo->device, REG_WR_ENABLE, 1);
	return 0;
}

static u32 bilinear_resizer_plane_size(struct tnt_processor *pr)
{
	return BILINEAR_RESIZER_OUT_W * BILINEAR_RESIZER_OUT_H;
}

static int bilinear_resizer_info(struct tnt_processor *pr,
				 struct processor_layout *l)
{
	l->w = BILINEAR_RESIZER_OUT_W;
	l->h = BILINEAR_RESIZER_OUT_H;
	l->size = bilinear_resizer_plane_size(pr);
	return 0;
}

static int bilinear_resizer_set_addr(struct tnt_processor *pr, void *b)
{
	struct tnt_device *dev = get_dev(pr);
	struct tnt_buf *buf = (struct tnt_buf *)b;
	struct alc_chunk *chunk = buf->vb.planes[pr->plane_id].mem_priv;

	tnt_write(dev, REG_WR_SET_PTR_DMA13, chunk->addr);
	return 0;
}

static long bilinear_resizer_ioctl(struct v4l2_subdev *sd, unsigned int cmd,
				   void *arg)
{
	return 0;
}

struct tnt_processor_ops bilinear_resizer_ops = {
	.plane_size = bilinear_resizer_plane_size,
	.set_addr = bilinear_resizer_set_addr,
	.info = bilinear_resizer_info,
};

const struct v4l2_subdev_core_ops bilinear_resizer_core_ops = {
	.ioctl = bilinear_resizer_ioctl,
};

const struct v4l2_subdev_ops bilinear_resizer_sdev_ops = {
	.core = &bilinear_resizer_core_ops,
};
