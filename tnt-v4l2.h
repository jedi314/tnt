/** 
 \file tnt-v4l2.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_V4L2_H
#define _TNT_V4L2_H

#include "tntvdo.h"

int tnt_video_init(struct tntvdo *vdo, char *name);
int tnt_g_fmt_vid_cap_vdo(struct tntvdo *vdo, struct v4l2_format *f);
int tnt_try_fmt_vid_cap_vdo(struct tntvdo *vdo, struct v4l2_format *f);
int tnt_s_fmt_vid_cap_vdo(struct tntvdo *vdo, struct v4l2_format *f);

#endif
