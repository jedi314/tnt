/** 
 \file tnt-strobe.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_STROBO_H
#define _TNT_STROBO_H

#include <linux/types.h>
#include <media/v4l2-device.h>
#include "tnt-io.h"

enum strobe_channel {
	EXTERNAL = 0,
	INTERNAL = 8,
};

enum strobe_level {
	LEVEL_LOW = 0,
	LEVEL_HIGH,
};

enum strobe_connection {
	CONNECT_GND = 0,
	CONNECT_CHA,
	CONNECT_CHB,
	CONNECT_CHAB,
	CONNECT_SW,
};

enum strobe_status {
	STEPUP_LOW,
	STEPUP_HIGH_BY_UBOOT,
	STEPUP_HIGH_BY_KERNEL,
	MOSFET_HIGH,
};

struct strobe_unit {
	struct tnt_device *dev;
	enum strobe_channel ch;
	struct tdm_debug dbg;
	enum strobe_connection connection;

	u32 level;
};

struct tnt_strobe {
	struct v4l2_subdev strbdev;
	struct strobe_unit external;
	struct strobe_unit internal;
	struct gpio_desc *stepup;
	struct gpio_desc *mosfet;
	struct tdm_debug dbg;
	struct timer_list timer;
	enum strobe_status status;
};

struct tntvdo;
struct tntdrv;

void strobe_put(struct tnt_strobe *strb);
int strobe_init(struct tntdrv *drv);

inline int connect_strobe_channel(struct strobe_unit *unit,
				  enum strobe_connection source);
inline int disconnect_strobe_channel(struct strobe_unit *unit,
				     enum strobe_connection source);
inline void strobe_level(struct strobe_unit *unit, int level);

#endif
