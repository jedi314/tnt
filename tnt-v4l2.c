/** 
 \file tnt-v4l2-ioctl.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Contains all v4l2 and ioctl and syscalls used
*/

#include <media/v4l2-common.h>
#include <media/v4l2-device.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-fh.h>
#include <media/v4l2-event.h>
#include <linux/delay.h>
#include <linux/version.h>

#include "tnt-video-formats.h"
#include "tnt-allocator.h"
#include "tnt-v4l2.h"
#include "tnt-fpga-utils.h"
#include "tnt-imx265.h"
#include "tnt-cmd.h"
#include "tnt-subdev.h"
#include "tnt-access.h"
#include "tnt-queue.h"
#include "tnt-rle.h"

#include "fpga-cs.h"
#include "tnt-dbg.h"
#include "tnt-dima.h"
#include "tnt-processor.h"

inline int dequeue_buffers_by_ts(struct tntdrv *drv, struct tnt_frames *frames);
inline int dequeue_buffers_by_uid(struct tntdrv *drv,
				  struct tnt_frames *frames);

static inline struct alc_chunk *get_chunk(struct tnt_buf *buf)
{
	return buf->vb.planes[0].mem_priv;
}

static inline int get_minor(struct tntvdo *vdo)
{
	return vdo->video_dev.minor / 2;
}

static inline int getsize(struct tnt_video_fmt *fmt)
{
	return fmt->width * fmt->height * fmt->bpp / 8;
}

struct tnt_video_fmt *format_by_fourcc(struct tntdrv *drv, int fourcc)
{
	struct tnt_video_fmt *fmt;
	struct tnt_sensor_desc *desc = &drv->sensors[0];
	int i;

	for (i = 0; i < drv->n_sensors; i++) {
		fmt = &desc[i].fmt;
		pr_info("%s %d%s%d\n", fmt->name, fmt->fourcc,
			fmt->fourcc == fourcc ? "==" : "!=", fourcc);

		if (fmt->fourcc == fourcc)
			return fmt;
	}

	pr_info("Unknown pixelformat:'%4.4s'\n", (char *)&fourcc);
	return NULL;
}

/* v4l2 ioctl operations */

static int tnt_querycap(struct file *filp, void *priv,
			struct v4l2_capability *cap)
{
	struct tnt_fh *fh = priv;
	struct tntvdo *vdo = fh->dev;

	strcpy(cap->driver, vdo->video_dev.name);
	strcpy(cap->card, " Basic 2.0");
	cap->version = KERNEL_VERSION(5, 14, 0);
	strcpy(cap->bus_info, "platform:tnt");

	cap->device_caps = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_READWRITE |
			   V4L2_CAP_STREAMING;
	cap->capabilities = cap->device_caps | V4L2_CAP_DEVICE_CAPS;
	return 0;
}

static int tnt_enum_input(struct file *filp, void *priv,
			  struct v4l2_input *input)
{
	if (input->index != 0)
		return -EINVAL;

	input->type = V4L2_INPUT_TYPE_CAMERA;
	input->std = V4L2_STD_ALL; /* Not sure what should go here */

	/* TODO: Split CHA CHB inputs */
	strcpy(input->name, "CHA/CHB");
	return 0;
}

static int tnt_g_input(struct file *filp, void *priv, unsigned int *i)
{
	*i = 0;
	return 0;
}

static int tnt_s_input(struct file *filp, void *priv, unsigned int i)
{
	if (i != 0)
		return -EINVAL;
	return 0;
}

static int tnt_enum_fmt_vid_cap(struct file *filp, void *priv,
				struct v4l2_fmtdesc *fmt)
{
	struct tntvdo *vdo = video_drvdata(filp);
	struct tnt_video_fmt *format;
	struct tntdrv *drv = vdo->driver;

	if (fmt->index >= drv->n_sensors)
		return -EINVAL;

	format = &drv->sensors[fmt->index].fmt;
	strlcpy(fmt->description, format->name, sizeof(fmt->description));
	fmt->pixelformat = format->fourcc;
	return 0;
}

static int tnt_s_std(struct file *filp, void *priv, v4l2_std_id std)
{
	return 0;
}

static int tnt_g_std(struct file *filp, void *priv, v4l2_std_id *std)
{
	*std = V4L2_STD_NTSC_M;
	return 0;
}

int tnt_g_fmt_vid_cap_vdo(struct tntvdo *vdo, struct v4l2_format *f)
{
	struct tnt_video_fmt *format;

	format = vdo->current_fmt;

	pr_info("Get pixelformat %s %d\n", format->name, format->fourcc);

	f->fmt.pix.width = format->width;
	f->fmt.pix.height = format->height;
	f->fmt.pix.field = V4L2_FIELD_NONE;
	f->fmt.pix.pixelformat = format->fourcc;
	/* TODO: Set currectly depth */
	f->fmt.pix.bytesperline = f->fmt.pix.width * 3;
	f->fmt.pix.sizeimage = f->fmt.pix.height * f->fmt.pix.bytesperline;
	f->fmt.pix.colorspace = V4L2_COLORSPACE_DEFAULT;
	return 0;
}

int tnt_try_fmt_vid_cap_vdo(struct tntvdo *vdo, struct v4l2_format *f)
{
	struct tnt_video_fmt *fmt;
	struct tntdrv *drv = vdo->driver;

	fmt = format_by_fourcc(drv, f->fmt.pix.pixelformat);
	if (!fmt) {
		pr_info("Fourcc format (0x%08x) invalid.\n",
			f->fmt.pix.pixelformat);
		return -EINVAL;
	}

	f->fmt.pix.field = V4L2_FIELD_NONE;

	/* force dimension; if is used for future dimension options */
	if (f->fmt.pix.height != fmt->height)
		f->fmt.pix.height = fmt->height;
	if (f->fmt.pix.width != fmt->width)
		f->fmt.pix.width = fmt->width;

	pr_info("Fourcc format %s.\n", fmt->name);

	f->fmt.pix.width &= ~0x03;

	/* TODO: remember depth */
	f->fmt.pix.bytesperline = f->fmt.pix.width * 3 >> 3;
	f->fmt.pix.sizeimage = f->fmt.pix.height * f->fmt.pix.bytesperline;
	f->fmt.pix.colorspace = V4L2_COLORSPACE_DEFAULT;

	return 0;
}

int tnt_s_fmt_vid_cap_vdo(struct tntvdo *vdo, struct v4l2_format *f)
{
	int ret;
	struct tnt_video_fmt *fmt;
	struct tntdrv *drv = vdo->driver;

	pr_info("Set pixelformat %d\n", f->fmt.pix.pixelformat);
	ret = tnt_try_fmt_vid_cap_vdo(vdo, f);
	if (ret < 0) {
		pr_info("No fmt 0x%08x\n", f->fmt.pix.pixelformat);
		return ret;
	}
	pr_info("Pick format %d\n", f->fmt.pix.pixelformat);
	fmt = format_by_fourcc(drv, f->fmt.pix.pixelformat);
	pr_info("Select format %d\n", fmt->fourcc);

	vdo->current_fmt = fmt;
	return 0;
}

static int tnt_g_fmt_vid_cap(struct file *filp, void *priv,
			     struct v4l2_format *f)
{
	struct tntvdo *vdo = (struct tntvdo *)priv;
	return tnt_g_fmt_vid_cap_vdo(vdo, f);
}

static int tnt_try_fmt_vid_cap(struct file *filp, void *priv,
			       struct v4l2_format *f)
{
	struct tntvdo *vdo = video_drvdata(filp);
	return tnt_try_fmt_vid_cap_vdo(vdo, f);
}

static int tnt_s_fmt_vid_cap(struct file *filp, void *priv,
			     struct v4l2_format *f)
{
	struct tntvdo *vdo = video_drvdata(filp);
	return tnt_s_fmt_vid_cap_vdo(vdo, f);
}

static void clean_ctx(struct tntdrv *drv, struct tntvdo *vdo,
		      struct tnt_allocator *allocator)
{
	release_ctx(allocator, vdo->cntx_id);
	if (get_minor(vdo) == 0 && drv->rle_enable) {
		int source = FG_SOURCE(drv->grabmode);
		int id = source == DOUBLE ? 2 : 1;
		pr_info("RLE Request release ctx# %d\n", id);
		release_ctx(allocator, id);
	}
	tnt_processors_release_ctx(allocator, vdo);

	if (drv->queue_refs > 0)
		drv->queue_refs--;

	//pr_info(" %d refs %d, num: %d\n", get_minor(vdo), drv->queue_refs,  drv->queue.num_buffers);
	if (drv->queue_refs == 0 && drv->queue.num_buffers > 0) {
		tnt_deactivate_all_buffers(drv);
		tnt_remove_active_frame(drv);
		tnt_remove_busy_frame(drv);
		tnt_vb2_core_queue_release(&drv->queue);
		INIT_LIST_HEAD(&drv->dqueued_list);
		//INIT_LIST_HEAD(&drv->cache_list);
		drv->queue.owner = NULL;
	}
}

static int tnt_reqbufs(struct file *file, void *priv,
		       struct v4l2_requestbuffers *p)
{
	//struct tnt_fh *fh = priv;
	struct tntvdo *vdo = video_drvdata(file);
	struct tntdrv *drv = vdo->driver;

	struct video_device *vdev = video_devdata(file);
	struct tnt_allocator *allocator = drv->allocator;
	int vdo_id = get_minor(vdo);
	int ret = 0;

	if (p->count == 0) {
		clean_ctx(drv, vdo, allocator);
	} else {
		drv->queue_refs++;
		pr_info("REQBUF(VDO=%d): Requested n=%d buffers\n", vdo_id,
			p->count);
	}
	ret = tnt_core_reqbufs(vdev->queue, p->memory, &p->count);
	if (ret) {
		return ret;
	}
	drv->n_buf = p->count;
	atomic_set(&drv->n_active, 0);

	/* return ctx id */
	p->reserved[0] = allocator->n_ctxs - 1;
	drv->last_timestamp = 0;
	//ret = vb2_ioctl_reqbufs(file, priv, p);
	return ret;
}

int tnt_querybuf(struct file *file, void *priv, struct v4l2_buffer *p)
{
	struct video_device *vdev = video_devdata(file);

	/* No need to call vb2_queue_is_busy(), anyone can query buffers. */
	return tnt_vb2_querybuf(vdev->queue, p);
}

/* The queue is busy if there is a owner and you are not that owner. */
static inline bool vb2_queue_is_busy(struct video_device *vdev,
				     struct file *file)
{
	return vdev->queue->owner && vdev->queue->owner != file->private_data;
}

int q_buffer(struct tntdrv *drv, struct v4l2_buffer *vb);
int tnt_qbuf(struct file *file, void *priv, struct v4l2_buffer *p)
{
	struct tntvdo *vdo = video_drvdata(file);
	struct tntdrv *drv = vdo->driver;
	int ret;

	/* if (vb2_queue_is_busy(vdev, file)) */
	/* 	return -EBUSY; */

	if (p->index >= drv->queue.num_buffers) {
		return -EINVAL;
	}

	ret = q_buffer(drv, p);
	return ret;
}

void tnt_prepare_grab(struct tntdrv *drv)
{
	int i;
	struct tnt_device *active_dev;

	for (i = 0; i < DOUBLE; i++) {
		active_dev = &drv->active_vdo[i]->device;
		if (active_dev == NULL)
			continue;
		tnt_fpga_prepare_grab(active_dev);
	}
}

static __maybe_unused int trig_single_channel(struct tnt_device *dev, void *b)
{
	tnt_fpga_prepare_grab(dev);
	call_devop(dev, set_buffer_addr, dev, b);
	tnt_write(dev, START, 1);

	return 0;
}

static struct tnt_device *trig_channel(struct tntdrv *drv, void *b)
{
	int i;
	struct tntvdo *vdo;
	struct tnt_device *dev = NULL;
	int source = FG_SOURCE(drv->grabmode);
	u32 start = source == DOUBLE ? START_DOUBLE : START;

	for (i = 0; i < DOUBLE; i++) {
		vdo = drv->active_vdo[i];
		if (vdo == NULL) {
			continue;
		}
		dev = &vdo->device;
		tnt_fpga_prepare_grab(dev);
		call_devop(dev, set_buffer_addr, dev, b);
	}

	dev = source == DOUBLE ? &drv->device : dev;
	tnt_write(dev, start, 1);

	return dev;
}

bool image_ready(struct tnt_device *dev);

int vdo_grab_image(struct tntvdo *vdo, bool block)
{
	struct tntdrv *drv = vdo->driver;

	struct tnt_device *dev;
	int gmode = FG_GMODE(drv->grabmode);
	int err = 0;

	if (drv->queue.num_buffers == 0) {
		return -EINVAL;
	}

	if (gmode != TRIGGED)
		return -EINVAL;

	drv->active = tnt_activate_older_buffer(&drv->active_list);

	if (drv->active == NULL)
		return -EINVAL;

	dev = trig_channel(drv, drv->active);
	if (dev == NULL)
		return -EINVAL;

	if (block) {
		err = wait_event_interruptible_timeout(
			vdo->wq_head, image_ready(dev) == 1, 1000);
		//err = wait_until_image_ready(dev);
		if (err)
			return err;
		//queue_current_buffer(drv, drv->active);
	}

	return 0;
}

static int tnt_streamon(struct file *file, void *priv, enum v4l2_buf_type i)
{
	struct tntvdo *vdo = video_drvdata(file);
	struct tntdrv *drv = vdo->driver;

	struct vb2_queue *q = &drv->queue;
	struct video_device *vdev = &vdo->video_dev;
	int ret = 0;
	int gmode = FG_GMODE(drv->grabmode);
	bool block = (file->f_flags & O_NONBLOCK) == O_NONBLOCK ? false : true;

	if (gmode == CONTINUOUS) {
		tnt_stats_reset(&drv->stats);
		tnt_stats_start(&drv->stats);

		drv->stop = 0;
	}

	if (vb2_queue_is_busy(vdev, file)) {
		return -EBUSY;
	}

	if (vb2_fileio_is_active(q)) {
		pr_info("file io in progress\n");
		return -EBUSY;
	}

	if (drv->queue.streaming != 1) {
		ret = tnt_vb2_core_streamon(q, i);
	}

	if (gmode == TRIGGED) {
		vdo_grab_image(vdo, block);
		return ret;
	}

	return ret;
}

inline bool device_mapped(struct tnt_device *dev)
{
	return dev->org != NULL;
}
static inline void disable_all_interrupts(struct tntdrv *drv)
{
	int i;
	struct tntvdo *vdo;

	tnt_fpga_reset_interrupts(&drv->device);
	tnt_fpga_disable_interrupts(&drv->device);

	for (i = 0; i < DOUBLE; i++) {
		/* Disable all interrupts. Sensors will be init*/
		vdo = drv->active_vdo[i];
		if (vdo == NULL) {
			continue;
		}
		/* Happens vdo could be not mapped but 
           udev tries open /dev/video*  */
		if (device_mapped(&vdo->device)) {
			tnt_fpga_disable_trigged_interrupt(&vdo->device);
		}
	}
}

inline void wait_last_img_queued(struct tntdrv *drv)
{
	drv->stop = 2;
}

static inline void tnt_shutdown(struct tntdrv *drv)
{
	struct tnt_device *dev = &drv->device;

	tnt_fpga_stop_fsm(dev);
	tnt_fpga_disable_interrupts(dev);
	//tnt_fpga_reset_counters(dev);
	tnt_fpga_set_trigged_gmode(dev);
	drv->o = 0;
	drv->oo = 0;
}

static int tnt_streamoff(struct file *file, void *fh, enum v4l2_buf_type i)
{
	struct tntvdo *vdo = video_drvdata(file);
	struct tntdrv *drv = vdo->driver;
	int gmode = FG_GMODE(drv->grabmode);

	switch (gmode) {
	case CONTINUOUS:
		wait_last_img_queued(drv);
		tnt_shutdown(drv);
		tnt_remove_active_frame(drv);
		tnt_remove_busy_frame(drv);
		break;
	case TRIGGED:
		tnt_remove_active_frame(drv);
		tnt_remove_busy_frame(drv);
		tnt_deactivate_all_buffers(drv);
		break;
	}

	return 0;
}

int dqueue_buffer(struct tntdrv *drv, int index);

static int tnt_dqbuf(struct file *file, void *priv, struct v4l2_buffer *p)
{
	struct tntvdo *vdo = video_drvdata(file);
	struct tntdrv *drv = vdo->driver;
	struct video_device *vdev = video_devdata(file);
	int ret = 0;

	if (vb2_queue_is_busy(vdev, file))
		return -EBUSY;

	ret = dq_buffer(drv, p);
	return ret;
}

static __maybe_unused int tnt_queryctrl(struct file *filp, void *fh,
					struct v4l2_queryctrl *a)
{
	struct tntvdo *vdo = video_drvdata(filp);
	struct vdo_ctrls *ctrls = &vdo->ctrls;
	struct v4l2_ctrl *gain;

	gain = v4l2_ctrl_find(&ctrls->handler, a->id);
	//pr_info("name %s, val %d\n", ->name, gain->val );
	// return v4l2_queryctrl(&ctrls->handler, a);
	return 0;
}

/* IOCTL ops */

static const struct v4l2_ioctl_ops tnt_ioctl_ops = {
	.vidioc_enum_input = tnt_enum_input,
	.vidioc_g_input = tnt_g_input,
	.vidioc_s_input = tnt_s_input,
	.vidioc_s_std = tnt_s_std,
	.vidioc_g_std = tnt_g_std,

	.vidioc_enum_fmt_vid_cap = tnt_enum_fmt_vid_cap,
	.vidioc_try_fmt_vid_cap = tnt_try_fmt_vid_cap,
	.vidioc_g_fmt_vid_cap = tnt_g_fmt_vid_cap,
	.vidioc_s_fmt_vid_cap = tnt_s_fmt_vid_cap,

	.vidioc_querycap = tnt_querycap,
	.vidioc_reqbufs = tnt_reqbufs,
	.vidioc_querybuf = tnt_querybuf,
	.vidioc_qbuf = tnt_qbuf,
	.vidioc_dqbuf = tnt_dqbuf,
	.vidioc_streamon = tnt_streamon,
	.vidioc_streamoff = tnt_streamoff,

	/* .vidioc_queryctrl    = tnt_queryctrl, */
	/* .vidioc_g_parm		= tnt_g_parm, */
	/* .vidioc_s_parm		= tnt_s_parm, */
	/* .vidioc_enum_framesizes = tnt_enum_framesizes, */
	/* .vidioc_enum_frameintervals = tnt_enum_frameintervals, */
};

/* --------------------------------------------------------------------------*/
/* V4L2 file ops */
/* v4l2 dev File operations */

static int vdo_open(struct file *filp)
{
	struct tntvdo *vdo = video_drvdata(filp);
	struct tntdrv *drv = vdo->driver;
	struct tnt_fh *fh;

	if (vdo->users > 0)
		return -EBUSY;

	vdo->users++;
	//mutex_lock(&vdo->lock);

	//pr_info("VDO[%s] open by \"%s\" (pid %i)\n", DEV_NAME(filp), current->comm, current->pid);

	/* allocate and initialize per filehandle data */
	fh = kzalloc(sizeof(*fh), GFP_KERNEL);
	if (!fh) {
		vdo->users--;
		//mutex_unlock(&vdo->lock);
		return -ENOMEM;
	}
	v4l2_fh_init(&fh->fh, &vdo->video_dev);
	fh->dev = vdo;
	filp->private_data = fh;
	vdo->fh = fh;
	vdo->fh->current_fmt = vdo->current_fmt;

	disable_all_interrupts(drv);
	//mutex_unlock(&vdo->lock);

	/* TODO: prefer to use struct  */
	return 0;
}

static inline void clean_vdo(struct tntvdo *vdo)
{
	/* acquisition points */
	memset(vdo->wlists, 0, sizeof(vdo->wlists));
	spin_lock_init(&vdo->acq_point_lock);
	atomic_set(&vdo->update_acq, 0);
	vdo->c_point = 0;
	vdo->c_wlist = 0;

	/* current configuration */
	vdo->current_wlist = NULL;
	vdo->nbuffers = 0;

	/* wait queue */
	mutex_init(&vdo->wq_mutex);
	init_waitqueue_head(&vdo->wq_head);
	atomic_set(&vdo->wq_irq, 0);

	vdo_ctrl_init(vdo);
}

static int vdo_release(struct file *filp)
{
	struct tnt_fh *fh = filp->private_data;
	struct tntvdo *vdo = fh->dev;
	struct tntdrv *drv = vdo->driver;
	struct tnt_device *dev = &drv->device;
	struct tnt_allocator *allocator = drv->allocator;
	//mutex_lock(&vdo->lock);

	if (FG_GMODE(drv->grabmode) == CONTINUOUS) {
		wait_last_img_queued(drv);
		tnt_shutdown(drv);
	} else {
		pr_info("VDO[%s] closed by \"%s\" (pid %i)\n", DEV_NAME(filp),
			current->comm, current->pid);
		tnt_fpga_disable_trigged_interrupt(&vdo->device);
	}

	v4l2_fh_del(&fh->fh);
	v4l2_fh_exit(&fh->fh);
	//mutex_unlock(&vdo->lock);
	kfree(fh);

	vdo->users--;
	//pr_info("Close %d\n", get_minor(vdo));

	clean_ctx(drv, vdo, allocator);

	if (!vdo->users) {
		clean_vdo(vdo);
		tnt_fpga_reset_all(dev);
	}

	return 0;
}

static inline int copy_chunk(struct tnt_video_fmt *fmt, void __user *to,
			     void *from, int *n)
{
	struct alc_chunk *chunk = (struct alc_chunk *)from;
	int size;

	size = getsize(fmt);
	*n += size;
	return copy_to_user(to, chunk->vaddr, size);
}
static inline int copy_rle(void __user *to, void *from, int *n)
{
	struct alc_chunk *chunk = (struct alc_chunk *)from;
	return copy_to_user(to, chunk->vaddr, *n);
}
static int copy_touser(struct tntvdo *vdo, void __user *to, void *from,
		       unsigned long *n)
{
	int size = 0;
	int err = 0;
	struct tnt_buf *buf = (struct tnt_buf *)from;
	struct tntdrv *drv = vdo->driver;
	int minor = get_minor(vdo);

	switch (drv->grabmode) {
	case TRIGGED_CHA:
		err = copy_chunk(drv->cha.current_fmt, to,
				 buf->vb.planes[0].mem_priv, &size);
		if (drv->rle_enable) {
			size = tnt_rle_get_size_blob(drv);
			err = copy_rle(to, buf->vb.planes[1].mem_priv, &size);
		}
		break;
	case TRIGGED_CHB:
		err = copy_chunk(drv->chb.current_fmt, to,
				 buf->vb.planes[0].mem_priv, &size);
		break;
	case TRIGGED_DOUBLE:
		err = copy_chunk(drv->active_vdo[minor]->current_fmt, to,
				 buf->vb.planes[minor].mem_priv, &size);
		if (err)
			return err;

		if (drv->rle_enable && minor == 0) {
			size = tnt_rle_get_size_blob(drv);
			err = copy_rle(to, buf->vb.planes[2].mem_priv, &size);
		}
		//err = copy_chunk(drv->ctx.current_fmt, to, buf->vb.planes[1].mem_priv, &size);
		break;
	default:
		return -EINVAL;
	}
	return size;
}

/*
 * Read a frame from the device.
 */
static ssize_t vdo_read(struct file *filp, char __user *buffer, size_t len,
			loff_t *pos)
{
	struct tntvdo *vdo = video_drvdata(filp);
	struct tnt_buf *frame;
	struct tntdrv *drv = vdo->driver;
	struct list_head *list = &drv->queue.done_list;
	unsigned long size;

	if (list_empty(list))
		return -EINVAL;

	frame = list_first_entry(list, struct tnt_buf, vb.done_entry);
	pr_info("buffer: %px\n", frame);

	size = copy_touser(vdo, buffer, frame, &size);
	*pos += size;

	return size;
}

static __poll_t vdo_poll(struct file *filp, struct poll_table_struct *pt)
{
	struct tnt_device *dev;
	struct tntvdo *vdo = video_drvdata(filp);
	struct tntdrv *drv = vdo->driver;
	int gmode = FG_GMODE(drv->grabmode);
	int source = FG_SOURCE(drv->grabmode);
	int ret = 0;

	if (gmode == CONTINUOUS)
		return EPOLLIN;

	WARN_ON(!&vdo->wq_mutex);

	if (&vdo->wq_mutex && mutex_lock_interruptible(&vdo->wq_mutex))
		return EPOLLERR;

	if (source == CHB)
		dev = &drv->chb.device;
	else
		dev = &drv->cha.device;

	poll_wait(filp, &vdo->wq_head, pt);
	if (atomic_read(&vdo->wq_irq)) {
		atomic_set(&vdo->wq_irq, 0);
		ret = EPOLLIN;
	}

	if (&vdo->wq_mutex)
		mutex_unlock(&vdo->wq_mutex);

	return ret;
}

static int vdo_mmap(struct file *filp, struct vm_area_struct *vma)
{
	struct tntvdo *vdo = video_drvdata(filp);
	struct video_device *vdev = &vdo->video_dev;
	struct tnt_allocator *allocator = vdo->driver->allocator;
	int prid;

	prid = tnt_mmap_processors(vdo, vma);
	if (prid >= 0) /* >=0 is a valid proccessor id then mmap done*/
		return 0;

	/* get offset from chunk addr;  see __find_plane_by_offset */
	vma->vm_pgoff =
		alc_chunk_offset(allocator, vdo->cntx_id, vma->vm_pgoff);
	//vma->vm_page_prot = pgprot_writecombine(vma->vm_page_prot);
	return tnt_vb2_mmap(vdev->queue, vma);
}

void dqueue_buffers(struct tntdrv *drv, struct tnt_frames *frm, int n)
{
	int i, ret;
	struct v4l2_buffer vb = {
		.type = V4L2_BUF_TYPE_VIDEO_CAPTURE,
		.memory = V4L2_MEMORY_MMAP,
	};

	for (i = 0; i < n; i++) {
		vb.index = frm->index[i];
		ret = dq_buffer(drv, &vb);
	}
}

void sort_frames_ts(struct tntdrv *drv, struct tnt_frames *frm);

static inline int wait_return_code(int ret)
{
	switch (ret) {
	case 0:
		return -ETIMEDOUT;
	case -ERESTARTSYS:
		return -ERESTARTSYS;
	default:
		return 0;
	}
	return 0;
}

/**
\brief Waits for frames in future
The requiring process is put in
sleep state by the kernel until
drv->n_future == 0

\param drv: pointer to drv struct 
\return 0 or a negative error value
*/
inline int wait_future(struct tntdrv *drv)
{
	int ret;
	unsigned long timeout = msecs_to_jiffies(INTERRUPT_WAIT_TIME);

	ret = wait_event_interruptible_timeout(drv->wait_future_frames,
					       drv->n_future == 0, timeout);
	return wait_return_code(ret);
}

/**
\brief Waits for frames in future
matching uid

The requiring process is put in
sleep state by the kernel until
drv->last_uid < frames->uid

\param drv: pointer to drv struct 
\param uid: uid to wait to
\param ms: the timeout in ms

Timeout is computed ad (n_future_frames+2)*Period

\return 0 or a negative error value
*/

inline int wait_next_frames(struct tntdrv *drv, u64 uid, u64 ms)
{
	int ret;
	unsigned long timeout = msecs_to_jiffies(ms);

	ret = wait_event_interruptible_timeout(drv->wait_future_frames,
					       drv->last_uid >= uid, timeout);
	return wait_return_code(ret);
}

int copy_metadata(struct tnt_metadata *md, struct tnt_buf *buf,
		  struct tntvdo *vdo);

static long vdo_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct tntvdo *vdo = video_drvdata(filp);
	struct tntdrv *drv = vdo->driver;
	struct tnt_frames frames;
	int ret = 0, n = 0;
	char *name = DEV_NAME(filp);

	mutex_lock(&vdo->m);

	//pr_info("IOCTL [%s]: cmd: 0x%x\n", DEV_NAME(filp), cmd);
	switch (cmd) {
	case TNT_DIMA:
		ret = ioctl_dima(filp, cmd, arg);
		break;
	case TNT_STATS:
		ret = ioctl_stats(filp, cmd, arg);
		break;
	case TNT_DQBUF: {
		ret = copy_from_user(&frames, (struct tnt_frames *)arg,
				     sizeof(frames));
		ret = dequeue_buffers_by_ts(drv, &frames);
		if (ret) {
			//pr_info("Exit from TNT_DQBUF ioctl caused by dqueue_buffers");
			goto iexit;
		}
		ret = copy_to_user((struct tnt_frames *)arg, &frames,
				   sizeof(frames));
	} break;
	case TNT_DQBUF_UID: {
		ret = copy_from_user(&frames, (struct tnt_frames *)arg,
				     sizeof(frames));
		ret = dequeue_buffers_by_uid(drv, &frames);
		if (ret) {
			pr_info("Exit from TNT_DQBUF_UID ioctl caused by dqueue_buffers");
			goto iexit;
		}
		ret = copy_to_user((struct tnt_frames *)arg, &frames,
				   sizeof(frames));
	} break;
	case TNT_WAIT_NEXT: {
		u64 timeout;
		u64 uid;

		ret = copy_from_user(&frames, (struct tnt_frames *)arg,
				     sizeof(frames));
		timeout = (frames.pre + 2) * div64_u64(drv->period, 1000000);
		uid = frames.uid;
		ret = wait_next_frames(drv, uid, timeout);
		if (ret) {
			pr_info("Exit from TNT_WAIT_NEXT frames for timeout!!!\n");
			goto iexit;
		}
		ret = dequeue_buffers_by_uid(drv, &frames);
		if (ret) {
			pr_info("Exit from TNT_WAIT_NEXT ioctl caused by dqueue_buffers_by_uid");
			goto iexit;
		}
		ret = copy_to_user((struct tnt_frames *)arg, &frames,
				   sizeof(frames));
	} break;
	case TNT_ENUM_PROCESSOR: {
		struct processor_layout pl;
		ret = copy_from_user(&pl, (struct processors_layout *)arg,
				     sizeof(pl));
		if (pl.index == -1) {
			strcpy(pl.name, "Video");
			pl.plane_id = vdo->device.plane;
			strcpy(pl.devname, vdo->video_dev.name);
			pl.w = vdo->current_fmt->width;
			pl.h = vdo->current_fmt->height;
			pl.size = vdo->current_fmt->sz;
		} else {
			ret = processor_info(vdo, &pl);
			if (ret) {
				ret = -EINVAL;
				goto iexit;
			}
		}
		ret = copy_to_user((struct processor_layout *)arg, &pl,
				   sizeof(pl));
	} break;
	case TNT_PROCESSOR_INFO: {
		struct processor_layout pl;
		ret = copy_from_user(&pl, (struct processors_layout *)arg,
				     sizeof(pl));
		ret = get_processor_info(vdo, &pl);
		if (ret) {
			ret = -EINVAL;
			goto iexit;
		}
		ret = copy_to_user((struct processor_layout *)arg, &pl,
				   sizeof(pl));
	} break;
	case TNT_NPLANES: {
		struct nplanes p;
		n = vdo->n_processor + 1;
		ret = copy_from_user(&p, (struct nplanes *)arg, sizeof(p));
		p.n = n;
		ret = copy_to_user((struct nplanes *)arg, &p, sizeof(p));
		break;
	}
	case TNT_GET_METADATA: {
		struct tnt_metadata md;
		struct tnt_buf *buf = NULL;
		int index = ((struct tnt_metadata *)arg)->index;

		//ret = copy_from_user(&md, (struct tnt_metadata*)arg, sizeof(md));
		buf = container_of(drv->queue.bufs[index], struct tnt_buf, vb);
		copy_metadata(&md, buf, vdo);
		ret = copy_to_user((struct tnt_metadata *)arg, &md, sizeof(md));
		if (ret) {
			ret = -EINVAL;
			goto iexit;
		}
		break;
	}
	case TNT_INIT_SENSOR:
	case TNT_SET_ACQUISITION_POINTS:
		v4l2_subdev_call(&vdo->sensor, core, ioctl, cmd, (void *)arg);
		break;
	default:
		ret = video_ioctl2(filp, cmd, arg);
		if (ret == -ENOIOCTLCMD)
			pr_info("%s: Unhandled ioctl cmd [ 0x%08x ] (err=%d)\n",
				name, cmd, ret);
		else if (ret)
			pr_info("%s: Error ioctl cmd [ 0x%08x ] (err=%d)\n",
				name, cmd, ret);
	}

iexit:
	mutex_unlock(&vdo->m);
	return ret;
}

static const struct v4l2_file_operations vdo_file_ops = {
	.owner = THIS_MODULE,
	.open = vdo_open,
	.release = vdo_release,
	.read = vdo_read,
	.poll = vdo_poll,
	.mmap = vdo_mmap,
	.unlocked_ioctl = vdo_ioctl, //video_ioctl2,
};

static const struct video_device tnt_video_device = {
	.name = "tnt",
	.minor = -1,
	.fops = &vdo_file_ops,
	.ioctl_ops = &tnt_ioctl_ops,
	.release = video_device_release_empty,
};

/* --------------------------------------------------------------------------*/

int tnt_register_processor_subdev(struct tntvdo *vdo);

/**
\brief Initialize and register all stuff
about v4l2 videodev a and v4l2 subdevices(sensors)

\param vdo: pointer to vdo
\return 0 or errno
*/
int tnt_video_init(struct tntvdo *vdo, char *name)
{
	int ret;
	int n = 0;
	struct video_device *vdev;

	/* init mutex */
	mutex_init(&vdo->lock);

	/* init spinlock */
	spin_lock_init(&vdo->driver->vb_lock);

	/* register v4l2 device */
	n = snprintf(vdo->v4l2_dev.name, sizeof(vdo->v4l2_dev.name), "tnt-%s",
		     name);
	ret = v4l2_device_register(vdo->device.dev, &vdo->v4l2_dev);
	if (ret < 0) {
		pr_info("v4l2_device_register() failed: %d\n", ret);
		return ret;
	} else {
		pr_info("[%s] v4l2 device registered\n", name);
	}

	/* video device */
	vdev = &vdo->video_dev;
	*vdev = tnt_video_device;
	vdev->v4l2_dev = &vdo->v4l2_dev;
	vdev->lock = &vdo->lock;
	vdev->device_caps = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING |
			    V4L2_CAP_READWRITE;
	video_set_drvdata(vdev, vdo);
	n = snprintf(vdev->name, sizeof(vdev->name), "%s", name);
	vdev->name[n] = 0;

	mutex_lock(&vdo->lock);
	ret = video_register_device(&vdo->video_dev, VFL_TYPE_GRABBER, -1);
	if (ret) {
		pr_err("Error registering videodev [%d]\n", ret);
		mutex_unlock(&vdo->lock);
		goto error_videodev;
	}
	mutex_unlock(&vdo->lock);

	/* sub devices */
	ret = tnt_register_subdev(vdo, name);
	if (ret)
		goto error_subdev;

	return 0;

error_subdev:
error_videodev:
	v4l2_device_unregister(&vdo->v4l2_dev);
	return ret; /* Success */
}

static __maybe_unused void tnt_video_device_init(struct video_device *vd,
						 struct v4l2_device *v4l2_dev)
{
	*vd = tnt_video_device;
	vd->v4l2_dev = v4l2_dev;
}
