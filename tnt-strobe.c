/** 
 \file tnt-strobo.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
 Some useful conventin to avoid misunderstanding:

 + SOURCE is the channel which uses a strobe unit:
    * ChA
    * ChB
    * ChAB (both channels)
    * SW emul

 + STROBE_UNIT could be:
    * internal (STROBE_A)
    * external (STROBE B)
*/

#include <linux/gpio/consumer.h>

#include "tnt-dbg.h"
#include "tnt-strobe.h"
#include "tnt-cmd.h"

#define SELECT_SOURCE 0x20000
#define SELECT_LEVEL 0x20004

#define STROBE_SET_TIME 0x2001C

#define SET_TIME_STROBE_EMUL 0x10
#define START_STROBE_EMUL 0x14

#define US_TO_TICKS 33
#define EMUL_US_TO_CLK 50
#define STRB_UNCONNECTED 7

#define WAIT_FOR_STEPUP_READY 10000 /* mS */

/* Internal facilitation */
static inline enum strobe_connection vdo_strobe_source(int vdo_id)
{
	return vdo_id + 1;
}

static inline bool not_connected(struct strobe_unit *unit)
{
	return unit->dev == NULL;
}

static inline struct tntvdo *get_connected_vdo(struct strobe_unit *unit)
{
	struct tntvdo *vdo = NULL;

	if (unit->dev == NULL)
		return NULL;

	vdo = container_of(unit->dev, struct tntvdo, device);
	return vdo;
}

static inline struct tnt_strobe *get_strobe(struct strobe_unit *unit)
{
	struct tnt_strobe *strb;

	switch (unit->ch) {
	case INTERNAL:
		strb = container_of(unit, struct tnt_strobe, internal);
		break;
	case EXTERNAL:
		strb = container_of(unit, struct tnt_strobe, external);
		break;
	default:
		return NULL;
	}

	return strb;
}

static inline struct tntdrv *get_drv(struct strobe_unit *unit)
{
	struct tntdrv *drv;
	struct tnt_strobe *strb;

	strb = get_strobe(unit);
	if (strb == NULL)
		return NULL;

	drv = container_of(strb, struct tntdrv, strobe);
	return drv;
}

static inline struct tnt_device *get_drv_dev(struct strobe_unit *unit)
{
	struct tntdrv *drv;

	drv = get_drv(unit);
	return &drv->device;
}

static inline struct tntvdo *get_vdo(struct strobe_unit *unit, int vdo_id)
{
	struct tntdrv *drv;
	struct tntvdo *vdo = NULL;

	if (vdo_id < 0 || vdo_id > 2)
		return NULL;

	drv = get_drv(unit);
	vdo = drv->active_vdo[vdo_id - 1];
	return vdo;
}

static inline struct strobe_unit *get_unit(struct tnt_strobe *strobe,
					   enum strobe_channel ch)
{
	if (ch < EXTERNAL || ch > INTERNAL)
		return NULL;

	return &strobe->external + ch / INTERNAL;
}

static inline void strobe_write(struct strobe_unit *unit, int reg, int val)
{
	struct tnt_device *dev = get_drv_dev(unit);
	int addr = unit->ch + reg;

	if (dev == NULL)
		return;

	tnt_write(dev, addr, val);
}

static inline void strobe_source(struct tnt_device *dev,
				 struct strobe_unit *unit,
				 enum strobe_connection conn)
{
	strobe_write(unit, SELECT_SOURCE, conn);
	unit->connection = conn;
}

/* Exported ApI */

inline void strobe_level(struct strobe_unit *unit, int level)
{
	strobe_write(unit, SELECT_LEVEL, level & 1);
}

inline void strobe_set_time(struct tnt_device *dev, u32 us)
{
	u32 ticks = us * US_TO_TICKS;
	tnt_write(dev, STROBE_SET_TIME, ticks);
}

inline void strobe_time(struct strobe_unit *unit, u32 us)
{
	struct tnt_device *dev;
	struct tntvdo *vdo;

	if (unit == NULL)
		return;

	dev = unit->dev;
	if (dev == NULL)
		return;

	vdo = get_connected_vdo(unit);
	if (vdo == NULL)
		return;

	vdo->ctrls.w.strobe = us;

	strobe_set_time(dev, us);
}

inline void connect_strobe_source(struct strobe_unit *unit, struct tntvdo *vdo,
				  enum strobe_connection source)
{
	struct tnt_device *dev;

	dev = &vdo->driver->device;
	vdo->strobe = unit;
	unit->dev = &vdo->device;
	strobe_source(dev, unit, source);
}

inline int connect_strobe_channel(struct strobe_unit *unit,
				  enum strobe_connection source)
{
	struct tntvdo *vdo;

	vdo = get_vdo(unit, source);
	if (vdo == NULL)
		return -EINVAL;

	connect_strobe_source(unit, vdo, source);
	return 0;
}

inline int disconnect_strobe_channel(struct strobe_unit *unit,
				     enum strobe_connection source)
{
	struct tnt_device *dev;
	struct tntvdo *vdo;

	vdo = get_vdo(unit, source);
	if (vdo == NULL)
		return -EINVAL;

	dev = &vdo->driver->device;
	vdo->strobe = NULL;
	unit->dev = NULL;
	strobe_source(dev, unit, CONNECT_GND);

	return 0;
}

/* DebugFs */

static int source_open(struct inode *i_file, struct file *file)
{
	struct strobe_unit *unit = (struct strobe_unit *)i_file->i_private;
	file->private_data = unit;

	return 0;
}

static ssize_t get_source_type(struct file *file, char __user *buf,
			       size_t count, loff_t *f_pos)
{
	struct strobe_unit *unit = file->private_data;
	char source[32];
	int n, ret;

	if (*f_pos != 0) {
		return 0;
	}

	switch (unit->connection) {
	case CONNECT_GND:
		n = sprintf(source, "No connection\n");
		break;
	case CONNECT_CHA:
		n = sprintf(source, "Channel A\n");
		break;
	case CONNECT_CHB:
		n = sprintf(source, "Channel B\n");
		break;
	case CONNECT_CHAB:
		n = sprintf(source, "Both Channel A and B\n");
		break;
	case CONNECT_SW:
		n = sprintf(source, "Software emulation\n");
		break;
	default:
		return -EINVAL;
	}
	ret = copy_to_user(buf, source, n);

	if (ret != 0)
		return -EFAULT;

	*f_pos += n;
	return n;
}

static ssize_t change_source(struct file *file, const char __user *buf,
			     size_t count, loff_t *f_pos)
{
	struct strobe_unit *unit = file->private_data;
	int vdo_id;
	int ret;

	ret = sscanf(buf, "%d", &vdo_id);
	if (ret != 1)
		return -EINVAL;

	if (vdo_id < 0 || vdo_id > DOUBLE)
		return -EINVAL;

	ret = connect_strobe_channel(unit, vdo_id);
	if (ret)
		return ret;

	*f_pos += count;
	return count;
}

static ssize_t desource(struct file *file, const char __user *buf, size_t count,
			loff_t *f_pos)
{
	struct strobe_unit *unit = file->private_data;
	int vdo_id;
	int ret;

	ret = sscanf(buf, "%d", &vdo_id);
	if (ret != 1)
		return -EINVAL;

	if (vdo_id < 0 || vdo_id > DOUBLE)
		return -EINVAL;

	disconnect_strobe_channel(unit, vdo_id);

	*f_pos += count;
	return count;
}

static ssize_t get_time(struct file *file, char __user *buf, size_t count,
			loff_t *f_pos)
{
	struct strobe_unit *unit = file->private_data;
	struct tntvdo *vdo;
	char source[32];
	int n, ret;

	if (*f_pos != 0) {
		return 0;
	}

	if (not_connected(unit))
		return -EINVAL;

	vdo = get_connected_vdo(unit);
	if (vdo == NULL)
		return -EINVAL;

	n = sprintf(source, "%d\n", vdo->ctrls.w.strobe);
	ret = copy_to_user(buf, source, n);

	if (ret != 0)
		return -EFAULT;

	*f_pos += n;
	return n;
}

static ssize_t change_time(struct file *file, const char __user *buf,
			   size_t count, loff_t *f_pos)
{
	struct strobe_unit *unit = file->private_data;
	int time;
	int ret;

	ret = sscanf(buf, "%d", &time);
	if (ret != 1)
		return -EINVAL;

	strobe_time(unit, time);

	*f_pos += count;
	return count;
}

static ssize_t get_level(struct file *file, char __user *buf, size_t count,
			 loff_t *f_pos)
{
	struct strobe_unit *unit = file->private_data;
	char source[32];
	int n, ret;

	if (*f_pos != 0) {
		return 0;
	}

	n = sprintf(source, "%d\n", unit->level);
	ret = copy_to_user(buf, source, n);

	if (ret != 0)
		return -EFAULT;

	*f_pos += n;
	return n;
}

static ssize_t set_level(struct file *file, const char __user *buf,
			 size_t count, loff_t *f_pos)
{
	struct strobe_unit *unit = file->private_data;
	int level;
	int ret;

	ret = sscanf(buf, "%d", &level);
	if (ret != 1)
		return -EINVAL;

	strobe_level(unit, level);
	*f_pos += count;
	return count;
}

static struct file_operations source_ops = {
	.open = source_open,
	.read = get_source_type,
	.write = change_source,
};

static struct file_operations desource_ops = {
	.open = source_open,
	.write = desource,
};

static struct file_operations time_ops = {
	.open = source_open,
	.read = get_time,
	.write = change_time,
};

static struct file_operations level_ops = {
	.open = source_open,
	.read = get_level,
	.write = set_level,
};

/* Initialization  */

static int strobe_unit_init(struct strobe_unit *unit, const char *name,
			    struct tdm_debug *parent, enum strobe_channel ch)
{
	int ret;
	struct tdm_watch strobe_unit[] = {
		{ "connect", 0600, TDM_CUSTOM, &source_ops },
		{ "disconnect", 0400, TDM_CUSTOM, &desource_ops },
		{ "level", 0600, TDM_CUSTOM, &level_ops },
		{ "time", 0600, TDM_CUSTOM, &time_ops },
		{}
	};

	unit->dbg.parent = parent->dir;
	unit->dbg.dirname = (char *)name;
	unit->ch = ch;

	ret = tdm_init(&unit->dbg);
	if (ret)
		return ret;

	tdm_append_watches(&unit->dbg, &strobe_unit[0], unit);
	return 0;
}

/*  */
static inline void mosfet_set(struct timer_list *list)
{
	struct tnt_strobe *strb = container_of(list, struct tnt_strobe, timer);
	struct gpio_desc *mosfet = strb->mosfet;

	gpiod_set_value_cansleep(mosfet, 1);
	strb->status = MOSFET_HIGH;
	pr_info("**** STROBE MOSFET set!\n");
}

static inline bool is_high(struct gpio_desc *pin)
{
	return gpiod_get_value_cansleep(pin);
}

static inline bool is_output(struct gpio_desc *pin)
{
	return gpiod_get_direction(pin) == 0;
}

static inline s64 ms_from_boot(void)
{
	return ktime_to_ms(ktime_get_boottime());
}

static inline struct gpio_desc *config_mosfet_pin(struct device *dev)
{
	struct gpio_desc *pin;

	pin = gpiod_get_index(dev, "strobe", 1, GPIOD_OUT_LOW);
	if (IS_ERR(pin)) {
		pr_err("strobe: no GPIO consumer 'strobe' found\n");
		return pin;
	}

	return pin;
}

static inline struct gpio_desc *
config_stepup_pin(struct tnt_strobe *strobe, struct device *dev, s64 *wait_time)
{
	struct gpio_desc *pin;

	/* get strobe-gpios from device-tree */
	pin = gpiod_get_index(dev, "strobe", 0, GPIOD_ASIS);
	if (IS_ERR(pin)) {
		if (-PTR_ERR(pin) == EBUSY) {
			pr_info("Gpiod: stepup pin owned by other driver (perhaps tstrobo ???)\n");
		} else {
			pr_info("Gpiod: error get pin stepup (index=%d)[0x%lx]\n",
				0, PTR_ERR(pin));
		}
		return pin;
	}

	*wait_time = WAIT_FOR_STEPUP_READY;

	/* check pin direction */
	if (!is_output(pin)) {
		pr_info("strobe: stepup pin is input; swap ot output!!!!\n");
		gpiod_direction_output(pin, 1);
		strobe->status = STEPUP_HIGH_BY_KERNEL;
	} else if (is_high(pin)) {
		/* if dir just was out and value was high 
           uboot had set stepup pin then 
           wait time is reduced  */
		*wait_time -= ms_from_boot();
		*wait_time = max((s64)0, *wait_time);
		strobe->status = STEPUP_HIGH_BY_UBOOT;
		pr_info("strobe: stepup enabled at boot; wait for %lld ms\n",
			*wait_time);
	} else {
		/* but if dir is out and value is low, just set pin */
		gpiod_set_value_cansleep(pin, 1);
		strobe->status = STEPUP_HIGH_BY_KERNEL;
	}

	return pin;
}

#define MS(a) (jiffies + msecs_to_jiffies(a))

int strobe_gpio(struct tntdrv *drv)
{
	struct device *dev = drv->device.dev;
	struct gpio_desc *pin;
	struct tnt_strobe *strb = &drv->strobe;
	s64 wait_time = 0;

	pin = config_stepup_pin(strb, dev, &wait_time);
	if (IS_ERR(pin))
		return PTR_ERR(pin);
	strb->stepup = pin;

	pin = config_mosfet_pin(dev);
	if (IS_ERR(pin))
		return PTR_ERR(pin);
	strb->mosfet = pin;

	mod_timer(&strb->timer, MS(wait_time));
	return 0;
}

static int strobe_open(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh)
{
	return 0;
}

static int strobe_close(struct v4l2_subdev *subdev, struct v4l2_subdev_fh *fh)
{
	return 0;
}

__maybe_unused static long strobe_ioctl(struct v4l2_subdev *sd,
					unsigned int cmd, void *arg)
{
	struct tnt_strobe *strb = (struct tnt_strobe *)v4l2_get_subdevdata(sd);
	struct strobe_data *sdata = (struct strobe_data *)arg;
	struct strobe_unit *unit = get_unit(strb, sdata->channel);
	int ret = 0;

	if (unit == NULL)
		ret = -EINVAL;

	switch (cmd) {
	case STROBE_LEVEL:
		strobe_level(unit, sdata->value);
		break;
	case STROBE_CONNECT:
		ret = connect_strobe_channel(unit, sdata->value);
		break;
	case STROBE_TIME:
		strobe_time(unit, sdata->value);
		break;
	case STROBE_UNITS:
		*(int *)arg = 2;
		break;
	default:
		pr_info("%s: Wrong ioctl cmd [ 0x%08x ]\n", sd->name, cmd);
		ret = -ENOTTY;
	}
	return ret;
}

const struct v4l2_subdev_internal_ops strobe_internal_ops = {
	.open = strobe_open,
	.close = strobe_close,

};

static const struct v4l2_subdev_core_ops strobe_core_ops = {
	.ioctl = strobe_ioctl,
};

static const struct v4l2_subdev_ops strobe_ops = {
	.core = &strobe_core_ops,
};

static int strobe_subdev(struct tntdrv *drv)
{
	int ret;
	struct v4l2_subdev *sd = &drv->strobe.strbdev;
	char *name = "strobe";
	int i;
	struct tntvdo *vdo = NULL;

	for (i = 0; i < DOUBLE; i++) {
		vdo = drv->installed_vdo[i];
		if (vdo != NULL)
			break;
	}

	/* sub device */
	pr_info("Init [%s] subdev: \n", name);
	v4l2_subdev_init(sd, &strobe_ops);

	snprintf(sd->name, sizeof(sd->name), "%s", name);
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;
	v4l2_set_subdevdata(sd, &drv->strobe);
	sd->dev = drv->device.dev;
	sd->internal_ops = &strobe_internal_ops;

	ret = v4l2_device_register_subdev(&vdo->v4l2_dev, sd);
	if (ret) {
		pr_err("tnt subdev nodes registration failed (err=%d)\n", ret);
		return ret;
	}

	ret = v4l2_device_register_subdev_nodes(&vdo->v4l2_dev);
	if (ret) {
		pr_err("tnt subdev nodes registration failed (err=%d)\n", ret);
		return ret;
	}

	return 0;
}

int strobe_init(struct tntdrv *drv)
{
	int ret = 0;
	struct tnt_strobe *strobe = &drv->strobe;
	struct tdm_watch wstrobe[] = {
		{ "power", 0600, TDM_UINT32, 0, { &strobe->status } }, {}
	};

	strobe->dbg.dirname = "strobe";
	strobe->dbg.parent = drv->queue_trace.dir;
	strobe->status = STEPUP_LOW;

	ret = tdm_init(&strobe->dbg);
	if (ret)
		return ret;

	tdm_append_watches(&strobe->dbg, &wstrobe[0], strobe);
	strobe_unit_init(&strobe->internal, "internal", &strobe->dbg, INTERNAL);
	strobe_unit_init(&strobe->external, "external", &strobe->dbg, EXTERNAL);

	/* managed by tstrobo
    timer_setup(&strobe->timer, mosfet_set, 0);
    ret = strobe_gpio(drv); */
	ret = strobe_subdev(drv);

	return ret;
}

void strobe_put(struct tnt_strobe *strb)
{
	/* managed by tstrobo
    gpiod_put(strb->stepup);
    gpiod_put(strb->mosfet);
    del_timer(&strb->timer); */
}
