/** 
 \file spi-sensor.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Spi wrapped and shileded by FPGA 

*/

#include <linux/jiffies.h>
#include <linux/sched.h>
#include <linux/wait_bit.h>

#include "debugfs/trace.h"
#include "spi-sensor.h"
#include "tnt-io.h"
#include "fpga-cs.h"

int tnt_wait_until_ntimes(ftest f, void *param, int timeout, int n)
{
	unsigned long t1;
	unsigned long delay;
	bool ok;
	int times = 0;

	delay = msecs_to_jiffies(timeout);
	t1 = jiffies + delay;
	while (time_before(jiffies, t1)) {
		schedule();
		ok = f(param);
		if (ok)
			times++;

		if (times == n) {
			return 0;
		}
	}

	return 1;
}

static bool transaction_done(struct tnt_device *dev)
{
	int v;
	v = tnt_read(dev, SPI_WAIT);
	dev->cache = v;

	return (v & 1) == 0;
}

static int wait_until_transaction_done(struct tnt_device *dev, u8 *value,
				       int timeout)
{
	u32 v = 1;

	v = tnt_wait_until(transaction_done, dev, timeout);
	if (value != NULL) {
		*value = (dev->cache >> 16) & 0xFF;
	}
	return (v & 1) == 0 ? 0 : -EINVAL;
}

/**
\brief To start spi transaction the  
SPI_NUM_BIT must be set.

\param n_bit: #bit to trasmit
\return 
*/
static void spi_start_transaction(struct tnt_device *dev, int n, bool mod)
{
	tnt_write(dev, mod ? SPI_NUM_BIT_MOD : SPI_NUM_BIT, n * 8);
}

static int __spi_check(struct spi_header *head)
{
	if (head->size > 4) {
		pr_info("__spi_check: size error (%d)\n", head->size);
		return -EINVAL;
	}

	if (head->id < SPI_MIN_ID || head->id > SPI_MAX_ID) {
		pr_info("__spi_check: id error (%d)\n", head->id);
		return -EINVAL;
	}

	return 0;
}

static void __spi_set_header(struct tnt_device *dev, u8 id, u8 reg,
			     enum transaction tran, bool mod)
{
	u32 header;

	header = (reg << SPI_REG_OFFSET) | tran | id;
	tnt_write(dev, mod ? SPI_CHIP_ID_ADDR_MOD : SPI_CHIP_ID_ADDR, header);
}

static int __spi_read(struct tnt_device *dev, struct spi_header *head, u8 reg,
		      u8 *value)
{
	int ret;

	__spi_set_header(dev, head->id, reg, T_READ, false);
	spi_start_transaction(dev, head->size, false);
	ret = wait_until_transaction_done(dev, value, SPI_TIMEOUT);
	return ret;
}

int tnt_spi_write(struct tnt_device *dev, struct spi_header *head, u32 value,
		  bool wait, bool mod)
{
	int ret = 0;

	__spi_set_header(dev, head->id, head->reg, T_WRITE, mod);
	tnt_write(dev, mod ? SPI_DATA_MOD : SPI_DATA, value);
	spi_start_transaction(dev, head->size, mod);
	if (wait)
		ret = wait_until_transaction_done(dev, NULL, SPI_TIMEOUT);

	return ret;
}

int tnt_spi_read(struct tnt_device *dev, struct spi_header *head, u32 *value)
{
	int i;
	int ret = 0;
	u8 bval;

	*value = 0;
	for (i = 0; i < head->size; i++) {
		ret = __spi_read(dev, head, head->reg + i, &bval);
		if (ret < 0)
			return ret;
		*value |= bval << (i * 8);
	}
	return 0;
}

u32 tnt_spi_mask(struct spi_data *data, u32 value)
{
	u32 val = 0;

	val = value & ~data->header.mask;
	val = val | (data->value << data->header.org);

	return val;
}

int tnt_spi_register(struct tnt_device *dev, struct spi_data *data)
{
	int ret = 0;
	u32 value;

	ret = __spi_check(&data->header);
	if (ret < 0)
		return ret;

	ret = tnt_spi_read(dev, &data->header, &value);
	if (ret < 0)
		return ret;
	value = tnt_spi_mask(data, value);
	ret = tnt_spi_write(dev, &data->header, value, true, false);

	return ret;
}
