/** 
 \file tnt-videobuf2-v4l2.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#include "tnt-videobuf2-core.h"
#include "tnt-allocator.h"
#include "list.h"

static int debug;
#define dprintk(level, fmt, arg...)                                            \
	do {                                                                   \
		if (debug >= level)                                            \
			pr_info("vb2-v4l2: %s: " fmt, __func__, ##arg);        \
	} while (0)

static inline void copy_to_plane(struct v4l2_plane *p, struct vb2_plane *plane)
{
	struct alc_chunk *chunk;

	chunk = plane->mem_priv;
	p->length = plane->length;
	p->bytesused = plane->length;
	p->m.mem_offset = chunk->addr;
	//pr_info("address: %llx\n", chunk->addr);
}

static inline int get_plane_size(struct tntdrv *drv, int ctx_id)
{
	return drv->allocator->ctxs[ctx_id].chunk_size;
}

/**
\brief fills in  a struct v4l2_buffer with information from
vb2_bufer returned to userspace

param vb: pointer to struct vb2_buffer
\param pb: pointer to user space buffer
*/
static void tnt_fill_user_buffer(struct vb2_buffer *vb, void *pb)
{
	struct v4l2_buffer *b = pb;
	struct vb2_queue *q = vb->vb2_queue;
	struct tntdrv *drv = container_of(q, struct tntdrv, queue);
	struct vb2_plane *plane;
	struct v4l2_plane *v4l2_plane;
	int i;

	if (b->m.planes == NULL)
		return;

	/* TODO: RLE MISSING */
	for (i = 0; i < drv->n_planes; i++) {
		plane = &vb->planes[i];
		v4l2_plane = &b->m.planes[i];
		//pr_info("Plane %d\n", i);
		copy_to_plane(v4l2_plane, plane);
	}

	/* This is for test */
	//*((char*)chunk->vaddr) = vb->index;
}

static const struct vb2_buf_ops tnt_v4l2_buf_ops = {
	//.verify_planes_array	= __verify_planes_array_core,
	.fill_user_buffer = tnt_fill_user_buffer,
	//.fill_vb2_buffer	= __fill_vb2_buffer,
	//.copy_timestamp		= __copy_timestamp,
};

/*  ------- */

/*
 * __verify_planes_array() - verify that the planes array passed in struct
 * v4l2_buffer from userspace can be safely used
 */
static int __verify_planes_array(struct vb2_buffer *vb,
				 const struct v4l2_buffer *b)
{
	if (!V4L2_TYPE_IS_MULTIPLANAR(b->type))
		return 0;

	/* Is memory for copying plane information present? */
	if (b->m.planes == NULL) {
		dprintk(1,
			"multi-planar buffer passed but planes array not provided\n");
		return -EINVAL;
	}

	if (b->length < vb->num_planes || b->length > VB2_MAX_PLANES) {
		dprintk(1,
			"incorrect planes array length, expected %d, got %d\n",
			vb->num_planes, b->length);
		return -EINVAL;
	}

	return 0;
}

/*
 * vb2_querybuf() - query video buffer information
 * @q:		videobuf queue
 * @b:		buffer struct passed from userspace to vidioc_querybuf handler
 *		in driver
 *
 * Should be called from vidioc_querybuf ioctl handler in driver.
 * This function will verify the passed v4l2_buffer structure and fill the
 * relevant information for the userspace.
 *
 * The return values from this function are intended to be directly returned
 * from vidioc_querybuf handler in driver.
 */
int tnt_vb2_querybuf(struct vb2_queue *q, struct v4l2_buffer *b)
{
	struct vb2_buffer *vb;
	int ret;

	if (b->type != q->type) {
		dprintk(1, "wrong buffer type\n");
		return -EINVAL;
	}

	if (b->index >= q->num_buffers) {
		dprintk(1, "buffer index out of range\n");
		return -EINVAL;
	}
	vb = q->bufs[b->index];
	ret = __verify_planes_array(vb, b);
	if (!ret) {
		tnt_vb2_core_querybuf(q, b->index, b);
	}
	return ret;
}

/* ---- PICK ----- */

struct vb2_buffer *pick_from_list(struct list_head *list, void *el,
				  struct list_ops *ops)
{
	struct v4l2_buffer *vb = (struct v4l2_buffer *)el;
	struct vb2_buffer *b;

	b = foreach_entry(list, ops, vb);
	if (b == NULL)
		return NULL;

	return b;
}

void *insert_to_list(struct list_head *list, struct list_head *entry,
		     struct list_ops *ops)
{
	struct list_head *el;

	/* insert ordered in queue list */
	el = insert_ordered(list, entry, ops);

	return entry;
}

static const struct vb2_list_ops tnt_list_ops = { .pick = pick_from_list,
						  .insert = insert_to_list };

/* ----- Q ----- */

static int vb2_queue_or_prepare_buf(struct vb2_queue *q, struct v4l2_buffer *b,
				    const char *opname)
{
	if (b->type != q->type) {
		dprintk(1, "%s: invalid buffer type\n", opname);
		return -EINVAL;
	}

	if (b->index >= q->num_buffers) {
		dprintk(1, "%s: buffer index out of range\n", opname);
		return -EINVAL;
	}

	if (q->bufs[b->index] == NULL) {
		/* Should never happen */
		dprintk(1, "%s: buffer is NULL\n", opname);
		return -EINVAL;
	}

	if (b->memory != q->memory) {
		dprintk(1, "%s: invalid memory type\n", opname);
		return -EINVAL;
	}

	return __verify_planes_array(q->bufs[b->index], b);
}

int tnt_vb2_qbuf(struct vb2_queue *q, struct v4l2_buffer *b)
{
	int ret;

	if (vb2_fileio_is_active(q)) {
		dprintk(1, "file io in progress\n");
		return -EBUSY;
	}

	ret = vb2_queue_or_prepare_buf(q, b, "qbuf");
	return ret ? ret : tnt_vb2_core_qbuf(q, b->index, b);
}

/* ----- DQ ----- */

int tnt_vb2_dqbuf(struct vb2_queue *q, struct v4l2_buffer *b, bool nonblocking)
{
	int ret;

	if (vb2_fileio_is_active(q)) {
		pr_info("file io in progress\n");
		return -EBUSY;
	}

	if (b->type != q->type) {
		pr_info("invalid buffer type\n");
		return -EINVAL;
	}

	ret = tnt_vb2_core_dqbuf(q, NULL, b, nonblocking);

	/*
	 *  After calling the VIDIOC_DQBUF V4L2_BUF_FLAG_DONE must be
	 *  cleared.
	 */
	b->flags &= ~V4L2_BUF_FLAG_DONE;

	return ret;
}

int tnt_vb2_queue_init(struct vb2_queue *q)
{
	/*
	 * Sanity check
	 */
	if (WARN_ON(!q) ||
	    WARN_ON(q->timestamp_flags & ~(V4L2_BUF_FLAG_TIMESTAMP_MASK |
					   V4L2_BUF_FLAG_TSTAMP_SRC_MASK)))
		return -EINVAL;

	/* Warn that the driver should choose an appropriate timestamp type */
	WARN_ON((q->timestamp_flags & V4L2_BUF_FLAG_TIMESTAMP_MASK) ==
		V4L2_BUF_FLAG_TIMESTAMP_UNKNOWN);

	/* Warn that vb2_memory should match with v4l2_memory */
	if (WARN_ON(VB2_MEMORY_MMAP != (int)V4L2_MEMORY_MMAP) ||
	    WARN_ON(VB2_MEMORY_USERPTR != (int)V4L2_MEMORY_USERPTR) ||
	    WARN_ON(VB2_MEMORY_DMABUF != (int)V4L2_MEMORY_DMABUF))
		return -EINVAL;

	if (q->buf_struct_size == 0)
		q->buf_struct_size = sizeof(struct vb2_v4l2_buffer);

	q->buf_ops = &tnt_v4l2_buf_ops;
	q->list_ops = &tnt_list_ops;
	q->is_multiplanar = V4L2_TYPE_IS_MULTIPLANAR(q->type);
	q->is_output = V4L2_TYPE_IS_OUTPUT(q->type);
	q->copy_timestamp =
		(q->timestamp_flags & V4L2_BUF_FLAG_TIMESTAMP_MASK) ==
		V4L2_BUF_FLAG_TIMESTAMP_COPY;
	/*
	 * For compatibility with vb1: if QBUF hasn't been called yet, then
	 * return EPOLLERR as well. This only affects capture queues, output
	 * queues will always initialize waiting_for_buffers to false.
	 */
	q->quirk_poll_must_check_waiting_for_buffers = true;

	return tnt_vb2_core_queue_init(q);
}
