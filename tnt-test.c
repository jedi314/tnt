/** 
 \file tnt-test.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#include <stdio.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <linux/videodev2.h>
#include <signal.h>
#include <unistd.h>

#include "tnt-cmd.h"
#include "include/allocator.h"
#include "/home/jedi/sv1/lab/profile/profile.h"
#include <send_image_wrapper.h>

#define MB(a) (a) * 1024 * 1024
#define SIZE 3 * 1024 * 1024
#define WIDTH 2048 //1920
#define HEIGHT 1536 //1080

#define IMG_SIZE WIDTH *HEIGHT
#define GRAY_8 0

static uint8_t *create_chessboard_uint8(unsigned int w, unsigned int h)
{
	uint8_t *img = NULL;
	int i, j;

	img = (uint8_t *)malloc(w * h);
	if (img == NULL) {
		printf("Test image allocation failed\n");
		return NULL;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			img[j * w + i] = (i / 32 + j / 32) % 2 ? 255 : 0;
		}
	}

	return img;
}

void dump(char *desc, void *addr, int len)
{
	int i;
	unsigned char buff[17];
	unsigned char *pc = (unsigned char *)addr;

	/* Output description if given. */
	if (desc != NULL)
		printf("%s:\n", desc);

	/* Process every byte in the data. */
	for (i = 0; i < len; i++) {
		/* Multiple of 16 means new line (with line offset). */
		if ((i % 16) == 0) {
			/* Just don't print ASCII for the zeroth line. */
			if (i != 0)
				printf("  %s\n", buff);
			printf("  %04x ", i);
		}

		/* Now the hex code for the specific character. */
		printf(" %02x", pc[i]);

		/* And store a printable ASCII character for later. */
		if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
			buff[i % 16] = '.';
		} else {
			buff[i % 16] = pc[i];
		}

		buff[(i % 16) + 1] = '\0';
	}

	/* Pad out last line if not exactly 16 characters. */
	while ((i % 16) != 0) {
		printf("   ");
		i++;
	}
	printf("  %s\n", buff);
}

int test_subdev()
{
	int fd;
	struct tnt_cmd tc;

	tc.channel = 0;
	tc.value = TRIGGED_OCR;

	fd = open("/dev/v4l-subdev1", O_NONBLOCK);
	if (fd < 0) {
		perror("subdev:");
		return 1;
	}

	ioctl(fd, TNT_SET_GRABMODE, &tc);
	close(fd);
	return 0;
}

enum { DISABLED = 0, ENABLED = 1 };

#define MAX_BUFS 128
struct video_device {
	int fd;
	int sd;
	int enabled;
	char *name;
	char *subdev;
	int n_bufs;
	int size;
	uint64_t org;
	char *bufs[MAX_BUFS];
	void *socket;
	TAddressIP ip;
	Tbanco banco;
	int grabmode;
	struct video_device *next;
};

typedef int (*test_fn)(void *);
struct tnt_test {
	char *name;
	test_fn fn;
	int enable;
	void *param;
};

static double av_copy(int *dest, int *src, int size, int n)
{
	double ms;
	int i;
	DECLARE_TIM;

	ms = 0;
	for (i = 0; i < n; i++) {
		START_TIME;
		memcpy(dest, src, size);
		DUMP_TIME;
		ms += mS(get_real_time(&tim));
	}
	return ms / n;
}

int test_mem()
{
	int fd;
	int v[SIZE / sizeof(int)];
#ifdef X86
	int z[SIZE / sizeof(int)];
#endif
	int i;
	int *buf;

	double ms;

	for (i = 0; i < SIZE / sizeof(int); i++) {
		v[i] = i;
		//printf("[0] %d  %d\n", i, v[i]);
	}

#ifndef X86
	fd = open("/dev/video0", O_RDWR);
	if (fd < 0) {
		perror("open");
		return 3;
	}

	buf = mmap(0, SIZE, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED, fd,
		   0);
	if (buf == MAP_FAILED) {
		perror("mmap");
		return 4;
	}
#else
	buf = &z[0];
#endif

	ms = av_copy(buf, v, SIZE, 10);
	printf("Real Time %f ms\n", ms);

	ms = av_copy(v, buf, SIZE, 10);
	printf("Real Time %f ms\n", ms);

#ifndef X86
	munmap(buf, SIZE);
	close(fd);
#endif
	return 0;
}

int test_open_video(void *param)
{
	struct video_device *vd = (struct video_device *)param;

	vd->fd = open(vd->name, O_RDWR);
	if (vd->fd < 0) {
		perror("open");
		return 1;
	}

	//ioctl(vd->fd, TNT_GRAB_FRAME);

	return 0;
}

int test_set_grabmode(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int ret = 0;
	int mode = vd->grabmode;

	vd->sd = open(vd->subdev, O_RDWR);
	if (vd->sd < 0) {
		perror("open");
		return 1;
	}
	printf("subdev: %s opened\n", vd->subdev);
	ret = ioctl(vd->sd, TNT_SET_GRABMODE, &mode);
	if (ret)
		perror("subdev:");
	close(vd->sd);
	return 0;
}

int test_querycap(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	struct v4l2_capability caps;
	int ret;

	ret = ioctl(vd->fd, VIDIOC_QUERYCAP, &caps);
	if (ret) {
		perror("test_querycap");
		return 1;
	}
	printf("driver: %s, card: %s\n", caps.driver, caps.card);

	return 0;
}

int test_ctrl(int fd, long unsigned cmd, struct v4l2_control *ctrl,
	      char *action)
{
	int ret;

	ret = ioctl(fd, cmd, ctrl);
	if (ret) {
		perror("test_queryctrl");
		return 1;
	}
	printf("%s %u\n", action, ctrl->value);
	return ret;
}

int test_v4l2_ctrl(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	struct v4l2_queryctrl queryctrl;
	struct v4l2_control ctrl;
	int ret;
	int val = 480;

	memset(&queryctrl, 0, sizeof(queryctrl));

	queryctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL;
	while (0 == ioctl(vd->fd, VIDIOC_QUERYCTRL, &queryctrl)) {
		if (!(queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)) {
			memset(&ctrl, 0, sizeof(ctrl));
			ctrl.id = queryctrl.id;
			ret = ioctl(vd->fd, VIDIOC_G_CTRL, &ctrl);
			if (ret == 0) {
				printf("Control %s - value: %u\n",
				       queryctrl.name, ctrl.value);
			}
		}
		queryctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
	}

	if (errno != EINVAL) {
		perror("VIDIOC_QUERYCTRL");
		return 2;
	}

	ctrl.id = V4L2_CID_GAIN;
	ctrl.value = val;
	test_ctrl(vd->fd, VIDIOC_S_CTRL, &ctrl, "Set Gain value to");
	test_ctrl(vd->fd, VIDIOC_G_CTRL, &ctrl, "Get Gain value at");
	if (ctrl.value != val) {
		printf("Mismatch gain valu %d != %d\n", val, ctrl.value);
		return 3;
	}

	ctrl.id = V4L2_CID_EXPOSURE;
	ctrl.value = 2000;
	test_ctrl(vd->fd, VIDIOC_S_CTRL, &ctrl, "Set Expo value to");

	return 0;
}

int test_read_frame(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	char *image;
	int size;

	image = malloc(IMG_SIZE * 2);
	memset(image, 0, IMG_SIZE * 2);
	size = read(vd->fd, image, IMG_SIZE * 2);
	if (size < 0) {
		perror(__FUNCTION__);
		return 1;
	}
	dump(NULL, image, 300);
	printf("Image Read OK\n");
	//dump(NULL, image+IMG_SIZE, 300);
	vd->banco.pixel_ptr = image;
	return 0;
}

int test_send_frame(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int i = 0;

	do {
		if (!vd->enabled) {
			vd = vd->next;
			continue;
		}

		if (vd->banco.pixel_ptr == NULL)
			vd->banco.pixel_ptr =
				create_chessboard_uint8(WIDTH, HEIGHT);

		SendImage(vd->socket, &vd->banco);
		printf("Send Image video%d OK\n", i++);
		getchar();
		vd = vd->next;
	} while (vd != NULL);

	return 0;
}

int test_ioctl_video(void *param)
{
	//struct video_device *vd = (struct video_device*)param;
	//int fd = vd->fd;
	int ret = 0;
	//struct tnt_cmd cmd;

	//cmd.value = 2;
	//cmd.channel = 1;

	// ret = ioctl(fd, TNT_PERIOD, &cmd);
	if (ret)
		perror("ioctl");
	return 0;
}

int test_reqbuf(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int fd = vd->fd;

	struct v4l2_requestbuffers bufrequest;

	bufrequest.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	bufrequest.memory = V4L2_MEMORY_MMAP;
	bufrequest.count = vd->n_bufs;

	if (ioctl(fd, VIDIOC_REQBUFS, &bufrequest) < 0) {
		perror("VIDIOC_REQBUFS");
		exit(1);
	}

	printf(">> COUNT: %d\n", bufrequest.count);
	getchar();
	return 0;
}

int test_streamon(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int fd = vd->fd;
	enum v4l2_buf_type type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (-1 == ioctl(fd, VIDIOC_STREAMON, &type)) {
		perror("Streamon");
		return 2;
	}

	getchar();
	return 0;
}

//int test_querybuf(void *param)

static int test_checkbufs(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int i;
	char val;
	char **bufs = vd->bufs;
	int n = vd->n_bufs;

	for (i = 0; i < n; i++) {
		val = *((unsigned int *)bufs[i]);
		printf("addr %p %d\n", bufs[i], val);
		if (val != i) {
			printf("Error mismatch index %d != %d\n", val, i);
			return 1;
		}
	}
	return 0;
}

int test_unmap(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int i;
	char **bufs = vd->bufs;

	for (i = 0; i < vd->n_bufs; i++) {
		munmap(bufs[i], vd->size);
	}

	return 0;
}

int test_querybuf(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int fd = vd->fd;
	struct v4l2_buffer bufferinfo;
	int i;
	int ret = 0;
	int tsize = 6220800;
	uint64_t addr = 0;

	vd->size = tsize;
	memset(&bufferinfo, 0, sizeof(bufferinfo));

	bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	bufferinfo.memory = V4L2_MEMORY_MMAP;

	for (i = 0; i < vd->n_bufs; i++) {
		bufferinfo.index = i;

		if (ioctl(fd, VIDIOC_QUERYBUF, &bufferinfo) < 0) {
			perror("VIDIOC_QUERYBUF");
			exit(1);
		}

		addr = vd->org | bufferinfo.m.offset;
		vd->bufs[i] =
			mmap(0, tsize, PROT_READ, MAP_SHARED, vd->fd, addr);
		if (vd->bufs[i] == MAP_FAILED) {
			perror("mmap");
			return 1;
		}
		printf("MAP buffer n. 0x%03d (%x) @ vaddr: %p\n",
		       bufferinfo.index, bufferinfo.m.offset, vd->bufs[i]);
	}
	return ret;
}

int test_qbuf(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int fd = vd->fd;
	struct v4l2_buffer b;
	int i;
	int ret = 0;

	memset(&b, 0, sizeof(b));

	b.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	b.memory = V4L2_MEMORY_MMAP;

	for (i = 0; i < vd->n_bufs; i++) {
		b.index = i;
		ret = ioctl(fd, VIDIOC_QBUF, &b);
		if (ret < 0)
			perror("VIDIOC_QBUF");
	}

	getchar();
	return 0;
}

int test_dqbuf(void *param)
{
	struct video_device *vd = (struct video_device *)param;
	int fd = vd->fd;
	struct v4l2_buffer b;
	int i;
	int ret = 0;
	int nqb[] = { 1, 3, -1 };

	memset(&b, 0, sizeof(b));

	b.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	b.memory = V4L2_MEMORY_MMAP;

	for (i = 0; nqb[i] > 0; i++) {
		b.index = nqb[i];
		ret = ioctl(fd, VIDIOC_DQBUF, &b);
		if (ret < 0)
			perror("VIDIOC_DQBUF");
	}

	getchar();
	return 0;
}

struct sig_param {
	int fd;
	void *buf;
};

struct sig_param sp;

static void sigint_handler(int signo)
{
	if (signo == SIGINT)
		printf("received SIGINT\n");
}

int test_allocator(void *param)
{
	char *name = (char *)param;
	int fd;
	int tsize;
	int ret;
	char *buf = NULL;
	struct alc_memory mem1, mem2;

	signal(SIGINT, sigint_handler);

	memset(&mem1, 0, sizeof(struct alc_memory));
	memset(&mem2, 0, sizeof(struct alc_memory));

	fd = open(name, O_RDWR);
	if (fd < 0) {
		perror("allocator open");
		return -1;
	}

	printf("allocator opened....\n");
	getchar();

	sp.fd = fd;

	mem1.size = MB(5);
	mem1.name = "pippo";
	mem1.count = 100;
	ret = ioctl(fd, TNT_ALLOC_CTX, &mem1);
	printf("Name: %s\nChunk Size: %d\nCount: %d\nId: %d\nAddress: %lx\n",
	       mem1.name, mem1.size, mem1.count, mem1.id, mem1.addr);

	getchar();

	mem2.size = MB(4);
	mem2.name = "pluto";
	mem2.count = 100;
	ret = ioctl(fd, TNT_ALLOC_CTX, &mem2);
	printf("Name: %s\nChunk Size: %d\nCount: %d\nId: %d\nAddress: %lx\n",
	       mem2.name, mem2.size, mem2.count, mem2.id, mem2.addr);

	/* mem2.val= '*'; */
	/* ret = ioctl(fd, TNT_INIT_STRIP, &mem2); */
	/* printf("\n\nName: %s\nMem ID: %d\nAddress: %lx\nRet: %d\n", */
	/*        mem2.name, */
	/*        mem2.id, */
	/*        mem2.addr, ret ); */
	tsize = mem2.size * mem2.count;
	buf = mmap(0, tsize, PROT_WRITE | PROT_READ, MAP_SHARED, fd, mem2.addr);
	if (buf == (void *)-1) {
		perror("mmap");
	} else {
		printf("mmap @ %p\n", buf);
		sp.buf = buf;
		dump(NULL, buf, 100);
		printf("size %d\n", tsize);
		getchar();
	}
	//buf[0] = '$' ;
	memset(buf, '$', mem2.size);
	dump(NULL, buf, 100);
	getchar();

	munmap(buf, tsize);
	close(fd);
	return ret;
}

int main(int argc, char **argv)
{
	struct tnt_test *t;
	int ret;
	int i = 0;

	struct video_device vd0 = {
        .name = "/dev/video0",
        .subdev = "/dev/v4l-subdev0",
        .n_bufs = 5,
        .org = 0x0, //0x800000000
        .grabmode = TRIGGED_OCR,
        .enabled = 1,
        .ip = {
            .adr = {
                .adr1 = 172,
                .adr2 = 25,
                .adr3 = 99,
                .adr4 = 6,
            }
        },
        .banco = {
            .codice_oggetto = 0, //TIL_OBJ_CODE_MEM_SYSTEM,
            .numero_oggetto = 0,
            .dim_head_byte = sizeof(Tbanco),
            .dim_x_banco = WIDTH,
            .dim_y_banco = HEIGHT,
            .dim_oggetto_byte = IMG_SIZE,
			.bits_pixel = 8,  //12,
			.tipo_immagine = 0, //6,
            .image_format = 0, //TIL_IMAGE_FORMAT_RAW;
            .pixel_ptr = NULL,
        },
        .next = NULL,
    };

	struct tnt_test fn[] = {
		{ "SetGrabMode", test_set_grabmode, ENABLED, &vd0 },
		{ "Open Video0", test_open_video, ENABLED, &vd0 },
		{ "QUERY caps0", test_querycap, ENABLED, &vd0 },
		{ "QUERY ctrl0", test_v4l2_ctrl, ENABLED, &vd0 },
		{ "REQBUF syscall", test_reqbuf, ENABLED, &vd0 },
		{ "QBUF syscall", test_qbuf, ENABLED, &vd0 },
		{ "STREAMON syscall", test_streamon, ENABLED, &vd0 },
		{ "READ syscall", test_read_frame, ENABLED, &vd0 },
		{ "SEND syscall", test_send_frame, ENABLED, &vd0 },
		{}
	};

	if (argc > 2) {
		printf("Wrong parameters # (%d)]\n", argc);
		return 2;
	}

	if (argc == 2) {
		printf("Wrong parameters # %s\n", argv[1]);
		sscanf(argv[1], "%hhu.%hhu.%hhu.%hhu", &vd0.ip.adr.adr1,
		       &vd0.ip.adr.adr2, &vd0.ip.adr.adr3, &vd0.ip.adr.adr4);
	}

	vd0.socket = OpenSocketToSendImage(NULL, vd0.ip, 0, 0);
	if (vd0.socket == NULL) {
		printf("merda\n");
		return 1;
	} else {
		printf("Open Socket @ %hhu.%hhu.%hhu.%hhu\n", vd0.ip.adr.adr1,
		       vd0.ip.adr.adr2, vd0.ip.adr.adr3, vd0.ip.adr.adr4);
	}

	for (t = &fn[0]; t->fn; t++) {
		if (!t->enable)
			continue;

		printf("Test #%d - %s\n", i++, t->name);
		ret = t->fn(t->param);
		if (ret) {
			printf("Failed\n");
			break;
		}
	}

	printf("exit...\n");
	getchar();

	close(vd0.fd);
	if (vd0.banco.pixel_ptr)
		free(vd0.banco.pixel_ptr);

	CloseSocketToSendImage(vd0.socket);

	return ret;
}
