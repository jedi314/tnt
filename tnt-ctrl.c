/** 
 \file tnt-ctrl.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#include "tnt-ctrl.h"
#include "tnt-optical-density.h"
#include "tnt-fpga-utils.h"
#include "tnt-imx265.h"
#include "tntvdo.h"

#define MIN_GAIN 128
#define MAX_GAIN 608 /* 480 (dBm) */

#define MIN_RGAIN 1
#define MAX_RGAIN 2047 /* 480 (dBm) */

#define MIN_GGAIN 1
#define MAX_GGAIN 2047 /* 480 (dBm) */

#define MIN_BGAIN 1
#define MAX_BGAIN 2047 /* 480 (dBm) */

#define MIN_SHUTTER 1
#define MAX_SHUTTER 400000

#define MIN_STROBE 0
#define MAX_STROBE 1000

#define V4L2_NEW_EXECUTE_ONWRITE(...)                                          \
	ctrl = v4l2_ctrl_new_std(__VA_ARGS__);                                 \
	ctrl->flags |= V4L2_CTRL_FLAG_EXECUTE_ON_WRITE

inline u32 tnt_cutoff_shutter(u32 s, u64 p);
inline void strobe_time(struct strobe_unit *unit, u32 us);

static int vdo_s_ctrl(struct v4l2_ctrl *ctrl)
{
	struct tntvdo *vdo =
		container_of(ctrl->handler, struct tntvdo, ctrls.handler);
	struct vdo_ctrls *ctrls = &vdo->ctrls;
	struct tnt_device *dev = &vdo->device;
	u32 tmp;

	if (ctrl->flags & V4L2_CTRL_FLAG_INACTIVE)
		return 0;

	switch (ctrl->id) {
	case V4L2_CID_GAIN:
		ctrls->w.gain = tnt_fpga_set_gain(dev, ctrl->val);
		break;
	case V4L2_CID_RED_BALANCE:
		ctrls->w.r_gain = tnt_fpga_set_red_gain(dev, ctrl->val);
		break;
	case V4L2_CID_GREEN_BALANCE:
		ctrls->w.g_gain = tnt_fpga_set_green_gain(dev, ctrl->val);
		break;
	case V4L2_CID_BLUE_BALANCE:
		ctrls->w.b_gain = tnt_fpga_set_blue_gain(dev, ctrl->val);
		break;
	case V4L2_CID_EXPOSURE:
		tmp = tnt_cutoff_shutter(ctrl->val, vdo->driver->period);
		ctrls->w.shutter = tnt_fpga_set_expo(dev, tmp);
		break;
	case V4L2_CID_FLASH_TIMEOUT:
		strobe_time(vdo->strobe, ctrl->val);
		ctrls->w.strobe = ctrl->val;
		//ctrls->w.strobe = tnt_fpga_set_strobo(dev, ctrl->val);
		break;
	case V4L2_CID_HFLIP:
		ctrls->hflip = ctrl->val;
		break;
	case V4L2_CID_VFLIP:
		ctrls->vflip = ctrl->val;
		break;
	case V4L2_CID_OPTICAL_DENSITY_START_X:
		tnt_optical_density_set_window(dev, OD_START_X, ctrl->val);
		break;
	case V4L2_CID_OPTICAL_DENSITY_END_X:
		tnt_optical_density_set_window(dev, OD_END_X, ctrl->val);
		break;
	case V4L2_CID_OPTICAL_DENSITY_START_Y:
		tnt_optical_density_set_window(dev, OD_START_Y, ctrl->val);
		break;
	case V4L2_CID_OPTICAL_DENSITY_END_Y:
		tnt_optical_density_set_window(dev, OD_END_Y, ctrl->val);
		break;
	case V4L2_CID_TEST_MODE:
		ctrls->test_mode = tnt_fpga_test_mode(dev, ctrl->val);
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static int vdo_g_volatile_ctrl(struct v4l2_ctrl *ctrl)
{
	struct tntvdo *vdo =
		container_of(ctrl->handler, struct tntvdo, ctrls.handler);
	struct tnt_device *dev = &vdo->device;

	if (ctrl->flags & V4L2_CTRL_FLAG_INACTIVE)
		return 0;
	switch (ctrl->id) {
	case V4L2_CID_OPTICAL_DENSITY_Y:
		ctrl->val = tnt_optical_density_color(dev, OD_Y);
		break;
	case V4L2_CID_OPTICAL_DENSITY_CB:
		ctrl->val = tnt_optical_density_color(dev, OD_CB);
		break;
	case V4L2_CID_OPTICAL_DENSITY_CR:
		ctrl->val = tnt_optical_density_color(dev, OD_CR);
		break;
	case V4L2_CID_MODEL:
		tnt_fpga_get_model(dev, &ctrl->val);
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static const struct v4l2_ctrl_ops vdo_ctrl_ops = {
	.s_ctrl = vdo_s_ctrl,
	.g_volatile_ctrl = vdo_g_volatile_ctrl
};

static const struct v4l2_ctrl_config ctrl_optical_density_y = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_OPTICAL_DENSITY_Y,
	.name = "Optical Density Y",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_READ_ONLY | V4L2_CTRL_FLAG_VOLATILE,
	.min = 0,
	.step = 1
};
static const struct v4l2_ctrl_config ctrl_optical_density_cb = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_OPTICAL_DENSITY_CB,
	.name = "Optical Density Cb",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_READ_ONLY | V4L2_CTRL_FLAG_VOLATILE,
	.min = 0,
	.step = 1
};
static const struct v4l2_ctrl_config ctrl_optical_density_cr = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_OPTICAL_DENSITY_CR,
	.name = "Optical Density Cr",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_READ_ONLY | V4L2_CTRL_FLAG_VOLATILE,
	.min = 0,
	.step = 1
};
static const struct v4l2_ctrl_config ctrl_optical_density_start_x = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_OPTICAL_DENSITY_START_X,
	.name = "Optical Density Start X",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_WRITE_ONLY | V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
	.min = 0,
	.max = 4800,
	.step = 1
};
static const struct v4l2_ctrl_config ctrl_optical_density_end_x = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_OPTICAL_DENSITY_END_X,
	.name = "Optical Density End X",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_WRITE_ONLY | V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
	.min = 0,
	.max = 4800,
	.step = 1
};
static const struct v4l2_ctrl_config ctrl_optical_density_start_y = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_OPTICAL_DENSITY_START_Y,
	.name = "Optical Density Start Y",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_WRITE_ONLY | V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
	.min = 0,
	.max = 4800,
	.step = 1
};
static const struct v4l2_ctrl_config ctrl_optical_density_end_y = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_OPTICAL_DENSITY_END_Y,
	.name = "Optical Density End Y",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_WRITE_ONLY | V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
	.min = 0,
	.max = 4800,
	.step = 1
};

static const struct v4l2_ctrl_config ctrl_test_mode = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_TEST_MODE,
	.name = "Test Mode",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_WRITE_ONLY | V4L2_CTRL_FLAG_EXECUTE_ON_WRITE,
	.min = 0,
	.max = 3,
	.step = 1
};

static const struct v4l2_ctrl_config ctrl_model = {
	.ops = &vdo_ctrl_ops,
	.id = V4L2_CID_MODEL,
	.name = "Sensor Model",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.flags = V4L2_CTRL_FLAG_READ_ONLY | V4L2_CTRL_FLAG_VOLATILE,
	.min = 0,
	.max = 0x1FF,
	.step = 1
};

int vdo_ctrl_sensor_enable(struct tntvdo *vdo, bool en)
{
	struct v4l2_ctrl_handler *hdl = vdo->video_dev.ctrl_handler;
	if (!hdl)
		return -EINVAL;

	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_GAIN), en);
	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_EXPOSURE), en);
	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_HFLIP), en);
	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_VFLIP), en);
	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_RED_BALANCE), en);
	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_GREEN_BALANCE), en);
	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_BLUE_BALANCE), en);
	v4l2_ctrl_activate(v4l2_ctrl_find(hdl, V4L2_CID_FLASH_TIMEOUT), en);

	return 0;
}

/* */

int vdo_ctrl_init(struct tntvdo *vdo)
{
	int ret;
	struct v4l2_ctrl *ctrl;
	struct v4l2_ctrl_handler *hdl = &vdo->ctrls.handler;

	ret = v4l2_ctrl_handler_init(hdl, N_VDO_CTRL);
	if (ret < 0) {
		pr_info("Error creating crtl handler\n");
		return ret;
	}

	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_GAIN, MIN_GAIN,
				 MAX_GAIN, 1, 314);
	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_EXPOSURE,
				 MIN_SHUTTER, MAX_SHUTTER, 1, 300);
	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_HFLIP, 0, 1, 1,
				 0);
	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_VFLIP, 0, 1, 1,
				 0);
	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_RED_BALANCE,
				 MIN_RGAIN, MAX_RGAIN, 1, 128);
	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_GREEN_BALANCE,
				 MIN_GGAIN, MAX_GGAIN, 1, 128);
	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_BLUE_BALANCE,
				 MIN_BGAIN, MAX_BGAIN, 1, 128);
	V4L2_NEW_EXECUTE_ONWRITE(hdl, &vdo_ctrl_ops, V4L2_CID_FLASH_TIMEOUT,
				 MIN_STROBE, MAX_STROBE, 1, 0);

	/* custom */
	v4l2_ctrl_new_custom(hdl, &ctrl_optical_density_y, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_optical_density_cb, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_optical_density_cr, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_optical_density_start_x, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_optical_density_end_x, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_optical_density_start_y, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_optical_density_end_y, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_test_mode, NULL);
	v4l2_ctrl_new_custom(hdl, &ctrl_model, NULL);

	/* Disable all sensor related controls as
	 * sensors will be initialized later on*/
	vdo_ctrl_sensor_enable(vdo, 0);

	if (hdl->error) {
		pr_info("Error Creating ctrl element (%d)\n", hdl->error);
		v4l2_ctrl_handler_free(hdl);
		return hdl->error;
	}
	vdo->v4l2_dev.ctrl_handler = NULL;
	vdo->video_dev.ctrl_handler = hdl;

	v4l2_ctrl_handler_setup(hdl);
	vdo->device.ctrls = &vdo->ctrls;

	return 0;
}

static __maybe_unused void vdo_ctrl_free(struct tntvdo *vdo)
{
	v4l2_ctrl_handler_free(&vdo->ctrls.handler);
}
