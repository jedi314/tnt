/** 
 \file remapper.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief 
 This is a Axle counter chain represented by following scenario

 +---------+            +-------------+         +-------------+ 
 |         |/|          |             |         |             |
-+ CAMERA  | |----+-----+   RESIZER   +----+----+   REMAPPER  +-----+       
 |         |\|    |     |             |    |    |             |     |
 +---------+      |     +-------------+    |    +-------------+     |
                  |                        |                        |
                  |                        |                        |
            +-----V------+           +-----V------+          +------V-------+
            |            |           |            |          |              |
            | FULL FRAME |           |   RESIZED  |          |   REMAPPED&  |
            | 1920x1080  |           |   480x270  |          |    PADDED    |
            |            |           |            |          |   480x288    |
            |            |           |            |          |              |
            +------------+           +------------+          +--------------+
 


*/

#ifndef _TNT_REMAPPER_H
#define _TNT_REMAPPER_H

#define RST_COUNTER_B 0x00020174 /* REG_WR */
#define RST_COUNTER_C 0x00020178 /* REG_WR */
#define PTR_DMA10 0x0002017C /* REG_WR */
#define PTR_DMA15_RD 0x00020180 /* REG_WR */
#define PTR_DMA15_PIXEL_SIZE 0x00020184 /* REG_WR */
#define PTR_DMA15_EN_START 0x00020188 /* REG_WR */
#define TRG_NEW_DELTA_COEFF_REMAPPER 0x0002018C /* REG_WR */
#define SET_DELTA_COEFF_REMAPPER 0x00020190 /* REG_WR */

#define BADDR_SRC_IMG_REMAPPER 0x00020194 /* REG_WR */
#define FIRST_LINE_MIN_IMG_REMAPPER 0x00020198 /* REG_WR */
#define FIRST_LINE_MAX_IMG_REMAPPER 0x0002019C /* REG_WR */
#define BADDR_LUT_REMAPPER 0x000201A0 /* REG_WR */
#define START_RD_REMAPPER 0x000201A4 /* REG_WR */
#define PTR_DMA12 0x000201B4 /* REG_WR */
#define EN_DMA12 0x000201B8 /* REG_WR */
#define PTR_DMA11 0x000201BC /* REG_WR */
#define EN_DMA11_REMAPPER_OUTPUT 0x000201C0 /* REG_WR */
#define START_REMAPPER_SELECTOR 0x000201C4 /* REG_WR */
#define NUM_ROWS_TO_START_REMAPPER 0x000201C8 /* REG_WR */
#define ENABLE_BURST_REMAINING_DMA12 0x000201CC /* REG_WR */

/* RD */
#define CNT_RE_LVAL_OUT_CHAIN 0x00010044 /* REG_RD */
#define DBG_REMAPPER 0x00010048 /* REG_RD */
#define DBG_CNT_RE_FVAL_REMAPPER 0x0001004C /* REG_RD */
#define DBG_CNT_RE_LVAL_REMAPPER 0x00010050 /* REG_RD */
#define DBG_CNT_RE_DVAL_REMAPPER 0x00010054 /* REG_RD */
#define DBG_CNT_RE_FVAL_REMAPPER_PACK 0x00010058 /* REG_RD */
#define DBG_CNT_RE_LVAL_REMAPPER_PACK 0x0001005C /* REG_RD */
#define DBG_CNT_RE_DVAL_REMAPPER_PACK 0x00010060 /* REG_RD */
#define BUSY_OUT_REMAPPER 0x00010064 /* REG_RD */
#define REMAPPING_TIME 0x00010068 /* REG_RD */

/* FPGA base = 0xB300000  */
#define FLAG_SINGLE_SENS 0xB3000058 /* REG_WR */
#define RST_CNT_IRQ_TEST_B 0xB3000070 /* REG_WR */
#define RST_CNT_IRQ_TEST_C 0xB3000074 /* REG_WR */
#define FPGA_APPLICATION_CODE 0xB3010014 /* REG_RD */

#endif
