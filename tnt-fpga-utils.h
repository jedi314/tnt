#ifndef _TNT_FPGA_UTILS_H_
#define _TNT_FPGA_UTILS_H_

#include "tnt-io.h"
#include "tnt-imx265.h"

enum output_type {
	SENSOR_IMAGE = 0,
	PATTERN,
};

#define USEC2TICK (50)

inline void tnt_fpga_reset_align(struct tnt_device *dev, u32 enable);

bool is_sensor_busy(struct tnt_device *dev);
bool is_sensor_ready(struct tnt_device *dev);

bool image_ready(struct tnt_device *dev);
int wait_until_image_ready(struct tnt_device *dev);
int tnt_fpga_prepare_grab(struct tnt_device *dev);

/* dma ops */
void tnt_fpga_enable_main_dma(struct tnt_device *dev);
void tnt_fpga_disable_main_dma(struct tnt_device *dev);
void tnt_fpga_enable_cr_dma(struct tnt_device *dev);
void tnt_fpga_disable_cr_dma(struct tnt_device *dev);
void tnt_fpga_enable_cb_dma(struct tnt_device *dev);
void tnt_fpga_disable_cb_dma(struct tnt_device *dev);
void tnt_fpga_enable_dma(struct tnt_device *dev);
void tnt_fpga_disable_dma(struct tnt_device *dev);
/*  */

void tnt_fpga_set_continuous_gmode(struct tnt_device *dev);
void tnt_fpga_set_trigged_gmode(struct tnt_device *dev);
void tnt_fpga_frame_counter(struct tnt_device *dev, u32 *catched, u32 *frames);
void tnt_fpga_reset_interrupts(struct tnt_device *dev);
void tnt_fpga_enable_interrupts(struct tnt_device *dev);
void tnt_fpga_disable_interrupts(struct tnt_device *dev);
void tnt_fpga_enable_trigged_interrupt(struct tnt_device *dev);
void tnt_fpga_disable_trigged_interrupt(struct tnt_device *dev);
void tnt_fpga_reset_counters(struct tnt_device *dev);
void tnt_fpga_set_period(struct tnt_device *dev, int p);
void tnt_fpga_double_source_enable(struct tnt_device *dev);
void tnt_fpga_double_source_disable(struct tnt_device *dev);
void tnt_fpga_reset_all(struct tnt_device *dev);

int tnt_fpga_read_busy_catch(struct tnt_device *dev);
int tnt_fpga_read_end_frame(struct tnt_device *dev);
void tnt_fpga_clear_busy_catch(struct tnt_device *dev);
void tnt_fpga_clear_end_frame(struct tnt_device *dev);
void tnt_fpga_clear_single_frame(struct tnt_device *dev);

void tnt_fpga_start_fsm(struct tnt_device *dev);
void tnt_fpga_stop_fsm(struct tnt_device *dev);

void tnt_fpga_output_type(enum output_type out, struct tnt_device *dev);
void tnt_fpga_catch(struct tnt_device *dev, int v);
void tnt_fpga_hwsw(struct tnt_device *dev);
void tnt_fpga_shutdown_continuous(struct tnt_device *dev);

u32 tnt_fpga_set_gain(struct tnt_device *dev, u32 value);
u32 tnt_fpga_set_expo(struct tnt_device *dev, u32 us);
u32 tnt_fpga_set_strobo(struct tnt_device *dev, u32 us);
u32 tnt_fpga_set_red_gain(struct tnt_device *dev, u32 value);
u32 tnt_fpga_set_green_gain(struct tnt_device *dev, u32 value);
u32 tnt_fpga_set_blue_gain(struct tnt_device *dev, u32 value);

inline u32 tnt_dma_cha_words(struct tnt_device *dev, u32 dma_ch);
inline u32 tnt_dma_chb_words(struct tnt_device *dev, u32 dma_ch);

u32 tnt_fpga_test_mode(struct tnt_device *dev, u32 value);
u32 tnt_fpga_get_model(struct tnt_device *dev, u32 *value);

inline u32 tnt_fpga_fsm(struct tnt_device *dev);

#endif
