/** 
 \file tnt-io.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief 
*/

#ifndef _TNTIO_H_
#define _TNTIO_H_

#include <linux/types.h>
#include <linux/ioport.h>
#include "tnt-video-formats.h"
#include "tnt-ctrl.h"

#define TO_REG(d, a) ((d)->org + (a))

struct tnt_pool {
	dma_addr_t paddr;
	int __iomem *vaddr;
};
struct device_ops;

/**
 * struct tnt_device -  Video IP device structure
 * @subdev: V4L2 subdevice
 * @dev: (OF) device
 * @iomem: device I/O register space remapped to kernel virtual memory
 * @clk: video core clock
 * @saved_ctrl: saved control register for resume / suspend
 */
struct tnt_device {
	struct device *dev;
	void __iomem *org;
	struct resource *res;
	struct tnt_reg *regs; /**! debugfs register interface  */
	struct device_ops *ops;
	struct vdo_ctrls *ctrls;

	int plane;
	int cache;
	dma_addr_t phy;
	void *virt;
	char *name;
	int id;
};

struct device_ops {
	int (*init)(struct tnt_device *dev);
	int (*reset)(struct tnt_device *dev);
	int (*config)(struct tnt_device *dev);
	int (*pixel_frame)(struct tnt_device *dev, struct tnt_video_fmt *fmt);
	int (*grab)(struct tnt_device *dev, void *b);
	int (*set_buffer_addr)(struct tnt_device *dev, void *b);
	u32 (*expo_us_to_value)(struct tnt_device *dev, u32 us);
	u32 (*set_gain)(struct tnt_device *dev, u32 gain);
	u32 (*test_mode)(struct tnt_device *dev, u32 value);
	u32 (*get_model)(struct tnt_device *dev, u32 *value);

	int (*enable_dma)(struct tnt_device *dev);
	int (*disable_dma)(struct tnt_device *dev);
	bool (*image_ready)(struct tnt_device *dev);
};

#define call_devop(dev, op, args...)                                           \
	({                                                                     \
		int retval = 0;                                                \
		if (dev->ops && dev->ops->op)                                  \
			retval = dev->ops->op(args);                           \
		retval;                                                        \
	})

void tnt_write(struct tnt_device *tntdev, int reg, int val);
u32 tnt_read(struct tnt_device *tntdev, int reg);
u32 to_reg(u32 cmd, u32 ch);
void tnt_reset_irq(struct tnt_device *tntdev, u32 ch);
void tnt_disable_intr(struct tnt_device *tntdev, u32 ch);
void tnt_enable_intr(struct tnt_device *tntdev, u32 ch);

#endif
