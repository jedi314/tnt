/** 
 \file tnt-cmd.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief
*/

#ifndef _TNT_CMD_H
#define _TNT_CMD_H

#include <linux/types.h>

enum tnt_source {
	CHA = 0, // 0b00,
	CHB = 1, // 0b01,
	DOUBLE = 2, // 0b10.

	N_DEV,
};

enum tnt_gmode {
	TRIGGED = 0, // 0b000,
	CONTINUOUS = 4, // 0b100,
};

enum tnt_grabmode {
	TRIGGED_CHA = TRIGGED | CHA,
	TRIGGED_CHB = TRIGGED | CHB,
	TRIGGED_DOUBLE = TRIGGED | DOUBLE,

	CONTINUOUS_CHA = CONTINUOUS | CHA,
	CONTINUOUS_CHB = CONTINUOUS | CHB,
	CONTINUOUS_DOUBLE = CONTINUOUS | DOUBLE,

	TNT_N_MODES
};

#define FG_SOURCE(s) ((s)&0b011)
#define FG_GMODE(m) ((m)&0b100)
#define FG_MODE(s, m) (FG_SOURCE(s) | FG_GMODE(m))

enum tnt_sensors {
	CMOS_IMX265_BW = 162,
	CMOS_IMX265_COL = 163,
	CMOS_IMX250_BW = 164,
	CMOS_IMX250_COL = 165,
	CMOS_IMX250_BW_POLARIZED_CROPPED_3MP = 166,
	CMOS_IMX250_COL_POLARIZED_CROPPED_3MP = 167,
	SONY_IMX265_BW_CROPPED_2MP =
		168, //!< Sony IMX265 monochrome cropped 1920x1080
	SONY_IMX265_COL_CROPPED_2MP = 169,
};

struct tnt_cmd {
	int reg;
	int value;
};

#define V4L2_CID_OPTICAL_DENSITY_BASE (V4L2_CID_USER_MEYE_BASE)
#define V4L2_CID_OPTICAL_DENSITY_Y (V4L2_CID_USER_MEYE_BASE + 1)
#define V4L2_CID_OPTICAL_DENSITY_CB (V4L2_CID_USER_MEYE_BASE + 2)
#define V4L2_CID_OPTICAL_DENSITY_CR (V4L2_CID_USER_MEYE_BASE + 3)
#define V4L2_CID_OPTICAL_DENSITY_START_X (V4L2_CID_USER_MEYE_BASE + 4)
#define V4L2_CID_OPTICAL_DENSITY_END_X (V4L2_CID_USER_MEYE_BASE + 5)
#define V4L2_CID_OPTICAL_DENSITY_START_Y (V4L2_CID_USER_MEYE_BASE + 6)
#define V4L2_CID_OPTICAL_DENSITY_END_Y (V4L2_CID_USER_MEYE_BASE + 7)
#define V4L2_CID_TEST_MODE (V4L2_CID_USER_MEYE_BASE + 8)
#define V4L2_CID_MODEL (V4L2_CID_USER_MEYE_BASE + 9)

struct tnt_od_ch {
	uint32_t y;
	uint32_t cb;
	uint32_t cr;
};

struct tnt_optd {
	uint32_t idx;
	struct tnt_od_ch cha;
	struct tnt_od_ch chb;
};

enum tnt_processing {
	TNT_OPTICAL_DENSITY, //!< Optical density
	TNT_RLE, //!< Rle
	TNT_REMAPPING, //!< Image remapping
	TNT_BILINEAR_RESIZE, //!< Bilinear interpolation resizer
};

#define RLE_ALGO_NUM 2
#define TNT_RLE_LUT_LEN 256

struct tnt_rle_conf {
	uint32_t max_hor_size_for_reload_start; // Distanza dopo la quale la coordinata di start stroke si resetta
	uint32_t max_hor_size_char; // Larghezza massima stroke
	uint32_t min_hor_size_char; // Larghezza minima stroke
	uint32_t num_differential_pair; // Numero di transizioni consecutive da verificare (1-3)
	uint32_t pixel_distance; // Distanza pixel transizione(il differenziale � fatto tra il pixel attuale e quello a pixel_distance)
	uint32_t num_byte_max; // Dimensione massima RLE (multiplo di 512 byte)
	uint8_t lut[TNT_RLE_LUT_LEN];
	uint32_t size_y;
	uint32_t size_x;
	uint32_t black_plates_enable;
};

enum search_type {
	BY_TIMESTAMP,
	BY_UID,
};

/**
   Represent the domain of a function 
   F: T ---> I
   from time space to v4l2 Index space
   f(t) = i;
   
   Based on search type the id 
   could be timestamp or uid

   The domain request is done by a v4l2 custom 
   ioctl TNT_DQBUF which struct has the following
   form.
*/
struct tnt_frames {
	//enum search_type search;
	uint64_t timestamp;
	uint64_t uid;
	uint64_t n_future;
	int pre; /**!< # of frames before timestamp  */
	int post; /**!< # of frames after  timestamp  */
	int index[150]; /**!< array of a couple of id, value, value is search creiteria */
	int n;
};

#define MAX_ACQ_POINTS (6)

struct acq_point {
	uint32_t shutter;
	uint32_t gain;
	uint32_t strobe;
	uint32_t r_gain;
	uint32_t g_gain;
	uint32_t b_gain;
};

struct acq_points {
	uint32_t n;
	struct acq_point pts[MAX_ACQ_POINTS];
};

struct tnt_metadata {
	struct acq_point acq;
	struct tnt_od_ch od;
	uint64_t uid;
	uint64_t timestamp;
	uint64_t realtime;
	uint32_t i_acq;
	uint32_t index;
	uint32_t ch;
};

struct tnt_sensor {
	enum tnt_sensors type; /*!< Sensor code to be set */
	char format_name[16]; /*!< Sensor video format name  */
	uint32_t pixel_type; /*!< Sensor video format code  */
	uint32_t width; /*!< Video format  width */
	uint32_t height; /*!< Video format  height  */
	uint32_t bpp; /*!< Video bit per pixel  */
	uint32_t channel_id; /*!< channel sensor is connected to  */
};

struct processor_layout {
	char name[16];
	char devname[32];
	int plane_id;
	uint32_t w;
	uint32_t h;
	uint32_t size;

	int index;
	int status;
};

struct lut_info {
	uint32_t w;
	uint32_t h;
	uint64_t phy;
	uint32_t size;
	uint32_t max_displ;
	uint32_t delta_max;
};

struct delta {
	u32 ymin;
	u32 ymax;
};

struct elsize {
	uint32_t width;
	uint32_t height;
};

enum dima_cmd {
	DIMA_ENABLE,
	DIMA_DISABLE,
	DIMA_IS_OPENED,
	DIMA_IS_ENABLED,
	DIMA_STATE,
};

struct strobe_data {
	u32 channel;
	u32 value;
};

struct nplanes {
	int n;
};

struct tnt_cmd_stats {
	uint32_t cmd;
	uint64_t value;
	char desc[64];
};

enum stats_cmd {
	STATS_AVERAGE = 0,
	STATS_FPS,
	STATS_FSM,
	STATS_REINIT,
};

#define TNT_MAGIC 'T'
#define TNT_GRAB_FRAME _IO(TNT_MAGIC, 1)
#define TNT_SET_GRABMODE _IOW(TNT_MAGIC, 2, int)
#define TNT_SET_OUTPUT _IOW(TNT_MAGIC, 3, int)
#define TNT_SET_PERIOD _IOW(TNT_MAGIC, 4, int)
#define TNT_INIT_SENSOR _IOWR(TNT_MAGIC, 5, struct tnt_sensor)

#define TNT_GET_OPTD _IOWR(TNT_MAGIC, 6, struct tnt_optd)
#define TNT_ENABLE_PROCS _IOW(TNT_MAGIC, 7, int)
#define TNT_FINALIZE _IOW(TNT_MAGIC, 8, int)
#define TNT_SET_ACQUISITION_POINTS _IOW(TNT_MAGIC, 9, struct acq_points)

#define TNT_QBUF _IOWR(TNT_MAGIC, 10, struct tnt_frames)
#define TNT_DQBUF _IOWR(TNT_MAGIC, 11, struct tnt_frames)
#define TNT_SET_RLE_ALGO_A _IOW(TNT_MAGIC, 12, struct tnt_rle_conf)
#define TNT_SET_RLE_ALGO_B _IOW(TNT_MAGIC, 13, struct tnt_rle_conf)
#define TNT_GET_METADATA _IOWR(TNT_MAGIC, 14, struct tnt_metadata)
#define TNT_DQBUF_UID _IOWR(TNT_MAGIC, 15, struct tnt_frames)
#define TNT_WAIT_NEXT _IOWR(TNT_MAGIC, 16, struct tnt_frames)

#define TNT_ENUM_PROCESSOR _IOWR(TNT_MAGIC, 17, struct processor_layout)
#define TNT_PROCESSOR_INFO _IOWR(TNT_MAGIC, 22, struct processor_layout)

#define REMAPPER_REQUEST_LUT _IOWR(TNT_MAGIC, 18, struct lut_info)
#define REMAPPER_NEW_DELTA_COEF _IOW(TNT_MAGIC, 19, struct delta)
#define REMAPPER_SET_DELTA_COEF _IOW(TNT_MAGIC, 20, uint64_t)
#define REMAPPER_INIT _IO(TNT_MAGIC, 21)
#define REMAPPER_GET_FMT _IOR(TNT_MAGIC, 22, struct elsize)
#define REMAPPER_ENABLE _IO(TNT_MAGIC, 23)

#define STROBE_LEVEL _IOWR(TNT_MAGIC, 24, struct strobe_data)
#define STROBE_CONNECT _IOWR(TNT_MAGIC, 25, struct strobe_data)
#define STROBE_TIME _IOWR(TNT_MAGIC, 26, struct strobe_data)
#define STROBE_UNITS _IOR(TNT_MAGIC, 27, int)

#define TNT_NPLANES _IOR(TNT_MAGIC, 28, struct nplanes)
#define TNT_DIMA _IOWR(TNT_MAGIC, 29, struct tnt_cmd)
#define TNT_STATS _IOWR(TNT_MAGIC, 30, struct tnt_cmd_stats)

#endif
