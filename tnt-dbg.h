/** 
 \file tnt-dbg.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Debug stuff
*/

#ifndef _TNT_DEBUG_H
#define _TNT_DEBUG_H

#include "tntvdo.h"

void dump_process(void);
void dump_mem(char *desc, void *addr, int len);
void trace_queue_lists(struct tntdrv *drv, struct tdm_debug *dbg);
void free_queue_trace(struct tntdrv *drv);

struct tnt_buf *tnt_pick_active_buffer(struct tntdrv *drv,
				       struct v4l2_buffer *b, fn_compare comp);
struct tnt_buf *tnt_pick_done_buffer(struct tntdrv *drv, struct v4l2_buffer *b,
				     fn_compare comp);

int tnt_vb2_qbuf(struct vb2_queue *q, struct v4l2_buffer *b);

#endif
