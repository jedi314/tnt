/** 
 \file list.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief linked dlist bufeer algorithms.
 a linst has generally the following <name> convention:
 + <name>_list is the header of list.
 + <name>_entry the entry if a list element.
 
*/

#ifndef _LIST_H_
#define _LIST_H_

#include "linux/types.h"

typedef int (*fn_compare)(void *, void *);

struct list_ops {
	void *(*convert)(struct list_head *el,
			 void *); /*!< called to convert element in list */
	int (*match)(void *,
		     void *); /*!< called to compare an element in list */
	void *ptr; /*!< pointer to auxiliary data */
	// void (*execute)(void*, void*);                  /*!< called after a found element  */

	/*  */
};

#define call_list_op(lops, op, pos, args...)                                   \
	((lops)->op ? lops->op(pos, args) : pos)

#define call_list_null(lops, op, pos, args...)                                 \
	((lops)->op ? lops->op(pos, args) : 0)

void *foreach_entry(struct list_head *list, struct list_ops *ops, void *b);
void *insert_ordered(struct list_head *list, struct list_head *new,
		     struct list_ops *ops);
bool just_insert(struct list_head *el);

#endif
