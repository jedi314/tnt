/** 
 \file formats.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2020 Tnttile s.r.l.
 

 \brief a list of v4l2 format possibly implemented in FPGA device

 VIDEO FORMATS
 The following table describes the legal string values to be used for
 the xlnx,vid-formats property.  To the left is the string value and the
 two columns to the right describe how this is mapped to an equivalent V4L2
 and DRM fourcc code---respectively---by the driver.

 IP FORMAT       DTS String      V4L2 Fourcc             DRM Fourcc
 -------------|----------------|----------------------|---------------------
 RGB8            bgr888          V4L2_PIX_FMT_RGB24      DRM_FORMAT_BGR888
 BGR8            rgb888          V4L2_PIX_FMT_BGR24      DRM_FORMAT_RGB888
 RGBX8           xbgr8888        V4L2_PIX_FMT_BGRX32     DRM_FORMAT_XBGR8888
 RGBA8           abgr8888        <not supported>         DRM_FORMAT_ABGR8888
 BGRA8           argb8888        <not supported>         DRM_FORMAT_ARGB8888
 BGRX8           xrgb8888        V4L2_PIX_FMT_XBGR32     DRM_FORMAT_XRGB8888
 RGBX10          xbgr2101010     V4L2_PIX_FMT_XBGR30     DRM_FORMAT_XBGR2101010
 RGBX12          xbgr2121212     V4L2_PIX_FMT_XBGR40     <not supported>
 RGBX16          rgb16           V4L2_PIX_FMT_BGR40      <not supported>
 YUV8            vuy888          V4L2_PIX_FMT_VUY24      DRM_FORMAT_VUY888
 YUVX8           xvuy8888        V4L2_PIX_FMT_XVUY32     DRM_FORMAT_XVUY8888
 YUYV8           yuyv            V4L2_PIX_FMT_YUYV       DRM_FORMAT_YUYV
 UYVY8           uyvy            V4L2_PIX_FMT_UYVY       DRM_FORMAT_UYVY
 YUVA8           avuy8888        <not supported>         DRM_FORMAT_AVUY
 YUVX10          yuvx2101010     V4L2_PIX_FMT_XVUY10     DRM_FORMAT_XVUY2101010
 Y8              y8              V4L2_PIX_FMT_GREY       DRM_FORMAT_Y8
 Y10             y10             V4L2_PIX_FMT_Y10        DRM_FORMAT_Y10
 Y_UV8           nv16            V4L2_PIX_FMT_NV16       DRM_FORMAT_NV16
 Y_UV8           nv16            V4L2_PIX_FMT_NV16M      DRM_FORMAT_NV16
 Y_UV8_420       nv12            V4L2_PIX_FMT_NV12       DRM_FORMAT_NV12
 Y_UV8_420       nv12            V4L2_PIX_FMT_NV12M      DRM_FORMAT_NV12
 Y_UV10          xv20            V4L2_PIX_FMT_XV20M      DRM_FORMAT_XV20
 Y_UV10          xv20            V4L2_PIX_FMT_XV20       <not supported>
 Y_UV10_420      xv15            V4L2_PIX_FMT_XV15M      DRM_FORMAT_XV15
 Y_UV10_420      xv15            V4L2_PIX_FMT_XV20       <not supported>



*/

#ifndef _TNT_VIDEO_FORMATS_H
#define _TNT_VIDEO_FORMATS_H

enum pixel_type {
	GRAY8 = 0,
	RGB565 = 1,
	RGB323 = 2,
	RGB888 = 3,
	BAYER = 4,
	MOSAIC = 5,
	YCBCR420 = 6,
	BGR888 = 7,
	CRYCBY422 = 8,
	RGBY_32BIT = 9,
	GRAY_16 = 10,
	BAYER_10 = 11,
	RGB0_32BIT = 12, // Aggiunta per trilinear GigaBit (RGB+8bit a 0)
	BGR888_HF =
		13, // Aggiunta per trilinear GigaBit (BGR888 + Header + Footer)
	GRAY_8_HF =
		14, // Aggiunta per PSeries e Quadrilinear GigaBit (GRAY_8 + Header + Footer)
	RGB_565_HF = 15, // RGB656 + Header + Footer
	YCBCR_111 = 16,
	BAYER_GR = 20,
	BAYER_RG = 21,
	BAYER_GB = 22,
	BAYER_BG = 23,
	HLS_888 = 24,
	GRAY_14 = 25,
	NV12 = 26,
};

struct tnt_pixel_fmt {
	char *name;
	enum pixel_type type;
	u32 fourcc;
};

struct tnt_video_fmt {
	char *name;
	enum pixel_type type;
	u32 width;
	u32 height;
	u32 bpp;
	u32 sz;
	u32 fourcc;
};

#endif
