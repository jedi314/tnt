/** 
 \file tnt-imx265.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 
 

 \brief Low level SONY IMX265 
*/

#include <linux/jiffies.h>
#include <linux/sched.h>
#include <linux/wait_bit.h>
#include <linux/delay.h>

#include "debugfs/trace.h"
#include "fpga-cs.h"
#include "spi-sensor.h"
#include "tnt-io.h"
#include "tnt-fpga-utils.h"
#include "tnt-imx265.h"

static inline void start_grab(struct tnt_device *dev, int expo)
{
	tnt_write(dev, SET_EXPO, expo);
	tnt_write(dev, START, 1);
}

/*  */

bool tnt_fpga_is_sensor_busy(struct tnt_device *dev)
{
	int value = 0;

	value = tnt_read(dev, SENSOR_BUSY);

	return value == 0;
}

bool tnt_fpga_is_sensor_ready(struct tnt_device *dev)
{
	int value = 0;

	value = tnt_read(dev, SENSOR_READY);

	return value == 0;
}

inline u32 tnt_fpga_set_gain(struct tnt_device *dev, u32 val)
{
	u32 gain;

	gain = call_devop(dev, set_gain, dev, val);
	return gain;
}

inline u32 tnt_fpga_set_red_gain(struct tnt_device *dev, u32 value)
{
	tnt_write(dev, GAIN_R_CSC, value);
	return value;
}

inline u32 tnt_fpga_set_green_gain(struct tnt_device *dev, u32 value)
{
	tnt_write(dev, GAIN_G_CSC, value);
	return value;
}

inline u32 tnt_fpga_set_blue_gain(struct tnt_device *dev, u32 value)
{
	tnt_write(dev, GAIN_B_CSC, value);
	return value;
}

inline u32 tnt_fpga_set_expo(struct tnt_device *dev, u32 us)
{
	u32 shutter;

	shutter = call_devop(dev, expo_us_to_value, dev, us);
	tnt_write(dev, SET_EXPO, shutter);
	return us;
}

inline __maybe_unused void tnt_fpga_output_type(enum output_type out,
						struct tnt_device *dev)
{
	tnt_write(dev, SET_FLAG_DMA0_SENS_NPATT, out);
}

inline u32 tnt_fpga_test_mode(struct tnt_device *dev, u32 value)
{
	u32 test_mode;

	test_mode = call_devop(dev, test_mode, dev, value);
	return test_mode;
}

inline u32 tnt_fpga_get_model(struct tnt_device *dev, u32 *value)
{
	u32 model;

	model = call_devop(dev, get_model, dev, value);
	return model;
}

/* Grab */
inline void strobe_set_time(struct tnt_device *dev, u32 us);

int tnt_fpga_prepare_grab(struct tnt_device *dev)
{
	struct vdo_ctrls *ctrls = dev->ctrls;

	tnt_fpga_set_expo(dev, ctrls->w.shutter);
	strobe_set_time(dev, ctrls->w.strobe);
	tnt_fpga_set_gain(dev, ctrls->w.gain);
	tnt_fpga_set_red_gain(dev, ctrls->w.r_gain);
	tnt_fpga_set_green_gain(dev, ctrls->w.g_gain);
	tnt_fpga_set_blue_gain(dev, ctrls->w.b_gain);

	return 0;
}

bool image_ready(struct tnt_device *dev)
{
	bool ret;
	ret = call_devop(dev, image_ready, dev);
	return ret;
}

int wait_until_image_ready(struct tnt_device *dev)
{
	int v;
	int timeout = 2000;

	v = tnt_wait_until(image_ready, dev, timeout);
	return v == 0 ? v : -EINVAL;
}

/* Dma operations */

inline __maybe_unused void tnt_fpga_enable_main_dma(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_FLOW_DDR, 1);
}

inline __maybe_unused void tnt_fpga_disable_main_dma(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_FLOW_DDR, 0);
}

inline __maybe_unused void tnt_fpga_enable_cr_dma(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_LOC_DMA_CR, 1);
}

inline __maybe_unused void tnt_fpga_disable_cr_dma(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_LOC_DMA_CR, 0);
}

inline __maybe_unused void tnt_fpga_enable_cb_dma(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_LOC_DMA_CB, 1);
}

inline __maybe_unused void tnt_fpga_disable_cb_dma(struct tnt_device *dev)
{
	tnt_write(dev, ENABLE_LOC_DMA_CB, 0);
}

void tnt_fpga_enable_dma(struct tnt_device *dev)
{
	call_devop(dev, enable_dma, dev);
}

void tnt_fpga_disable_dma(struct tnt_device *dev)
{
	call_devop(dev, disable_dma, dev);
}

/* Continuous */

inline __maybe_unused void tnt_fpga_set_continuous_gmode(struct tnt_device *dev)
{
	tnt_write(dev, MODE_CONTINUOUS, 1);
}

inline __maybe_unused void tnt_fpga_set_trigged_gmode(struct tnt_device *dev)
{
	tnt_write(dev, MODE_CONTINUOUS, 0);
}

inline __maybe_unused void tnt_fpga_start_fsm(struct tnt_device *dev)
{
	tnt_write(dev, START_TICK_FSM, 1);
}

inline __maybe_unused void tnt_fpga_stop_fsm(struct tnt_device *dev)
{
	tnt_write(dev, STOP_TICK_FSM, 1);
}

inline __maybe_unused void tnt_fpga_hwsw(struct tnt_device *dev)
{
	tnt_write(dev, TRG_HWnSW_MANAGE, 0);
}

void tnt_fpga_frame_counter(struct tnt_device *dev, u32 *catched, u32 *frames)
{
	u32 n_frames;

	n_frames = tnt_read(dev, COUNT_FRAME);
	*catched = (n_frames >> 16) & 0xffff;
	*frames = n_frames & 0xffff;
}

void tnt_fpga_reset_counters(struct tnt_device *dev)
{
	tnt_write(dev, RST_CNT_BUSY_CATCH, 1);
	tnt_write(dev, RST_CNT_CHA, 1);
	tnt_write(dev, RST_CNT_CHB, 1);
	tnt_write(dev, RST_CNT_IRQ, 1);
}

void inline tnt_fpga_reset_all(struct tnt_device *dev)
{
	tnt_fpga_shutdown_continuous(dev);
}

inline void tnt_fpga_set_period(struct tnt_device *dev, int p)
{
	u32 ticks = p * USEC2TICK;
	tnt_write(dev, SET_PERIOD_FSM, ticks);
}

inline void tnt_fpga_double_source_enable(struct tnt_device *dev)
{
	tnt_write(dev, FLAG_DOUBLE, 1);
}

inline void tnt_fpga_double_source_disable(struct tnt_device *dev)
{
	tnt_write(dev, FLAG_DOUBLE, 0);
}

/* Interrupts */

inline void tnt_fpga_clear_busy_catch(struct tnt_device *dev)
{
	tnt_write(dev, RST_IRQ_BUSY_CATCH, 1);
}

inline void tnt_fpga_clear_end_frame(struct tnt_device *dev)
{
	tnt_write(dev, ACK_INTERRUPT_ACQ_FRAME, 1);
}

inline void tnt_fpga_clear_single_frame(struct tnt_device *dev)
{
	tnt_write(dev, RST_IRQ_END_PHOTO, 1);
}

inline int tnt_fpga_read_busy_catch(struct tnt_device *dev)
{
	int status;

	status = tnt_read(dev, BUSY_CATCH_IRQ);
	return status;
}

inline int tnt_fpga_read_end_frame(struct tnt_device *dev)
{
	int status;

	status = tnt_read(dev, END_FRAME_IRQ);
	return status;
}

void tnt_fpga_reset_interrupts(struct tnt_device *dev)
{
	tnt_fpga_clear_busy_catch(dev);
	tnt_fpga_clear_end_frame(dev);
}

void tnt_fpga_enable_interrupts(struct tnt_device *dev)
{
	tnt_write(dev, SET_MASK_IRQ_BUSY_CATCH, 1);
	tnt_write(dev, SET_MASK_IRQ_END_FRAME, 1);
}

inline void tnt_fpga_disable_interrupts(struct tnt_device *dev)
{
	tnt_write(dev, SET_MASK_IRQ_BUSY_CATCH, 0);
	tnt_write(dev, SET_MASK_IRQ_END_FRAME, 0);
}

void tnt_fpga_enable_trigged_interrupt(struct tnt_device *dev)
{
	tnt_write(dev, SET_MASK_IRQ_END_PHOTO, 1);
}

void tnt_fpga_disable_trigged_interrupt(struct tnt_device *dev)
{
	tnt_write(dev, SET_MASK_IRQ_END_PHOTO, 0);
}

void tnt_fpga_catch(struct tnt_device *dev, int v)
{
	tnt_write(dev, FLAG_DOUBLE, 1);
	tnt_write(dev, CATCH_FRAME_FSM, v);
}

void tnt_fpga_shutdown_continuous(struct tnt_device *dev)
{
	tnt_fpga_disable_interrupts(dev);
	tnt_fpga_stop_fsm(dev);
	tnt_fpga_set_trigged_gmode(dev);
}

inline void tnt_fpga_reset_align(struct tnt_device *dev, u32 enable)
{
	tnt_write(dev, RST_SENS_ALLIGN_STATUS, 1);
}

inline u32 tnt_dma_cha_words(struct tnt_device *dev, u32 dma_ch)
{
	u32 n;
	u32 dma = MONITOR_DMA_CHA_NUM_WORD + (dma_ch << 12);

	n = tnt_read(dev, dma);
	return n;
}

inline u32 tnt_dma_chb_words(struct tnt_device *dev, u32 dma_ch)
{
	u32 n;
	u32 dma = MONITOR_DMA_CHB_NUM_WORD + (dma_ch << 12);

	n = tnt_read(dev, dma);

	return n;
}

inline u32 tnt_fpga_fsm(struct tnt_device *dev)
{
	return tnt_read(dev, FPGA_STATE);
}
